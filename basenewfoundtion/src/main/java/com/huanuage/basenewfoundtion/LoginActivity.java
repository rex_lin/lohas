package com.huanuage.basenewfoundtion;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.huanuage.HttpClient.utils.APICallbacks;
import com.huanuage.baselibrary.utils.EmailAddressValidator;
import com.huanuage.baselibrary.utils.PasswordVaildator;
import com.huanuage.baselibrary.utils.StringHelper;
import com.huanuage.basenewfoundtion.apiclient.*;
import com.huanuage.basenewfoundtion.model.*;
import com.huanuage.basenewfoundtion.untils.*;

import java.util.HashMap;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends Activity {

    // UI references.
    private EditText mEmailView;
    private EditText mPasswordView;
    private Button mRegister;
    private Button mLoginView;
    private TextView mEmailErrorView;
    private TextView mPasswordErrorView;
    private CheckBox mRememberMeView;
    private static boolean mIsRememberMe = false;
    private static String mAccount = "";
    private Button mForgetPasswordView;
    private APICallbacks mApiCallBack;
    private Dialog dialogForgetPassword;
    private Dialog dialogForgetPasswordSuccess;
    private Button mBtnForgetPasswordView;
    private EditText mForgetPasswordEmailView;
    private TextView mForgetPasswordEmailErrorView;
    private ProgressDialog mPrgerssDialog;
    private Handler handler = new Handler();
    private Handler mLoginSuccessHandler;
    private Runnable mCloseDialogForgetPasswordSuccessRunnable = new Runnable() {
        @Override
        public void run() {
            dialogForgetPasswordSuccess.dismiss();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_login);
        mLoginSuccessHandler = Utils.getLoginSuccessCallbackHandler();

        //UI 控制項初始化
        mEmailView = (EditText) findViewById(R.id.email);
        mEmailErrorView = (TextView) findViewById(R.id.emial_error);
        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordErrorView = (TextView) findViewById(R.id.password_error);
        mRememberMeView = (CheckBox) findViewById(R.id.remember_me);
        mRegister = (Button) findViewById(R.id.btn_register);
        mForgetPasswordView = (Button) findViewById(R.id.forget_password);
        mLoginView = (Button) findViewById(R.id.email_sign_in_button);
        mPrgerssDialog = new ProgressDialog(this);
        mPrgerssDialog.setCancelable(false);
        mPrgerssDialog.setCanceledOnTouchOutside(false);
        dialogForgetPassword = new Dialog(LoginActivity.this, R.style.MyDialog);
        dialogForgetPassword.setContentView(R.layout.dialog_forget_password);
        mBtnForgetPasswordView = (Button) dialogForgetPassword.findViewById(R.id.btn_forget_password);
        mForgetPasswordEmailView = (EditText) dialogForgetPassword.findViewById(R.id.forget_password_email);
        mForgetPasswordEmailErrorView = (TextView) findViewById(R.id.forget_password_email_error);
        dialogForgetPasswordSuccess = new Dialog(LoginActivity.this, R.style.MyDialog);
        dialogForgetPasswordSuccess.setContentView(R.layout.dialog_forget_password_success);


        //帳號初始化
        SharedPreferences sharedPreferences = SharedPreferencesHelper.getSharedPreferences(LoginActivity.this);
        mIsRememberMe = sharedPreferences.getBoolean(Utils.TAG_REMEMBERME, false);
        if (mIsRememberMe) {
            mRememberMeView.setChecked(mIsRememberMe);
            mAccount = sharedPreferences.getString(Utils.TAG_ACCOUNT, "");
            if (StringHelper.IsNullOrWhiteSpace(mAccount) == false)
                mEmailView.setText(mAccount);
        }

        //驗證機制
        final EmailAddressValidator emailAddressValidator = new EmailAddressValidator(mEmailView, mEmailErrorView, R.string.login_email_error_format, R.string.login_email_error_format);
        mEmailView.setOnFocusChangeListener(emailAddressValidator);
        final PasswordVaildator passwordVaildator = new PasswordVaildator(mPasswordView, mPasswordErrorView, R.string.login_password_error_format, R.string.login_password_error_format);
        mPasswordView.setOnFocusChangeListener(passwordVaildator);
        final EmailAddressValidator forgetPasswordEmailAddressValidator = new EmailAddressValidator(mForgetPasswordEmailView, mForgetPasswordEmailErrorView, R.string.login_email_error_format, R.string.login_email_error_format);
        mForgetPasswordEmailView.setOnFocusChangeListener(forgetPasswordEmailAddressValidator);

        //按鍵事件
        mLoginView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emailAddressValidator.onFocusChange(view, false);
                passwordVaildator.onFocusChange(view, false);
                if (emailAddressValidator.isCheckSuccess() && passwordVaildator.isCheckSuccess()) {
                    mPrgerssDialog.setMessage(getString(R.string.login_prgerss_messag));
                    mPrgerssDialog.show();
                    LoginAccount loginAccount = new LoginAccount(mEmailView.getText().toString(), mPasswordView.getText().toString());
                    APIClientToAuth.getInstance().postLogin(LoginActivity.this, loginAccount, mApiCallBack);
                }
            }
        });
        mRegister.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
                finish();
            }
        });
        mRememberMeView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                SharedPreferences sharedPreferences = SharedPreferencesHelper.getSharedPreferences(LoginActivity.this);
                mIsRememberMe = b;
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean(Utils.TAG_REMEMBERME, mIsRememberMe);
                editor.commit();
            }
        });
        mBtnForgetPasswordView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                forgetPasswordEmailAddressValidator.onFocusChange(view, false);
                if (forgetPasswordEmailAddressValidator.isCheckSuccess()) {
                    mPrgerssDialog.setMessage(getString(R.string.dialog_forget_password_prgerss_messag));
                    mPrgerssDialog.show();
                    ForgetUserPassword forgetUserPassword = new ForgetUserPassword(mForgetPasswordEmailView.getText().toString());
                    APIClientToAuth.getInstance().postForgetUserPassword(LoginActivity.this, forgetUserPassword, mApiCallBack);
                }
            }
        });
        mForgetPasswordView.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogForgetPassword.show();
            }
        });

        mEmailView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mAccount = charSequence.toString();
                SharedPreferences sharedPreferences = SharedPreferencesHelper.getSharedPreferences(LoginActivity.this);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(Utils.TAG_ACCOUNT, mAccount);
                editor.commit();
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        //API CallBack
        mApiCallBack = new APICallbacks() {
            @Override
            public void onSuccess(int task, int statusCode, HashMap<String, String> headers, Object responseObject) {
                String strLog = "";
                switch (task) {
                    case Utils.TASK_POST_LOGIN:



                        strLog += "Task：TASK_POST_LOGIN\n";
                        LoginSuccess loginSuccess = (LoginSuccess) responseObject;
                        APIClientToBackend.getInstance().getCheckTicket(LoginActivity.this, loginSuccess.code, mApiCallBack);
                        break;
                    case Utils.TASK_GET_CHECK_TICKET:



                        strLog += "Task：TASK_GET_CHECK_TICKET\n";
                        APIClientToBackend.getInstance().getUserInfo(LoginActivity.this, mApiCallBack);
                        break;
                    case Utils.TASK_GET_USER_INFO:


                        strLog += "Task：TASK_GET_USER_INFO\n";
                        mPrgerssDialog.dismiss();
                        Intent intent = new Intent();
                        UserInfo userInfo = (UserInfo) responseObject;
                        if (userInfo.IsBinding == false) {
                            intent.setClass(LoginActivity.this, GroupBindingActivity.class);
                            startActivity(intent);
                            finish();
                        } else if (mLoginSuccessHandler == null) {
                            intent.setClass(LoginActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            mLoginSuccessHandler.sendEmptyMessage(1);
                            finish();
                        }
                        break;
                    case Utils.TASK_POST_FORGET_USER_PASSWORD:
                        strLog += "Task：TASK_POST_FORGET_USER_PASSWORD\n";
                        mPrgerssDialog.dismiss();
                        dialogForgetPassword.dismiss();
                        dialogForgetPasswordSuccess.show();
                        handler.postDelayed(mCloseDialogForgetPasswordSuccessRunnable, 3000);
                        break;
                }
                Log.d("LoginActivity", "onSuccess\n" + strLog);
            }

            @Override
            public void onFailure(int task, int statusCode, HashMap<String, String> headers, String responseString, Throwable throwable) {
                String strLog = "";
                switch (task) {
                    case Utils.TASK_POST_FORGET_USER_PASSWORD:
                        strLog += "Task：TASK_POST_FORGET_USER_PASSWORD\n";
                        break;
                    case Utils.TASK_GET_CHECK_TICKET:
                        strLog += "Task：TASK_GET_CHECK_TICKET\n";
                        break;
                    case Utils.TASK_GET_USER_INFO:
                        strLog += "Task：TASK_GET_USER_INFO\n";
                        break;
                    case Utils.TASK_POST_LOGIN:
                        strLog += "Task：TASK_POST_LOGIN\n";
                        break;
                }
                if (task == Utils.TASK_POST_FORGET_USER_PASSWORD) {
                    Toast.makeText(LoginActivity.this, getString(R.string.dialog_forget_password_failure_message), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(LoginActivity.this, getString(R.string.login_failure_message), Toast.LENGTH_SHORT).show();
                }
                Log.d("LoginActivity", "onFailure\n" + strLog, throwable);
                mPrgerssDialog.dismiss();
                Utils.Logout(LoginActivity.this);
            }

            @Override
            public void onNetworkFailure(int task, int statusCode, Throwable throwable) {
                String strLog = "";
                switch (task) {
                    case Utils.TASK_POST_FORGET_USER_PASSWORD:
                        strLog += "Task：TASK_POST_FORGET_USER_PASSWORD\n";
                        break;
                    case Utils.TASK_GET_CHECK_TICKET:
                        strLog += "Task：TASK_GET_CHECK_TICKET\n";
                        break;
                    case Utils.TASK_POST_LOGIN:
                        strLog += "Task：TASK_POST_LOGIN\n";
                        break;
                }
                Log.d("LoginActivity", "onNetworkFailure\n" + strLog, throwable);
                Toast.makeText(LoginActivity.this, getString(R.string.network_failure), Toast.LENGTH_SHORT).show();
                mPrgerssDialog.dismiss();
                Utils.Logout(LoginActivity.this);
            }
        };

        if (BuildConfig.DEBUG) {
            mEmailView.setText("a@a.a");
            mPasswordView.setText("1qazXSW@");
        }
        if (StringHelper.IsNullOrWhiteSpace(SharedPreferencesHelper.getAuthorization(getApplicationContext())) == false) {
            mPrgerssDialog.show();
            APIClientToBackend.getInstance().getUserInfo(LoginActivity.this, mApiCallBack);
        }
    }
}

