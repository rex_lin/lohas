package com.huanuage.basenewfoundtion;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.huanuage.HttpClient.utils.APICallbacks;
import com.huanuage.basenewfoundtion.apiclient.*;
import com.huanuage.basenewfoundtion.model.*;
import com.huanuage.baselibrary.view.CustomTabHost;

import java.util.Calendar;
import java.util.HashMap;

public class MyPointsFragment extends Fragment {
    private CustomTabHost tabHost;
    public static final String TAG_TAB_EXTRA_NAME = "MyPointsTab";
    private TextView mOwnPoints;
    private TextView mAvailablePoints;
    private APICallbacks mAPICallback;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAPICallback = new APICallbacks() {
            @Override
            public void onSuccess(int task, int statusCode, HashMap<String, String> headers, Object data) {
                BaseBackendResponse<OwnPoint> baseBackendResponse = (BaseBackendResponse<OwnPoint>) data;
                mOwnPoints.setText(String.valueOf(baseBackendResponse.Data.OwnPoint));
                mAvailablePoints.setText(String.valueOf(baseBackendResponse.Data.AvailablePoint));
            }

            @Override
            public void onFailure(int task, int statusCode, HashMap<String, String> headers, String responseString, Throwable throwable) {
                Log.d("MyPointsListFragment", "onFailure", throwable);
            }

            @Override
            public void onNetworkFailure(int task, int statusCode, Throwable throwable) {
                Log.d("MyPointsListFragment", "onNetworkFailure", throwable);
                Toast.makeText(getActivity(), getString(R.string.network_failure), Toast.LENGTH_SHORT).show();
            }
        };
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_points, container, false);

        mOwnPoints = (TextView) view.findViewById(R.id.own_points);
        mAvailablePoints = (TextView) view.findViewById(R.id.available_points);
        APIClientToBackend.getInstance().getOwnPoint(getActivity(), mAPICallback);

        tabHost = (CustomTabHost) view.findViewById(R.id.custom_tab_host);
        tabHost.setup(getChildFragmentManager());
        Bundle bundle = new Bundle();
        bundle.putSerializable(TAG_TAB_EXTRA_NAME, MyPointsListFragment.MyPointsTab.ThreeMonth);
        tabHost.addTab(tabHost.newTabSpec(MyPointsListFragment.MyPointsTab.ThreeMonth.toString()).setIndicator(getTabView(getString(R.string.mypoints_tab_threemonth_text))), MyPointsListFragment.class, bundle);

        bundle = new Bundle();
        bundle.putSerializable(TAG_TAB_EXTRA_NAME, MyPointsListFragment.MyPointsTab.SixMonth);
        tabHost.addTab(tabHost.newTabSpec(MyPointsListFragment.MyPointsTab.SixMonth.toString()).setIndicator(getTabView(getString(R.string.mypoints_tab_sixmonth_text))), MyPointsListFragment.class, bundle);

        Calendar calendar = Calendar.getInstance();
        int thisYear = calendar.get(Calendar.YEAR);
        bundle = new Bundle();
        bundle.putSerializable(TAG_TAB_EXTRA_NAME, MyPointsListFragment.MyPointsTab.ThisYear);
        tabHost.addTab(tabHost.newTabSpec(MyPointsListFragment.MyPointsTab.ThisYear.toString()).setIndicator(getTabView(String.valueOf(thisYear))), MyPointsListFragment.class, bundle);

        bundle = new Bundle();
        bundle.putSerializable(TAG_TAB_EXTRA_NAME, MyPointsListFragment.MyPointsTab.LastYear);
        tabHost.addTab(tabHost.newTabSpec(MyPointsListFragment.MyPointsTab.LastYear.toString()).setIndicator(getTabView(String.valueOf(thisYear - 1))), MyPointsListFragment.class, bundle);

        bundle = new Bundle();
        bundle.putSerializable(TAG_TAB_EXTRA_NAME, MyPointsListFragment.MyPointsTab.BeforeLastYear);
        tabHost.addTab(tabHost.newTabSpec(MyPointsListFragment.MyPointsTab.BeforeLastYear.toString()).setIndicator(getTabView(String.valueOf(thisYear - 2))), MyPointsListFragment.class, bundle);
        return view;
    }

    private View getTabView(String tabText) {
        View tabView = LayoutInflater.from(getActivity()).inflate(R.layout.item_my_points_tab, null);
        ((TextView) tabView.findViewById(R.id.tab_text)).setText(tabText);
        return tabView;
    }
}
