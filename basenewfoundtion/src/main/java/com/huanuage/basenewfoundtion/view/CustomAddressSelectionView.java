package com.huanuage.basenewfoundtion.view;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.huanuage.HttpClient.utils.APICallbacks;
import com.huanuage.baselibrary.utils.StringHelper;
import com.huanuage.baselibrary.utils.UUIDHelper;
import com.huanuage.basenewfoundtion.apiclient.APIClientToBackend;
import com.huanuage.baselibrary.interfaces.*;
import com.huanuage.basenewfoundtion.model.*;
import com.huanuage.basenewfoundtion.untils.*;
import com.huanuage.basenewfoundtion.R;
import com.huanuage.baselibrary.utils.LogHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * 地址維護功能
 */
public class CustomAddressSelectionView extends LinearLayout {
    private Spinner mCountrySpinner;
    private BaseAddressAdapter<Country> mCountryAdapter;
    private Spinner mCountySpinner;
    private BaseAddressAdapter<County> mCountyAdapter;
    private Spinner mCitySpinner;
    private BaseAddressAdapter<CityArea> mCityAdapter;
    private EditText mAddressView;
    private static List<Country> mCountryList;
    private static List<County> mCountyList;
    private static List<CityArea> mCityList;
    private APICallbacks mApiCallback;
    private ProgressDialog mProgressDialog;
    private static final String TAG = "CustomAddressSelectionView";
    private final Context mContext;
    private Address mDefaultAddress;

    public CustomAddressSelectionView(Context context) {
        super(context);
        mContext = context;
        init();
    }

    public CustomAddressSelectionView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        init();
    }

    public CustomAddressSelectionView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CustomAddressSelectionView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        mContext = context;
        init();
    }

    /**
     * 初始化
     */
    private void init() {
        LayoutInflater.from(this.mContext).inflate(R.layout.custom_address_selection, this);
        setOrientation(LinearLayout.VERTICAL);

        //UI物件初始化
        mCountrySpinner = (Spinner) findViewById(R.id.address_selection_country);
        mCountySpinner = (Spinner) findViewById(R.id.address_selection_county);
        mCitySpinner = (Spinner) findViewById(R.id.address_selection_city);
        mAddressView = (EditText) findViewById(R.id.address_selection_address);

        mCountryAdapter = new BaseAddressAdapter<Country>(mContext, android.R.layout.simple_spinner_item);
        mCountryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mCountryAdapter.add(Country.getHintItem(mContext.getString(R.string.coustom_address_selection_country_hint)));
        mCountrySpinner.setAdapter(mCountryAdapter);

        mCountyAdapter = new BaseAddressAdapter<County>(mContext, android.R.layout.simple_spinner_item);
        mCountyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mCountyAdapter.add(County.getHintItem(mContext.getString(R.string.coustom_address_selection_county_hint)));
        mCountySpinner.setAdapter(mCountyAdapter);

        mCityAdapter = new BaseAddressAdapter<CityArea>(mContext, android.R.layout.simple_spinner_item);
        mCityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mCityAdapter.add(CityArea.getHintItem(mContext.getString(R.string.coustom_address_selection_city_hint)));
        mCitySpinner.setAdapter(mCityAdapter);

        //Layout Preview 不執，避免抱錯
        if (isInEditMode() == false) {
            mProgressDialog = new ProgressDialog(mContext);
            mProgressDialog.setMessage(mContext.getString(R.string.coustom_address_progress_message));
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.setCancelable(false);
        }

        //API回呼事件
        mApiCallback = new APICallbacks() {
            @Override
            public void onSuccess(int task, int statusCode, HashMap<String, String> headers, Object data) {
                if (task == Utils.TASK_GET_ADDRESS_LIST_ITEM) {
                    AddressListItem addressListItem = ((BaseBackendResponse<AddressListItem>) data).Data;
                    mCountryList = addressListItem.Country;
                    mCountryList.add(0, Country.getHintItem(mContext.getString(R.string.coustom_address_selection_country_hint)));
                    mCountyList = addressListItem.County;
                    mCountyList.add(0, County.getHintItem(mContext.getString(R.string.coustom_address_selection_county_hint)));
                    mCityList = addressListItem.Area;
                    mCityList.add(0, CityArea.getHintItem(mContext.getString(R.string.coustom_address_selection_city_hint)));
                    mCountrySpinner.setSelection(0, true);
                    mCountryAdapter.clear();
                    mCountryAdapter.addAll(mCountryList);
                    mCountryAdapter.notifyDataSetChanged();
                    mCountySpinner.setSelection(0, true);
                    mCountyAdapter.clear();
                    mCountyAdapter.addAll(mCountyList);
                    mCountyAdapter.notifyDataSetChanged();
                    mCitySpinner.setSelection(0, true);
                    mCityAdapter.clear();
                    mCityAdapter.addAll(mCityList);
                    mCityAdapter.notifyDataSetChanged();
                    setDefaultCountry();
                    setDefaultCounty();
                    setDefaultCityArea();
                }
                mProgressDialog.dismiss();
            }

            @Override
            public void onFailure(int task, int statusCode, HashMap<String, String> headers, String responseString, Throwable throwable) {
                LogHelper.w(TAG, throwable);
                mProgressDialog.dismiss();
            }

            @Override
            public void onNetworkFailure(int task, int statusCode, Throwable throwable) {
                LogHelper.w(TAG, throwable);
                Toast.makeText(mContext, mContext.getString(R.string.network_failure), Toast.LENGTH_SHORT).show();
                mProgressDialog.dismiss();
            }
        };

        //事件初始化
        mCountrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mCountySpinner.setVisibility(View.GONE);
                Country country = (Country) mCountrySpinner.getSelectedItem();
                if (UUIDHelper.IsNullOrEmpty(country.CountrySerialNo)) {
                    mCountyAdapter.notifyAllDataSetChanged();
                    mCountySpinner.setSelection(0, true);
                } else {
                    final County county = (County) mCountySpinner.getSelectedItem();
                    mCountyAdapter.getFilter().filter(country.getUuid().toString(), new Filter.FilterListener() {
                        @Override
                        public void onFilterComplete(int i) {
                            mCountySpinner.setSelection(mCountyAdapter.getItemIndexById(county.CountySerialNo), true);
                            if (mCountyAdapter.getCount() > 1) {
                                mCountySpinner.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        mCountySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mCitySpinner.setVisibility(View.GONE);
                County county = (County) mCountySpinner.getSelectedItem();
                if (UUIDHelper.IsNullOrEmpty(county.CountySerialNo)) {
                    mCityAdapter.notifyAllDataSetChanged();
                    mCitySpinner.setSelection(0, true);
                } else {
                    final CityArea cityArea = (CityArea) mCitySpinner.getSelectedItem();
                    mCityAdapter.getFilter().filter(county.getUuid().toString(), new Filter.FilterListener() {
                        @Override
                        public void onFilterComplete(int i) {
                            mCitySpinner.setSelection(mCityAdapter.getItemIndexById(cityArea.CityAreaSerialNo), true);
                            if (mCountyAdapter.getCount() > 1) {
                                mCountrySpinner.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        //初始化資料
        if (isInEditMode() == false) {
            if (mCountryList == null || mCountryList.isEmpty()) {
                mProgressDialog.show();
                APIClientToBackend.getInstance().getAddressListItem(mContext, mApiCallback);
            } else {
                mCountryAdapter.clear();
                mCountryAdapter.addAll(mCountryList);
                mCountryAdapter.notifyDataSetChanged();
                mCountyAdapter.clear();
                mCountyAdapter.addAll(mCountyList);
                mCountyAdapter.notifyDataSetChanged();
                mCityAdapter.clear();
                mCityAdapter.addAll(mCityList);
                mCityAdapter.notifyDataSetChanged();
                setDefaultCountry();
                setDefaultCounty();
                setDefaultCityArea();
            }
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        //關閉回復初始化
        if (isInEditMode() == false) {
            mCountrySpinner.setSelection(0, true);
            mAddressView.setText("");
        }
        ((InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(getRootView().getWindowToken(), 0);
    }

    /**
     * 設定預設資料
     *
     * @param address
     */
    public void setAddress(Address address) {
        try {
            this.mDefaultAddress = address.cloneModel(false);
        } catch (Exception e) {
            e.printStackTrace();
            this.mDefaultAddress = null;
        }
        setDefaultCountry();
        setDefaultCounty();
        setDefaultCityArea();
        if (this.mDefaultAddress != null && StringHelper.IsNullOrWhiteSpace(this.mDefaultAddress.Address) == false) {
            mAddressView.setText(this.mDefaultAddress.Address);
        }
    }

    /**
     * 設定預設國家
     */
    private void setDefaultCountry() {
        if (this.mDefaultAddress != null && this.mDefaultAddress.country != null && UUIDHelper.IsNullOrEmpty(this.mDefaultAddress.country.getUuid()) == false) {
            int defaultIndex = mCountryAdapter.getItemIndexById(this.mDefaultAddress.country.getUuid());
            if (defaultIndex > 0) {
                mCountrySpinner.setSelection(defaultIndex, true);
                this.mDefaultAddress.country = null;
            }
        }
    }

    /**
     * 設定預設縣市
     */
    private void setDefaultCounty() {
        if (this.mDefaultAddress != null && this.mDefaultAddress.county != null && UUIDHelper.IsNullOrEmpty(this.mDefaultAddress.county.getUuid()) == false) {
            int defaultIndex = mCountyAdapter.getItemIndexById(this.mDefaultAddress.county.getUuid());
            if (defaultIndex > 0) {
                mCountySpinner.setSelection(defaultIndex, true);
                this.mDefaultAddress.county = null;
            }
        }
    }

    /**
     * 設定預設鄉鎮
     */
    private void setDefaultCityArea() {
        if (this.mDefaultAddress != null && this.mDefaultAddress.cityArea != null && UUIDHelper.IsNullOrEmpty(this.mDefaultAddress.cityArea.getUuid()) == false) {
            int defaultIndex = mCityAdapter.getItemIndexById(this.mDefaultAddress.cityArea.getUuid());
            if (defaultIndex > 0) {
                mCitySpinner.setSelection(defaultIndex, true);
                this.mDefaultAddress.cityArea = null;
            }
        }
    }

    /**
     * 取得地址
     *
     * @return
     */
    public Address getAddress() {
        Address address = new Address();
        Country country = ((Country) mCountrySpinner.getSelectedItem());
        if (country != null && UUIDHelper.IsNullOrEmpty(country.getUuid()) == false) {
            address.country = country.clone();
        }

        County county = ((County) mCountySpinner.getSelectedItem());
        if (county != null && UUIDHelper.IsNullOrEmpty(county.getUuid()) == false) {
            address.county = county.clone();
        }

        CityArea cityArea = ((CityArea) mCitySpinner.getSelectedItem());
        if (cityArea != null && UUIDHelper.IsNullOrEmpty(cityArea.getUuid()) == false) {
            address.cityArea = cityArea.clone();
        }

        address.Address = mAddressView.getText().toString();
        return address;
    }

    /**
     * BaseAddressAdapter
     *
     * @param <T> 物件，需繼承iBaseModel<T>
     */
    class BaseAddressAdapter<T extends iArrayAdapterModel & iGetUuid> extends BaseAdapter {
        private static final String TAG = "BaseAddressAdapter";
        ItemFilter filter;
        List<T> mList = new ArrayList<>();
        List<T> mFilterList = new ArrayList<>();
        Context context;
        int textViewResourceId;
        int resource;
        private int mDropDownResource;

        private LayoutInflater mInflater;

        public BaseAddressAdapter(Context context, @LayoutRes int resource) {
            init(context, resource, 0, null);
        }

        public BaseAddressAdapter(Context context, @LayoutRes int resource, @IdRes int textViewResourceId) {
            init(context, resource, textViewResourceId, null);
        }

        public BaseAddressAdapter(Context context, @LayoutRes int resource, T[] objects) {
            init(context, resource, 0, objects == null ? null : Arrays.asList(objects));
        }

        public BaseAddressAdapter(Context context, @LayoutRes int resource, @IdRes int textViewResourceId, T[] objects) {
            init(context, resource, textViewResourceId, objects == null ? null : Arrays.asList(objects));
        }

        public BaseAddressAdapter(Context context, @LayoutRes int resource, List<T> objects) {
            init(context, resource, textViewResourceId, objects);
        }

        public BaseAddressAdapter(Context context, @LayoutRes int resource, @IdRes int textViewResourceId, List<T> objects) {
            init(context, resource, textViewResourceId, objects);
        }

        private void init(Context context, @LayoutRes int resource, @IdRes int textViewResourceId, List<T> objects) {
            mInflater = LayoutInflater.from(context);
            this.context = context;
            this.resource = resource;
            this.mDropDownResource = resource;
            this.textViewResourceId = textViewResourceId;
            if (objects != null) {
                mList.clear();
                mList.addAll(objects);
                mFilterList.clear();
                mFilterList.addAll(objects);
            }
        }

        public void clear() {
            mList.clear();
            mFilterList.clear();
        }

        public void add(T object) {
            mList.add(object);
            mFilterList.add(object);
        }

        public void add(int index, T object) {
            mList.add(index, object);
            mFilterList.add(index, object);
        }

        public void addAll(List<T> objects) {
            mList.addAll(objects);
            mFilterList.addAll(objects);
        }

        @Override
        public T getItem(int position) {
            return mFilterList.get(position);
        }

        @Override
        public long getItemId(int i) {
            return mFilterList.get(i).hashCode();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;
            TextView text;

            if (convertView == null) {
                view = mInflater.inflate(resource, parent, false);
            } else {
                view = convertView;
            }

            try {
                if (textViewResourceId == 0) {
                    //  If no custom field is assigned, assume the whole resource is a TextView
                    text = (TextView) view;
                } else {
                    //  Otherwise, find the TextView field within the layout
                    text = (TextView) view.findViewById(textViewResourceId);
                }
            } catch (ClassCastException e) {
                LogHelper.e(TAG, "You must supply a resource ID for a TextView");
                throw new IllegalStateException(
                        "ArrayAdapter requires the resource ID to be a TextView", e);
            }

            T item = getItem(position);
            if (item instanceof CharSequence) {
                text.setText((CharSequence) item);
            } else {
                text.setText(item.toString());
            }

            return view;
        }

        public void setDropDownViewResource(@LayoutRes int resource) {
            this.mDropDownResource = resource;
        }


        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            View view;
            TextView text;

            if (convertView == null) {
                view = mInflater.inflate(mDropDownResource, parent, false);
            } else {
                view = convertView;
            }

            try {
                if (textViewResourceId == 0) {
                    //  If no custom field is assigned, assume the whole resource is a TextView
                    text = (TextView) view;
                } else {
                    //  Otherwise, find the TextView field within the layout
                    text = (TextView) view.findViewById(textViewResourceId);
                }
            } catch (ClassCastException e) {
                LogHelper.e(TAG, "You must supply a resource ID for a TextView");
                throw new IllegalStateException(
                        "ArrayAdapter requires the resource ID to be a TextView", e);
            }

            T item = getItem(position);
            if (item instanceof CharSequence) {
                text.setText((CharSequence) item);
            } else {
                text.setText(item.toString());
            }

            return view;
        }

        @Override
        public int getCount() {
            return mFilterList.size();
        }

        public int getItemIndexById(UUID id) {
            if (UUIDHelper.IsNullOrEmpty(id) == false) {
                for (int i = 1; i < mFilterList.size(); i++) {
                    if (id.equals(mFilterList.get(i).getUuid())) {
                        return i;
                    }
                }
            }
            return 0;
        }

        public ItemFilter getFilter() {
            if (filter == null)
                filter = new ItemFilter();
            return filter;
        }

        public void notifyAllDataSetChanged() {
            mFilterList.clear();
            mFilterList.addAll(mList);
            notifyDataSetChanged();
        }

        private class ItemFilter extends Filter {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                String filterString = constraint.toString().toLowerCase();

                long upperId = UUID.fromString(filterString).hashCode();
                FilterResults results = new FilterResults();

                final List<T> list = mList;

                int count = list.size();
                final ArrayList<T> nlist = new ArrayList<>();

                T filterable;

                for (int i = 0; i < count; i++) {
                    filterable = list.get(i);
                    if (filterable.isFilter(upperId)) {
                        nlist.add(filterable);
                    }
                }

                results.values = nlist;
                results.count = nlist.size();

                return results;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mFilterList = (ArrayList<T>) results.values;
                notifyDataSetChanged();
            }
        }
    }
}
