package com.huanuage.basenewfoundtion.view;

import android.app.DialogFragment;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

import com.huanuage.basenewfoundtion.model.Address;
import com.huanuage.basenewfoundtion.R;

/**
 * Created by kai_tseng on 2015/11/6.
 */
public class CustomAddressSelectionDialog extends DialogFragment {
    private CustomAddressSelectionView selectionView;
    private Button mBtnSend;
    private Button mBtnCancel;
    private Handler mBtnSendCallback;
    private Address mAddress;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.custom_address_selection_dialog, container, false);
        mBtnSend = (Button) view.findViewById(R.id.address_selection_dialog_send);
        mBtnCancel = (Button) view.findViewById(R.id.address_selection_dialog_cancel);
        selectionView = (CustomAddressSelectionView) view.findViewById(R.id.address_selection);

        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);

        mBtnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAddress = selectionView.getAddress();
                if (mBtnSendCallback != null) {
                    Message msg = new Message();
                    msg.obj = mAddress;
                    mBtnSendCallback.sendMessage(msg);
                }
                getDialog().cancel();
            }
        });

        mBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog().cancel();
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        selectionView.setAddress(this.mAddress);
    }

    public void setBtnSendCallback(Handler handler) {
        this.mBtnSendCallback = handler;
    }

    public void setAddress(Address address) {
        try {
            this.mAddress = address.cloneModel(false);
        } catch (Exception e) {
            e.printStackTrace();
            this.mAddress = null;
        }
    }
}
