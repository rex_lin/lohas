package com.huanuage.basenewfoundtion;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;

import com.huanuage.HttpClient.utils.APICallbacks;
import com.huanuage.HttpClient.utils.HttpResponseException;
import com.huanuage.baselibrary.utils.DoubleCheckEditTextVaildator;
import com.huanuage.baselibrary.utils.GsonHelper;
import com.huanuage.baselibrary.utils.PasswordVaildator;
import com.huanuage.baselibrary.utils.StringHelper;
import com.huanuage.basenewfoundtion.apiclient.*;
import com.huanuage.basenewfoundtion.model.*;
import com.huanuage.basenewfoundtion.untils.*;

import java.lang.reflect.Type;
import java.util.HashMap;


public class ChangePasswordFragment extends Fragment {

    // UI references.
    private EditText mOldPasswordView;
    private TextView mOldPasswordErrorView;
    private EditText mNewPasswordView;
    private TextView mNewPasswordErrorView;
    private EditText mChkPasswordView;
    private TextView mChkPasswordErrorView;
    private ProgressDialog mPrgerssDialog;
    private Button mBtnChangePasswordView;
    private String userName;
    private String password;
    private APICallbacks mApiCallBack;
    private Handler handler = new Handler();
    private Dialog dialogChangePasswordSuccess;
    private Runnable mCloseDialogChangePasswordSuccessRunnable = new Runnable() {
        @Override
        public void run() {
            dialogChangePasswordSuccess.dismiss();
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences sharedPreferences = SharedPreferencesHelper.getSharedPreferences(getActivity());

        userName = sharedPreferences.getString(Utils.TAG_ACCOUNT, "");
        if (BuildConfig.DEBUG) {
            userName = "kai_tseng@huanuage.com";
        }
        if (StringHelper.IsNullOrWhiteSpace(userName)) {
            Toast.makeText(getActivity(), getString(R.string.change_password_username_fail), Toast.LENGTH_SHORT).show();
            Utils.Logout(getActivity());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_change_password, container, false);

        //UI 控制項初始化
        mOldPasswordView = (EditText) view.findViewById(R.id.oldpassword);
        mOldPasswordErrorView = (TextView) view.findViewById(R.id.oldpassword_error);
        mNewPasswordView = (EditText) view.findViewById(R.id.newpassword);
        mNewPasswordErrorView = (TextView) view.findViewById(R.id.newpassword_error);
        mChkPasswordView = (EditText) view.findViewById(R.id.chk_password);
        mChkPasswordErrorView = (TextView) view.findViewById(R.id.chk_password_error);
        mPrgerssDialog = new ProgressDialog(getActivity());
        mPrgerssDialog.setMessage(getString(R.string.change_password_prgerss_message));
        mPrgerssDialog.setCancelable(false);
        mPrgerssDialog.setCanceledOnTouchOutside(false);
        mBtnChangePasswordView = (Button) view.findViewById(R.id.btn_change_password);
        dialogChangePasswordSuccess = new Dialog(getActivity(), R.style.MyDialog);
        dialogChangePasswordSuccess.setContentView(R.layout.dialog_forget_password_success);
        ((TextView) dialogChangePasswordSuccess.findViewById(R.id.tv_dialog_content)).setText(getString(R.string.dialog_change_password_success_message));

        //驗證機制
        final PasswordVaildator oldPasswordVaildator = new PasswordVaildator(mOldPasswordView, mOldPasswordErrorView, R.string.login_password_error_fail, R.string.login_password_error_fail);
        oldPasswordVaildator.setNonFocusBackgroundId(R.mipmap.accountext);
        oldPasswordVaildator.setFocusBackgroundId(R.mipmap.accountext);
        oldPasswordVaildator.setErrorBackgroundId(R.mipmap.accountext_red);
        mOldPasswordView.setOnFocusChangeListener(oldPasswordVaildator);

        final PasswordVaildator passwordVaildator = new PasswordVaildator(mNewPasswordView, mNewPasswordErrorView, R.string.register_password_error, R.string.register_password_error) {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b == false) {
                    mNewPasswordErrorView.setVisibility(View.GONE);
                    if (Build.VERSION.SDK_INT >= 23) {
                        mNewPasswordErrorView.setTextColor(getResources().getColor(R.color.color_d9534f, null));
                    } else {
                        mNewPasswordErrorView.setTextColor(getResources().getColor(R.color.color_d9534f));
                    }
                } else {
                    mNewPasswordErrorView.setText(getString(R.string.hint_register_password));
                    mNewPasswordErrorView.setTextColor(Color.BLUE);
                    mNewPasswordErrorView.setVisibility(View.VISIBLE);
                }
                super.onFocusChange(view, b);
            }
        };
        passwordVaildator.setNonFocusBackgroundId(R.mipmap.accountext);
        passwordVaildator.setFocusBackgroundId(R.mipmap.accountext_blue);
        passwordVaildator.setErrorBackgroundId(R.mipmap.accountext_red);
        mNewPasswordView.setOnFocusChangeListener(passwordVaildator);

        final DoubleCheckEditTextVaildator doubleCheckEditTextVaildator = new DoubleCheckEditTextVaildator(mChkPasswordView, mNewPasswordView, mChkPasswordErrorView, R.string.login_password_error_fail, R.string.login_password_error_fail);
        doubleCheckEditTextVaildator.setFocusBackgroundId(R.mipmap.accountext);
        doubleCheckEditTextVaildator.setNonFocusBackgroundId(R.mipmap.accountext);
        doubleCheckEditTextVaildator.setErrorBackgroundId(R.mipmap.accountext_red);
        mChkPasswordView.setOnFocusChangeListener(doubleCheckEditTextVaildator);

        //按鍵事件
        dialogChangePasswordSuccess.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                Utils.Logout(getActivity());
            }
        });
        mBtnChangePasswordView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                oldPasswordVaildator.onFocusChange(view, false);
                passwordVaildator.onFocusChange(view, false);
                doubleCheckEditTextVaildator.onFocusChange(view, false);
                if (oldPasswordVaildator.isCheckSuccess() && passwordVaildator.isCheckSuccess() && doubleCheckEditTextVaildator.isCheckSuccess()) {
                    mPrgerssDialog.show();
                    ChangePassword changePassword = new ChangePassword(userName, mOldPasswordView.getText().toString(), mNewPasswordView.getText().toString(), mChkPasswordView.getText().toString());
                    APIClientToAuth.getInstance().postChangePassword(getActivity(), changePassword, mApiCallBack);
                }
            }
        });

        //API CallBack
        mApiCallBack = new APICallbacks() {
            @Override
            public void onSuccess(int task, int statusCode, HashMap<String, String> headers, Object responseObject) {
                mPrgerssDialog.dismiss();
                dialogChangePasswordSuccess.show();
                handler.postDelayed(mCloseDialogChangePasswordSuccessRunnable, 3000);
                Log.d("ChangePasswordFragment", "onSuccess");
            }

            @Override
            public void onFailure(final int task, final int statuscode, HashMap<String, String> headers, String responseString, Throwable throwable) {
                Type modelStateType = new TypeToken<ModelState<ChangePasswordFailureModelState>>() {
                }.getType();
                if (throwable instanceof HttpResponseException) {
                    try {
                        ModelState<ChangePasswordFailureModelState> modelState = GsonHelper.DeserializeObject(throwable.getMessage(), modelStateType);
                        if (StringHelper.IsNullOrWhiteSpace(modelState.message)) {
                            mOldPasswordView.setBackgroundResource(R.mipmap.accountext_red);
                            mOldPasswordErrorView.setText(getString(R.string.login_password_error_fail));
                            mOldPasswordErrorView.setVisibility(View.VISIBLE);
                        } else {
                            if (modelState.modelState.OldPassword != null && modelState.modelState.OldPassword.length > 0) {
                                mOldPasswordView.setBackgroundResource(R.mipmap.accountext_red);
                                mOldPasswordErrorView.setText(getString(R.string.login_password_error_fail));
                                mOldPasswordErrorView.setVisibility(View.VISIBLE);
                            }
                            if (modelState.modelState.NewPassword != null && modelState.modelState.NewPassword.length > 0) {
                                mNewPasswordView.setBackgroundResource(R.mipmap.accountext_red);
                                mNewPasswordErrorView.setText(getString(R.string.register_password_error));
                                mNewPasswordErrorView.setVisibility(View.VISIBLE);
                            }
                            if (modelState.modelState.ConfirmPassword != null && modelState.modelState.ConfirmPassword.length > 0) {
                                mChkPasswordView.setBackgroundResource(R.mipmap.accountext_red);
                                mChkPasswordErrorView.setText(getString(R.string.register_check_password_error));
                                mChkPasswordErrorView.setVisibility(View.VISIBLE);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                Log.d("ChangePasswordFragment", "onFailure", throwable);
                mPrgerssDialog.dismiss();
            }

            @Override
            public void onNetworkFailure(int task, int statusCode, Throwable throwable) {
                String strLog = "";
                switch (task) {
                    case Utils.TASK_POST_ACCOUNT_IS_REGISTERED:
                        strLog += "Task：TASK_POST_ACCOUNT_IS_REGISTERED\n";
                        break;
                    case Utils.TASK_POST_REGISTER:
                        strLog += "Task：TASK_POST_REGISTER\n";
                        break;
                    case Utils.TASK_POST_LOGIN:
                        strLog += "Task：TASK_POST_LOGIN\n";
                        break;
                }
                Log.d("RegisterActivity", "onNetworkFailure\n" + strLog, throwable);
                Toast.makeText(getActivity(), getString(R.string.network_failure), Toast.LENGTH_SHORT).show();
                mPrgerssDialog.dismiss();
            }
        };
        if (BuildConfig.DEBUG) {
            mOldPasswordView.setText("Qq32165498");
            mNewPasswordView.setText("Qqq32165498");
            mChkPasswordView.setText("Qqq32165498");
        }
        return view;
    }


}
