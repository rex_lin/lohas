package com.huanuage.basenewfoundtion;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.huanuage.HttpClient.utils.APICallbacks;
import com.huanuage.baselibrary.utils.DoubleCheckEditTextVaildator;
import com.huanuage.baselibrary.utils.EmailAddressValidator;
import com.huanuage.baselibrary.utils.PasswordVaildator;
import com.huanuage.basenewfoundtion.apiclient.*;
import com.huanuage.basenewfoundtion.model.*;
import com.huanuage.basenewfoundtion.untils.*;

import org.json.JSONObject;

import java.util.HashMap;

public class RegisterActivity extends Activity {

    // UI references.
    private EditText mEmailView;
    private EditText mPasswordView;
    private EditText mChkPasswordView;
    private TextView mEmailErrorView;
    private TextView mPasswordErrorView;
    private TextView mChkPasswordErrorView;
    private Button mBtnRegisterView;
    private TextView mBtnGotoLoginView;
    private ProgressDialog mPrgerssDialog;
    private String userName;
    private String password;
    private APICallbacks mApiCallBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //UI 控制項初始化
        mEmailView = (EditText) findViewById(R.id.email);
        mPasswordView = (EditText) findViewById(R.id.password);
        mChkPasswordView = (EditText) findViewById(R.id.chk_password);
        mBtnRegisterView = (Button) findViewById(R.id.btn_register);
        mEmailErrorView = (TextView) findViewById(R.id.email_error);
        mPasswordErrorView = (TextView) findViewById(R.id.password_error);
        mChkPasswordErrorView = (TextView) findViewById(R.id.chk_password_error);
        mBtnGotoLoginView = (TextView) findViewById(R.id.btn_goto_login);
        mPrgerssDialog = new ProgressDialog(this);
        mPrgerssDialog.setMessage(getString(R.string.register_prgerss_message));
        mPrgerssDialog.setCancelable(false);
        mPrgerssDialog.setCanceledOnTouchOutside(false);

        //驗證機制
        final EmailAddressValidator emailAddressValidator = new EmailAddressValidator(mEmailView, mEmailErrorView, R.string.login_email_error_format, R.string.login_email_error_format);
        emailAddressValidator.setFocusBackgroundId(R.mipmap.accountext);
        emailAddressValidator.setNonFocusBackgroundId(R.mipmap.accountext);
        emailAddressValidator.setErrorBackgroundId(R.mipmap.accountext_red);
        mEmailView.setOnFocusChangeListener(emailAddressValidator);

        final PasswordVaildator passwordVaildator = new PasswordVaildator(mPasswordView, mPasswordErrorView, R.string.register_password_error, R.string.register_password_error) {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b == false) {
                    mPasswordErrorView.setVisibility(View.GONE);
                    if (Build.VERSION.SDK_INT >= 23) {
                        mPasswordErrorView.setTextColor(getResources().getColor(R.color.color_d9534f, null));
                    } else {
                        mPasswordErrorView.setTextColor(getResources().getColor(R.color.color_d9534f));
                    }
                } else {
                    mPasswordErrorView.setText(getString(R.string.hint_register_password));
                    mPasswordErrorView.setTextColor(Color.BLUE);
                    mPasswordErrorView.setVisibility(View.VISIBLE);
                }
                super.onFocusChange(view, b);
            }
        };
        passwordVaildator.setNonFocusBackgroundId(R.mipmap.accountext);
        passwordVaildator.setFocusBackgroundId(R.mipmap.accountext_blue);
        passwordVaildator.setErrorBackgroundId(R.mipmap.accountext_red);
        mPasswordView.setOnFocusChangeListener(passwordVaildator);

        final DoubleCheckEditTextVaildator doubleCheckEditTextVaildator = new DoubleCheckEditTextVaildator(mChkPasswordView, mPasswordView, mChkPasswordErrorView, R.string.login_password_error_fail, R.string.login_password_error_fail);
        doubleCheckEditTextVaildator.setFocusBackgroundId(R.mipmap.accountext);
        doubleCheckEditTextVaildator.setNonFocusBackgroundId(R.mipmap.accountext);
        doubleCheckEditTextVaildator.setErrorBackgroundId(R.mipmap.accountext_red);
        mChkPasswordView.setOnFocusChangeListener(doubleCheckEditTextVaildator);

        //按鍵事件
        mBtnRegisterView.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                emailAddressValidator.onFocusChange(view, false);
                passwordVaildator.onFocusChange(view, false);
                doubleCheckEditTextVaildator.onFocusChange(view, false);
                if (emailAddressValidator.isCheckSuccess() && passwordVaildator.isCheckSuccess() && doubleCheckEditTextVaildator.isCheckSuccess()) {
                    mPrgerssDialog.show();
                    userName = mEmailView.getText().toString();
                    password = mPasswordView.getText().toString();
                    APIClientToAuth.getInstance().postAccountIsRegistered(RegisterActivity.this, userName, mApiCallBack);
                }
            }
        });
        mBtnGotoLoginView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        //API CallBack
        mApiCallBack = new APICallbacks() {
            @Override
            public void onSuccess(int task, int statusCode, HashMap<String, String> headers, Object responseObject) {
                String strLog = "";
                switch (task) {
                    case Utils.TASK_POST_ACCOUNT_IS_REGISTERED:
                        strLog += "Task：TASK_POST_ACCOUNT_IS_REGISTERED\n";
                        boolean isRegistered = true;
                        try {
                            AccountIsRegistered accountIsRegistered = (AccountIsRegistered) responseObject;
                            isRegistered = accountIsRegistered.AccountIsRegistered;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        strLog += "AccountIsRegistered：" + isRegistered + "\n";
                        if (isRegistered == false) {
                            Register register = new Register(mEmailView.getText().toString(), mPasswordView.getText().toString(), mChkPasswordView.getText().toString());
                            APIClientToAuth.getInstance().postRegister(RegisterActivity.this, register, mApiCallBack);
                        } else {
                            mEmailErrorView.setText(getString(R.string.register_account_is_registered));
                            mEmailErrorView.setVisibility(View.VISIBLE);
                            mEmailView.setBackgroundResource(R.mipmap.accountext_red);
                            mPrgerssDialog.dismiss();
                        }
                        break;
                    case Utils.TASK_POST_REGISTER:
                        strLog += "Task：TASK_POST_REGISTER\n";
                        LoginAccount loginAccount = new LoginAccount(userName, password);
                        SharedPreferences sharedPreferences = SharedPreferencesHelper.getSharedPreferences(RegisterActivity.this);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString(Utils.TAG_ACCOUNT, userName);
                        editor.commit();
                        APIClientToAuth.getInstance().postLogin(RegisterActivity.this, loginAccount, mApiCallBack);
                        break;
                    case Utils.TASK_POST_LOGIN:
                        strLog += "Task：TASK_POST_LOGIN\n";
                        Intent intent = new Intent();
                        intent.setClass(RegisterActivity.this, GroupBindingActivity.class);
                        intent.putExtra(Utils.TAG_GROUP_BINDING_EXTRA, (LoginSuccess) responseObject);
                        startActivity(intent);
                        finish();
                        break;
                }
                Log.d("RegisterActivity", "onSuccess\n" + strLog);
            }

            @Override
            public void onFailure(final int task, final int statuscode, HashMap<String, String> headers, String responseString, Throwable throwable) {
                String strLog = "";
                switch (task) {
                    case Utils.TASK_POST_ACCOUNT_IS_REGISTERED:
                        strLog += "Task：TASK_POST_ACCOUNT_IS_REGISTERED\n";
                        mEmailErrorView.setText(getString(R.string.register_account_is_registered));
                        mEmailErrorView.setVisibility(View.VISIBLE);
                        mEmailView.setBackgroundResource(R.mipmap.accountext_red);
                        break;
                    case Utils.TASK_POST_REGISTER:
                        strLog += "Task：TASK_POST_REGISTER\n";
                        Toast.makeText(RegisterActivity.this, getString(R.string.register_failure), Toast.LENGTH_SHORT).show();
                        break;
                    case Utils.TASK_POST_LOGIN:
                        strLog += "Task：TASK_POST_LOGIN\n";
                        Toast.makeText(RegisterActivity.this, getString(R.string.register_login_failure), Toast.LENGTH_SHORT).show();
                        break;
                }
                Log.d("RegisterActivity", "onFailure\n" + strLog, throwable);
                mPrgerssDialog.dismiss();
            }

            @Override
            public void onNetworkFailure(int task, int statusCode, Throwable throwable) {
                String strLog = "";
                switch (task) {
                    case Utils.TASK_POST_ACCOUNT_IS_REGISTERED:
                        strLog += "Task：TASK_POST_ACCOUNT_IS_REGISTERED\n";
                        break;
                    case Utils.TASK_POST_REGISTER:
                        strLog += "Task：TASK_POST_REGISTER\n";
                        break;
                    case Utils.TASK_POST_LOGIN:
                        strLog += "Task：TASK_POST_LOGIN\n";
                        break;
                }
                Log.d("RegisterActivity", "onNetworkFailure\n" + strLog, throwable);
                Toast.makeText(RegisterActivity.this, getString(R.string.network_failure), Toast.LENGTH_SHORT).show();
                mPrgerssDialog.dismiss();
            }
        };

        if (BuildConfig.DEBUG) {
            mEmailView.setText("kai_tseng@huanuage.com");
            mPasswordView.setText("Qq32165498");
            mChkPasswordView.setText("Qq32165498");
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.setClass(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
