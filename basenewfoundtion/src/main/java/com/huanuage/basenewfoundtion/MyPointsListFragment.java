package com.huanuage.basenewfoundtion;

import android.content.res.ColorStateList;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import com.huanuage.HttpClient.utils.APICallbacks;
import com.huanuage.baselibrary.BaseApiListFragment;
import com.huanuage.baselibrary.utils.StringHelper;
import com.huanuage.basenewfoundtion.apiclient.*;
import com.huanuage.basenewfoundtion.model.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MyPointsListFragment extends BaseApiListFragment {
    private APICallbacks mAPICallback;
    private CustomAdapter<RewardPoints> customAdapter;
    ColorStateList oldTextColors = null;
    int oldTextPaintFlags = 0;
    public List<RewardPoints> mList = new ArrayList<RewardPoints>();
    public MyPointsTab myPointsTab;
    public static final SimpleDateFormat mHistoryDateSimpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myPointsTab = (MyPointsTab) getArguments().getSerializable(MyPointsFragment.TAG_TAB_EXTRA_NAME);
        mAPICallback = new APICallbacks() {
            @Override
            public void onSuccess(int task, int statusCode, HashMap<String, String> headers, Object data) {
                disableRefreshLayout();
                BaseBackendResponse<List<RewardPoints>> baseBackendResponse = (BaseBackendResponse<List<RewardPoints>>) data;
                if (mPage == 0) {
                    mList.clear();
                }
                mList.addAll(baseBackendResponse.Data);
                customAdapter.ChangeData(mList);
                //customAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(int task, int statusCode, HashMap<String, String> headers, String responseString, Throwable throwable) {
                disableRefreshLayout();
                customAdapter.ChangeData();
                Log.d("MyPointsListFragment", "onFailure", throwable);
            }

            @Override
            public void onNetworkFailure(int task, int statusCode, Throwable throwable) {
                disableRefreshLayout();
                Log.d("MyPointsListFragment", "onNetworkFailure", throwable);
                Toast.makeText(getActivity(), getString(R.string.network_failure), Toast.LENGTH_SHORT).show();
            }
        };
        customAdapter = new CustomAdapter<RewardPoints>(getActivity(), mList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                ViewHolder holder = null;
                RewardPoints myPointsHistory = mList.get(position);
                if (convertView == null) {
                    convertView = LayoutInflater.from(mContext).inflate(R.layout.item_my_points_list_entity, null);
                    holder = new ViewHolder();
                    holder.RowNumber = (TextView) convertView.findViewById(R.id.index);
                    holder.CreateDate = (TextView) convertView.findViewById(R.id.history_date);
                    holder.PointIn = (TextView) convertView.findViewById(R.id.import_points);
                    holder.PointOut = (TextView) convertView.findViewById(R.id.export_points);
                    holder.Item = (TextView) convertView.findViewById(R.id.remark);
                    convertView.setTag(holder);
                    if (oldTextColors == null)
                        oldTextColors = holder.PointIn.getTextColors();
                    if (oldTextPaintFlags == 0)
                        oldTextPaintFlags = holder.Item.getPaintFlags();
                } else {
                    holder = (ViewHolder) convertView.getTag();
                }
                holder.RowNumber.setText(String.valueOf(myPointsHistory.RowNumber));
                holder.CreateDate.setText(mHistoryDateSimpleDateFormat.format(myPointsHistory.CreateDate));
                if (myPointsHistory.PointIn != 0) {
                    holder.PointIn.setText(String.format("%s%d", (myPointsHistory.PointIn > 0 ? "+" : ""), myPointsHistory.PointIn));
                    if (Build.VERSION.SDK_INT >= 23) {
                        holder.PointIn.setTextColor(getResources().getColor(R.color.color_258e4c, null));
                    } else {
                        holder.PointIn.setTextColor(getResources().getColor(R.color.color_258e4c));
                    }
                } else {
                    holder.PointIn.setText(R.string.mypoints_item_list_entity_points_empty_text);
                    holder.PointIn.setTextColor(oldTextColors);
                }
                if (myPointsHistory.PointOut != 0) {
                    holder.PointOut.setText(String.format("%s%d", (myPointsHistory.PointOut > 0 ? "-" : ""), myPointsHistory.PointOut));
                    if (Build.VERSION.SDK_INT >= 23) {
                        holder.PointOut.setTextColor(getResources().getColor(R.color.color_550202, null));
                    } else {
                        holder.PointOut.setTextColor(getResources().getColor(R.color.color_550202));
                    }
                } else {
                    holder.PointOut.setText(R.string.mypoints_item_list_entity_points_empty_text);
                    holder.PointOut.setTextColor(oldTextColors);
                }
                holder.Item.setText(myPointsHistory.Item);
                if (StringHelper.IsNullOrWhiteSpace(myPointsHistory.ShoppingId) == false) {
                    if (Build.VERSION.SDK_INT >= 23) {
                        holder.Item.setTextColor(getResources().getColor(R.color.color_04326b, null));
                    } else {
                        holder.Item.setTextColor(getResources().getColor(R.color.color_04326b));
                    }
                    holder.Item.setPaintFlags(oldTextPaintFlags | Paint.UNDERLINE_TEXT_FLAG);
                } else {
                    holder.Item.setTextColor(oldTextColors);
                    holder.Item.setPaintFlags(oldTextPaintFlags);
                }
                return convertView;
            }

            class ViewHolder {
                public TextView RowNumber;
                public TextView CreateDate;
                public TextView PointIn;
                public TextView PointOut;
                public TextView Item;
            }
        };
        //executeApi();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
        return false;
    }

    @Override
    public CustomAdapter getAdapter() {
        return customAdapter;
    }

    @Override
    public View getHeaderView() {
        return LayoutInflater.from(getActivity()).inflate(R.layout.item_my_points_list_header, null);
    }

    @Override
    protected void executeApi() {
        enableRefreshLayout();
        if (myPointsTab == null) {
            myPointsTab = (MyPointsTab) getArguments().getSerializable(MyPointsFragment.TAG_TAB_EXTRA_NAME);
        }
        APIClientToBackend.getInstance().GetRewardPoints(getActivity(), mPage + 1, 10, myPointsTab.getValue(), mAPICallback);
    }

    public enum MyPointsTab {
        ThreeMonth(1), SixMonth(2), ThisYear(3), LastYear(4), BeforeLastYear(5);
        int value;

        MyPointsTab(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }
}
