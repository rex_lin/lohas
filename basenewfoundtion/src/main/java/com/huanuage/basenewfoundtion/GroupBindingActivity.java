package com.huanuage.basenewfoundtion;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.huanuage.HttpClient.utils.APICallbacks;
import com.huanuage.baselibrary.utils.BaseEditTextValidatorOnFocusChange;
import com.huanuage.baselibrary.utils.StringHelper;
import com.huanuage.baselibrary.utils.UUIDHelper;
import com.huanuage.basenewfoundtion.apiclient.APIClientToBackend;
import com.huanuage.basenewfoundtion.model.GroupMemberVerification;
import com.huanuage.basenewfoundtion.untils.Utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

public class GroupBindingActivity extends Activity implements TextView.OnEditorActionListener {

    private EditText mBirthdayView;
    private TextView mBirthdayErrorView;
    private EditText mEmployeeNumberView;
    private TextView mEmployeeNumberErrorView;
    private EditText mCompanyCodeView;
    private TextView mCompanyCodeErrorView;
    private EditText mUserNameView;
    private TextView mUserNameErrorView;
    private EditText mCallingCodeView;
    private EditText mPhoneView;
    private EditText mAddressView;
    private Button mBtnBindingView;
    private Calendar myCalendar = Calendar.getInstance();
    private DatePickerDialog datePickerDialog;
    private DatePickerDialog.OnDateSetListener dateSetListener;
    private APICallbacks mApiCallBack;
    private ProgressDialog mPrgerssDialog;
    private static final SimpleDateFormat mBirthdayFormat = new SimpleDateFormat("yyyyMMdd");
//    private CustomAddressSelectionDialog addressSelectionDialog;
//    private Address address;
    private boolean IsBinding = false;
    private Handler mLoginSuccessHandler;

//    private Handler mAddressSelectionHandler = new Handler() {
//        @Override
//        public void handleMessage(Message msg) {
//            super.handleMessage(msg);
//            if (msg.obj != null && msg.obj instanceof Address) {
//                address = (Address) msg.obj;
//                mAddressView.setText(String.format("%s%s%s%s",
//                        address.country == null || UUIDHelper.IsNullOrEmpty(address.country.CountrySerialNo) ? "" : address.country.CountryName.trim(),
//                        address.county == null || UUIDHelper.IsNullOrEmpty(address.county.CountySerialNo) ? "" : address.county.CountyName.trim(),
//                        address.cityArea == null || UUIDHelper.IsNullOrEmpty(address.cityArea.CityAreaSerialNo) ? "" : address.cityArea.CityAreaName.trim(),
//                        StringHelper.IsNullOrWhiteSpace(address.Address) ? "" : address.Address.trim()));
//            } else {
//                address = null;
//                mAddressView.setText("");
//            }
//        }
//    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_binding);
        mLoginSuccessHandler = Utils.getLoginSuccessCallbackHandler();

        //UI 控制項初始化
        mBirthdayView = (EditText) findViewById(R.id.birthday);
        mBirthdayErrorView = (TextView) findViewById(R.id.birthday_error);
        mEmployeeNumberView = (EditText) findViewById(R.id.employeenumber);
        mEmployeeNumberErrorView = (TextView) findViewById(R.id.employeenumber_error);
        mCompanyCodeView = (EditText) findViewById(R.id.company_code);
        mCompanyCodeErrorView = (TextView) findViewById(R.id.company_code_error);
        mBtnBindingView = (Button) findViewById(R.id.btn_binding);
        mPrgerssDialog = new ProgressDialog(this);
        mPrgerssDialog.setCancelable(false);
        mPrgerssDialog.setCanceledOnTouchOutside(false);
        mPrgerssDialog.setMessage(getString(R.string.group_binding_prgerss_messag));
        mUserNameView = (EditText) findViewById(R.id.user_name);
        mUserNameErrorView = (TextView) findViewById(R.id.user_name_error);
        mCallingCodeView = (EditText) findViewById(R.id.calling_code);
        mPhoneView = (EditText) findViewById(R.id.phone);
        mAddressView = (EditText) findViewById(R.id.address);

        //Listener初始化
        final BaseEditTextValidatorOnFocusChange mCompanyCodeViewValidator = new BaseEditTextValidatorOnFocusChange(mCompanyCodeView, mCompanyCodeErrorView) {
            @Override
            public boolean validate(String value) {
                return true;
            }
        };
        mCompanyCodeViewValidator.setRequiredTextId(R.string.group_binding_company_code_hint);
        mCompanyCodeViewValidator.setFocusBackgroundId(R.mipmap.accountext);
        mCompanyCodeViewValidator.setNonFocusBackgroundId(R.mipmap.accountext);
        mCompanyCodeViewValidator.setErrorBackgroundId(R.mipmap.accountext_red);

        final BaseEditTextValidatorOnFocusChange mEmployeeNumberViewValidator = new BaseEditTextValidatorOnFocusChange(mEmployeeNumberView, mEmployeeNumberErrorView) {
            @Override
            public boolean validate(String value) {
                return true;
            }
        };
        mEmployeeNumberViewValidator.setRequiredTextId(R.string.group_binding_employeenumber_hint);
        mEmployeeNumberViewValidator.setFocusBackgroundId(R.mipmap.accountext);
        mEmployeeNumberViewValidator.setNonFocusBackgroundId(R.mipmap.accountext);
        mEmployeeNumberViewValidator.setErrorBackgroundId(R.mipmap.accountext_red);


        final BaseEditTextValidatorOnFocusChange mUserNameViewValidator = new BaseEditTextValidatorOnFocusChange(mUserNameView, mUserNameErrorView) {
            @Override
            public boolean validate(String value) {
                return true;
            }
        };
        mUserNameViewValidator.setRequiredTextId(R.string.group_binding_username_hint);
        mUserNameViewValidator.setFocusBackgroundId(R.mipmap.accountext);
        mUserNameViewValidator.setNonFocusBackgroundId(R.mipmap.accountext);
        mUserNameViewValidator.setErrorBackgroundId(R.mipmap.accountext_red);

        dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateBirthdayView();
                mUserNameView.requestFocus();
                ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
            }
        };
        final BaseEditTextValidatorOnFocusChange mBirthdayViewValidator = new BaseEditTextValidatorOnFocusChange(mBirthdayView, mBirthdayErrorView, R.string.group_binding_birthday_error_format, R.string.group_binding_birthday_hint) {
            @Override
            public boolean validate(String value) {
                return value.matches("^(((19)|([2-9][0-9]))\\d{2}((0[1-9])|(1[0-2]))((0[1-9])|([12][0-9])|(3[01])))$");
            }
        };

        Button.OnClickListener mBtnBindingOnClickListener = new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCompanyCodeViewValidator.onFocusChange(view, false);
                mEmployeeNumberViewValidator.onFocusChange(view, false);
                mBirthdayViewValidator.onFocusChange(view, false);
                mUserNameViewValidator.onFocusChange(view, false);
                if (mCompanyCodeViewValidator.isCheckSuccess() && mEmployeeNumberViewValidator.isCheckSuccess() && mBirthdayViewValidator.isCheckSuccess() && mUserNameViewValidator.isCheckSuccess()) {
                    GroupMemberVerification groupMemberVerification = new GroupMemberVerification(mCompanyCodeView.getText().toString(), mEmployeeNumberView.getText().toString(), mBirthdayView.getText().toString(), mUserNameView.getText().toString());
                    if (StringHelper.IsNullOrWhiteSpace(mCallingCodeView.getText().toString()) == false) {
                        groupMemberVerification.phoneCode = mCallingCodeView.getText().toString();
                    }
                    if (StringHelper.IsNullOrWhiteSpace(mPhoneView.getText().toString()) == false) {
                        groupMemberVerification.phoneNumber = mPhoneView.getText().toString();
                    }
//                    if (address != null) {
//                        if (!(address.country == null || UUIDHelper.IsNullOrEmpty(address.country.CountrySerialNo))) {
//                            groupMemberVerification.countrySerialNo = address.country.CountrySerialNo;
//                        }
//                        if (!(address.county == null || UUIDHelper.IsNullOrEmpty(address.county.CountySerialNo))) {
//                            groupMemberVerification.countySerialNo = address.county.CountySerialNo;
//                        }
//                        if (!(address.cityArea == null || UUIDHelper.IsNullOrEmpty(address.cityArea.CityAreaSerialNo))) {
//                            groupMemberVerification.cityAreaSerialNo = address.cityArea.CityAreaSerialNo;
//                        }
//                        if (StringHelper.IsNullOrWhiteSpace(address.Address) == false) {
//                            groupMemberVerification.address = address.Address;
//                        }
//                    }
                    groupMemberVerification.address = mAddressView.getText().toString();
                    mPrgerssDialog.show();
                    APIClientToBackend.getInstance().putGroupMemberVerification(GroupBindingActivity.this, groupMemberVerification, mApiCallBack);
                }
            }
        };

        //Dialog初始化
        datePickerDialog = new DatePickerDialog(GroupBindingActivity.this, dateSetListener, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));

        //事件初始化
        mCompanyCodeView.setOnFocusChangeListener(mCompanyCodeViewValidator);

        mEmployeeNumberView.setOnFocusChangeListener(mEmployeeNumberViewValidator);

        mEmployeeNumberView.setOnEditorActionListener(this);

        mBirthdayView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePickerDialog.show();
            }
        });
        mBirthdayView.setOnFocusChangeListener(mBirthdayViewValidator);

        mBtnBindingView.setOnClickListener(mBtnBindingOnClickListener);

        mUserNameView.setOnFocusChangeListener(mUserNameViewValidator);
        //mPhoneView.setOnEditorActionListener(this);

//        mAddressView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                showAddressSelectionDialog();
//            }
//        });

        mApiCallBack = new APICallbacks() {
            @Override
            public void onSuccess(int task, int statusCode, HashMap<String, String> headers, Object responseObject) {
                String strLog = "";
                switch (task) {
                    case Utils.TASK_PUT_GROUP_MEMBER_VERIFICATION:
                        strLog += "Task：TASK_PUT_GROUP_MEMBER_VERIFICATION\n";
                        IsBinding = true;
                        if (mLoginSuccessHandler == null) {
                            Intent intent = new Intent();
                            intent.setClass(GroupBindingActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            mLoginSuccessHandler.sendEmptyMessage(1);
                            finish();
                        }
                        break;
                }
                Log.d("GroupBindingActivity", "onSuccess\n" + strLog);
                mPrgerssDialog.dismiss();
            }

            @Override
            public void onFailure(int task, int statusCode, HashMap<String, String> headers, String responseString, Throwable throwable) {
                String strLog = "";
                switch (task) {
                    case Utils.TASK_PUT_GROUP_MEMBER_VERIFICATION:
                        strLog += "Task：TASK_PUT_GROUP_MEMBER_VERIFICATION\n";
                        Toast.makeText(GroupBindingActivity.this, getString(R.string.group_binding_fail), Toast.LENGTH_SHORT).show();
                        break;
                }
                Log.d("GroupBindingActivity", "onFailure\n" + strLog, throwable);
                mPrgerssDialog.dismiss();
            }

            @Override
            public void onNetworkFailure(int task, int statusCode, Throwable throwable) {
                String strLog = "";
                switch (task) {
                    case Utils.TASK_PUT_GROUP_MEMBER_VERIFICATION:
                        strLog += "Task：TASK_PUT_GROUP_MEMBER_VERIFICATION\n";
                        break;
                }
                Log.d("GroupBindingActivity", "onNetworkFailure\n" + strLog, throwable);
                Toast.makeText(GroupBindingActivity.this, getString(R.string.network_failure), Toast.LENGTH_SHORT).show();
                mPrgerssDialog.dismiss();
            }
        };

        if (BuildConfig.DEBUG) {
            mCompanyCodeView.setText("MTK201510260007");
            mEmployeeNumberView.setText("MTK000048");
            mBirthdayView.setText("19571121");
        }
    }

    private void updateBirthdayView() {
        mBirthdayView.setText(mBirthdayFormat.format(myCalendar.getTime()));
    }

    @Override
    public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
        if (actionId == EditorInfo.IME_ACTION_NEXT) {
            int viewId = textView.getId();
            if (viewId == R.id.employeenumber) {
                mBirthdayView.performClick();
                return true;
            } /*else if (viewId == R.id.phone) {
                mAddressView.performClick();
                return true;
            }*/
        }
        return false;
    }

//    private void showAddressSelectionDialog() {
//        if (addressSelectionDialog == null) {
//            addressSelectionDialog = new CustomAddressSelectionDialog();
//            addressSelectionDialog.setBtnSendCallback(mAddressSelectionHandler);
//        }
//        addressSelectionDialog.setAddress(address);
//        addressSelectionDialog.show(getFragmentManager(), "addressSelectionDialog");
//    }

    @Override
    protected void onDestroy() {
        if (IsBinding == false)
            Utils.Logout(GroupBindingActivity.this);
        super.onDestroy();
    }
}
