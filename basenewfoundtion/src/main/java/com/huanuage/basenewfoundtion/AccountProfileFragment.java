package com.huanuage.basenewfoundtion;

import android.app.ProgressDialog;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.huanuage.HttpClient.utils.APICallbacks;
import com.huanuage.baselibrary.utils.LogHelper;
import com.huanuage.baselibrary.utils.UUIDHelper;
import com.huanuage.baselibrary.view.CircularImageView;
import com.huanuage.basenewfoundtion.apiclient.APIClientToBackend;
import com.huanuage.basenewfoundtion.model.AccountProfile;
import com.huanuage.basenewfoundtion.model.BaseBackendResponse;
import com.huanuage.basenewfoundtion.untils.Utils;

import java.util.HashMap;


public class AccountProfileFragment extends Fragment {
    private static final String TAG = "AccountProfileFragment";

    private TextView mUserNameView;
    private CircularImageView mPhotoView;
    private EditText mNickNameView;
    private Spinner mGenderView;
    private EditText mImIdView;
    private EditText mCallingCodeView;
    private EditText mPhoneView;
//    private CustomAddressSelectionView mAddressSelectionView;
    private EditText mAddressView;
    private Button mBtnSendView;
    private APICallbacks mApiCallback;
    private AccountProfile accountProfile = new AccountProfile();
    private ProgressDialog mPrgerssDialog;

    enum Gender {
        NONE("", 0),
        Male("Male", 1),
        Female("Female", 2);

        private String stringValue;
        private int intValue;

        private Gender(String toString, int value) {
            stringValue = toString;
            intValue = value;
        }

        public int getValue() {
            return intValue;
        }

        @Override
        public String toString() {
            return stringValue;
        }

        public static Gender getByInt(int value) {
            Gender gender = Gender.NONE;
            for (Gender tmp : Gender.values()) {
                if (tmp.getValue() == value) {
                    gender = tmp;
                    break;
                }
            }
            return gender;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_account_profile, container, false);

        //UI初始化
        mUserNameView = (TextView) view.findViewById(R.id.user_name);
        mPhotoView = (CircularImageView) view.findViewById(R.id.photo);
        mNickNameView = (EditText) view.findViewById(R.id.nick_name);
        mGenderView = (Spinner) view.findViewById(R.id.gender);
        mImIdView = (EditText) view.findViewById(R.id.im_id);
        mCallingCodeView = (EditText) view.findViewById(R.id.calling_code);
        mPhoneView = (EditText) view.findViewById(R.id.phone);
//        mAddressSelectionView = (CustomAddressSelectionView) view.findViewById(R.id.address_selection);
        mAddressView = (EditText)view.findViewById(R.id.address);
        mBtnSendView = (Button) view.findViewById(R.id.btn_send);

        mPrgerssDialog = new ProgressDialog(getActivity());
        mPrgerssDialog.setCancelable(false);
        mPrgerssDialog.setCanceledOnTouchOutside(false);
        mPrgerssDialog.setMessage(getString(R.string.coustom_address_progress_message));

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(container.getContext(),
                R.array.account_profile_gender_item, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mGenderView.setAdapter(adapter);

        mBtnSendView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPrgerssDialog.setMessage(getString(R.string.account_profile_prgerss_send_message));
                mPrgerssDialog.show();
                if (accountProfile == null)
                    accountProfile = new AccountProfile();
                accountProfile.NickName = mNickNameView.getText().toString();
                accountProfile.Gender = Gender.getByInt(mGenderView.getSelectedItemPosition()).toString();
                accountProfile.ImId = mImIdView.getText().toString();
                accountProfile.PhoneCode = mCallingCodeView.getText().toString();
                accountProfile.PhoneNumber = mPhoneView.getText().toString();
//                Address address = mAddressSelectionView.getAddress();
//                accountProfile.CountrySerialNo = address.getCountrySerialNo();
//                accountProfile.CountySerialNo = address.getCountySerialNo();
//                accountProfile.CityAreaSerialNo = address.getCityAreaSerialNo();
//                accountProfile.Address = address.Address;
                accountProfile.Address=mAddressView.getText().toString();
                APIClientToBackend.getInstance().putAccountProfile(getActivity(), accountProfile, mApiCallback);
            }
        });

        //API回傳初始化
        mApiCallback = new APICallbacks() {
            @Override
            public void onSuccess(int task, int statusCode, HashMap<String, String> headers, Object data) {
                switch (task) {
                    case Utils.TASK_GET_ACCOUNT_PROFILE:
                        accountProfile = ((BaseBackendResponse<AccountProfile>) data).Data;
                        mUserNameView.setText(accountProfile.Name);
                        try {
                            mPhotoView.setImageURI(Uri.parse(accountProfile.PhotoPath));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        mNickNameView.setText(accountProfile.NickName);
                        try {
                            mGenderView.setSelection(Gender.valueOf(accountProfile.Gender).getValue());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        mImIdView.setText(accountProfile.ImId);
                        mCallingCodeView.setText(accountProfile.PhoneCode);
                        mPhoneView.setText(accountProfile.PhoneNumber);
//                        Address address = new Address();
//                        if (UUIDHelper.IsNullOrEmpty(accountProfile.CountrySerialNo) == false) {
//                            address.country = new Country();
//                            address.country.CountrySerialNo = accountProfile.CountrySerialNo;
//                        }
//                        if (UUIDHelper.IsNullOrEmpty(accountProfile.CountySerialNo) == false) {
//                            address.county = new County();
//                            address.county.CountySerialNo = accountProfile.CountySerialNo;
//                        }
//                        if (UUIDHelper.IsNullOrEmpty(accountProfile.CityAreaSerialNo) == false) {
//                            address.cityArea = new CityArea();
//                            address.cityArea.CityAreaSerialNo = accountProfile.CityAreaSerialNo;
//                        }
//                        address.Address = accountProfile.Address;
//                        mAddressSelectionView.setAddress(address);
                        mAddressView.setText(accountProfile.Address);
                        break;
                    case Utils.TASK_PUT_ACCOUNT_PROFILE:
                        Toast.makeText(getActivity(), R.string.account_profile_send_success_message, Toast.LENGTH_SHORT).show();
                        break;
                }
                mPrgerssDialog.dismiss();
            }

            @Override
            public void onFailure(int task, int statusCode, HashMap<String, String> headers, String responseString, Throwable throwable) {
                LogHelper.w(TAG, responseString, throwable);
                switch (task) {
                    case Utils.TASK_GET_ACCOUNT_PROFILE:
                        Toast.makeText(getActivity(), getString(R.string.network_failure), Toast.LENGTH_SHORT).show();
                        break;
                    case Utils.TASK_PUT_ACCOUNT_PROFILE:
                        Toast.makeText(getActivity(), R.string.account_profile_send_fail_message, Toast.LENGTH_SHORT).show();
                        break;
                }
                mPrgerssDialog.dismiss();
            }

            @Override
            public void onNetworkFailure(int task, int statusCode, Throwable throwable) {
                LogHelper.w(TAG, throwable);
                Toast.makeText(getActivity(), getString(R.string.network_failure), Toast.LENGTH_SHORT).show();
                mPrgerssDialog.dismiss();
            }
        };
        mPrgerssDialog.show();
        APIClientToBackend.getInstance().getAccountProfile(getActivity(), mApiCallback);
        return view;
    }


}
