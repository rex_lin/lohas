package com.huanuage.basenewfoundtion.model;

import java.io.Serializable;

public class Register implements Serializable {
	public Register(String email, String password, String confirmPassword) {
		this.Email = email;
		this.Password = password;
		this.ConfirmPassword = confirmPassword;
	}

	public final String Email;
	public final String Password;
	public final String ConfirmPassword;
}
