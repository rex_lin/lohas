package com.huanuage.basenewfoundtion.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by kai_tseng on 2015/10/29.
 */
public class RewardPoints implements Serializable {
    /**
     * 序號
     */
    public int RowNumber;
    /**
     * 日期
     */
    public Date CreateDate;
    /**
     * 匯入點數[+]
     */
    public int PointIn;
    /**
     * 使用點數[-]
     */
    public int PointOut;
    /**
     * 中文項目
     */
    public String Item;
    /**
     * 風行天的交易編號
     */
    public String ShoppingId;
    /**
     * 是否退款
     */
    public boolean IsReturn;
}
