package com.huanuage.basenewfoundtion.model;

import java.io.Serializable;

/**
 * Created by kai_tseng on 2015/11/1.
 */
public class OwnPoint implements Serializable {
    /**
     * 擁有點數
     */
    public int OwnPoint;
    /**
     * 可使用點數
     */
    public int AvailablePoint;
}
