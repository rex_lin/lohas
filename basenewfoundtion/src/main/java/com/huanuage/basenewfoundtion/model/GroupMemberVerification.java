package com.huanuage.basenewfoundtion.model;

import java.io.Serializable;
import java.util.UUID;

public class GroupMemberVerification implements Serializable {
    public GroupMemberVerification(String companyCode, String employeeId,
                                   String birthday, String userName) {
        this.companyCode = companyCode;
        this.employeeId = employeeId;
        this.birthday = birthday;
        this.userName = userName;
    }

    public final String companyCode;
    public final String employeeId;
    public final String birthday;
    public final String userName;
    public String phoneCode;
    public String phoneNumber;
    public UUID countrySerialNo;
    public UUID countySerialNo;
    public UUID cityAreaSerialNo;
    public String address;
}
