package com.huanuage.basenewfoundtion.model;

import java.io.Serializable;

public class ChangePassword implements Serializable {
	public ChangePassword(String userName, String oldPassword,
			String newPassword, String confirmPassword) {
		this.UserName = userName;
		this.OldPassword = oldPassword;
		this.NewPassword = newPassword;
		this.ConfirmPassword = confirmPassword;
	}

	public final String UserName;
	public final String OldPassword;
	public final String NewPassword;
	public final String ConfirmPassword;
}
