package com.huanuage.basenewfoundtion.model;

public class BaseBackendResponseMeta {
	/**
	 * 目前頁數
	 */
	public int PageNumber;
	/**
	 * 一頁顯示資料
	 */
	public int PageSize;
	/**
	 * 總資料數
	 */
	public int PageCount;
	/**
	 * HttpStatusCode
	 */
	public int HttpStatusCode;
}
