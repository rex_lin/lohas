package com.huanuage.basenewfoundtion.model;

import java.io.Serializable;
import java.net.URLEncoder;

public class LoginAccount implements Serializable {
	public LoginAccount(String username, String password) {
		this.username = username;
		this.password = password;
	}

	public final String username;
	public final String password;
	public final String grant_type = "password";

	public String getFormData() {
		try {
			return String.format("username=%s&password=%s&grant_type=%s",
					URLEncoder.encode(username, "UTF-8"),
					URLEncoder.encode(password, "UTF-8"),
					URLEncoder.encode(grant_type, "UTF-8"));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
