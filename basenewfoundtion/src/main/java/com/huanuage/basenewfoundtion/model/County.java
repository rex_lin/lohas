package com.huanuage.basenewfoundtion.model;

import com.google.gson.annotations.SerializedName;
import com.huanuage.baselibrary.utils.UUIDHelper;
import com.huanuage.baselibrary.interfaces.*;

import java.io.Serializable;
import java.util.UUID;

/**
 * Created by kai_tseng on 2015/11/9.
 */
public class County implements Serializable, iArrayAdapterModel, iGetUuid {
    /**
     * 縣市流水號
     */
    @SerializedName("SerialNo")
    public UUID CountySerialNo;

    /**
     * 縣市名稱
     */
    @SerializedName("Name")
    public String CountyName;

//    /**
//     * 鄉鎮列表
//     */
//    public List<CityArea> cityAreaList;

    /**
     * 上一層流水號
     */
    public UUID UpSerialNo;

    @Override
    public String toString() {
        return CountyName;
    }

    public static County getHintItem(String name) {
        County county = new County();
        county.CountyName = name;
        return county;
    }

    @Override
    public int hashCode() {
        if (CountySerialNo == null)
            return 0;
        return CountySerialNo.hashCode();
    }

    @Override
    public County clone() {
        County county = new County();
        county.CountySerialNo = this.CountySerialNo;
        county.CountyName = this.CountyName;
        county.UpSerialNo = this.UpSerialNo;
        return county;
    }

    @Override
    public boolean isFilter(long upperId) {
        if (UUIDHelper.IsNullOrEmpty(CountySerialNo) == true) return true;
        long mThisUpperId = 0;
        if (UpSerialNo != null)
            mThisUpperId = UpSerialNo.hashCode();
        return mThisUpperId == upperId;
    }

    @Override
    public UUID getUuid() {
        return CountySerialNo;
    }
}
