package com.huanuage.basenewfoundtion.model;

import java.io.Serializable;

public class ModelState<T> implements Serializable {
	public String message;
	public T modelState;
}
