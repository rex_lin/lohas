package com.huanuage.basenewfoundtion.model;

import com.google.gson.annotations.SerializedName;
import com.huanuage.baselibrary.utils.UUIDHelper;
import com.huanuage.baselibrary.interfaces.*;

import java.io.Serializable;
import java.util.UUID;

/**
 * Created by kai_tseng on 2015/11/9.
 */
public class Country implements Serializable, iArrayAdapterModel, iGetUuid {
    /**
     * 國家流水號
     */
    @SerializedName("SerialNo")
    public UUID CountrySerialNo;

    /**
     * 國家名稱
     */
    @SerializedName("Name")
    public String CountryName;

//    /**
//     * 縣市列表
//     */
//    public List<County> countyList;

    @Override
    public String toString() {
        return CountryName;
    }

    @Override
    public boolean isFilter(long upperId) {
        if (UUIDHelper.IsNullOrEmpty(CountrySerialNo) == true) return true;
        return false;
    }

    public static Country getHintItem(String name) {
        Country country = new Country();
        country.CountryName = name;
        return country;
    }

    @Override
    public int hashCode() {
        if (CountrySerialNo == null)
            return 0;
        return CountrySerialNo.hashCode();
    }

    @Override
    public Country clone() {
        Country country = new Country();
        country.CountrySerialNo = this.CountrySerialNo;
        country.CountryName = this.CountryName;
        return country;
    }

    @Override
    public UUID getUuid() {
        return CountrySerialNo;
    }
}
