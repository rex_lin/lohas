package com.huanuage.basenewfoundtion.model;

import java.io.Serializable;
import java.util.UUID;

public class UserInfo implements Serializable {
	/**
	 * 群組編號
	 */
	public UUID GroupSerialId;
	/**
	 * 使用者id
	 */
	public UUID UserId;
	/**
	 * 工號
	 */
	public String EmployeeId;
	/**
	 * 使用者mail
	 */
	public String UserEmail;
	/**
	 * 使用者中文姓名
	 */
	public String UserName;
	/**
	 * 使用者電話
	 */
	public String UserPhone;
	/**
	 * 群組代碼
	 */
	public String GroupCode;
	/**
	 * 群組名稱
	 */
	public String GroupName;
	/**
	 * 組織代碼
	 */
	public UUID OrganizationUnitSerialId;
	/**
	 * 組織名稱
	 */
	public String OrganizationUnitName;
	/**
	 * 主管代碼
	 */
	public String LeaderSerialId;
	/**
	 * 主管名稱
	 */
	public String LeaderName;
	/**
	 * 是否綁定
	 */
	public boolean IsBinding;

}
