package com.huanuage.basenewfoundtion.model;

import java.io.Serializable;

public class ForgetUserPassword implements Serializable {
	public ForgetUserPassword(String email) {
		this(email, "zh-tw");
	}

	public ForgetUserPassword(String email, String language) {
		this.Email = email;
		this.lang = language;
	}

	public final String Email;
	public final String lang;
}
