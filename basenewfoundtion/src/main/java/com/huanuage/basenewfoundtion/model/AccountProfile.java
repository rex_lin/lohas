package com.huanuage.basenewfoundtion.model;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

/**
 * 使用者資訊
 */
public class AccountProfile implements Serializable {
    /**
     * 使用者工號
     */
    public String UserId;

    /**
     * 姓名
     */
    public String Name;

    /**
     * 生日
     */
    public String Birthday;

    /**
     * 暱稱
     */
    public String NickName;

    /**
     * 性別
     */
    public String Gender;

    /**
     * 國家流水號
     */
    public UUID CountrySerialNo;

    /**
     * 國家名稱
     */
    public String CountryName;

    /**
     * 縣市流水號
     */
    public UUID CountySerialNo;

    /**
     * 縣市名稱
     */
    public String CountyName;

    /**
     * 鄉鎮流水號
     */
    public UUID CityAreaSerialNo;

    /**
     * 鄉鎮名稱
     */
    public String CityAreaName;

    /**
     * 地址
     */
    public String Address;

    /**
     * 電話國碼
     */
    public String PhoneCode;

    /**
     * 電話
     */
    public String PhoneNumber;

    /**
     * 最後修改時間
     */
    public Date LatestUpdateTime;

    /**
     * imId
     */
    public String ImId;

    /**
     * Im照片連結
     */
    public String PhotoPath;
}
