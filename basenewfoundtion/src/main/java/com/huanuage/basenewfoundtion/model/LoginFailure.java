package com.huanuage.basenewfoundtion.model;

import java.io.Serializable;

public class LoginFailure implements Serializable {
	public String error;
	public String error_description;
}
