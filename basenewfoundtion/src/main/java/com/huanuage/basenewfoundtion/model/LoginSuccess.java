package com.huanuage.basenewfoundtion.model;

import java.io.Serializable;

public class LoginSuccess implements Serializable {
	public String access_token;
	public String token_type;
	public long expires_in;
	public String userName;
//	@SerializedName(".issued")
//	public Date issued;
//	@SerializedName(".expires")
//	public Date expires;
	public String code;
}
