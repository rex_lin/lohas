package com.huanuage.basenewfoundtion.model;

import com.huanuage.baselibrary.interfaces.iBaseModel;

import java.io.Serializable;
import java.util.UUID;

/**
 * Created by kai_tseng on 2015/11/9.
 */
public class Address implements Serializable, iBaseModel<Address> {
    public Country country;
    public County county;
    public CityArea cityArea;
    public String Address;

    @Override
    public UUID getId() {
        return null;
    }

    @Override
    public Address cloneModel(boolean onlyCloneFirstLevel) {
        Address address = new Address();
        address.Address = this.Address;
        if (onlyCloneFirstLevel) {
            address.country = this.country;
            address.county = this.county;
            address.cityArea = this.cityArea;
        } else {
            address.country = this.country == null ? null : this.country.clone();
            address.county = this.county == null ? null : this.county.clone();
            address.cityArea = this.cityArea == null ? null : this.cityArea.clone();
        }
        return address;
    }

    public UUID getCountrySerialNo() {
        UUID CountrySerialNo = null;
        if (country != null)
            CountrySerialNo = country.CountrySerialNo;
        return CountrySerialNo;
    }

    public UUID getCountySerialNo() {
        UUID CountySerialNo = null;
        if (county != null)
            CountySerialNo = county.CountySerialNo;
        return CountySerialNo;
    }

    public UUID getCityAreaSerialNo() {
        UUID CityAreaSerialNo = null;
        if (cityArea != null)
            CityAreaSerialNo = cityArea.CityAreaSerialNo;
        return CityAreaSerialNo;
    }
}
