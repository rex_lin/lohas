package com.huanuage.basenewfoundtion.model;

import com.google.gson.annotations.SerializedName;
import com.huanuage.baselibrary.utils.UUIDHelper;
import com.huanuage.baselibrary.interfaces.*;

import java.io.Serializable;
import java.util.UUID;

/**
 * 鄉鎮
 */
public class CityArea implements Serializable, iArrayAdapterModel, iGetUuid {
    /**
     * 鄉鎮流水號
     */
    @SerializedName("SerialNo")
    public UUID CityAreaSerialNo;

    /**
     * 鄉鎮名稱
     */
    @SerializedName("Name")
    public String CityAreaName;

    /**
     * 郵遞區號
     */
    public String ZipCode;

    /**
     * 上一層流水號
     */
    public UUID UpSerialNo;

    @Override
    public String toString() {
        return CityAreaName;
    }

    @Override
    public boolean isFilter(long upperId) {
        if (UUIDHelper.IsNullOrEmpty(CityAreaSerialNo) == true) return true;
        long mThisUpperId = 0;
        if (UpSerialNo != null)
            mThisUpperId = UpSerialNo.hashCode();
        return mThisUpperId == upperId;
    }

    public static CityArea getHintItem(String name) {
        CityArea cityArea = new CityArea();
        cityArea.CityAreaName = name;
        return cityArea;
    }

    @Override
    public int hashCode() {
        if (CityAreaSerialNo == null)
            return 0;
        return CityAreaSerialNo.hashCode();
    }

    @Override
    public CityArea clone() {
        CityArea cityArea = new CityArea();
        cityArea.CityAreaSerialNo = this.CityAreaSerialNo;
        cityArea.CityAreaName = this.CityAreaName;
        cityArea.ZipCode = this.ZipCode;
        cityArea.UpSerialNo = this.UpSerialNo;
        return cityArea;
    }

    @Override
    public UUID getUuid() {
        return CityAreaSerialNo;
    }
}
