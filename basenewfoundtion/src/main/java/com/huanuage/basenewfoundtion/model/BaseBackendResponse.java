package com.huanuage.basenewfoundtion.model;

import java.io.Serializable;

public class BaseBackendResponse<T> implements Serializable {
    public BaseBackendResponseMeta Meta;
    public BaseBackendResponseError Error;
    public T Data;
}
