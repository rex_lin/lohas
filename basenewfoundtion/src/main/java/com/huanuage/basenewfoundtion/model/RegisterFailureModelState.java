package com.huanuage.basenewfoundtion.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RegisterFailureModelState implements Serializable {
	@SerializedName("model.Email")
	public String[] Email;
	@SerializedName("model.Password")
	public String[] Password;
	@SerializedName("model.ConfirmPassword")
	public String[] ConfirmPassword;
}
