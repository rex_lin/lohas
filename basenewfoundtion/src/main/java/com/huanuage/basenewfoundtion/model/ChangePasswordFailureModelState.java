package com.huanuage.basenewfoundtion.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ChangePasswordFailureModelState implements Serializable {
	@SerializedName("model.UserName")
	public String[] UserName;
	@SerializedName("model.OldPassword")
	public String[] OldPassword;
	@SerializedName("model.NewPassword")
	public String[] NewPassword;
	@SerializedName("model.ConfirmPassword")
	public String[] ConfirmPassword;
}
