package com.huanuage.basenewfoundtion.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * 國家_縣市_鄉鎮 For ios_android
 */
public class AddressListItem implements Serializable {
    public List<Country> Country;
    public List<County> County;
    @SerializedName("CityArea")
    public List<CityArea> Area;
}
