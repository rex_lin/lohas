package com.huanuage.basenewfoundtion.model;

public class BaseBackendResponseError {
	/**
	 * 錯誤狀態
	 */
	public String Status;

	/**
	 * 錯誤訊息的title
	 */
	public String Title;

	/**
	 * 錯誤訊息
	 */
	public String Detail;
}
