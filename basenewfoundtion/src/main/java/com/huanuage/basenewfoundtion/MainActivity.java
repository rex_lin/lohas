package com.huanuage.basenewfoundtion;

import android.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.huanuage.basenewfoundtion.untils.Utils;

public class MainActivity extends FragmentActivity {
    FrameLayout mFLMainView;
    Fragment currectFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_main);
        mFLMainView = (FrameLayout) findViewById(R.id.fl_main);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ActionBar actionBar = getActionBar();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_base_main, menu);
        super.onCreateOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment fragment = null;
        String fragmentTag = "";
        if (id == com.huanuage.basenewfoundtion.R.id.action_change_password) {
            fragmentTag = ChangePasswordFragment.class.getName();
            fragment = fragmentManager.findFragmentByTag(fragmentTag);
            if (fragment == null) {
                fragment = new ChangePasswordFragment();
                fragment.setArguments(getIntent().getExtras());
            } else if (fragment.isVisible()) {
                return true;
            }
        } else if (id == com.huanuage.basenewfoundtion.R.id.action_my_points) {
            fragmentTag = MyPointsFragment.class.getName();
            fragment = fragmentManager.findFragmentByTag(fragmentTag);
            if (fragment == null) {
                fragment = new MyPointsFragment();
            } else if (fragment.isVisible()) {
                return true;
            }
        } else if (id == com.huanuage.basenewfoundtion.R.id.action_logout) {
            Utils.Logout(MainActivity.this);
            return true;
        } else if (id == com.huanuage.basenewfoundtion.R.id.action_account_profile) {
            fragmentTag = AccountProfileFragment.class.getName();
            fragment = fragmentManager.findFragmentByTag(fragmentTag);
            if (fragment == null) {
                fragment = new AccountProfileFragment();
            } else if (fragment.isVisible()) {
                return true;
            }
        } else {
            return true;
        }
        currectFragment = fragment;
        fragmentTransaction.replace(com.huanuage.basenewfoundtion.R.id.fl_main, currectFragment, fragmentTag)
                .addToBackStack(null).commit();

        return super.onOptionsItemSelected(item);
    }
}
