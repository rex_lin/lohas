package com.huanuage.basenewfoundtion.untils;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by kai_tseng on 2015/11/10.
 */
public class SharedPreferencesHelper {
    private static SharedPreferences sharedPreferences;
    private static Context applicationContext;

    public static SharedPreferences getSharedPreferences(final Context context) {
        if (applicationContext == null) {
            if (context instanceof Application) {
                applicationContext = context;
            } else {
                applicationContext = context.getApplicationContext();
            }
        }
        if (sharedPreferences == null)
            sharedPreferences = applicationContext.getSharedPreferences(applicationContext.getPackageName(), Context.MODE_PRIVATE);
        return sharedPreferences;
    }

    /**
     * 取得憑證
     *
     * @param context
     * @return
     */
    public static String getAuthorization(final Context context) {
        SharedPreferences sharedPreferences = getSharedPreferences(context);
        return sharedPreferences.getString(Utils.TAG_AUTHORIZATION, "");
    }

    /**
     * 儲存憑證
     *
     * @param context
     * @param token
     */
    public static void setAuthorization(final Context context, final String token) {
        SharedPreferences sharedPreferences = getSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(Utils.TAG_AUTHORIZATION, token);
        editor.commit();
    }

    /**
     * 移除憑證
     *
     * @param context
     */
    public static void removeAuthorization(final Context context) {
        SharedPreferences sharedPreferences = getSharedPreferences(context);
        if (sharedPreferences.contains(Utils.TAG_AUTHORIZATION)) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.remove(Utils.TAG_AUTHORIZATION);
            editor.commit();
        }
    }
}
