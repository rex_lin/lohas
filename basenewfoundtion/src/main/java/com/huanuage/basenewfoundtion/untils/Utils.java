package com.huanuage.basenewfoundtion.untils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.util.Log;

import com.huanuage.HttpClient.utils.APICallbacks;
import com.huanuage.basenewfoundtion.LoginActivity;
import com.huanuage.basenewfoundtion.apiclient.APIClientToBackend;
import com.huanuage.basenewfoundtion.model.BaseBackendResponse;
import com.huanuage.basenewfoundtion.model.OwnPoint;

import java.util.HashMap;

/**
 * Created by kai_tseng on 2015/10/20.
 */
public class Utils {
    // tag
    public static final String TAG_PREFERENCE = "BaseNewFoundtion";
    public static final String TAG_ACCESS_TOKEN = "access_token";
    public static final String TAG_AUTHORIZATION = "Authorization";
    public static final String TAG_EXPIRES_IN = "expires_in";

    public final static String TAG_GROUP_BINDING_EXTRA = "LoginSuccess";

    public final static String TAG_ACCOUNT = "LoginAccount";
    public final static String TAG_REMEMBERME = "LoginRemember";

    // task index
    /**
     * 登入
     */
    public static final int TASK_POST_LOGIN = 0;
    /**
     * 註冊
     */
    public static final int TASK_POST_REGISTER = 1;
    /**
     * 帳號是否已註冊
     */
    public static final int TASK_POST_ACCOUNT_IS_REGISTERED = 2;
    /**
     * 寄出忘記密碼信件
     */
    public static final int TASK_POST_FORGET_USER_PASSWORD = 3;
    /**
     * 修改密碼
     */
    public static final int TASK_POST_CHANGE_PASSWORD = 4;
    /**
     * 登入Code取得Token
     */
    public static final int TASK_GET_CHECK_TICKET = 5;
    /**
     * 群組人員綁定
     */
    public static final int TASK_PUT_GROUP_MEMBER_VERIFICATION = 6;
    /**
     * 取得使用者資訊
     */
    public static final int TASK_GET_USER_INFO = 7;
    /**
     * 交易紀錄清單查詢
     */
    public static final int TASK_GET_REWARD_POINTS = 8;
    /**
     * 查詢擁有點數
     */
    public static final int TASK_GET_OWN_POINTS = 9;
    /**
     * 取得使用者資訊
     */
    public static final int TASK_GET_ACCOUNT_PROFILE = 10;
    /**
     * 更新使用者資訊
     */
    public static final int TASK_PUT_ACCOUNT_PROFILE = 11;
    /**
     * 更新使用者IM照片
     */
    public static final int TASK_PUT_ACCOUNT_PROFILE_IMIMAGE = 12;
    /**
     * 取得國家列表
     */
    public static final int TASK_GET_LIST_COUNTRY = 13;
    /**
     * 國家取得縣市列表
     */
    public static final int TASK_GET_LIST_COUNTY_BY_COUNTRY = 14;
    /**
     * 縣市取得鄉鎮列表
     */
    public static final int TASK_GET_LIST_CITYAREA_BY_COUNTY = 15;
    /**
     * 國家_縣市_鄉鎮 For ios_android
     */
    public static final int TASK_GET_ADDRESS_LIST_ITEM = 16;


    /**
     * 登入成功後，轉跳頁面用
     */
    private static Handler mLoginSuccessHandler;

    /**
     * 設定 登入成功執行事件
     *
     * @param handler
     */
    public static void setLoginSuccessCallbackHandler(Handler handler) {
        mLoginSuccessHandler = handler;
    }

    /**
     * 設定 取得登入成功執行事件
     *
     * @return
     */
    public static Handler getLoginSuccessCallbackHandler() {
        return mLoginSuccessHandler;
    }

    /**
     * 登出
     *
     * @param activity
     */
    public static void Logout(final Activity activity) {
        SharedPreferencesHelper.removeAuthorization(activity);
        if ((activity instanceof LoginActivity) == false) {
            Intent intent = new Intent();
            intent.setClass(activity, LoginActivity.class);
            activity.startActivity(intent);
            activity.finish();
        }
    }

    /**
     * 取得會員可用點數
     * Message.what(結果狀態碼)：200成功/400失敗/408網路異常
     * Message.arg1(可用點數)
     *
     * @param context
     * @param handler
     */
    public static void getAvailablePoints(@NonNull final Context context, @NonNull final Handler handler) {
        APICallbacks mAPICallback = new APICallbacks() {
            @Override
            public void onSuccess(int task, int statusCode, HashMap<String, String> headers, Object data) {
                BaseBackendResponse<OwnPoint> baseBackendResponse = (BaseBackendResponse<OwnPoint>) data;
                Message message = handler.obtainMessage(200);
                message.arg1 = baseBackendResponse.Data.AvailablePoint;
                handler.sendMessage(message);
            }

            @Override
            public void onFailure(int task, int statusCode, HashMap<String, String> headers, String responseString, Throwable throwable) {
                Log.d("getAvailablePoints", "onFailure", throwable);
                Message message = handler.obtainMessage(400);
                message.arg1 = 0;
                handler.sendMessage(message);
            }

            @Override
            public void onNetworkFailure(int task, int statusCode, Throwable throwable) {
                Log.d("getAvailablePoints", "onNetworkFailure", throwable);
                Message message = handler.obtainMessage(408);
                message.arg1 = 0;
                handler.sendMessage(message);
            }
        };
        APIClientToBackend.getInstance().getOwnPoint(context, mAPICallback);
    }
}
