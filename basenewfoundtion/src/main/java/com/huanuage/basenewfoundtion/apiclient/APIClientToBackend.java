package com.huanuage.basenewfoundtion.apiclient;

import android.content.Context;
import android.net.Uri;
import android.net.Uri.Builder;

import com.google.gson.reflect.TypeToken;
import com.huanuage.HttpClient.HttpClient;
import com.huanuage.HttpClient.handler.ObjectHttpResponseHandler;
import com.huanuage.HttpClient.handler.TextHttpResponseHandler;
import com.huanuage.HttpClient.utils.APICallbacks;
import com.huanuage.HttpClient.utils.ApiVerificationHelper;
import com.huanuage.HttpClient.utils.CookieManagerHelper;
import com.huanuage.HttpClient.utils.CustomHttpStatusCode;
import com.huanuage.baselibrary.utils.GsonHelper;
import com.huanuage.HttpClient.utils.HttpResponse;
import com.huanuage.HttpClient.utils.HttpResponseException;
import com.huanuage.baselibrary.utils.StringHelper;

import com.huanuage.basenewfoundtion.model.*;
import com.huanuage.basenewfoundtion.untils.*;

import java.io.File;
import java.lang.reflect.Type;
import java.net.HttpCookie;
import java.net.URI;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

public class APIClientToBackend extends HttpClient {
    /**
     * Server路徑
     */
    private static URI baseURI = URI
            .create("http://hnfoundationtst.cloudapp.net:8888");
    private static Uri baseUri = Uri.parse(baseURI.toString());

    private APIClientToBackend() {
        setTimeOut(20 * 1000);
    }

    /**
     * 靜態APIClient
     */
    private static APIClientToBackend apiClient;

    /**
     * APIClient 實作
     *
     * @return APIClient
     */
    public static APIClientToBackend getInstance() {
        if (apiClient == null) {
            apiClient = new APIClientToBackend();
        }
        return apiClient;
    }

    /**
     * 設定API Server路徑
     *
     * @param uri
     */
    public static void setBaseUri(Uri uri) {
        baseUri = uri;
        baseURI = URI.create(uri.toString());
    }

    /**
     * 取得Headers:Token、CompanyId、EmployeeId
     *
     * @param context
     * @return Header[]
     */
    protected static HashMap<String, String> getHeaders(final Context context) {
        HashMap<String, String> headers = new HashMap<String, String>();
        // 設定Header
        headers.put(Utils.TAG_AUTHORIZATION, SharedPreferencesHelper.getAuthorization(context));
        return headers;
    }

    private static String setUrl(String method, String url) {
        return setUrl(method, url, null);
    }

    private static String setUrl(String method, String url,
                                 Map<String, String> params) {
        String strApiHash = "";
        long excuteTime = new Date().getTime();
        excuteTime = excuteTime / 1000;
        try {
            strApiHash = ApiVerificationHelper.ApiVerificationHash("POST", "/"
                    + url, excuteTime);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        Builder builder = baseUri.buildUpon().appendEncodedPath(url)
                .appendQueryParameter("time", String.valueOf(excuteTime))
                .appendQueryParameter("hash", strApiHash);
        if (params != null) {
            for (Entry<String, String> param : params.entrySet()) {
                builder = builder.appendQueryParameter(param.getKey(),
                        param.getValue());
            }
        }
        return builder.toString();
    }

    public static List<HttpCookie> getCookies() {
        return CookieManagerHelper.getInstance().getCookieStore().get(baseURI);
    }

    /**
     * 登入Code取得Token
     *
     * @param context
     * @param loginCode 登入Code
     * @param callback
     */
    public void getCheckTicket(final Context context, final String loginCode,
                               final APICallbacks callback) {
        CookieManagerHelper.getInstance().getCookieStore().removeAll();
        get(context, callback,
                baseUri.buildUpon().appendEncodedPath("api/auth/CheckTicket")
                        .appendQueryParameter("code", loginCode).toString(),
                new TextHttpResponseHandler() {

                    @Override
                    public void onSuccess(int statusCode,
                                          HashMap<String, String> headers,
                                          String responseString) {
                        boolean hasModuleSessionCookie = false;
                        String token = "";
                        for (HttpCookie httpCookie : getCookies()) {
                            if (httpCookie.getName().equalsIgnoreCase(
                                    "__ModuleSessionCookie")
                                    && StringHelper.IsNullOrEmpty(httpCookie
                                    .getValue()) == false) {
                                hasModuleSessionCookie = true;
                                token = httpCookie.getValue();
                                break;
                            }
                        }
                        if (hasModuleSessionCookie == false) {
                            SharedPreferencesHelper.removeAuthorization(context);
                            if (callback != null) {
                                callback.onFailure(
                                        Utils.TASK_GET_CHECK_TICKET,
                                        CustomHttpStatusCode.CHECK_TICKET_NO_MODULE_SESSION_COOKIE,
                                        headers,
                                        responseString,
                                        new HttpResponseException(
                                                CustomHttpStatusCode.CHECK_TICKET_NO_MODULE_SESSION_COOKIE,
                                                "未取得Token"));
                            }
                        } else {
                            SharedPreferencesHelper.setAuthorization(context, token);
                            if (callback != null) {
                                callback.onSuccess(Utils.TASK_GET_CHECK_TICKET,
                                        statusCode, headers, responseString);
                            }
                        }
                    }

                    @Override
                    public void onFailure(int statusCode,
                                          HashMap<String, String> headers,
                                          String responseString, Throwable throwable) {
                        if (callback != null) {
                            callback.onFailure(Utils.TASK_GET_CHECK_TICKET,
                                    statusCode, headers, responseString,
                                    throwable);
                        }
                    }

                    @Override
                    public void onNetworkFailure(int statusCode,
                                                 Throwable throwable) {
                        if (callback != null) {
                            callback.onNetworkFailure(
                                    Utils.TASK_GET_CHECK_TICKET, statusCode,
                                    throwable);
                        }
                    }
                });
    }

    /**
     * 群組人員綁定
     *
     * @param context
     * @param groupMemberVerification 綁定資料
     * @param callback
     */
    public void putGroupMemberVerification(final Context context,
                                           final GroupMemberVerification groupMemberVerification,
                                           final APICallbacks callback) {
        Type baseBackendResponseType = new TypeToken<BaseBackendResponse<String>>() {
        }.getType();
        put(context,
                callback,
                baseUri.buildUpon()
                        .appendEncodedPath("api/GroupMember/Verification")
                        .toString(), getHeaders(context),
                GsonHelper.SerializeObject(groupMemberVerification),
                jsonContentType, new ObjectHttpResponseHandler<BaseBackendResponse<String>>(baseBackendResponseType) {
                    @Override
                    public void onSuccess(int statusCode, HashMap<String, String> headers, BaseBackendResponse<String> responseObject) {
                        if (callback != null) {
                            if (HttpResponse
                                    .IsSuccessStatusCode(responseObject.Meta.HttpStatusCode)
                                    && responseObject.Error == null) {
                                callback.onSuccess(Utils.TASK_PUT_GROUP_MEMBER_VERIFICATION,
                                        responseObject.Meta.HttpStatusCode,
                                        headers, responseObject.Data);
                            } else {
                                callback.onFailure(Utils.TASK_PUT_GROUP_MEMBER_VERIFICATION, responseObject.Meta.HttpStatusCode, headers, GsonHelper.SerializeObject(responseObject), new HttpResponseException(responseObject.Meta.HttpStatusCode, GsonHelper.SerializeObject(responseObject.Error)));
                            }
                        }
                    }

                    @Override
                    public void onFailure(int statusCode,
                                          HashMap<String, String> headers,
                                          String responseString, Throwable throwable) {
                        if (callback != null) {
                            callback.onFailure(
                                    Utils.TASK_PUT_GROUP_MEMBER_VERIFICATION,
                                    statusCode, headers, responseString,
                                    throwable);
                        }
                    }

                    @Override
                    public void onNetworkFailure(int statusCode,
                                                 Throwable throwable) {
                        if (callback != null) {
                            callback.onNetworkFailure(
                                    Utils.TASK_PUT_GROUP_MEMBER_VERIFICATION,
                                    statusCode, throwable);
                        }
                    }
                });
    }

    /**
     * 取得使用者資訊
     *
     * @param context
     * @param callback
     */
    public void getUserInfo(final Context context, final APICallbacks callback) {
        Type baseBackendResponseType = new TypeToken<BaseBackendResponse<UserInfo>>() {
        }.getType();
        get(context,
                callback,
                baseUri.buildUpon().appendEncodedPath("api/Session").toString(),
                getHeaders(context),
                new ObjectHttpResponseHandler<BaseBackendResponse<UserInfo>>(
                        baseBackendResponseType) {
                    @Override
                    public void onFailure(int statusCode,
                                          HashMap<String, String> headers,
                                          String responseString, Throwable throwable) {
                        if (callback != null) {
                            callback.onFailure(Utils.TASK_GET_USER_INFO,
                                    statusCode, headers, responseString,
                                    throwable);
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode,
                                          HashMap<String, String> headers,
                                          BaseBackendResponse<UserInfo> responseObject) {
                        if (callback != null) {
                            if (HttpResponse
                                    .IsSuccessStatusCode(responseObject.Meta.HttpStatusCode)
                                    && responseObject.Error == null) {
                                callback.onSuccess(Utils.TASK_GET_USER_INFO,
                                        responseObject.Meta.HttpStatusCode,
                                        headers, responseObject.Data);
                            } else {
                                callback.onFailure(Utils.TASK_GET_USER_INFO, responseObject.Meta.HttpStatusCode, headers, GsonHelper.SerializeObject(responseObject), new HttpResponseException(responseObject.Meta.HttpStatusCode, GsonHelper.SerializeObject(responseObject.Error)));
                            }
                        }
                    }

                    @Override
                    public void onNetworkFailure(int statusCode,
                                                 Throwable throwable) {
                        if (callback != null) {
                            callback.onNetworkFailure(Utils.TASK_GET_USER_INFO,
                                    statusCode, throwable);
                        }
                    }
                });
    }

    /**
     * 交易紀錄清單查詢
     *
     * @param context
     * @param pagenumber 目前頁數
     * @param pagesize   一頁顯示筆數
     * @param range      日期區間(1:前三個月,2:前六個月,3:當年度,4:前一年,5:前二年)
     * @param callback
     */
    public void GetRewardPoints(final Context context, final int pagenumber, final int pagesize, final int range, final APICallbacks callback) {
        Type baseBackendResponseType = new TypeToken<BaseBackendResponse<List<RewardPoints>>>() {
        }.getType();
        get(context, callback, baseUri.buildUpon().appendEncodedPath("api/RewardPoints").appendQueryParameter("PageNumber", String.valueOf(pagenumber)).appendQueryParameter("PageSize", String.valueOf(pagesize)).appendQueryParameter("range", String.valueOf(range)).toString(), getHeaders(context), new ObjectHttpResponseHandler<BaseBackendResponse<List<RewardPoints>>>(
                        baseBackendResponseType) {
                    @Override
                    public void onFailure(int statusCode,
                                          HashMap<String, String> headers,
                                          String responseString, Throwable throwable) {
                        if (callback != null) {
                            callback.onFailure(Utils.TASK_GET_REWARD_POINTS,
                                    statusCode, headers, responseString,
                                    throwable);
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode,
                                          HashMap<String, String> headers,
                                          BaseBackendResponse<List<RewardPoints>> responseObject) {
                        if (callback != null) {
                            if (HttpResponse
                                    .IsSuccessStatusCode(responseObject.Meta.HttpStatusCode)
                                    && responseObject.Error == null) {
                                callback.onSuccess(Utils.TASK_GET_REWARD_POINTS,
                                        responseObject.Meta.HttpStatusCode,
                                        headers, responseObject);
                            } else {
                                callback.onFailure(Utils.TASK_GET_REWARD_POINTS, responseObject.Meta.HttpStatusCode, headers, GsonHelper.SerializeObject(responseObject), new HttpResponseException(responseObject.Meta.HttpStatusCode, GsonHelper.SerializeObject(responseObject.Error)));
                            }
                        }
                    }

                    @Override
                    public void onNetworkFailure(int statusCode,
                                                 Throwable throwable) {
                        if (callback != null) {
                            callback.onNetworkFailure(Utils.TASK_GET_REWARD_POINTS,
                                    statusCode, throwable);
                        }
                    }
                }
        );
    }

    /**
     * 查詢擁有點數
     *
     * @param context
     * @param callback
     */
    public void getOwnPoint(final Context context, final APICallbacks callback) {
        Type baseBackendResponseType = new TypeToken<BaseBackendResponse<OwnPoint>>() {
        }.getType();
        get(context, callback, baseUri.buildUpon().appendEncodedPath("api/RewardPoints/OwnPoint").toString(), getHeaders(context), new ObjectHttpResponseHandler<BaseBackendResponse<OwnPoint>>(
                        baseBackendResponseType) {
                    @Override
                    public void onFailure(int statusCode,
                                          HashMap<String, String> headers,
                                          String responseString, Throwable throwable) {
                        if (callback != null) {
                            callback.onFailure(Utils.TASK_GET_OWN_POINTS,
                                    statusCode, headers, responseString,
                                    throwable);
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode,
                                          HashMap<String, String> headers,
                                          BaseBackendResponse<OwnPoint> responseObject) {
                        if (callback != null) {
                            if (HttpResponse
                                    .IsSuccessStatusCode(responseObject.Meta.HttpStatusCode)
                                    && responseObject.Error == null) {
                                callback.onSuccess(Utils.TASK_GET_OWN_POINTS,
                                        responseObject.Meta.HttpStatusCode,
                                        headers, responseObject);
                            } else {
                                callback.onFailure(Utils.TASK_GET_OWN_POINTS, responseObject.Meta.HttpStatusCode, headers, GsonHelper.SerializeObject(responseObject), new HttpResponseException(responseObject.Meta.HttpStatusCode, GsonHelper.SerializeObject(responseObject.Error)));
                            }
                        }
                    }

                    @Override
                    public void onNetworkFailure(int statusCode,
                                                 Throwable throwable) {
                        if (callback != null) {
                            callback.onNetworkFailure(Utils.TASK_GET_OWN_POINTS,
                                    statusCode, throwable);
                        }
                    }
                }
        );
    }

    /**
     * 取得使用者資訊
     *
     * @param context
     * @param callback
     */
    public void getAccountProfile(final Context context, final APICallbacks callback) {
        Type baseBackendResponseType = new TypeToken<BaseBackendResponse<AccountProfile>>() {
        }.getType();
        get(context, callback, baseUri.buildUpon().appendEncodedPath("api/AccountProfile/Get").toString(), getHeaders(context), new ObjectHttpResponseHandler<BaseBackendResponse<AccountProfile>>(
                        baseBackendResponseType) {
                    @Override
                    public void onFailure(int statusCode,
                                          HashMap<String, String> headers,
                                          String responseString, Throwable throwable) {
                        if (callback != null) {
                            callback.onFailure(Utils.TASK_GET_ACCOUNT_PROFILE,
                                    statusCode, headers, responseString,
                                    throwable);
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode,
                                          HashMap<String, String> headers,
                                          BaseBackendResponse<AccountProfile> responseObject) {
                        if (callback != null) {
                            if (HttpResponse
                                    .IsSuccessStatusCode(responseObject.Meta.HttpStatusCode)
                                    && responseObject.Error == null) {
                                callback.onSuccess(Utils.TASK_GET_ACCOUNT_PROFILE,
                                        responseObject.Meta.HttpStatusCode,
                                        headers, responseObject);
                            } else {
                                callback.onFailure(Utils.TASK_GET_ACCOUNT_PROFILE, responseObject.Meta.HttpStatusCode, headers, GsonHelper.SerializeObject(responseObject), new HttpResponseException(responseObject.Meta.HttpStatusCode, GsonHelper.SerializeObject(responseObject.Error)));
                            }
                        }
                    }

                    @Override
                    public void onNetworkFailure(int statusCode,
                                                 Throwable throwable) {
                        if (callback != null) {
                            callback.onNetworkFailure(Utils.TASK_GET_ACCOUNT_PROFILE,
                                    statusCode, throwable);
                        }
                    }
                }
        );
    }

    /**
     * 更新使用者資訊
     *
     * @param context
     * @param accountProfile 使用者資訊
     * @param callback
     */
    public void putAccountProfile(final Context context, final AccountProfile accountProfile, final APICallbacks callback) {
        Type baseBackendResponseType = new TypeToken<BaseBackendResponse<AccountProfile>>() {
        }.getType();
        put(context, callback, baseUri.buildUpon().appendEncodedPath("api/AccountProfile/Put").toString(), getHeaders(context), GsonHelper.SerializeObject(accountProfile), jsonContentType, new ObjectHttpResponseHandler<BaseBackendResponse<AccountProfile>>(
                        baseBackendResponseType) {
                    @Override
                    public void onFailure(int statusCode,
                                          HashMap<String, String> headers,
                                          String responseString, Throwable throwable) {
                        if (callback != null) {
                            callback.onFailure(Utils.TASK_PUT_ACCOUNT_PROFILE,
                                    statusCode, headers, responseString,
                                    throwable);
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode,
                                          HashMap<String, String> headers,
                                          BaseBackendResponse<AccountProfile> responseObject) {
                        if (callback != null) {
                            if (HttpResponse
                                    .IsSuccessStatusCode(responseObject.Meta.HttpStatusCode)
                                    && responseObject.Error == null) {
                                callback.onSuccess(Utils.TASK_PUT_ACCOUNT_PROFILE,
                                        responseObject.Meta.HttpStatusCode,
                                        headers, responseObject);
                            } else {
                                callback.onFailure(Utils.TASK_PUT_ACCOUNT_PROFILE, responseObject.Meta.HttpStatusCode, headers, GsonHelper.SerializeObject(responseObject), new HttpResponseException(responseObject.Meta.HttpStatusCode, GsonHelper.SerializeObject(responseObject.Error)));
                            }
                        }
                    }

                    @Override
                    public void onNetworkFailure(int statusCode,
                                                 Throwable throwable) {
                        if (callback != null) {
                            callback.onNetworkFailure(Utils.TASK_PUT_ACCOUNT_PROFILE,
                                    statusCode, throwable);
                        }
                    }
                }
        );
    }

    /**
     * 更新使用者 IM照片
     *
     * @param context
     * @param imageFile Im照片
     * @param callback
     */
    public void putAccountProfileImImage(final Context context, final File imageFile, final APICallbacks callback) {
        Type baseBackendResponseType = new TypeToken<BaseBackendResponse<AccountProfile>>() {
        }.getType();
        putFile(context, callback, baseUri.buildUpon().appendEncodedPath("api/AccountProfile/ImImage").toString(), getHeaders(context), "Attach", imageFile, new ObjectHttpResponseHandler<BaseBackendResponse<AccountProfile>>(
                        baseBackendResponseType) {
                    @Override
                    public void onFailure(int statusCode,
                                          HashMap<String, String> headers,
                                          String responseString, Throwable throwable) {
                        if (callback != null) {
                            callback.onFailure(Utils.TASK_PUT_ACCOUNT_PROFILE_IMIMAGE,
                                    statusCode, headers, responseString,
                                    throwable);
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode,
                                          HashMap<String, String> headers,
                                          BaseBackendResponse<AccountProfile> responseObject) {
                        if (callback != null) {
                            if (HttpResponse
                                    .IsSuccessStatusCode(responseObject.Meta.HttpStatusCode)
                                    && responseObject.Error == null) {
                                callback.onSuccess(Utils.TASK_PUT_ACCOUNT_PROFILE_IMIMAGE,
                                        responseObject.Meta.HttpStatusCode,
                                        headers, responseObject);
                            } else {
                                callback.onFailure(Utils.TASK_PUT_ACCOUNT_PROFILE_IMIMAGE, responseObject.Meta.HttpStatusCode, headers, GsonHelper.SerializeObject(responseObject), new HttpResponseException(responseObject.Meta.HttpStatusCode, GsonHelper.SerializeObject(responseObject.Error)));
                            }
                        }
                    }

                    @Override
                    public void onNetworkFailure(int statusCode,
                                                 Throwable throwable) {
                        if (callback != null) {
                            callback.onNetworkFailure(Utils.TASK_PUT_ACCOUNT_PROFILE_IMIMAGE,
                                    statusCode, throwable);
                        }
                    }
                }
        );
    }

    /**
     * 取得國家列表
     *
     * @param context
     * @param callback
     */
    public void getListCountry(final Context context, final APICallbacks callback) {
        Type baseBackendResponseType = new TypeToken<BaseBackendResponse<List<Country>>>() {
        }.getType();
        get(context, callback, baseUri.buildUpon().appendEncodedPath("api/Country").toString(), getHeaders(context), new ObjectHttpResponseHandler<BaseBackendResponse<List<Country>>>(
                        baseBackendResponseType) {
                    @Override
                    public void onFailure(int statusCode,
                                          HashMap<String, String> headers,
                                          String responseString, Throwable throwable) {
                        if (callback != null) {
                            callback.onFailure(Utils.TASK_GET_LIST_COUNTRY,
                                    statusCode, headers, responseString,
                                    throwable);
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode,
                                          HashMap<String, String> headers,
                                          BaseBackendResponse<List<Country>> responseObject) {
                        if (callback != null) {
                            if (HttpResponse
                                    .IsSuccessStatusCode(responseObject.Meta.HttpStatusCode)
                                    && responseObject.Error == null) {
                                callback.onSuccess(Utils.TASK_GET_LIST_COUNTRY,
                                        responseObject.Meta.HttpStatusCode,
                                        headers, responseObject);
                            } else {
                                callback.onFailure(Utils.TASK_GET_LIST_COUNTRY, responseObject.Meta.HttpStatusCode, headers, GsonHelper.SerializeObject(responseObject), new HttpResponseException(responseObject.Meta.HttpStatusCode, GsonHelper.SerializeObject(responseObject.Error)));
                            }
                        }
                    }

                    @Override
                    public void onNetworkFailure(int statusCode,
                                                 Throwable throwable) {
                        if (callback != null) {
                            callback.onNetworkFailure(Utils.TASK_GET_LIST_COUNTRY,
                                    statusCode, throwable);
                        }
                    }
                }
        );
    }

    /**
     * 國家取得縣市列表
     *
     * @param context
     * @param CountrySerialNo 國家流水號
     * @param callback
     */
    public void getListCounty(final Context context, final UUID CountrySerialNo, final APICallbacks callback) {
        Type baseBackendResponseType = new TypeToken<BaseBackendResponse<List<County>>>() {
        }.getType();
        get(context, callback, baseUri.buildUpon().appendEncodedPath("api/County").appendQueryParameter("CountrySerialNo", CountrySerialNo.toString()).toString(), getHeaders(context), new ObjectHttpResponseHandler<BaseBackendResponse<List<County>>>(
                        baseBackendResponseType) {
                    @Override
                    public void onFailure(int statusCode,
                                          HashMap<String, String> headers,
                                          String responseString, Throwable throwable) {
                        if (callback != null) {
                            callback.onFailure(Utils.TASK_GET_LIST_COUNTY_BY_COUNTRY,
                                    statusCode, headers, responseString,
                                    throwable);
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode,
                                          HashMap<String, String> headers,
                                          BaseBackendResponse<List<County>> responseObject) {
                        if (callback != null) {
                            if (HttpResponse
                                    .IsSuccessStatusCode(responseObject.Meta.HttpStatusCode)
                                    && responseObject.Error == null) {
                                callback.onSuccess(Utils.TASK_GET_LIST_COUNTY_BY_COUNTRY,
                                        responseObject.Meta.HttpStatusCode,
                                        headers, responseObject);
                            } else {
                                callback.onFailure(Utils.TASK_GET_LIST_COUNTY_BY_COUNTRY, responseObject.Meta.HttpStatusCode, headers, GsonHelper.SerializeObject(responseObject), new HttpResponseException(responseObject.Meta.HttpStatusCode, GsonHelper.SerializeObject(responseObject.Error)));
                            }
                        }
                    }

                    @Override
                    public void onNetworkFailure(int statusCode,
                                                 Throwable throwable) {
                        if (callback != null) {
                            callback.onNetworkFailure(Utils.TASK_GET_LIST_COUNTY_BY_COUNTRY,
                                    statusCode, throwable);
                        }
                    }
                }
        );
    }

    /**
     * 縣市取得鄉鎮列表
     *
     * @param context
     * @param CountySerialNo 縣市流水號
     * @param callback
     */
    public void getListCityArea(final Context context, final UUID CountySerialNo, final APICallbacks callback) {
        Type baseBackendResponseType = new TypeToken<BaseBackendResponse<List<CityArea>>>() {
        }.getType();
        get(context, callback, baseUri.buildUpon().appendEncodedPath("api/CityArea").appendQueryParameter("CountySerialNo", CountySerialNo.toString()).toString(), getHeaders(context), new ObjectHttpResponseHandler<BaseBackendResponse<List<CityArea>>>(
                        baseBackendResponseType) {
                    @Override
                    public void onFailure(int statusCode,
                                          HashMap<String, String> headers,
                                          String responseString, Throwable throwable) {
                        if (callback != null) {
                            callback.onFailure(Utils.TASK_GET_LIST_CITYAREA_BY_COUNTY,
                                    statusCode, headers, responseString,
                                    throwable);
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode,
                                          HashMap<String, String> headers,
                                          BaseBackendResponse<List<CityArea>> responseObject) {
                        if (callback != null) {
                            if (HttpResponse
                                    .IsSuccessStatusCode(responseObject.Meta.HttpStatusCode)
                                    && responseObject.Error == null) {
                                callback.onSuccess(Utils.TASK_GET_LIST_CITYAREA_BY_COUNTY,
                                        responseObject.Meta.HttpStatusCode,
                                        headers, responseObject);
                            } else {
                                callback.onFailure(Utils.TASK_GET_LIST_CITYAREA_BY_COUNTY, responseObject.Meta.HttpStatusCode, headers, GsonHelper.SerializeObject(responseObject), new HttpResponseException(responseObject.Meta.HttpStatusCode, GsonHelper.SerializeObject(responseObject.Error)));
                            }
                        }
                    }

                    @Override
                    public void onNetworkFailure(int statusCode,
                                                 Throwable throwable) {
                        if (callback != null) {
                            callback.onNetworkFailure(Utils.TASK_GET_LIST_CITYAREA_BY_COUNTY,
                                    statusCode, throwable);
                        }
                    }
                }
        );
    }

    /**
     * 國家_縣市_鄉鎮 For ios_android
     *
     * @param context
     * @param callback
     */
    public void getAddressListItem(final Context context, final APICallbacks callback) {
        Type baseBackendResponseType = new TypeToken<BaseBackendResponse<AddressListItem>>() {
        }.getType();
        get(context, callback, baseUri.buildUpon().appendEncodedPath("api/Address/All").toString(), getHeaders(context), new ObjectHttpResponseHandler<BaseBackendResponse<AddressListItem>>(
                        baseBackendResponseType) {
                    @Override
                    public void onFailure(int statusCode,
                                          HashMap<String, String> headers,
                                          String responseString, Throwable throwable) {
                        if (callback != null) {
                            callback.onFailure(Utils.TASK_GET_ADDRESS_LIST_ITEM,
                                    statusCode, headers, responseString,
                                    throwable);
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode,
                                          HashMap<String, String> headers,
                                          BaseBackendResponse<AddressListItem> responseObject) {
                        if (callback != null) {
                            if (HttpResponse
                                    .IsSuccessStatusCode(responseObject.Meta.HttpStatusCode)
                                    && responseObject.Error == null) {
                                callback.onSuccess(Utils.TASK_GET_ADDRESS_LIST_ITEM,
                                        responseObject.Meta.HttpStatusCode,
                                        headers, responseObject);
                            } else {
                                callback.onFailure(Utils.TASK_GET_ADDRESS_LIST_ITEM, responseObject.Meta.HttpStatusCode, headers, GsonHelper.SerializeObject(responseObject), new HttpResponseException(responseObject.Meta.HttpStatusCode, GsonHelper.SerializeObject(responseObject.Error)));
                            }
                        }
                    }

                    @Override
                    public void onNetworkFailure(int statusCode,
                                                 Throwable throwable) {
                        if (callback != null) {
                            callback.onNetworkFailure(Utils.TASK_GET_ADDRESS_LIST_ITEM,
                                    statusCode, throwable);
                        }
                    }
                }
        );
    }

    @Deprecated
    public void getUserId(final Context context, final APICallbacks callback) {
        get(context, callback,
                baseUri.buildUpon().appendEncodedPath("api/Session/UserId")
                        .toString(), getHeaders(context),
                new TextHttpResponseHandler() {

                    @Override
                    public void onNetworkFailure(int statusCode,
                                                 Throwable throwable) {
                        // TODO 自動產生的方法 Stub

                    }

                    @Override
                    public void onSuccess(int statusCode,
                                          HashMap<String, String> headers,
                                          String responseString) {
                        // TODO 自動產生的方法 Stub

                    }

                    @Override
                    public void onFailure(int statusCode,
                                          HashMap<String, String> headers,
                                          String responseString, Throwable throwable) {
                        // TODO 自動產生的方法 Stub

                    }
                });
    }
}
