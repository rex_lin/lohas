package com.huanuage.basenewfoundtion.apiclient;

import android.content.Context;
import android.net.Uri;
import android.net.Uri.Builder;

import com.google.gson.reflect.TypeToken;
import com.huanuage.HttpClient.HttpClient;
import com.huanuage.HttpClient.handler.ObjectHttpResponseHandler;
import com.huanuage.HttpClient.handler.TextHttpResponseHandler;
import com.huanuage.HttpClient.utils.APICallbacks;
import com.huanuage.HttpClient.utils.ApiVerificationHelper;
import com.huanuage.HttpClient.utils.HttpResponse;
import com.huanuage.HttpClient.utils.HttpResponseException;
import com.huanuage.baselibrary.utils.GsonHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.huanuage.basenewfoundtion.model.*;
import com.huanuage.basenewfoundtion.untils.*;

public class APIClientToAuth extends HttpClient {
    /**
     * Server路徑
     */
    private static Uri baseUri = Uri
            .parse("http://hnfoundationtst.cloudapp.net:9037");

    private APIClientToAuth() {
        setTimeOut(20 * 1000);
    }

    /**
     * 靜態APIClient
     */
    private static APIClientToAuth apiClient;

    /**
     * APIClient 實作
     *
     * @return APIClient
     */
    public static APIClientToAuth getInstance() {
        if (apiClient == null) {
            apiClient = new APIClientToAuth();
        }
        return apiClient;
    }

    /**
     * 設定API Server路徑
     *
     * @param uri
     */
    public static void setBaseUri(Uri uri) {
        baseUri = uri;
    }

    /**
     * 取得Headers:Token、CompanyId、EmployeeId
     *
     * @param context
     * @return Header[]
     */
    protected static HashMap<String, String> getHeaders(final Context context) {
        // // 當記憶體被釋放時，從儲存空間抓取Token、CompanyId、EmployeeId
        // SharedPreferences pref = context.getSharedPreferences(
        // Utils.TAG_PREFERENCE, Context.MODE_PRIVATE);
        // if (StringHelper.IsNullOrWhiteSpace(mUserAccessToken) == true) {
        // mUserAccessToken = pref.getString(Utils.TAG_ACCESS_TOKEN, "");
        // }
        //
        HashMap<String, String> headers = new HashMap<String, String>();
        // // 設定Header
        // // List<Header> headers = new ArrayList<Header>();
        // if (StringHelper.IsNullOrWhiteSpace(mUserAccessToken) == false) {
        // headers.put(Utils.TAG_AUTHORIZATION, mUserAccessToken);
        // }
        return headers;
    }

    private static String setUrl(String method, String url) {
        return setUrl(method, url, null);
    }

    private static String setUrl(String method, String url,
                                 Map<String, String> params) {
        String strApiHash = "";
        long excuteTime = new Date().getTime();
        excuteTime = excuteTime / 1000;
        try {
            strApiHash = ApiVerificationHelper.ApiVerificationHash("POST", "/"
                    + url, excuteTime);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        Builder builder = baseUri.buildUpon().appendEncodedPath(url)
                .appendQueryParameter("time", String.valueOf(excuteTime))
                .appendQueryParameter("hash", strApiHash);
        if (params != null) {
            for (Entry<String, String> param : params.entrySet()) {
                builder = builder.appendQueryParameter(param.getKey(),
                        param.getValue());
            }
        }
        return builder.toString();
    }

    /**
     * 登入：成功回傳LoginToken物件
     *
     * @param context
     * @param login    帳號密碼物件
     * @param callback 執行結果
     * @throws Exception
     */
    public void postLogin(final Context context, final LoginAccount login,
                          final APICallbacks callback) {
        post(context,
                callback,
                setUrl("POST", "Token"),
                login.getFormData(),
                formDataContentType,
                new ObjectHttpResponseHandler<LoginSuccess>(LoginSuccess.class) {
                    @Override
                    public void onSuccess(int statusCode,
                                          HashMap<String, String> headers,
                                          LoginSuccess responseObject) {
                        if (callback != null) {
                            callback.onSuccess(Utils.TASK_POST_LOGIN,
                                    statusCode, headers, responseObject);
                        }
                    }

                    @Override
                    public void onFailure(int statusCode,
                                          HashMap<String, String> headers,
                                          String responseString, Throwable throwable) {
                        if (callback != null) {
                            callback.onFailure(Utils.TASK_POST_LOGIN,
                                    statusCode, headers, responseString,
                                    throwable);
                        }
                    }

                    @Override
                    public void onNetworkFailure(int statusCode,
                                                 Throwable throwable) {
                        if (callback != null) {
                            callback.onNetworkFailure(Utils.TASK_POST_LOGIN,
                                    statusCode, throwable);
                        }
                    }
                });
    }

    /**
     * 註冊
     *
     * @param context
     * @param register
     * @param callback
     */
    public void postRegister(final Context context, final Register register,
                             final APICallbacks callback) {
        post(context, callback, setUrl("POST", "api/AccountApi/Register"),
                GsonHelper.SerializeObject(register), jsonContentType,
                new TextHttpResponseHandler() {

                    @Override
                    public void onSuccess(int statusCode,
                                          HashMap<String, String> headers,
                                          String responseString) {
                        if (callback != null) {
                            callback.onSuccess(Utils.TASK_POST_REGISTER,
                                    statusCode, headers, responseString);
                        }
                    }

                    @Override
                    public void onFailure(int statusCode,
                                          HashMap<String, String> headers,
                                          String responseString, Throwable throwable) {
                        if (callback != null) {
                            callback.onFailure(Utils.TASK_POST_REGISTER,
                                    statusCode, headers, responseString,
                                    throwable);
                        }
                    }

                    @Override
                    public void onNetworkFailure(int statusCode,
                                                 Throwable throwable) {
                        if (callback != null) {
                            callback.onNetworkFailure(Utils.TASK_POST_REGISTER,
                                    statusCode, throwable);
                        }
                    }
                });
    }

    /**
     * 檢查帳號是否已存在
     *
     * @param context
     * @param email
     * @param callback
     */
    public void postAccountIsRegistered(final Context context,
                                        final String email, final APICallbacks callback) {
        Type baseBackendResponseType = new TypeToken<BaseBackendResponse<AccountIsRegistered>>() {
        }.getType();
        JSONObject obj = new JSONObject();
        try {
            obj.put("Email", email);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(context, callback,
                setUrl("POST", "api/AccountApi/AccountIsRegistered"),
                obj.toString(), jsonContentType,
                new ObjectHttpResponseHandler<BaseBackendResponse<AccountIsRegistered>>(baseBackendResponseType) {

                    @Override
                    public void onFailure(int statusCode,
                                          HashMap<String, String> headers,
                                          String responseString, Throwable throwable) {
                        if (callback != null) {
                            callback.onFailure(
                                    Utils.TASK_POST_ACCOUNT_IS_REGISTERED,
                                    statusCode, headers, responseString,
                                    throwable);
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode, HashMap<String, String> headers, BaseBackendResponse<AccountIsRegistered> responseObject) {
                        if (callback != null) {
                            if (HttpResponse
                                    .IsSuccessStatusCode(responseObject.Meta.HttpStatusCode)
                                    && responseObject.Error == null) {
                                callback.onSuccess(Utils.TASK_POST_ACCOUNT_IS_REGISTERED,
                                        responseObject.Meta.HttpStatusCode,
                                        headers, responseObject.Data);
                            } else {
                                callback.onFailure(Utils.TASK_POST_ACCOUNT_IS_REGISTERED, responseObject.Meta.HttpStatusCode, headers, GsonHelper.SerializeObject(responseObject), new HttpResponseException(responseObject.Meta.HttpStatusCode, GsonHelper.SerializeObject(responseObject.Error)));
                            }
                        }
                    }

                    @Override
                    public void onNetworkFailure(int statuscode,
                                                 Throwable throwable) {
                        if (callback != null) {
                            callback.onNetworkFailure(
                                    Utils.TASK_POST_ACCOUNT_IS_REGISTERED,
                                    statuscode, throwable);
                        }
                    }
                });
    }

    /**
     * 寄出忘記密碼信件
     *
     * @param context
     * @param forgetUserPassword
     * @param callback
     */
    public void postForgetUserPassword(final Context context,
                                       final ForgetUserPassword forgetUserPassword,
                                       final APICallbacks callback) {
        post(context, callback,
                setUrl("POST", "api/AccountApi/ForgetUserPassword"),
                GsonHelper.SerializeObject(forgetUserPassword),
                jsonContentType, new TextHttpResponseHandler() {

                    @Override
                    public void onFailure(int statusCode,
                                          HashMap<String, String> headers,
                                          String responseString, Throwable throwable) {
                        if (callback != null) {
                            callback.onFailure(
                                    Utils.TASK_POST_FORGET_USER_PASSWORD,
                                    statusCode, headers, responseString,
                                    throwable);
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode,
                                          HashMap<String, String> headers,
                                          String responseString) {
                        if (callback != null) {
                            callback.onSuccess(
                                    Utils.TASK_POST_FORGET_USER_PASSWORD,
                                    statusCode, headers, responseString);
                        }
                    }

                    @Override
                    public void onNetworkFailure(int statuscode,
                                                 Throwable throwable) {
                        if (callback != null) {
                            callback.onNetworkFailure(
                                    Utils.TASK_POST_FORGET_USER_PASSWORD,
                                    statuscode, throwable);
                        }
                    }
                });
    }

    /**
     * 修改密碼
     *
     * @param context
     * @param changePassword
     * @param callback
     */
    public void postChangePassword(final Context context,
                                   final ChangePassword changePassword, final APICallbacks callback) {
        post(context, callback,
                setUrl("POST", "api/AccountApi/ChangePassword"),
                GsonHelper.SerializeObject(changePassword), jsonContentType,
                new TextHttpResponseHandler() {

                    @Override
                    public void onFailure(int statusCode,
                                          HashMap<String, String> headers,
                                          String responseString, Throwable throwable) {
                        if (callback != null) {
                            callback.onFailure(Utils.TASK_POST_CHANGE_PASSWORD,
                                    statusCode, headers, responseString,
                                    throwable);
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode,
                                          HashMap<String, String> headers,
                                          String responseString) {
                        if (callback != null) {
                            callback.onSuccess(Utils.TASK_POST_CHANGE_PASSWORD,
                                    statusCode, headers, responseString);
                        }
                    }

                    @Override
                    public void onNetworkFailure(int statuscode,
                                                 Throwable throwable) {
                        if (callback != null) {
                            callback.onNetworkFailure(
                                    Utils.TASK_POST_CHANGE_PASSWORD,
                                    statuscode, throwable);
                        }
                    }
                });
    }
}
