package com.huanuage.baselibrary;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.math.MathContext;
import java.util.List;

public abstract class BaseListFragment extends Fragment implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener, SwipeRefreshLayout.OnRefreshListener, AbsListView.OnScrollListener {
    private ListView mListView;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_base_list, null);
        mSwipeRefreshLayout = (SwipeRefreshLayout) root.findViewById(R.id.laySwipe);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_red_light, android.R.color.holo_blue_light, android.R.color.holo_green_light, android.R.color.holo_orange_light);

        mListView = (ListView) root.findViewById(R.id.list_view);

        View headerView = getHeaderView();
        if (headerView != null)
            ((LinearLayout) root.findViewById(R.id.list_header)).addView(headerView);

        View footerView = getFooterView();
        if (footerView != null)
            ((LinearLayout) root.findViewById(R.id.list_footer)).addView(footerView);

        FrameLayout listEmptyView = (FrameLayout) root.findViewById(R.id.list_empty);
        mListView.setEmptyView(listEmptyView);
        View emptyView = getEmptyView();
        if (emptyView != null) {
            listEmptyView.removeAllViews();
            listEmptyView.addView(emptyView);
        }

        mListView.setAdapter(getAdapter());
        mListView.setOnItemClickListener(this);
        mListView.setOnItemLongClickListener(this);
        mListView.setOnScrollListener(this);
        return root;
    }

    protected void disableRefreshLayout() {
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    protected void enableRefreshLayout() {
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setRefreshing(true);
        }
    }

    /**
     * 設定List Adapter
     *
     * @return
     */
    public abstract CustomAdapter getAdapter();

    /**
     * 設定List Empty View
     *
     * @return
     */
    public View getEmptyView() {
        return null;
    }

    /**
     * 設定List Header View
     *
     * @return
     */
    public View getHeaderView() {
        return null;
    }

    /**
     * 設定List Footer View
     *
     * @return
     */
    public View getFooterView() {
        return null;
    }

    /**
     * 自訂List Adapter
     *
     * @param <T>
     */
    public abstract class CustomAdapter<T> extends BaseAdapter {
        protected Context mContext;
        protected List<T> mList;

        public CustomAdapter(Context context, List<T> list) {
            mContext = context;
            mList = list;
        }

        public void ChangeData(List<T> list) {
            mList = list;
            notifyDataSetChanged();
        }

        public void ChangeData() {
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return mList.size();
        }

        @Override
        public Object getItem(int position) {
            return mList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }
    }
}
