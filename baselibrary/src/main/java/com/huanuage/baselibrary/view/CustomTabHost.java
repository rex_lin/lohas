package com.huanuage.baselibrary.view;

import android.app.LocalActivityManager;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TabHost;

import com.huanuage.baselibrary.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kai_tseng on 2015/11/3.
 */
public class CustomTabHost extends LinearLayout implements ViewPager.OnPageChangeListener, TabHost.OnTabChangeListener {
    private final TabHost tabHost;
    private final ViewPager viewPager;
    private TabAdapter tabAdapter;
    private final Context mContext;
    private final List<TabInfo> mList = new ArrayList<>();

    public CustomTabHost(Context context) {
        this(context, null);
    }

    public CustomTabHost(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        LayoutInflater.from(context).inflate(R.layout.custom_fragment_tab_host, this);
        setOrientation(LinearLayout.VERTICAL);
        this.tabHost = (TabHost) findViewById(android.R.id.tabhost);
        this.viewPager = (ViewPager) findViewById(R.id.viewpager);
        this.tabHost.setup();
        this.tabHost.setOnTabChangedListener(this);
        this.viewPager.addOnPageChangeListener(this);
    }

    public void setup(FragmentManager fragmentManager) {
        this.tabHost.setup();
        this.tabAdapter = new TabAdapter(fragmentManager);
        this.viewPager.setAdapter(this.tabAdapter);
    }

    public TabInfo newTabSpec(String tag) {
        TabInfo tabInfo = new TabInfo(this.tabHost, tag);
        return tabInfo;
    }

    public <T extends Fragment> void addTab(TabInfo tabInfo, Class<T> mClass, Bundle bundle) {
        if (this.mList.contains(tabInfo) == true)
            throw new RuntimeException("TabInfo Is Presence.");
        if (tabInfo.fragment != null)
            throw new RuntimeException("TabInfo Is Add To Other View.");
        tabInfo.mClass = mClass;
        tabInfo.bundle = bundle;
        this.tabHost.addTab(tabInfo.tabSpec);
        this.mList.add(tabInfo);
        this.tabAdapter.notifyDataSetChanged();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        tabHost.setCurrentTab(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onTabChanged(String s) {
        viewPager.setCurrentItem(tabHost.getCurrentTab(), false);
    }

    class TabAdapter extends FragmentPagerAdapter {
        public TabAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            return mList.get(position).getFragment();
        }

        @Override
        public int getCount() {
            return mList.size();
        }
    }

    public class TabInfo {
        private final String tag;
        private Class<?> mClass;
        private Bundle bundle;
        private Fragment fragment;
        private TabHost.TabSpec tabSpec;

        public TabInfo(TabHost tabHost, String tag) {
            this.tag = tag;
            tabSpec = tabHost.newTabSpec(tag).setContent(android.R.id.tabcontent);
        }

        public TabInfo setIndicator(CharSequence label) {
            tabSpec = tabSpec.setIndicator(label);
            return this;
        }

        public TabInfo setIndicator(CharSequence label, Drawable icon) {
            tabSpec = tabSpec.setIndicator(label, icon);
            return this;
        }

        public TabInfo setIndicator(View view) {
            tabSpec = tabSpec.setIndicator(view);
            return this;
        }

        Fragment getFragment() {
            if (fragment == null) {
                fragment = Fragment.instantiate(mContext, mClass.getName(), this.bundle);
            }
            return fragment;
        }
    }
}
