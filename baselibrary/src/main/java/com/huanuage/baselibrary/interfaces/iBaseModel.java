package com.huanuage.baselibrary.interfaces;

import java.util.UUID;

/**
 * 取得物件資料
 */
public interface iBaseModel<T> {
    /**
     * 取得物件顯示資料
     *
     * @return
     */
    String toString();

    /**
     * 取得物件資料
     *
     * @return
     */
    UUID getId();

    /**
     * 複製物件
     *
     * @param onlyCloneFirstLevel 是否只複製第一層，true:直接搬移下層，false:下層也複製一份
     * @return
     */
    T cloneModel(boolean onlyCloneFirstLevel);
}
