package com.huanuage.baselibrary.interfaces;

import java.util.UUID;

/**
 * Created by kai_tseng on 2015/11/9.
 */
public interface iGetUuid {
    UUID getUuid();
}
