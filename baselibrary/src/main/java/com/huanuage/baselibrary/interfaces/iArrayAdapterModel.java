package com.huanuage.baselibrary.interfaces;

/**
 * Created by kai_tseng on 2015/11/9.
 */
public interface iArrayAdapterModel {
    String toString();

    boolean isFilter(long upperId);
}
