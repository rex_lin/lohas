package com.huanuage.baselibrary;

import android.widget.AbsListView;

public abstract class BaseApiListFragment extends BaseListFragment {
    protected int mPage = 0;
    private boolean isFirstExecuteApi = true;
    private boolean isResume = false;

    @Override
    public void onResume() {
        super.onResume();
        isResume = true;
        firstExecuteApi();
    }

    @Override
    public void onRefresh() {
        mPage = 0;
        executeApi();
    }

    // The minimum amount of items to have below your current scroll
    // position
    // before loading more.
    private int visibleThreshold = 3;
    // The current offset index of data you have loaded
    private int currentPage = 0;
    // The total number of items in the dataset after the last load
    private int previousTotalItemCount = 0;
    // True if we are still waiting for the last set of data to load.
    private boolean loading = true;
    // Sets the starting page index
    private int startingPageIndex = 0;

    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem,
                         int visibleItemCount, int totalItemCount) {
        if (totalItemCount < previousTotalItemCount) {
            this.currentPage = this.startingPageIndex;
            this.previousTotalItemCount = totalItemCount;
            if (totalItemCount == 0) {
                this.loading = true;
            }
        }

        if (loading && (totalItemCount > previousTotalItemCount)) {
            loading = false;
            previousTotalItemCount = totalItemCount;
            currentPage++;
        }

        if (!loading
                && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
            mPage++;
            executeApi();
            loading = true;
        }
    }

    protected abstract void executeApi();

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        firstExecuteApi();
    }

    private void firstExecuteApi() {
        if (getUserVisibleHint() && isFirstExecuteApi && isResume) {
            isFirstExecuteApi = false;
            executeApi();
        }
    }
}
