package com.huanuage.baselibrary.utils;

import java.util.UUID;

public class UUIDHelper {
	public static final UUID Empty = new UUID(0, 0);

	public static boolean IsNullOrEmpty(UUID obj) {
		boolean result = obj == null;
		if (!result) {
			result = Empty.equals(obj);
		}
		return result;
	}

	public static boolean TryParse(String value, Ref<UUID> out) {
		boolean result = true;
		try {
			out.setValue(UUID.fromString(value));
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}
		return result;
	}
}
