package com.huanuage.baselibrary.utils;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by kai_tseng on 2015/10/20.
 */
public abstract class BaseEditTextValidatorOnFocusChange implements View.OnFocusChangeListener {
    protected EditText mValidateView;
    protected TextView mErrorView;
    protected int mErrorTextId = -1;
    protected boolean mIsRequired = false;
    protected int mRequiredTextId = -1;
    protected int mNonFocusBackgroundId = -1;
    protected int mFocusBackgroundId = -1;
    protected int mErrorBackgroundId = -1;
    protected boolean mShowError = false;

    /**
     * 初始化物件，非必填
     *
     * @param validateView 需驗證的View
     */
    public BaseEditTextValidatorOnFocusChange(EditText validateView) {
        this.mValidateView = validateView;
    }

    /**
     * 初始化物件，非必填
     *
     * @param validateView 需驗證的View
     * @param errorView    錯誤顯示的View
     */
    public BaseEditTextValidatorOnFocusChange(EditText validateView, TextView errorView) {
        this(validateView);
        this.mErrorView = errorView;
    }

    /**
     * 初始化物件，非必填
     *
     * @param validateView 需驗證的View
     * @param errorView    錯誤顯示的View
     * @param errorTextId  錯誤顯示的TextId
     */
    public BaseEditTextValidatorOnFocusChange(EditText validateView, TextView errorView, int errorTextId) {
        this(validateView, errorView);
        this.mErrorTextId = errorTextId;
    }

    /**
     * 初始化驗證物件，必填
     *
     * @param validateView   需驗證的View
     * @param errorView      錯誤顯示的View
     * @param errorTextId    錯誤顯示的TextId
     * @param requiredTextId 未填值顯示的TextId
     */
    public BaseEditTextValidatorOnFocusChange(EditText validateView, TextView errorView, int errorTextId, int requiredTextId) {
        this(validateView, errorView, errorTextId);
        this.mIsRequired = true;
        this.mRequiredTextId = requiredTextId;
    }

    /**
     * 設定 需驗證的View
     *
     * @param validateView 需驗證的View
     */
    public void setValidateView(EditText validateView) {
        this.mValidateView = validateView;
    }

    /**
     * 取得 需驗證的View
     *
     * @return 需驗證的View
     */
    public EditText getValidateView() {
        return this.mValidateView;
    }

    /**
     * 設定 錯誤顯示的View
     *
     * @param errorView 錯誤顯示的View
     */
    public void setErrorView(TextView errorView) {
        this.mErrorView = errorView;
    }

    /**
     * 取得 錯誤顯示的View
     *
     * @return 錯誤顯示的View
     */
    public TextView getErrorView() {
        return this.mErrorView;
    }

    /**
     * 設定 錯誤顯示的TextId
     *
     * @param errorTextId 錯誤顯示的TextId
     */
    public void setErrorTextId(int errorTextId) {
        this.mErrorTextId = errorTextId;
    }

    /**
     * 取得 錯誤顯示的TextId
     *
     * @return 錯誤顯示的TextId
     */
    public int getErrorTextId() {
        return this.mErrorTextId;
    }

    /**
     * 設定 未填值顯示的TextId
     * 不為-1將設定必填
     *
     * @param requiredTextId 未填值顯示的TextId
     */
    public void setRequiredTextId(int requiredTextId) {
        if (requiredTextId == -1) {
            this.mIsRequired = false;
        } else {
            this.mIsRequired = true;
        }
        this.mRequiredTextId = requiredTextId;
    }

    /**
     * 取得 未填值顯示的TextId
     *
     * @return 未填值顯示的TextId
     */
    public int getRequiredTextId() {
        return this.mRequiredTextId;
    }

    /**
     * 設定 是否必填
     *
     * @param isRequired 是否必填
     */
    public void setIsRequired(boolean isRequired) {
        this.mIsRequired = isRequired;
    }

    /**
     * 取得 是否必填
     *
     * @return 是否必填
     */
    public boolean getIsRequired() {
        return this.mIsRequired;
    }

    /**
     * 設定 未Focus背景ID
     *
     * @param nonFocusBackgroundId
     */
    public void setNonFocusBackgroundId(int nonFocusBackgroundId) {
        this.mNonFocusBackgroundId = nonFocusBackgroundId;
    }

    /**
     * 取得 未Focus背景ID
     *
     * @return
     */
    public int getNonFocusBackgroundId() {
        return this.mNonFocusBackgroundId;
    }

    /**
     * 設定 Focus背景ID
     *
     * @param focusBackgroundId
     */
    public void setFocusBackgroundId(int focusBackgroundId) {
        this.mFocusBackgroundId = focusBackgroundId;
    }

    /**
     * 取得 Focus背景ID
     *
     * @return
     */
    public int getFocusBackgroundId() {
        return this.mFocusBackgroundId;
    }

    /**
     * 設定 錯誤背景ID
     *
     * @param errorBackgroundId
     */
    public void setErrorBackgroundId(int errorBackgroundId) {
        this.mErrorBackgroundId = errorBackgroundId;
    }

    /**
     * 取得 錯誤背景ID
     *
     * @return
     */
    public int getErrorBackgroundId() {
        return this.mErrorBackgroundId;
    }

    /**
     * 取得 驗證結果
     *
     * @return
     */
    public boolean isCheckSuccess() {
        return this.mShowError;
    }

    /**
     * 驗證字串是否符合格式
     *
     * @param value 驗證值
     * @return 驗證結果
     */
    public abstract boolean validate(String value);

    @Override
    public void onFocusChange(View view, boolean b) {
        if (b == false) {
            String value = this.mValidateView.getText().toString();
            mShowError = true;
            if (StringHelper.IsNullOrWhiteSpace(value) == true) {
                if (this.mIsRequired == true) {
                    mShowError = false;
                    if (this.mErrorView != null && this.mRequiredTextId != -1) {
                        this.mErrorView.setText(this.mValidateView.getResources().getText(this.mRequiredTextId));
                    }
                }
            } else if (validate(value) == false) {
                mShowError = false;
                if (this.mErrorView != null && this.mErrorTextId != -1) {
                    this.mErrorView.setText(this.mValidateView.getResources().getText(this.mErrorTextId));
                }
            }
            if (mShowError == false) {
                if (this.mErrorBackgroundId != -1) {
                    this.mValidateView.setBackgroundResource(this.mErrorBackgroundId);
                }
            } else if (this.mNonFocusBackgroundId != -1) {
                this.mValidateView.setBackgroundResource(this.mNonFocusBackgroundId);
            }
            if (this.mErrorView != null) {
                if (mShowError == false) {
                    this.mErrorView.setVisibility(View.VISIBLE);
                } else {
                    this.mErrorView.setVisibility(View.GONE);
                }
            }
        } else if (this.mFocusBackgroundId != -1) {
            this.mValidateView.setBackgroundResource(this.mFocusBackgroundId);
        }
    }
}
