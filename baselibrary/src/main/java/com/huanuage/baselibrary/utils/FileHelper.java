package com.huanuage.baselibrary.utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.URLConnection;

/**
 * Created by kai_tseng on 2015/11/8.
 */
public class FileHelper {
    public final static String BOUNDARY = "==================================";
    public final static String HYPHENS = "--";
    public final static String CRLF = "\r\n";

    /**
     * File To Byte Arrays
     *
     * @param file
     * @return
     */
    public static byte[] fileToByte(File file) {
        if (file == null || file.isFile() == false)
            throw new RuntimeException("File is not exist.");
        try {
            int fileSize = (int) file.length();
            byte[] bytes = new byte[fileSize];
            BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(file));
            bufferedInputStream.read(bytes);
            bufferedInputStream.close();
            return bytes;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    /**
     * File To Content
     *
     * @param fieldName Form Data Field Name
     * @param file      File
     * @return
     */
    public static byte[] fileToContent(String fieldName, File file) {
        byte[] byteFile = fileToByte(file);

        // 下面是開始寫參數
        String fileName = file.getName();
        String strContentDisposition = "Content-Disposition: form-data; name=\"" + fieldName + "\"; filename=\"" + fileName + "\"";
        String strContentType = "Content-Type: " + URLConnection.guessContentTypeFromName(fieldName);

        byte[] byteContent = StringHelper.readStringToByteArray(HYPHENS + BOUNDARY + CRLF + strContentDisposition + CRLF + strContentType + CRLF + CRLF);

        byte[] bytes = new byte[byteFile.length + byteContent.length];
        System.arraycopy(byteContent, 0, bytes, 0, byteContent.length);
        System.arraycopy(byteFile, 0, bytes, byteContent.length, byteFile.length);
        return bytes;
    }
}
