package com.huanuage.baselibrary.utils;

import java.lang.reflect.Type;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

public class GsonHelper {
	private static Gson gson;

	private static Gson getGson() {
		if (gson == null)
			gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssz")
					.create();
		return gson;
	}

	public static <T> T DeserializeObject(JsonElement jsonElement,
			Class<T> mObjClass) {
		return getGson().fromJson(jsonElement, mObjClass);
	}

	public static <T> T DeserializeObject(String strJson, Class<T> mObjClass) {
		return getGson().fromJson(strJson, mObjClass);
	}

	public static <T> T DeserializeObject(String strJson, Type mObjClass) {
		return getGson().fromJson(strJson, mObjClass);
	}

	public static String SerializeObject(Object mObj) {
		return getGson().toJson(mObj);
	}
}
