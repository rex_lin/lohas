package com.huanuage.baselibrary.utils;

import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by kai_tseng on 2015/10/20.
 */
public class PasswordVaildator extends BaseEditTextValidatorOnFocusChange {
    public static final String strPasswordExpression = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).{8,}$";

    /**
     * 初始化驗證物件，非必填
     *
     * @param validateView 需驗證的View
     */
    public PasswordVaildator(EditText validateView) {
        super(validateView);
    }

    /**
     * 初始化驗證物件，非必填
     *
     * @param validateView 需驗證的View
     * @param errorView    錯誤顯示的View
     */
    public PasswordVaildator(EditText validateView, TextView errorView) {
        super(validateView, errorView);
    }

    /**
     * 初始化驗證物件，非必填
     *
     * @param validateView 需驗證的View
     * @param errorView    錯誤顯示的View
     * @param errorTextId  錯誤顯示的TextId
     */
    public PasswordVaildator(EditText validateView, TextView errorView, int errorTextId) {
        super(validateView, errorView, errorTextId);
    }

    /**
     * 初始化驗證物件，必填
     *
     * @param validateView   需驗證的View
     * @param errorView      錯誤顯示的View
     * @param errorTextId    錯誤顯示的TextId
     * @param requiredTextId 未填值顯示的TextId
     */
    public PasswordVaildator(EditText validateView, TextView errorView, int errorTextId, int requiredTextId) {
        super(validateView, errorView, errorTextId);
        this.mIsRequired = true;
        this.mRequiredTextId = requiredTextId;
    }

    @Override
    public boolean validate(String value) {
        return value.matches(strPasswordExpression);
    }
}
