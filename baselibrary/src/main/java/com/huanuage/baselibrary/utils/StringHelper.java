package com.huanuage.baselibrary.utils;

import java.security.MessageDigest;

public class StringHelper {

	/**
	 * 判斷是否為null或空字串或空白字串
	 * 
	 * @param obj
	 *            字串
	 * @return
	 */
	public static boolean IsNullOrWhiteSpace(String obj) {
		boolean result = obj == null;
		if (!result) {
			result = "".equals(obj.trim());
		}
		return result;
	}

	/**
	 * 判斷是否為null或空字串
	 * 
	 * @param obj
	 *            字串
	 * @return
	 */
	public static boolean IsNullOrEmpty(String obj) {
		boolean result = obj == null;
		if (!result) {
			result = "".equals(obj);
		}
		return result;
	}

	/**
	 * 字串轉byte[]
	 * 
	 * @param dataString
	 *            來源字串
	 * @return byte[]
	 * @throws Exception
	 */
	public static byte[] readStringToByteArray(String dataString) {
		byte[] data = null;
		if (dataString != null && dataString.isEmpty() == false)
			try {
				data = dataString.getBytes("UTF-8");
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		return data;
	}

	public static String byte2hex(byte[] value) {
		StringBuilder hex = new StringBuilder(value.length * 2);
		for (byte b : value)
			hex.append(String.format("%02x", b & 0xff));
		return hex.toString();
	}

	public static MessageDigest getMessageDigestInstance() {
		try {
			return MessageDigest.getInstance("SHA-256");
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static String encryptSHA256(String value) {
		MessageDigest sha = null;
		try {
			sha = MessageDigest.getInstance("SHA-256");
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		String strEncrypt = "";
		byte[] valueArray = readStringToByteArray(value);
		if (valueArray != null && valueArray.length > 0) {
			strEncrypt = byte2hex(sha.digest(valueArray));
		}
		return strEncrypt;
	}
}
