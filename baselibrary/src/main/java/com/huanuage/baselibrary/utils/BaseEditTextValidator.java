package com.huanuage.baselibrary.utils;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

/**
 * [底層]EditText onTextChanged 驗證
 */
public abstract class BaseEditTextValidator implements TextWatcher {
    protected EditText mValidateView;
    protected TextView mErrorView;
    protected int mErrorTextId = -1;
    protected boolean mIsRequired = false;
    protected int mRequiredTextId = -1;

    /**
     * 初始化物件，非必填
     *
     * @param validateView 需驗證的View
     */
    public BaseEditTextValidator(EditText validateView) {
        setValidateView(validateView);
    }

    /**
     * 初始化物件，非必填
     *
     * @param validateView 需驗證的View
     * @param errorView    錯誤顯示的View
     */
    public BaseEditTextValidator(EditText validateView, TextView errorView) {
        this(validateView);
        setErrorView(errorView);
    }

    /**
     * 初始化物件，非必填
     *
     * @param validateView 需驗證的View
     * @param errorView    錯誤顯示的View
     * @param errorTextId  錯誤顯示的TextId
     */
    public BaseEditTextValidator(EditText validateView, TextView errorView, int errorTextId) {
        this(validateView, errorView);
        setRequiredTextId(errorTextId);
    }

    /**
     * 初始化驗證物件，必填
     *
     * @param validateView   需驗證的View
     * @param errorView      錯誤顯示的View
     * @param errorTextId    錯誤顯示的TextId
     * @param requiredTextId 未填值顯示的TextId
     */
    public BaseEditTextValidator(EditText validateView, TextView errorView, int errorTextId, int requiredTextId) {
        this(validateView, errorView, errorTextId);
        setRequiredTextId(requiredTextId);
    }

    /**
     * 設定 需驗證的View
     *
     * @param validateView 需驗證的View
     */
    public void setValidateView(EditText validateView) {
        this.mValidateView = validateView;
    }

    /**
     * 取得 需驗證的View
     *
     * @return 需驗證的View
     */
    public EditText getValidateView() {
        return this.mValidateView;
    }

    /**
     * 設定 錯誤顯示的View
     *
     * @param errorView 錯誤顯示的View
     */
    public void setErrorView(TextView errorView) {
        this.mErrorView = errorView;
    }

    /**
     * 取得 錯誤顯示的View
     *
     * @return 錯誤顯示的View
     */
    public TextView getErrorView() {
        return this.mErrorView;
    }

    /**
     * 設定 錯誤顯示的TextId
     *
     * @param errorTextId 錯誤顯示的TextId
     */
    public void setErrorTextId(int errorTextId) {
        this.mErrorTextId = errorTextId;
    }

    /**
     * 取得 錯誤顯示的TextId
     *
     * @return 錯誤顯示的TextId
     */
    public int getErrorTextId() {
        return this.mErrorTextId;
    }

    /**
     * 設定 未填值顯示的TextId
     * 不為-1將設定必填
     *
     * @param requiredTextId 未填值顯示的TextId
     */
    public void setRequiredTextId(int requiredTextId) {
        if (requiredTextId == -1) {
            this.mIsRequired = false;
        } else {
            this.mIsRequired = true;
        }
        this.mRequiredTextId = requiredTextId;
    }

    /**
     * 取得 未填值顯示的TextId
     *
     * @return 未填值顯示的TextId
     */
    public int getRequiredTextId() {
        return this.mRequiredTextId;
    }

    /**
     * 設定 是否必填
     *
     * @param isRequired 是否必填
     */
    public void setIsRequired(boolean isRequired) {
        this.mIsRequired = isRequired;
    }

    /**
     * 取得 是否必填
     *
     * @return 是否必填
     */
    public boolean getIsRequired() {
        return this.mIsRequired;
    }

    /**
     * 驗證字串是否符合格式
     *
     * @param value 驗證值
     * @return 驗證結果
     */
    public abstract boolean validate(String value);

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        String value = this.mValidateView.getText().toString();
        boolean mShowError = true;
        if (StringHelper.IsNullOrWhiteSpace(value) == true) {
            if (this.mIsRequired == true) {
                mShowError = false;
                if (this.mErrorView != null && this.mRequiredTextId != -1) {
                    this.mErrorView.setText(this.mValidateView.getResources().getText(this.mRequiredTextId));
                }
            }
        } else if (validate(value) == false) {
            mShowError = false;
            if (this.mErrorView != null && this.mErrorTextId != -1) {
                this.mErrorView.setText(this.mValidateView.getResources().getText(this.mErrorTextId));
            }
        }
        if (this.mErrorView != null) {
            if (mShowError == false) {
                this.mErrorView.setVisibility(View.VISIBLE);
            } else {
                this.mErrorView.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void afterTextChanged(Editable editable) {
    }
}
