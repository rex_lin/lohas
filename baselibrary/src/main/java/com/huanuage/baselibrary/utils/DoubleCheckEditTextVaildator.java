package com.huanuage.baselibrary.utils;

import android.widget.EditText;
import android.widget.TextView;

/**
 * 驗證 EditText值 是否與 基準EdotText值 相同
 */
public class DoubleCheckEditTextVaildator extends BaseEditTextValidatorOnFocusChange {
    protected EditText mStandardView;

    /**
     * 設定 基準值的View
     *
     * @param standardView 基準值的View
     */
    public void setStandardView(EditText standardView) {
        this.mStandardView = standardView;
    }

    /**
     * 取得 基準值的View
     *
     * @return 基準值的View
     */
    public EditText getStandardView() {
        return this.mStandardView;
    }

    /**
     * 初始化物件
     *
     * @param validateView 需驗證的View
     * @param standardView 基準值的View
     */
    public DoubleCheckEditTextVaildator(EditText validateView, EditText standardView) {
        super(validateView);
        setStandardView(standardView);
    }

    /**
     * 初始化驗證物件，非必填
     *
     * @param validateView 需驗證的View
     * @param standardView 基準值的View
     * @param errorView    錯誤顯示的View
     */
    public DoubleCheckEditTextVaildator(EditText validateView, EditText standardView, TextView errorView) {
        super(validateView, errorView);
        setStandardView(standardView);
    }

    /**
     * 初始化驗證物件，非必填
     *
     * @param validateView 需驗證的View
     * @param standardView 基準值的View
     * @param errorView    錯誤顯示的View
     * @param errorTextId  錯誤顯示的TextId
     */
    public DoubleCheckEditTextVaildator(EditText validateView, EditText standardView, TextView errorView, int errorTextId) {
        super(validateView, errorView, errorTextId);
        setStandardView(standardView);
    }

    /**
     * 初始化驗證物件，必填
     *
     * @param validateView   需驗證的View
     * @param standardView   基準值的View
     * @param errorView      錯誤顯示的View
     * @param errorTextId    錯誤顯示的TextId
     * @param requiredTextId 未填值顯示的TextId
     */
    public DoubleCheckEditTextVaildator(EditText validateView, EditText standardView, TextView errorView, int errorTextId, int requiredTextId) {
        super(validateView, errorView, errorTextId, requiredTextId);
        setStandardView(standardView);
    }

    @Override
    public boolean validate(String value) {
        try {
            return this.mStandardView.getText().toString().equals(value);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
