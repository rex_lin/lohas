package com.huanuage.baselibrary.utils;

public class Ref<T> {
	private T value;

	public Ref() {
	}

	public Ref(T value) {
		this.value = value;
	}

	public T getValue() {
		return this.value;
	}

	public void setValue(T value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return this.value == null ? null : this.value.toString();
	}
}
