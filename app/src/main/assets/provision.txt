Linkfun APP
<br>
Linkfun APP為磐儀科技股份有限公司與磐雲智能股份有限公司，共同經營管理

之網路平台(以下簡稱本平台)。我們非常重視會員的隱私權且遵循「個人資料保

護法」之規定，因此制訂了隱私權保護政策，您可參考下列隱私權保護政策的

內容
<br>
一、個人資料之安全

保護會員的個人隱私是本平台重要的經營理念，當您提出申請、註冊使用

、下載、使用軟體或加入Linkfun會員時，即表示您已閱讀、瞭解並同意接受本

服務條款之所有內容，包含使用者名稱、生日、性別、email、地址，或其後使

用本平台服務所提供或產生之數據資料。<br>

在未經會員同意之下，我們絕不會將會員的個人資料提供予任何與本站服務無

關之第三人。會員應妥善保密自己的網路密碼及個人資料，不要將任何個人資

料，尤其是網路密碼提供給任何人。在使用完網站所提供的各項服務功能後，

切記要關閉瀏覽器視窗。<br>

二、 個人資料的利用<br>

在遵守相同的消費者隱私權保護協定規範下，本平台允許包括磐儀科技股

份有限公司與磐雲智能股份有限公司使用消費者資料。在消費者同意的情況下

，將提供交叉行銷使用，使用範圍依照原來所說明的使用目的，除非事先說明

、或依照相關法律規定，否則不會將資料提供給第三人、或移作其他目的使用

。<br>

利用之目的例示如下：<br><br>

以會員身份使用本平台提供之各項服務時，於頁面中自動顯示會員資訊。

宣傳廣告或行銷等

提供會員各種最新活動等資訊、透過電子郵件、郵件、電話等提供與服務有關

之資訊。 將會員所瀏覽之內容或廣告，依客戶之個人屬性或網站之瀏覽紀錄等

項目，進行個人化作業、會員使用服務之分析、新服務之開發或既有服務之改

善等。 針對民調、活動、留言版等之意見，或其他服務關連事項，與會員進行

聯繫。<br>

回覆客戶之詢問<br>

針對會員透過電子郵件、郵件、傳真、電話或其他任何直接間接連絡方式向本

平台所提出之詢問進行回覆。<br>

其他<br>

提供個別服務時，亦可能於上述規定之目的以外，利用個人資料。此時將在該

個別服務之網頁載明其要旨。<br>

三、資料安全<br>

為保障會員的隱私及安全，本平台會員帳號資料會用密碼保護。本平台並

盡力以合理之技術及程序，保障所有個人資料之安全。<br>

四、個人資料查詢或更正的方式<br>

會員對於其個人資料，有查詢及閱覽、製給複製本、補充或更正、停止電

腦處理及利用、刪除等需求時，立即聯絡我們 ，本平台將迅速進行處理。

刪除、解除安裝本APP，將不會刪除您的會員資料，若您想要修改、停用、刪

除您的個人資料或聯絡方式，或有任何建議，歡迎您直接聯絡我們。<br>

五、隱私權保護政策修訂<br>

本公司將會不定時修改及變更本服務權益，建議您隨時注意網站公告，異

動內容於公告日起生效，若您於變更或修改後繼續使用本服務，即視為您瞭解

並已接受及同意變更修改後之服務條款。<br>

最新會員權益與相關規定，以產品官網說明為準，修正相關權益時，將不另行

個別通知。會員如果對於本平台網站隱私權聲明、或與個人資料有關之相關事

項有任何疑問，歡迎立即 聯絡我們 。<br>