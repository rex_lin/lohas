package com.acloud.app;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.acloud.app.adapter.NavigationDrawerAdapter;
import com.acloud.app.dialog.CustomAdvPopupWindow;
import com.acloud.app.dialog.CustomPopupActivityWindow;
import com.acloud.app.dialog.CustomPopupWindow;
import com.acloud.app.tool.Pub;
import com.acloud.app.tool.TLog;
import com.acloud.app.util.PrefConstant;

import java.util.ArrayList;
import java.util.List;

public class NavigationDrawerFragment extends Fragment implements NavigationDrawerCallbacks {
    private static final String PREF_USER_LEARNED_DRAWER = "navigation_drawer_learned";
    private static final String STATE_SELECTED_POSITION = "selected_navigation_drawer_position";
    private static final String PREFERENCES_FILE = "com.acloud.app_settings";
    private NavigationDrawerCallbacks mCallbacks;
//    private RecyclerView mDrawerList;
    private View mFragmentContainerView;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mActionBarDrawerToggle;
    private boolean mUserLearnedDrawer;
    private boolean mFromSavedInstanceState;
    private int mCurrentSelectedPosition;

    loginClick mLoginClick;
    TextView mBtnLogin;

    private ExpandableListView mDrawerList;

    public interface loginClick {
        void onLoginClick();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_navigation_google, container, false);
        //TODO list
//        mDrawerList = (RecyclerView) view.findViewById(R.id.drawerList);
//        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
//        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
//        mDrawerList.setLayoutManager(layoutManager);
//        mDrawerList.setHasFixedSize(true);
        mDrawerList = (ExpandableListView) view.findViewById(R.id.drawerList);


        //TODO adapter
//        final List<NavigationItem> navigationItems = getMenu();
//        NavigationDrawerAdapter adapter = new NavigationDrawerAdapter(navigationItems);
//        adapter.setNavigationDrawerCallbacks(this);
        NavigationDrawerAdapter adapter = new NavigationDrawerAdapter(getActivity());
        //TODO list
//        mDrawerList.setAdapter(adapter);
        mDrawerList.setAdapter(adapter);
        //Login process
        mBtnLogin = (TextView) view.findViewById(R.id.txtUsername);
        mBtnLogin.setText(MyApplication.emil);

//        mBtnLogin.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mLoginClick.onLoginClick();
//                closeDrawer();
//            }
//        });
        mDrawerList.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                TLog.d("ooxx", this.getClass().getName() + " " + "groupPosition : " + groupPosition);
                switch (groupPosition){
                    case 0:
                        selectItem(MainActivity.MAIN_PAGE);
                        return true;
                    case 1:

                        return false;
                    case 2:
                        selectItem(MainActivity.ONLINE_SHOP);
                        return true;
                    case 3:
                        return true;
                    case 4:
                        selectItem(MainActivity.HAPPY_FUN);
                        return true;
                    case 5:
                        selectItem(MainActivity.SETTING_PAGE);

                        return true;
                    case 6:
                        selectItem(MainActivity.PROFILE_PAGE);
                        return true;
                    case 7:
                        selectItem(MainActivity.MODIFY_PASSWORD);
                        return true;
                    case 8:
                        selectItem(MainActivity.LOGOUT_PAGE);
                        return true;
                    case 9:
                        return true;
                    case 10:
                        return true;
                    case 11:
                        return true;
                }
                return true;
            }
        });
        mDrawerList.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                switch (childPosition){
                    case 0:
                        selectItem(MainActivity.STORE_PAGE);
                        break;
                    case 1:
                        selectItem(MainActivity.COLLOCATION_PAGE);
                        break;
                }
                return false;
            }
        });
        selectItem(mCurrentSelectedPosition);
        changeLoginStatus();
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUserLearnedDrawer = Boolean.valueOf(readSharedSetting(getActivity(), PREF_USER_LEARNED_DRAWER, "false"));
        if (savedInstanceState != null) {
            mCurrentSelectedPosition = savedInstanceState.getInt(STATE_SELECTED_POSITION);
            mFromSavedInstanceState = true;
        }
        mLoginClick = (loginClick) getActivity();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallbacks = (NavigationDrawerCallbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement NavigationDrawerCallbacks.");
        }
    }

    public ActionBarDrawerToggle getActionBarDrawerToggle() {
        return mActionBarDrawerToggle;
    }

    public void setActionBarDrawerToggle(ActionBarDrawerToggle actionBarDrawerToggle) {
        mActionBarDrawerToggle = actionBarDrawerToggle;
    }

    public void setup(int fragmentId, DrawerLayout drawerLayout, Toolbar toolbar) {
        mFragmentContainerView = getActivity().findViewById(fragmentId);
        if (mFragmentContainerView.getParent() instanceof ScrimInsetsFrameLayout) {
            mFragmentContainerView = (View) mFragmentContainerView.getParent();
        }
        mDrawerLayout = drawerLayout;
        mDrawerLayout.setStatusBarBackgroundColor(
                getResources().getColor(R.color.myPrimaryDarkColor));

        mActionBarDrawerToggle = new ActionBarDrawerToggle(getActivity(), mDrawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerStateChanged(int newState) {
                super.onDrawerStateChanged(newState);
                CustomPopupWindow.closeAllPopupWindow();
                CustomAdvPopupWindow.closeAllPopupWindow();
                CustomPopupActivityWindow.closeAllPopupWindow();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                if (!isAdded()) return;
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

                if (!isAdded()) return;
                if (!mUserLearnedDrawer) {
                    mUserLearnedDrawer = true;
                    saveSharedSetting(getActivity(), PREF_USER_LEARNED_DRAWER, "true");
                }

                getActivity().invalidateOptionsMenu();
            }
        };

        if (!mUserLearnedDrawer && !mFromSavedInstanceState)
            mDrawerLayout.openDrawer(mFragmentContainerView);

        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mActionBarDrawerToggle.syncState();
            }
        });

        mDrawerLayout.setDrawerListener(mActionBarDrawerToggle);
    }

    public void openDrawer() {
        mDrawerLayout.openDrawer(mFragmentContainerView);

    }

    public void closeDrawer() {
        mDrawerLayout.closeDrawer(mFragmentContainerView);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

//    public List<NavigationItem> getMenu() {
//        List<NavigationItem> items = new ArrayList<NavigationItem>();
//        /*
//        items.add(new NavigationItem(getResources().getString(R.string.drawer_item0), getResources().getDrawable(R.drawable.ico_house)));
//        items.add(new NavigationItem(getResources().getString(R.string.drawer_item1), getResources().getDrawable(R.drawable.ico_home)));
//        items.add(new NavigationItem(getResources().getString(R.string.drawer_item2), getResources().getDrawable(R.drawable.ico_member)));
//        items.add(new NavigationItem(getResources().getString(R.string.drawer_item3), getResources().getDrawable(R.drawable.ico_collection)));
//        items.add(new NavigationItem(getResources().getString(R.string.drawer_item4), getResources().getDrawable(R.drawable.ico_point)));
//        items.add(new NavigationItem(getResources().getString(R.string.drawer_item5), getResources().getDrawable(R.drawable.ico_activity)));
//        items.add(new NavigationItem(getResources().getString(R.string.drawer_item6), getResources().getDrawable(R.drawable.ico_coupon)));
//        items.add(new NavigationItem(getResources().getString(R.string.drawer_item7), getResources().getDrawable(R.drawable.ico_share)));
//        items.add(new NavigationItem(getResources().getString(R.string.drawer_item8), getResources().getDrawable(R.drawable.ico_serial)));
//        items.add(new NavigationItem(getResources().getString(R.string.drawer_item9), getResources().getDrawable(R.drawable.ico_info)));
//        items.add(new NavigationItem(getResources().getString(R.string.drawer_item10), getResources().getDrawable(R.drawable.ico_setting)));
//        */
//        items.add(new NavigationItem(getResources().getString(R.string.drawer_item0), getResources().getDrawable(R.drawable.drawer_home)));
//        items.add(new NavigationItem(getResources().getString(R.string.drawer_item1), getResources().getDrawable(R.drawable.drawer_street)));
//        items.add(new NavigationItem(getResources().getString(R.string.drawer_item2), getResources().getDrawable(R.drawable.drawer_member)));
//        items.add(new NavigationItem(getResources().getString(R.string.drawer_item3), getResources().getDrawable(R.drawable.drawer_favorite)));
//        items.add(new NavigationItem(getResources().getString(R.string.drawer_item4), getResources().getDrawable(R.drawable.drawer_mypoint)));
//        items.add(new NavigationItem(getResources().getString(R.string.drawer_item5), getResources().getDrawable(R.drawable.drawer_activity)));
//        items.add(new NavigationItem(getResources().getString(R.string.drawer_item6), getResources().getDrawable(R.drawable.drawer_coupon)));
//        items.add(new NavigationItem(getResources().getString(R.string.drawer_item6_2), getResources().getDrawable(R.drawable.drawer_buy)));
////        items.add(new NavigationItem(getResources().getString(R.string.drawer_item7), getResources().getDrawable(R.drawable.drawer_share)));
//        items.add(new NavigationItem(getResources().getString(R.string.drawer_item8), getResources().getDrawable(R.drawable.drawer_giftserial)));
//        items.add(new NavigationItem(getResources().getString(R.string.drawer_item9), getResources().getDrawable(R.drawable.drawer_about)));
//        items.add(new NavigationItem(getResources().getString(R.string.drawer_item10), getResources().getDrawable(R.drawable.drawer_setting)));
//        items.add(new NavigationItem(getResources().getString(R.string.drawer_item10_2), getResources().getDrawable(R.drawable.drawer_logout)));
//        items.add(new NavigationItem(getResources().getString(R.string.drawer_item10_3), getResources().getDrawable(R.drawable.drawer_password)));
//        return items;
//    }

    /**
     * Changes the icon of the drawer to back
     */
    public void showBackButton() {
        if (getActivity() instanceof ActionBarActivity) {
            ((ActionBarActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    /**
     * Changes the icon of the drawer to menu
     */
    public void showDrawerButton() {
        if (getActivity() instanceof ActionBarActivity) {
            ((ActionBarActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }
        mActionBarDrawerToggle.syncState();
    }

    void selectItem(int position) {
        mCurrentSelectedPosition = position;


        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(mFragmentContainerView);
        }
        if (mCallbacks != null) {
            mCallbacks.onNavigationDrawerItemSelected(position);
        }
        //TODO list
//        ((NavigationDrawerAdapter) mDrawerList.getAdapter()).selectPosition(position);
    }

    public boolean isDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(mFragmentContainerView);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mActionBarDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_SELECTED_POSITION, mCurrentSelectedPosition);

    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        selectItem(position);
    }

    public DrawerLayout getDrawerLayout() {
        return mDrawerLayout;
    }

    public void setDrawerLayout(DrawerLayout drawerLayout) {
        mDrawerLayout = drawerLayout;
    }

    public static void saveSharedSetting(Context ctx, String settingName, String settingValue) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(settingName, settingValue);
        editor.apply();
    }

    public static String readSharedSetting(Context ctx, String settingName, String defaultValue) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        return sharedPref.getString(settingName, defaultValue);
    }

    public void changeLoginStatus() {
        mBtnLogin.setText(MyApplication.emil);
        if (PrefConstant.getBoolean(getActivity(), Pub.IS_LOGIN, false)) {
//            mBtnLogin.setText(getString(R.string.btn_logout));
//            mBtnLogin.setEnabled(false);

        } else {
//            mBtnLogin.setText(getString(R.string.btn_login));
//            mBtnLogin.setEnabled(true);
        }

    }


    public int getmCurrentSelectedPosition() {
        return mCurrentSelectedPosition;
    }
}
