package com.acloud.app.view;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;

public class DefaultPhoto extends Canvas {
 
  private static final String TAG = DefaultPhoto.class.getName();

  public DefaultPhoto(Bitmap bmp,float width,float height) {
    super(bmp);

    drawBg(width,height);
    drawIcon(width,height);

  }
  
  private void drawBg(float x,float y){
	  Paint paint = new Paint();
	  paint.setAntiAlias(true);
	  paint.setStyle(Paint.Style.FILL);
	  paint.setColor(Color.parseColor("#DCDDDD"));
	  this.drawRoundRect(new RectF(0, 0, x, y), 0, 0, paint);
  }
  
  private void drawIcon(float x,float y){
	  Paint paint = new Paint();
	  paint.setAntiAlias(true);
	  paint.setStyle(Paint.Style.FILL);
	  paint.setColor(Color.parseColor("#B5B5B6"));
	  
	  float oneDpHowManyPx = (x/100);

	  
	  Path path=new Path();  
	  path.moveTo(oneDpHowManyPx*20,oneDpHowManyPx*70);
      path.lineTo(oneDpHowManyPx*80,oneDpHowManyPx*70);
      path.lineTo(oneDpHowManyPx*65,oneDpHowManyPx*30);
      path.lineTo(oneDpHowManyPx*50,oneDpHowManyPx*55);
      path.lineTo(oneDpHowManyPx*30,oneDpHowManyPx*45);
      path.lineTo(oneDpHowManyPx*20,oneDpHowManyPx*70);

      path.close();
      this.drawPath(path, paint); 
	  
	  
  }
  
 
}
