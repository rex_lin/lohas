package com.acloud.app.model;

import java.util.ArrayList;

/**
 * Created by skywind-10 on 2015/8/11.
 */
public class StoreCategoryMenuResult {
    String sc_id;
    String sc_name;

    public ArrayList<scaResult> scaResultArrayList;


    public ArrayList<scaResult> getScaResultArrayList() {
        return scaResultArrayList;
    }

    public void setScaResultArrayList(ArrayList<scaResult> scaResultArrayList) {
        this.scaResultArrayList = scaResultArrayList;
    }


    public String getSc_name() {
        return sc_name;
    }

    public void setSc_name(String sc_name) {
        this.sc_name = sc_name;
    }

    public String getSc_id() {
        return sc_id;
    }

    public void setSc_id(String sc_id) {
        this.sc_id = sc_id;
    }



    public static class scaResult {
        String sca_id;
        String sca_name;

        public String getSca_id() {
            return sca_id;
        }

        public void setSca_id(String sca_id) {
            this.sca_id = sca_id;
        }

        public String getSca_name() {
            return sca_name;
        }

        public void setSca_name(String sca_name) {
            this.sca_name = sca_name;
        }
    }
}
