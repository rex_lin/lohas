package com.acloud.app.model;

import java.util.ArrayList;

/**
 * Created by skywind-10 on 2015/8/11.
 */
public class ShoppingDistrictMenuResult {
    String update_datetime;
    String sd_id;
    String sd_name;

    public ArrayList<smResult> getSmResultArrayList() {
        return smResultArrayList;
    }

    public void setSmResultArrayList(ArrayList<smResult> smResultArrayList) {
        this.smResultArrayList = smResultArrayList;
    }

    public ArrayList<smResult> smResultArrayList;



    public String getSd_name() {
        return sd_name;
    }

    public void setSd_name(String sd_name) {
        this.sd_name = sd_name;
    }

    public String getSd_id() {
        return sd_id;
    }

    public void setSd_id(String sd_id) {
        this.sd_id = sd_id;
    }


    public static class smResult {
        String sm_id;
        String sm_name;

        public String getSm_id() {
            return sm_id;
        }

        public void setSm_id(String sm_id) {
            this.sm_id = sm_id;
        }

        public String getSm_name() {
            return sm_name;
        }

        public void setSm_name(String sm_name) {
            this.sm_name = sm_name;
        }
    }
}
