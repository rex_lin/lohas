package com.acloud.app.model;

/**
 * Created by skywind-10 on 2015/7/14.
 */
public class MyCouponListResult {
    String ex_coupon_id;
    String coupon_id;
    String title;
    String store_id;
    String store_name;
    String lat;
    String lng;
    String distance;
    String points;
    String thumbnail_url;
    String used_start_datetime;
    String used_end_datetime;
    String exchange_dateitme;
    String used_datetime;
    String status;

    public String getEx_coupon_id() {
        return ex_coupon_id;
    }

    public void setEx_coupon_id(String ex_coupon_id) {
        this.ex_coupon_id = ex_coupon_id;
    }

    public String getCoupon_id() {
        return coupon_id;
    }

    public void setCoupon_id(String coupon_id) {
        this.coupon_id = coupon_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStore_id() {
        return store_id;
    }

    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    public String getStore_name() {
        return store_name;
    }

    public void setStore_name(String store_name) {
        this.store_name = store_name;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getThumbnail_url() {
        return thumbnail_url;
    }

    public void setThumbnail_url(String thumbnail_url) {
        this.thumbnail_url = thumbnail_url;
    }

    public String getUsed_start_datetime() {
        return used_start_datetime;
    }

    public void setUsed_start_datetime(String used_start_datetime) {
        this.used_start_datetime = used_start_datetime;
    }

    public String getUsed_end_datetime() {
        return used_end_datetime;
    }

    public void setUsed_end_datetime(String used_end_datetime) {
        this.used_end_datetime = used_end_datetime;
    }

    public String getExchange_dateitme() {
        return exchange_dateitme;
    }

    public void setExchange_dateitme(String exchange_dateitme) {
        this.exchange_dateitme = exchange_dateitme;
    }

    public String getUsed_datetime() {
        return used_datetime;
    }

    public void setUsed_datetime(String used_datetime) {
        this.used_datetime = used_datetime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
