package com.acloud.app.model;

/**
 * Created by skywind-10 on 2015/7/14.
 */
public class MyCouponDetailResult {
    String ex_coupon_id;
    String coupon_id;
    String title;
    String content;
    String store_id;
    String store_name;
    String lat;
    String lng;
    String distance;
    String image_url;
    String used_start_datetime;
    String used_end_datetime;
    String exchange_datetime;
    String used_type;
    String status;
    String verify_code;

    public String getVerify_code() {
        return verify_code;
    }

    public void setVerify_code(String verify_code) {
        this.verify_code = verify_code;
    }

    public String getEx_coupon_id() {
        return ex_coupon_id;
    }

    public void setEx_coupon_id(String ex_coupon_id) {
        this.ex_coupon_id = ex_coupon_id;
    }

    public String getCoupon_id() {
        return coupon_id;
    }

    public void setCoupon_id(String coupon_id) {
        this.coupon_id = coupon_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getStore_id() {
        return store_id;
    }

    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    public String getStore_name() {
        return store_name;
    }

    public void setStore_name(String store_name) {
        this.store_name = store_name;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getUsed_start_datetime() {
        return used_start_datetime;
    }

    public void setUsed_start_datetime(String used_start_datetime) {
        this.used_start_datetime = used_start_datetime;
    }

    public String getUsed_end_datetime() {
        return used_end_datetime;
    }

    public void setUsed_end_datetime(String used_end_datetime) {
        this.used_end_datetime = used_end_datetime;
    }

    public String getExchange_datetime() {
        return exchange_datetime;
    }

    public void setExchange_datetime(String exchange_datetime) {
        this.exchange_datetime = exchange_datetime;
    }

    public String getUsed_type() {
        return used_type;
    }

    public void setUsed_type(String used_type) {
        this.used_type = used_type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


}
