package com.acloud.app.model;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CollectionStruct extends BaseStruct {
	private static final long serialVersionUID = -674618586461782942L;
    List<Data> list =new ArrayList<Data>();
	public CollectionStruct() {
        super();
    }

    public CollectionStruct(String syscode, String sysmsg) {
        super(syscode, sysmsg);
    }

    public void setList(List<Data> list) {
        this.list = list;
    }

    public List<Data> getList() {
        return list;
    }

    public static class Data implements Serializable {
        String
                store_id="",
                store_name="",
                fav_num="",
                rating="",
                rating_num="",
                avg_price="",
                thumbani_url="",
                sd_id="",
                sd_name="",
                sca_id="",
                sca_name="" ;


        public Data()
        {}

        public Data(String store_id,
                    String store_name,
                    String fav_num,
                    String rating,
                    String rating_num,
                    String avg_price,
                    String thumbani_url,
                    String sd_id,
                    String sd_name,
                    String sca_id,
                    String sca_name) {
            this.store_id = store_id;
            this.store_name = store_name;
            this.fav_num = fav_num;
            this.rating = rating;
            this.rating_num = rating_num;
            this.avg_price = avg_price;
            this.thumbani_url = thumbani_url;
            this.sd_id = sd_id;
            this.sd_name = sd_name;
            this.sca_id=sca_id;
            this.sca_name = sca_name;
        }

        public String getSca_name() {
            return sca_name;
        }

        public String getStore_id() {
            return store_id;
        }

        public String getStore_name() {
            return store_name;
        }

        public String getFav_num() {
            return fav_num;
        }

        public String getRating() {
            return rating;
        }

        public String getRating_num() {
            return rating_num;
        }

        public String getAvg_price() {
            return avg_price;
        }

        public String getThumbani_url() {
            return thumbani_url;
        }

        public String getSd_id() {
            return sd_id;
        }

        public String getSd_name() {
            return sd_name;
        }

        public void setStore_id(String store_id) {
            this.store_id = store_id;
        }

        public void setStore_name(String store_name) {
            this.store_name = store_name;
        }

        public void setFav_num(String fav_num) {
            this.fav_num = fav_num;
        }

        public void setRating(String rating) {
            this.rating = rating;
        }

        public void setRating_num(String rating_num) {
            this.rating_num = rating_num;
        }

        public void setAvg_price(String avg_price) {
            this.avg_price = avg_price;
        }

        public void setThumbani_url(String thumbani_url) {
            this.thumbani_url = thumbani_url;
        }

        public void setSd_id(String sd_id) {
            this.sd_id = sd_id;
        }

        public void setSd_name(String sd_name) {
            this.sd_name = sd_name;
        }

        public void setSca_name(String sca_name) {
            this.sca_name = sca_name;
        }
    }
}
