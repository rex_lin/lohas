package com.acloud.app.model;

import org.json.JSONObject;

/**
 * Created by skywind-10 on 2015/7/3.
 */

/**
 * UserProfile存成物件
 */
public class UserProfileResult {
    private String id;
    private String serialNo;
    private String email;
    private String name;
    private String bonus;
    private String phone;
    private String birthday;
    private String gender;
    private String memo;
    private String writeStatus;
    private String pushSwitch;
    private String pushToken;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBonus() {
        return bonus;
    }

    public void setBonus(String bonus) {
        this.bonus = bonus;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getWriteStatus() {
        return writeStatus;
    }

    public void setWriteStatus(String writeStatus) {
        this.writeStatus = writeStatus;
    }

    public String getPushSwitch() {
        return pushSwitch;
    }

    public void setPushSwitch(String pushSwitch) {
        this.pushSwitch = pushSwitch;
    }

    public String getPushToken() {
        return pushToken;
    }

    public void setPushToken(String pushToken) {
        this.pushToken = pushToken;
    }

    public UserProfileResult() {
    }

    /**
     * 將Json丟入解析
     *
     * @param object
     */
    public UserProfileResult(JSONObject object) {
        this.setId(object.optString("id"));
        this.setSerialNo(object.optString("serial_no"));
        this.setEmail(object.optString("email"));
        this.setName(object.optString("name"));
        this.setBonus(object.optString("bonus"));
        this.setPhone(object.optString("phone"));
        this.setBirthday(object.optString("birthday"));
        this.setGender(object.optString("gender"));
        this.setMemo(object.optString("memo"));
        this.setWriteStatus(object.optString("write_status"));
        this.setPushSwitch(object.optString("push_switch"));
        this.setPushToken(object.optString("push_token"));
    }
}
