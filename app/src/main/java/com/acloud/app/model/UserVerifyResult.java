package com.acloud.app.model;

/**
 * Created by skywind-10 on 2015/7/8.
 *  User 驗證結果
 */
public class UserVerifyResult {

    String is_verify;
    String uid;
    String authtoken;
    String first_login;
    String is_activity;
    String serial_no;
    String recommended_serial_no;
    String verifing_email;

    public String getIs_verify() {
        return is_verify;
    }

    public void setIs_verify(String is_verify) {
        this.is_verify = is_verify;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getAuthtoken() {
        return authtoken;
    }

    public void setAuthtoken(String authtoken) {
        this.authtoken = authtoken;
    }

    public String getFirst_login() {
        return first_login;
    }

    public void setFirst_login(String first_login) {
        this.first_login = first_login;
    }

    public String getIs_activity() {
        return is_activity;
    }

    public void setIs_activity(String is_activity) {
        this.is_activity = is_activity;
    }

    public String getSerial_no() {
        return serial_no;
    }

    public void setSerial_no(String serial_no) {
        this.serial_no = serial_no;
    }

    public String getRecommended_serial_no() {
        return recommended_serial_no;
    }

    public void setRecommended_serial_no(String recommended_serial_no) {
        this.recommended_serial_no = recommended_serial_no;
    }

    public String getVerifing_email() {
        return verifing_email;
    }

    public void setVerifing_email(String verifing_email) {
        this.verifing_email = verifing_email;
    }
}
