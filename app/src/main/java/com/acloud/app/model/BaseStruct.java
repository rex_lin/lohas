package com.acloud.app.model;

import java.io.Serializable;


/**
 * @author RavenYang
 * @version 20150623
 * 基本形態 , implements Serializable 方便傳遞物件
 */
@SuppressWarnings("serial")
public abstract class BaseStruct implements Serializable{
	
	String
            syscode="",	//api編號
            sysmsg="";	//錯誤碼

	public BaseStruct(){}
	
	public BaseStruct(String syscode, String sysmsg) {
		this.syscode = syscode;
		this.sysmsg = sysmsg;
	}
	
	public String getApi() {
		return syscode;
	}

	public void setApi(String syscode) {
        this.syscode = syscode;
	}

	public String getMsg() {
        return sysmsg;
	}

	public void setMsg(String sysmsg) {
        this.sysmsg = sysmsg;
	}

}
