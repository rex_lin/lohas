package com.acloud.app.model;

/**
 * Created by skywind-10 on 2015/7/3.
 */
public class StoreListResult {
    String store_id;
    String store_name;
    String thumbnail_url;
    String lat;
    String lng;
    String distance;
    String rating;
    String rating_num;
    String avg_price;
    String fav_num;
    String sd_id;
    String sd_name;
    String sca_id;
    String sca_name;

    public String getStore_id() {
        return store_id;
    }

    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    public String getStore_name() {
        return store_name;
    }

    public void setStore_name(String store_name) {
        this.store_name = store_name;
    }

    public String getThumbnail_url() {
        return thumbnail_url;
    }

    public void setThumbnail_url(String thumbnail_url) {
        this.thumbnail_url = thumbnail_url;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getRating_num() {
        return rating_num;
    }

    public void setRating_num(String rating_num) {
        this.rating_num = rating_num;
    }

    public String getAvg_price() {
        return avg_price;
    }

    public void setAvg_price(String avg_price) {
        this.avg_price = avg_price;
    }

    public String getFav_num() {
        return fav_num;
    }

    public void setFav_num(String fav_num) {
        this.fav_num = fav_num;
    }

    public String getSd_id() {
        return sd_id;
    }

    public void setSd_id(String sd_id) {
        this.sd_id = sd_id;
    }

    public String getSd_name() {
        return sd_name;
    }

    public void setSd_name(String sd_name) {
        this.sd_name = sd_name;
    }

    public String getSca_id() {
        return sca_id;
    }

    public void setSca_id(String sca_id) {
        this.sca_id = sca_id;
    }

    public String getSca_name() {
        return sca_name;
    }

    public void setSca_name(String sca_name) {
        this.sca_name = sca_name;
    }

    public StoreListResult() {

    }
}

