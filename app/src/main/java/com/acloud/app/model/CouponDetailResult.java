package com.acloud.app.model;

/**
 * Created by skywind-10 on 2015/7/7.
 */
public class CouponDetailResult {
    String coupon_id;
    String store_id;
    String store_name;
    String distance;
    String title;
    String content;
    String points;
    String image_url;
    String start_datetime;
    String end_datetime;
    String used_start_datetime;
    String used_end_datetime;
    String exchange_limit_amount;
    String remaining_amount;
    String allow_repeat;


    public String getAllow_repeat() {
        return allow_repeat;
    }

    public void setAllow_repeat(String allow_repeat) {
        this.allow_repeat = allow_repeat;
    }

    public CouponDetailResult() {
        super();
    }

    public String getCoupon_id() {
        return coupon_id;
    }

    public void setCoupon_id(String coupon_id) {
        this.coupon_id = coupon_id;
    }

    public String getStore_id() {
        return store_id;
    }

    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    public String getStore_name() {
        return store_name;
    }

    public void setStore_name(String store_name) {
        this.store_name = store_name;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getStart_datetime() {
        return start_datetime;
    }

    public void setStart_datetime(String start_datetime) {
        this.start_datetime = start_datetime;
    }

    public String getEnd_datetime() {
        return end_datetime;
    }

    public void setEnd_datetime(String end_datetime) {
        this.end_datetime = end_datetime;
    }

    public String getUsed_start_datetime() {
        return used_start_datetime;
    }

    public void setUsed_start_datetime(String used_start_datetime) {
        this.used_start_datetime = used_start_datetime;
    }

    public String getUsed_end_datetime() {
        return used_end_datetime;
    }

    public void setUsed_end_datetime(String used_end_datetime) {
        this.used_end_datetime = used_end_datetime;
    }

    /**
     * 可兌換數量
     *
     * @return
     */
    public String getExchange_limit_amount() {
        return exchange_limit_amount;
    }

    public void setExchange_limit_amount(String exchange_limit_amount) {
        this.exchange_limit_amount = exchange_limit_amount;
    }

    /**
     * 剩餘數量
     *
     * @return
     */
    public String getRemaining_amount() {
        return remaining_amount;
    }

    public void setRemaining_amount(String remaining_amount) {
        this.remaining_amount = remaining_amount;
    }
}
