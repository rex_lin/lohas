package com.acloud.app.model;

/**
 * @author RavenYang
 * @version 20150623
 * gmap 地圖
 */
public class GooleMapV3Struct extends BaseStruct {
	private static final long serialVersionUID = -674618585460782942L;
    String
       gmapUrl;
    String gmapPinTitle;
    String gmapLatitude;
    String gmapLongitude;
    String nowLatitude;

    public String getNowLongitude() {
        return nowLongitude;
    }

    public void setNowLongitude(String nowLongitude) {
        this.nowLongitude = nowLongitude;
    }

    public String getNowLatitude() {
        return nowLatitude;
    }

    public void setNowLatitude(String nowLatitude) {
        this.nowLatitude = nowLatitude;
    }

    String nowLongitude;

    public GooleMapV3Struct() {
        super();
    }

    public GooleMapV3Struct(String syscode, String sysmsg) {
        super(syscode, sysmsg);
    }

    public String getGmapUrl() {
        return gmapUrl;
    }

    public void setGmapUrl(String gmapUrl) {
        this.gmapUrl = gmapUrl;
    }

    public String getGmapPinTitle() {
        return gmapPinTitle;
    }

    public void setGmapPinTitle(String gmapPinTitle) {
        this.gmapPinTitle = gmapPinTitle;
    }

    public String getGmapLatitude() {
        return gmapLatitude;
    }

    public void setGmapLatitude(String gmapLatitude) {
        this.gmapLatitude = gmapLatitude;
    }

    public String getGmapLongitude() {
        return gmapLongitude;
    }

    public void setGmapLongitude(String gmapLongitude) {
        this.gmapLongitude = gmapLongitude;
    }
}
