package com.acloud.app.model;

/**
 * Created by skywind-10 on 2015/7/15.
 */
public class StoreDetailCouponResult {
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCoupon_id() {
        return coupon_id;
    }

    public void setCoupon_id(String coupon_id) {
        this.coupon_id = coupon_id;
    }

    String title;
    String coupon_id;
}
