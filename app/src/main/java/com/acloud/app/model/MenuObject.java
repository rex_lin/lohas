package com.acloud.app.model;

/**
 * Created by skywind on 2015/8/21.
 */
public class MenuObject {
    String id;

    public MenuObject(String id,String titile) {
        super();
        this.id = id;
        this.titile = titile;
    }

    public String getTitile() {
        return titile;
    }

    public void setTitile(String titile) {
        this.titile = titile;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    String titile;
}
