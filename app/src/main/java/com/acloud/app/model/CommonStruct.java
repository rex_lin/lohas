package com.acloud.app.model;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * @author RavenYang
 * @version 20150623
 * 通用資料結構 , 最基本承接單純api是否成功
 */
public class CommonStruct extends BaseStruct {
	private static final long serialVersionUID = -674618586460782942L;
    JSONArray jsonTestArr=new JSONArray();



    JSONObject jsonTestObj = new JSONObject();

    public CommonStruct() {
        super();
    }

    /**
     * @param syscode 回應碼
     * @param sysmsg 回應訊息
     */
    public CommonStruct(String syscode, String sysmsg) {
        super(syscode, sysmsg);
    }

    /**
     * 讀出json array
     */
    public JSONArray getJsonTestArr() {
        return jsonTestArr;
    }

    /**
     * @param jsonTestArr JsonArray ,存入整串用的
     */
    public void setJsonTestArr(JSONArray jsonTestArr) {
        this.jsonTestArr = jsonTestArr;
    }

    public JSONObject getJsonTestObj() {
        return jsonTestObj;
    }

    public void setJsonTestObj(JSONObject jsonTestObj) {
        this.jsonTestObj = jsonTestObj;
    }

}
