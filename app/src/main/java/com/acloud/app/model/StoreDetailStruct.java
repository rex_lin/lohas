package com.acloud.app.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author RavenYang
 * @version 20150623
 * 商店詳細資料
 */
public class StoreDetailStruct extends BaseStruct {
	private static final long serialVersionUID = -674518586461782942L;
    String store_id="";//店家ID
    String store_name="";//店家名稱
    String rating="";//評價
    String rating_num="";//評價人數
    String address="";//地址
    String lat="";//緯度
    String lng="";//經度
    String distance="";//距離
    String phone="";//電話
    String user_rating="";//若傳uid參數，查詢該使用者的評分並回傳，無則-1，沒有評分資訊
    String user_fav="";//若1
    String fav_num="";//收藏數
    String avg_price="";//均消/消費金額
    String business_hours="";//營業時間
    String content="";//說明
    String sd_id="";//商圈ID
    String sd_name="";//商圈名稱
    String sca_id="";//分類屬性ID
    String sca_name="";//分類屬性名稱
    List<String> images =new ArrayList<String>();//圖片連結字串陣列 Array

	public StoreDetailStruct() {
        super();
    }

    public StoreDetailStruct(String syscode, String sysmsg) {
        super(syscode, sysmsg);
    }

    public String getStore_id() {
        return store_id;
    }

    public String getStore_name() {
        return store_name;
    }

    public String getRating() {
        return rating;
    }

    public String getRating_num() {
        return rating_num;
    }

    public String getAddress() {
        return address;
    }

    public String getLat() {
        return lat;
    }

    public String getLng() {
        return lng;
    }

    public String getDistance() {
        return distance;
    }

    public String getPhone() {
        return phone;
    }

    public String getUser_rating() {
        return user_rating;
    }

    public String getFav_num() {
        return fav_num;
    }

    public String getAvg_price() {
        return avg_price;
    }

    public String getBusiness_hours() {
        return business_hours;
    }

    public String getContent() {
        return content;
    }

    public String getSd_id() {
        return sd_id;
    }

    public String getSd_name() {
        return sd_name;
    }

    public String getSca_id() {
        return sca_id;
    }

    public String getSca_name() {
        return sca_name;
    }

    public List<String> getImages() {
        return images;
    }

    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    public void setStore_name(String store_name) {
        this.store_name = store_name;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public void setRating_num(String rating_num) {
        this.rating_num = rating_num;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setUser_rating(String user_rating) {
        this.user_rating = user_rating;
    }

    public void setFav_num(String fav_num) {
        this.fav_num = fav_num;
    }

    public void setAvg_price(String avg_price) {
        this.avg_price = avg_price;
    }

    public void setBusiness_hours(String business_hours) {
        this.business_hours = business_hours;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setSd_id(String sd_id) {
        this.sd_id = sd_id;
    }

    public void setSd_name(String sd_name) {
        this.sd_name = sd_name;
    }

    public void setSca_id(String sca_id) {
        this.sca_id = sca_id;
    }

    public void setSca_name(String sca_name) {
        this.sca_name = sca_name;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }
}
