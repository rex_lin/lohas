package com.acloud.app;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PersistableBundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Toast;

import com.acloud.app.dialog.CustomAdvPopupWindow;
import com.acloud.app.dialog.CustomDialog;
import com.acloud.app.dialog.CustomDialogOrange;
import com.acloud.app.dialog.CustomPopupActivityWindow;
import com.acloud.app.dialog.CustomPopupWindow;
import com.acloud.app.dialog.CustomProgressDialog;
import com.acloud.app.fragment.AboutFragment;
import com.acloud.app.fragment.BounsRecordFragment;
import com.acloud.app.fragment.CouponDetailFragment;
import com.acloud.app.fragment.CouponListFragment;
import com.acloud.app.fragment.FavoriteFragment;
import com.acloud.app.fragment.GroupChatFragment;
import com.acloud.app.fragment.HappyFunFragment;
import com.acloud.app.fragment.LoginFragment;
import com.acloud.app.fragment.MainFragment;
import com.acloud.app.fragment.MyCouponDetailFragment;
import com.acloud.app.fragment.MyCouponFragment;
import com.acloud.app.fragment.OfferNumberFragment;
import com.acloud.app.fragment.OnlineShopFragment;
import com.acloud.app.fragment.SettingFragment;
import com.acloud.app.fragment.ShareFragment;
import com.acloud.app.fragment.ShowQRcodeFragment;
import com.acloud.app.fragment.StoreDetailFragment;
import com.acloud.app.fragment.StoreFragment;
import com.acloud.app.http.HttpGetMemberInfo;
import com.acloud.app.inferface.BaseInterface;
import com.acloud.app.model.CommonStruct;
import com.acloud.app.task.ActivityDetailQueryTask;
import com.acloud.app.task.ActivityListQueryTask;
import com.acloud.app.task.CouponDetailQueryTask;
import com.acloud.app.task.CouponListQueryTask;
import com.acloud.app.task.GetMemberInfoTask;
import com.acloud.app.task.MyCouponDetailTask;
import com.acloud.app.task.ProfileQueryTask;
import com.acloud.app.thread.DeviceCheckThread;
import com.acloud.app.thread.FavoriteIdsListThread;
import com.acloud.app.thread.FavoriteListThread;
import com.acloud.app.thread.GetStoreInfoThread;
import com.acloud.app.thread.StoreViewThread;
import com.acloud.app.thread.UpdateCollectionThread;
import com.acloud.app.tool.Pub;
import com.acloud.app.tool.TLog;
import com.acloud.app.util.BitmapHandler;
import com.acloud.app.util.GCMClientManager;
import com.acloud.app.util.LocatUtil;
import com.acloud.app.util.PrefConstant;
import com.acloud.app.util.UserProfileManager;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.huanuage.HttpClient.utils.APICallbacks;
import com.huanuage.basenewfoundtion.AccountProfileFragment;
import com.huanuage.basenewfoundtion.ChangePasswordFragment;
import com.huanuage.basenewfoundtion.LoginActivity;
import com.huanuage.basenewfoundtion.MyPointsFragment;
import com.huanuage.basenewfoundtion.apiclient.APIClientToAuth;
import com.huanuage.basenewfoundtion.model.LoginAccount;
import com.huanuage.basenewfoundtion.untils.SharedPreferencesHelper;
import com.huanuage.basenewfoundtion.untils.Utils;

import java.util.HashMap;


public class MainActivity extends ActionBarActivity implements NavigationDrawerCallbacks,
        NavigationDrawerFragment.loginClick, BaseInterface, FragmentManager.OnBackStackChangedListener {

    private Toolbar mToolbar;

    public NavigationDrawerFragment getmNavigationDrawerFragment() {
        return mNavigationDrawerFragment;
    }

    private NavigationDrawerFragment mNavigationDrawerFragment;
    private FragmentManager fragmentManager = this.getSupportFragmentManager();
    public Fragment nextFragment = null;
    private LocatUtil locatUtil;

    private String TAG = "MainActivity";
    private ActionBarDrawerToggle mActionBarDrawerToggle;

    private Activity mActivity;
    private GCMClientManager pushClientManager;
    private String PROJECT_NUMBER = Pub.GCM_PROJECT_NUMBER;

    public CustomProgressDialog mCustomProgressDialog;

    public int getToolBarHeight() {
        return ToolBarHeight;
    }

    public int getMStatusBarHeight() {
        return mStatusBarHeight;
    }

    public float getDPI() {
        return DPI;
    }

    public int ToolBarHeight, mStatusBarHeight;
    public float DPI;
    Bundle msavedInstanceState;
    public static final int MAIN_PAGE = 0;
    public static final int STORE_PAGE = 1;
    public static final int PROFILE_PAGE = 2;
    public static final int COLLOCATION_PAGE = 3;
    public static final int BONUS_RECORD_PAGE = 4;
    public static final int ACTIVITY_LIST_PAGE = 5;
    public static final int COUPON_LIST_PAGE = 6;
    public static final int SHARE_PAGE = 7;
    public static final int OFFER_NUMBER_PAGE = 8;
    public static final int ABOUT_PAGE = 9;
    public static final int SETTING_PAGE = 10;
    public static final int LOGOUT_PAGE = 11;
    public static final int MODIFY_PASSWORD = 12;
    public static final int ONLINE_SHOP = 13;
    public static final int HAPPY_FUN = 14;
    public static final int GROUP_CHAT = 15;

    Fragment currentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TLog.i("Ernest", "Activity bar onCreate");
        TLog.e(TAG, "DPI : " + BitmapHandler.getDensity(this));
        String token = SharedPreferencesHelper.getAuthorization(this);
        TLog.d("ooxx", this.getClass().getName() + " " + "token : " + token);
        GetMemberInfoTask getMemberInfoTask = new GetMemberInfoTask(this , token , new Handler(){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what){
                    case 0:
                        String result = (String)msg.obj;
// TODO
                        if(result.equals("false")){
                            Toast.makeText(mActivity, "登入失敗(Skywind)" , Toast.LENGTH_LONG).show();
                            Utils.Logout(mActivity);
                        }
                        break;
                }
            }
        });
//        PrefConstant.setString(this, Pub.VALUE_UID, token);

        DPI = BitmapHandler.getDensity(this);
        mActivity = this;

        mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(mToolbar);

        //initGCM
        initGoogleGCM();
        mNavigationDrawerFragment = (NavigationDrawerFragment) getFragmentManager().findFragmentById(R.id.fragment_drawer);
        mNavigationDrawerFragment.changeLoginStatus();
        mNavigationDrawerFragment.setup(R.id.fragment_drawer, (DrawerLayout) findViewById(R.id.drawer), mToolbar);

        mActionBarDrawerToggle = mNavigationDrawerFragment.getActionBarDrawerToggle();
        getSupportFragmentManager().addOnBackStackChangedListener(this);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getSupportActionBar() != null && getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    getSupportFragmentManager().popBackStack();
                } else {
                    mNavigationDrawerFragment.openDrawer();
                }
            }
        });
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
//        try {
//            PackageInfo info = getPackageManager().getPackageInfo(
//                    "com.acloud.app",
//                    PackageManager.GET_SIGNATURES);
//            for (Signature signature : info.signatures) {
//                MessageDigest md = MessageDigest.getInstance("SHA");
//                md.update(signature.toByteArray());
//                TLog.i("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
//
//            }
//        } catch (PackageManager.NameNotFoundException e) {
//
//        } catch (NoSuchAlgorithmException e) {
//
//        }
        getWindowProperty();
        //TODO GSP Check
//        if (!locatUtil.isOpenGps()){
//            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//            startActivity(intent);

        Utils.getAvailablePoints(MainActivity.this, new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        Toast.makeText(MainActivity.this, String.format("狀態 : %d\n點數 : %d", msg.what, msg.arg1), Toast.LENGTH_LONG).show();
                    }
                });

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        TLog.i("Ernest", "onPostResume");
    }

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        TLog.i("Ernest", "Activity onPostResume");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        TLog.i("Ernest", "ActivityResult mainActivity");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onResume() {
        super.onResume();
        TLog.i("Ernest", "onResume");
        msavedInstanceState = null;
        if (PrefConstant.getString(MainActivity.this, Pub.VALUE_UID, "").equals(""))
            return;

        FavoriteIdsListThread thread = new FavoriteIdsListThread(getApplicationContext(),
                            PrefConstant.getString(MainActivity.this, Pub.VALUE_UID, ""),
                            PrefConstant.getString(MainActivity.this, Pub.VALUE_AUTHTOKEN, ""),
                            new Handler() {
                                @Override
                                public void handleMessage(Message msg) {
                                    super.handleMessage(msg);

                                    switch (msg.what) {
                                        case 0:
                                            break;
                                        case 1:
                                            TLog.i(TAG, "Favorite " + ((CommonStruct) msg.getData().getSerializable(Pub.RETURN_VALUE_KEY)).getJsonTestArr());
                                            PrefConstant.setFavStoreList(MainActivity.this,
                                                    ((CommonStruct) msg.getData().getSerializable(Pub.RETURN_VALUE_KEY)).getJsonTestArr());
                                            break;
                                    }

                                }
                });
        thread.start();
        new DeviceCheckThread(
                MainActivity.this,
                PrefConstant.getString(MainActivity.this, Pub.VALUE_UID, ""),
                PrefConstant.getString(MainActivity.this, Pub.VALUE_AUTHTOKEN, ""),
                PrefConstant.getString(MainActivity.this, Pub.VALUE_LAST_PUSH_TOKEN, ""),
                new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        super.handleMessage(msg);
                        switch (msg.what) {
                            case 0:
                                break;
                            case 1:
                                TLog.i(TAG, "DeviceCheck " + ((CommonStruct) msg.getData().getSerializable(Pub.RETURN_VALUE_KEY)).getJsonTestObj());
                                //TODO 當確認要登出時,要呈現的畫面
//                                Toast.makeText(MainActivity.this, "請登出帳號", Toast.LENGTH_SHORT).show();
                                break;
                        }
                    }
                }
        ).start();
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        if (locatUtil == null)
            locatUtil = new LocatUtil(this);
        Location lc = locatUtil.getUpdatedLocation();
        //TODO  測試更快取得GPS
        TLog.i("Ernest", "onNavigationDrawerItemSelected");

        switch (position) {
            case MAIN_PAGE:
                changeFragment(new MainFragment());

                break;
            case STORE_PAGE:
//                TLog.i("Ernest", "Location : " + lc);
//                StoreListQueryTask storeListQueryTask = new StoreListQueryTask(MainActivity.this, mCustomProgressDialog,
//                        lc != null ? lc.getLatitude() + "" : "0",
//                        lc != null ? lc.getLongitude() + "" : "0");
//                storeListQueryTask.execute();
                changeFragment(new StoreFragment());
                break;
            case PROFILE_PAGE:
                //個人資料
                //先下載完資料 再進入畫面
                //處理Profile動作
//                ProfileQueryTask mProfileTask = new ProfileQueryTask(mActivity, mCustomProgressDialog);
//                mProfileTask.execute();
                setTitle(getString(R.string.drawer_item2));
                Fragment profilePageFragment = new AccountProfileFragment();
                profilePageFragment.setArguments(getIntent().getExtras());
                changeFragment(profilePageFragment);

                break;
            case COLLOCATION_PAGE:
                //預先讀取
                final CustomProgressDialog dialog = new CustomProgressDialog(this);
                final FavoriteListThread collectionListThread = new FavoriteListThread(
                        this,
                        PrefConstant.getString(MainActivity.this, Pub.VALUE_UID, ""),
                        PrefConstant.getString(MainActivity.this, Pub.VALUE_AUTHTOKEN, ""),
                        new Handler() {
                            @Override
                            public void handleMessage(Message msg) {
                                dialog.dismiss();
                                switch (msg.what) {
                                    case 1:
                                        //正常
                                        nextFragment = new FavoriteFragment();
                                        nextFragment.setArguments(msg.getData());
                                        fragmentManager.beginTransaction()
                                                .replace(R.id.container, nextFragment)
                                                .commit();
                                        break;
                                    case 0:
                                        //TODO 連線失敗提示錯誤訊息

                                        break;
                                }
                            }
                        }
                );

                dialog.initProgressDialog(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //TODO 取消的時候去做停止的事情
                                collectionListThread.stop();
                                dialog.dismiss();
                            }
                        }
                );

                dialog.show();
                collectionListThread.start();
                break;
            case BONUS_RECORD_PAGE:
//                changeFragment(new BounsRecordFragment());
                setTitle(getString(R.string.drawer_item4));
                Fragment bonusRecordPageFragment = new MyPointsFragment();
                bonusRecordPageFragment.setArguments(getIntent().getExtras());
                changeFragment(bonusRecordPageFragment);
                break;
            case ACTIVITY_LIST_PAGE:
                ActivityListQueryTask activityListQueryTask = new ActivityListQueryTask(mActivity, "0", "0");
                activityListQueryTask.execute();
                break;
            case COUPON_LIST_PAGE:
                //CouponList
                CouponListQueryTask couponListQueryTask = new CouponListQueryTask(mActivity, mCustomProgressDialog,
                        lc != null ? lc.getLatitude() + "" : "0",
                        lc != null ? lc.getLongitude() + "" : "0");
                couponListQueryTask.execute();
                break;
            case SHARE_PAGE:
                //Share
                changeFragment(new ShareFragment());
                break;
            case OFFER_NUMBER_PAGE:
                //
                changeFragment(new OfferNumberFragment());
                break;
            case ABOUT_PAGE:
                //About us
                changeFragment(new AboutFragment());
                break;
            case SETTING_PAGE:
                changeFragment(new SettingFragment());
                //Setting
                break;
            case LOGOUT_PAGE:
                Utils.Logout(mActivity);

                //Setting
                break;
            case MODIFY_PASSWORD:
                setTitle(getString(R.string.drawer_item10_3));
                Fragment changePasswordFragment = new ChangePasswordFragment();
                changePasswordFragment.setArguments(getIntent().getExtras());
                changeFragment(changePasswordFragment);
                break;
            case HAPPY_FUN:
                changeFragment(new HappyFunFragment());
                break;
            case GROUP_CHAT:
                break;
            case ONLINE_SHOP:
                changeFragment(new OnlineShopFragment());
                break;
            default:
                nextFragment = new MainFragment();
                fragmentManager.beginTransaction()
                        .replace(R.id.container, nextFragment)
                        .commit();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        CustomPopupWindow.closeAllPopupWindow();
        CustomAdvPopupWindow.closeAllPopupWindow();
        CustomPopupActivityWindow.closeAllPopupWindow();

        if (mNavigationDrawerFragment.isDrawerOpen())
            mNavigationDrawerFragment.closeDrawer();
        else {

            if(currentFragment instanceof HappyFunFragment){
                boolean canGoBack = ((HappyFunFragment)currentFragment).canGoBack();
                if (canGoBack)
                    return;
            }
            if(currentFragment instanceof OnlineShopFragment){
                boolean canGoBack = ((OnlineShopFragment)currentFragment).canGoBack();
                TLog.d("ooxx",this.getClass().getName() +" "+ "instanceof canGoBack : " + canGoBack );
                if (canGoBack)
                    return;
            }
            if(currentFragment instanceof AboutFragment){
                boolean canGoBack = ((AboutFragment)currentFragment).canGoBack();
                if (canGoBack)
                    return;
            }


            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                super.onBackPressed();
            } else {
                final CustomDialogOrange leaveDialog = new CustomDialogOrange(MainActivity.this,
                        getString(R.string.txt_do_you_leave));
                leaveDialog.initLeaveDialog(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        leaveDialog.dismiss();
                        finish();
                    }
                }, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        leaveDialog.dismiss();
                    }
                });
                leaveDialog.show();
            }
        }
    }

    @Override
    public void changeFragment(Fragment fragment) {
        currentFragment = fragment;
        TLog.i("Ernest", "msavedInstanceState : " + msavedInstanceState);
        if (msavedInstanceState != null)
            return;
        try {
            nextFragment = fragment;
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE); //清空popBackStack
            fragmentManager.beginTransaction()
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .replace(R.id.container, nextFragment)
                    .commit();

        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void changeBackFragment(Fragment fragment) {
        if (msavedInstanceState != null)
            return;

        try {
            nextFragment = fragment;
            fragmentManager.beginTransaction()
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .replace(R.id.container, nextFragment)
                    .addToBackStack(null) // if wanted can go back //TODO  why backstack is null
                    .commit();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLoginClick() {
        //判斷Login狀態
        if (PrefConstant.getBoolean(MainActivity.this, Pub.IS_LOGIN, false)) {
            FacebookSdk.sdkInitialize(getApplicationContext());
            LoginManager.getInstance().logOut();
            PrefConstant.setBoolean(MainActivity.this, Pub.IS_LOGIN, false);
            changeLoginStatus();
        } else {
            changeFragment(new LoginFragment());
            changeLoginStatus();
        }


    }

    /**
     * 設定TitleBar文字
     *
     * @param i
     */
    public void setTitle(String i) {
        if (mToolbar != null)
            mToolbar.setTitle(i);
    }

    @Override
    public void showErrorDialog(String code) {
        //TODO showErrorDialog
    }

    @Override
    public void clickItem(String itemId) {
        TLog.d("ooxx", this.getClass().getName() + " " + "itemId : " + itemId);
        Location lc = locatUtil.getUpdatedLocation();
//        TLog.i("Ernest", "Location : " + lc + " select : " + mNavigationDrawerFragment.getmCurrentSelectedPosition());

        int position = mNavigationDrawerFragment.getmCurrentSelectedPosition();

        switch (position) {
            case MAIN_PAGE:
                if (itemId.equals(STORE_PAGE + "")) {
                    mNavigationDrawerFragment.selectItem(STORE_PAGE);
                }
                if (itemId.equals(ACTIVITY_LIST_PAGE + "")) {
                    mNavigationDrawerFragment.selectItem(ACTIVITY_LIST_PAGE);
                }
                if (itemId.equals(COUPON_LIST_PAGE + "")) {
                    mNavigationDrawerFragment.selectItem(COUPON_LIST_PAGE);
                }
                if (itemId.equals(ONLINE_SHOP + "")) {
                    mNavigationDrawerFragment.selectItem(ONLINE_SHOP);
                }
                if (itemId.equals(GROUP_CHAT + "")) {
                    mNavigationDrawerFragment.selectItem(GROUP_CHAT);
                }
                if (itemId.equals(HAPPY_FUN + "")) {
                    mNavigationDrawerFragment.selectItem(HAPPY_FUN);
                }
                break;
            case STORE_PAGE:
                new GetStoreInfoThread(
                        MainActivity.this,
                        PrefConstant.getString(MainActivity.this, Pub.VALUE_UID, ""),

                        PrefConstant.getString(MainActivity.this, Pub.VALUE_AUTHTOKEN, ""),
                        itemId,
                        lc != null ? lc.getLatitude() + "" : "0",
                        lc != null ? lc.getLongitude() + "" : "0",
                        new Handler() {
                            @Override
                            public void handleMessage(Message msg) {
                                switch (msg.what) {
                                    case 1:
                                        //正常
                                        nextFragment = new StoreDetailFragment();
                                        nextFragment.setArguments(msg.getData());
                                        fragmentManager.beginTransaction()
                                                .replace(R.id.container, nextFragment)
                                                .addToBackStack("Detail")
                                                .commit();
                                        break;
                                    case 0:
                                        //TODO 連線失敗提示錯誤訊息

                                        break;
                                }
                            }
                        }

                ).start();
                //TODO檢查必要多做的可能
                new StoreViewThread(
                        MainActivity.this,
                        PrefConstant.getString(MainActivity.this, Pub.VALUE_UID, ""),

                        PrefConstant.getString(MainActivity.this, Pub.VALUE_AUTHTOKEN, ""),
                        itemId,
                        new Handler() {
                            @Override
                            public void handleMessage(Message msg) {
                                super.handleMessage(msg);
                                switch (msg.what) {
                                    case 1:
                                        //TODO 成功
                                        TLog.i(TAG, "StoreView success");
                                        break;
                                    case 0:
                                        //TODO 失敗
                                        break;
                                }
                            }
                        }

                ).start();

                break;
            case COLLOCATION_PAGE:
                // 跳轉到店家詳細
                new GetStoreInfoThread(
                        MainActivity.this,
                        PrefConstant.getString(MainActivity.this, Pub.VALUE_UID, ""),

                        PrefConstant.getString(MainActivity.this, Pub.VALUE_AUTHTOKEN, ""),
                        itemId,
                        lc != null ? lc.getLatitude() + "" : "0",
                        lc != null ? lc.getLongitude() + "" : "0",
                        new Handler() {
                            @Override
                            public void handleMessage(Message msg) {
                                switch (msg.what) {
                                    case 1:
                                        //正常
                                        nextFragment = new StoreDetailFragment();
                                        nextFragment.setArguments(msg.getData());
                                        fragmentManager.beginTransaction()
                                                .replace(R.id.container, nextFragment)
                                                .addToBackStack(null)
                                                .commit();
                                        break;
                                    case 0:
                                        //TODO 連線失敗提示錯誤訊息

                                        break;
                                }
                            }
                        }

                ).start();
                //TODO 必要多做的可能
                new StoreViewThread(
                        MainActivity.this,
                        PrefConstant.getString(MainActivity.this, Pub.VALUE_UID, ""),

                        PrefConstant.getString(MainActivity.this, Pub.VALUE_AUTHTOKEN, ""),
                        itemId,
                        new Handler() {
                            @Override
                            public void handleMessage(Message msg) {
                                super.handleMessage(msg);
                                switch (msg.what) {
                                    case 1:
                                        //TODO 成功
                                        TLog.i(TAG, "StoreView success");
                                        break;
                                    case 0:
                                        //TODO 失敗
                                        break;
                                }
                            }
                        }

                ).

                        start();

                break;
            case ACTIVITY_LIST_PAGE:
                //活動專區明細
                ActivityDetailQueryTask detailQueryTask = new ActivityDetailQueryTask(MainActivity.this,
                        PrefConstant.getString(MainActivity.this, Pub.VALUE_UID, ""),
                        PrefConstant.getString(MainActivity.this, Pub.VALUE_AUTHTOKEN, ""),
                        itemId,
                        lc != null ? lc.getLatitude() + "" : "0",
                        lc != null ? lc.getLongitude() + "" : "0");
                detailQueryTask.execute();
                break;
            case COUPON_LIST_PAGE:
                //優惠卷明細頁 //TODO有問題
                if (nextFragment instanceof CouponListFragment || nextFragment instanceof CouponDetailFragment ||
                        nextFragment instanceof StoreDetailFragment)

                {
                    CouponDetailQueryTask task = new CouponDetailQueryTask(MainActivity.this,
                            mCustomProgressDialog,
                            PrefConstant.getString(MainActivity.this, Pub.VALUE_UID, ""),
                            PrefConstant.getString(MainActivity.this, Pub.VALUE_AUTHTOKEN, ""),
                            itemId,
                            lc != null ? lc.getLatitude() + "" : "0",
                            lc != null ? lc.getLongitude() + "" : "0",
                            ""
                    );
                    task.execute();
                }

                if (nextFragment instanceof MyCouponFragment || nextFragment instanceof MyCouponDetailFragment ||
                        nextFragment instanceof ShowQRcodeFragment || nextFragment instanceof StoreDetailFragment)

                {
                    MyCouponDetailTask detailTask = new MyCouponDetailTask(MainActivity.this,
                            PrefConstant.getString(MainActivity.this, Pub.VALUE_UID, ""),
                            PrefConstant.getString(MainActivity.this, Pub.VALUE_AUTHTOKEN, ""),
                            itemId,
                            lc != null ? lc.getLatitude() + "" : "0",
                            lc != null ? lc.getLongitude() + "" : "0"
                    );
                    detailTask.execute();
                }

                break;
            default:
                break;
        }

    }

    @Override
    public void longClickItem(final String itemId) {
        final CustomDialog removeFevDialog = new CustomDialog(this);

        switch (mNavigationDrawerFragment.getmCurrentSelectedPosition()) {
            case COLLOCATION_PAGE:
                removeFevDialog.initRemoveFevDialog(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //確認

                                Location lc = locatUtil.getUpdatedLocation();
                                TLog.i("Ernest", "Location : " + lc);
                                new UpdateCollectionThread(
                                        MainActivity.this,
                                        PrefConstant.getString(MainActivity.this, Pub.VALUE_UID, ""),
                                        PrefConstant.getString(MainActivity.this, Pub.VALUE_AUTHTOKEN, ""),
                                        itemId,
                                        Pub.ACTION_REMOVE,
                                        lc != null ? lc.getLatitude() + "" : "0",
                                        lc != null ? lc.getLongitude() + "" : "0",
                                        new Handler() {
                                            @Override
                                            public void handleMessage(Message msg) {
                                                switch (msg.what) {
                                                    case 1:
                                                        //正常
                                                        ((FavoriteFragment) nextFragment).removeListItem(itemId);
                                                        break;
                                                    case 0:
                                                        //TODO 連線失敗提示錯誤訊息

                                                        break;
                                                }
                                            }
                                        }
                                ).start();

                                removeFevDialog.dismiss();
                            }
                        },
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //取消
                                removeFevDialog.dismiss();
                            }
                        }
                );
                break;

            default:
                break;
        }


        removeFevDialog.show();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // do something useful
                return (true);
        }

        return (super.onOptionsItemSelected(item));
    }

    @Override
    public void onBackStackChanged() {
        shouldDisplayHomeUpButton();
        TLog.i(TAG, "onBackStackChanged");
    }

    public void shouldDisplayHomeUpButton() {
        //Show Up Button
        if (getSupportActionBar() != null) {
            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                mActionBarDrawerToggle.setDrawerIndicatorEnabled(false); // order matters
                mNavigationDrawerFragment.showBackButton();
            } else {
                mNavigationDrawerFragment.showDrawerButton();
                mActionBarDrawerToggle.setDrawerIndicatorEnabled(true);
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        //This method is called when the up button is pressed. Just the pop back stack.
        getSupportFragmentManager().popBackStack();
        return true;
    }

    /**
     * Google GCM
     */
    public void initGoogleGCM() {
        pushClientManager = new GCMClientManager(this, PROJECT_NUMBER);
        pushClientManager.registerIfNeeded(new GCMClientManager.RegistrationCompletedHandler() {
            @Override
            public void onSuccess(String registrationId, boolean isNewRegistration) {
            }

            @Override
            public void onFailure(String ex) {
                super.onFailure(ex);
            }
        });
    }


    public LocatUtil getLocatUtil() {
        return locatUtil;
    }

    /**
     * StoreCouponDetail
     *
     * @param coupon_id
     */
    public void CouponDetail(String coupon_id) {
        Location lc = locatUtil.getUpdatedLocation();
        TLog.i("Ernest", "Location : " + lc);
        CouponDetailQueryTask task = new CouponDetailQueryTask(MainActivity.this,
                mCustomProgressDialog,
                PrefConstant.getString(MainActivity.this, Pub.VALUE_UID, ""),
                PrefConstant.getString(MainActivity.this, Pub.VALUE_AUTHTOKEN, ""),
                coupon_id,
                lc != null ? lc.getLatitude() + "" : "0",
                lc != null ? lc.getLongitude() + "" : "0",
                "store"
        );
        task.execute();
    }

    public void changeLoginStatus() {
        mNavigationDrawerFragment.changeLoginStatus();
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    private void getWindowProperty() {
        ViewTreeObserver vto = mToolbar.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    mToolbar.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                } else {
                    mToolbar.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
                ToolBarHeight = mToolbar.getHeight();
                mStatusBarHeight = getStatusBarHeight();
            }
        });

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        TLog.i("Ernest", " onSavedInstanceState");
        msavedInstanceState = outState;
    }
}
