package com.acloud.app;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.hardware.Camera;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.acloud.app.fragment.ActivityDetailFragment;
import com.acloud.app.http.HttpQRcodeScan;
import com.acloud.app.tool.Pub;
import com.acloud.app.tool.TLog;
import com.acloud.app.util.PrefConstant;
import com.acloud.app.view.CameraPreview;

import net.sourceforge.zbar.Config;
import net.sourceforge.zbar.Image;
import net.sourceforge.zbar.ImageScanner;
import net.sourceforge.zbar.Symbol;
import net.sourceforge.zbar.SymbolSet;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class qrcodescanner extends AppCompatActivity {
    private Camera mCamera;
    private CameraPreview mPreview;
    private Handler autoFocusHandler;

    private ImageView btnScan;
    private ImageScanner scanner;

    private boolean barcodeScanned = false;
    private boolean previewing = true;

    String qr_code_id;
    String activity_id;
    String store_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrcodescanner);
        System.loadLibrary( "iconv" );

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(mToolbar);
        try {

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }catch (NullPointerException e){
            e.printStackTrace();
        }

        //TODO 返回鍵
        initControls();
    }

    private void initControls() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        autoFocusHandler = new Handler();
        mCamera = getCameraInstance();

        // Instance barcode scanner
        scanner = new ImageScanner();
        scanner.setConfig(0, Config.X_DENSITY, 3);
        scanner.setConfig(0, Config.Y_DENSITY, 3);

        mPreview = new CameraPreview(qrcodescanner.this, mCamera, previewCb,
                autoFocusCB);
        FrameLayout preview = (FrameLayout) findViewById(R.id.cameraPreview);
        preview.addView(mPreview);

        btnScan = (ImageView) findViewById(R.id.btnScan);

        btnScan.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (barcodeScanned) {
                    barcodeScanned = false;
                    mCamera.setPreviewCallback(previewCb);
                    mCamera.startPreview();
                    previewing = true;
                    mCamera.autoFocus(autoFocusCB);
                }
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            // SCAdminTapToScanScreen.isFromAssetDetail = false;
            releaseCamera();
        }
        return super.onKeyDown(keyCode, event);
    }


    /**
     * A safe way to get an instance of the Camera object.
     */
    public static Camera getCameraInstance() {
        Camera c = null;
        try {
            c = Camera.open();
        } catch (Exception e) {
        }
        return c;
    }

    private void releaseCamera() {
        if (mCamera != null) {
            previewing = false;
            mCamera.setPreviewCallback(null);
            mCamera.release();
            mCamera = null;
        }
    }

    private Runnable doAutoFocus = new Runnable() {
        public void run() {
            if (previewing)
                mCamera.autoFocus(autoFocusCB);
        }
    };

    Camera.PreviewCallback previewCb = new Camera.PreviewCallback() {
        public void onPreviewFrame(byte[] data, Camera camera) {
            Camera.Parameters parameters = camera.getParameters();
            Camera.Size size = parameters.getPreviewSize();

            Image barcode = new Image(size.width, size.height, "Y800");
            barcode.setData(data);

            int result = scanner.scanImage(barcode);

            if (result != 0) {
                previewing = false;
                mCamera.setPreviewCallback(null);
                mCamera.stopPreview();

                SymbolSet syms = scanner.getResults();
                for (Symbol sym : syms) {

                    TLog.i("<<<<<<Asset Code>>>>> ",
                            "<<<<Bar Code>>> " + sym.getData());
                    String scanResult = sym.getData().trim();
                    parserJsonQR(scanResult);
                    barcodeScanned = true;

                    break;
                }
                new ScanTask(activity_id, store_id, qr_code_id, "0", "0").execute();
            }
        }
    };

    // Mimic continuous auto-focusing
    Camera.AutoFocusCallback autoFocusCB = new Camera.AutoFocusCallback() {
        public void onAutoFocus(boolean success, Camera camera) {
            autoFocusHandler.postDelayed(doAutoFocus, 1000);
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {

            case android.R.id.home:
                // app icon in action bar clicked; go home
                releaseCamera();
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private class ScanTask extends AsyncTask {
        ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
        HttpQRcodeScan qRcodeScan;

        public ScanTask(String aid, String storeid, String qrcode_id, String lat, String lng) {
            list.add(new BasicNameValuePair(Pub.VALUE_APIKEY, Pub.APIKEY));
            list.add(new BasicNameValuePair(Pub.VALUE_AUTHTOKEN,
                    PrefConstant.getString(qrcodescanner.this, Pub.VALUE_AUTHTOKEN, "")));
            list.add(new BasicNameValuePair(Pub.VALUE_UID,
                    PrefConstant.getString(qrcodescanner.this, Pub.VALUE_UID, "")));
            list.add(new BasicNameValuePair(Pub.VALUE_AID, aid));
            list.add(new BasicNameValuePair(Pub.VALUE_STOREID, storeid));
            list.add(new BasicNameValuePair(Pub.VALUE_QRCODE_ID, qrcode_id));
            list.add(new BasicNameValuePair(Pub.VALUE_LAT, lat));
            list.add(new BasicNameValuePair(Pub.VALUE_LNG, lng));
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            qRcodeScan = new HttpQRcodeScan(qrcodescanner.this, list);
            qRcodeScan.call();
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            if (qRcodeScan.ismSuccess()) {
                if (qRcodeScan.getSyscode().equals("200")) {
                    releaseCamera();
                    //成功
                    Intent i = new Intent();
                    i.putExtra(Pub.VALUE_SYSCODE, qRcodeScan.getSyscode());
                    i.putExtra("obtain_bonus", qRcodeScan.getObtain_bonus());
                    setResult(ActivityDetailFragment.QRCODE_ITEM, i);
                    finish();
                }else if (qRcodeScan.getSyscode().equals("400") || qRcodeScan.getSyscode().equals("503")){
                    releaseCamera();
                    Intent i = new Intent();
                    i.putExtra(Pub.VALUE_SYSCODE, qRcodeScan.getSyscode());
                    setResult(ActivityDetailFragment.QRCODE_ITEM, i);
                    finish();
                }
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
    }


    void parserJsonQR(String message) {
        try {
            JSONObject jsonObject = new JSONObject(message);
            qr_code_id = jsonObject.optString("qr_code_id");
            activity_id = jsonObject.optString("activity_id");
            store_id = jsonObject.optString("store_id");
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


}
