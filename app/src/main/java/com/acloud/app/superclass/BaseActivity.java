package com.acloud.app.superclass;


import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public abstract class BaseActivity extends Activity{
	/*Layout*/

	/*ver*/

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
    @Override
    protected void onResume() {
    	super.onResume();
    }
    
    @Override
    protected void onStop(){
    	super.onStop();
    }
    
    @Override
    public void onBackPressed(){
    	super.onBackPressed();
    }
    
    @Override
    protected void onPause() {
		super.onPause();
	}
    
	@Override
	protected void onDestroy(){
		super.onDestroy();
	}
	
	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
//		switch(item.getItemId()){
//			case 0:
//				break;
//			case 1:
//				switchTo(HomeActivity.class);
//		}
	  return super.onMenuItemSelected(featureId, item);
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		//第一次建立menu觸發
		return true;
	}
	
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) { 
    	//每次點menu觸發
//    	
//        menu.add(0,0,0,"文字");
//        menu.add(0,1,0,"文字");
 	
     return super.onPrepareOptionsMenu(menu);
    }


}
