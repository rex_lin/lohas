package com.acloud.app.superclass;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.acloud.app.inferface.FragmentInterface;


public abstract class BaseFragment extends Fragment {

	protected FragmentInterface listener = null;

	@Override
	public void onAttach(Activity activity){
		super.onAttach(activity);

		try{
			listener = (FragmentInterface)activity;
		}catch(ClassCastException e){
			e.printStackTrace();
		}

	}

	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);

	}

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
       View v=null;
       return v;
    }

	@Override
	public void onPause(){
		super.onPause();

	}

	@Override
	public void onDestroyView(){
		super.onDestroyView();

	}

	@Override
	public void onStart(){
		super.onStart();

	}

	@Override
	public void onStop(){
    	super.onStop();

    }

	@Override
	public void onResume(){
		super.onResume();

	}

	@Override
	public void onDetach(){
		super.onDetach();

	}

}
