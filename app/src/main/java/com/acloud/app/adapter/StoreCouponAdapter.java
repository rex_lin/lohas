package com.acloud.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.acloud.app.MainActivity;
import com.acloud.app.R;
import com.acloud.app.model.StoreDetailCouponResult;

import java.util.ArrayList;

/**
 * Created by skywind-10 on 2015/7/15.
 */
public class StoreCouponAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private Context mContext;
    ArrayList<StoreDetailCouponResult> list = new ArrayList<StoreDetailCouponResult>();

    public StoreCouponAdapter(Context context, ArrayList<StoreDetailCouponResult> arrayList) {
        inflater = LayoutInflater.from(context);
        mContext = context;
        this.list = arrayList;
    }

    // avoid date is null
    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.item_store_coupon, parent, false);
        TextView tvTitle = (TextView) convertView.findViewById(R.id.tvTitle);
        tvTitle.setText(list.get(position).getTitle());
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) mContext).CouponDetail(list.get(position).getCoupon_id());
            }
        });

        return convertView;
    }
}
