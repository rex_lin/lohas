package com.acloud.app.adapter;

import android.app.Activity;
import android.graphics.BitmapFactory;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.acloud.app.R;
import com.acloud.app.inferface.BaseInterface;
import com.acloud.app.model.MyCouponListResult;
import com.acloud.app.tool.TLog;
import com.acloud.app.util.BitmapHandler;
import com.acloud.app.util.DownLoadHandler;
import com.acloud.app.util.Util;

import java.io.InputStream;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * MyCouponList
 */
public class MyCouponAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    ArrayList<MyCouponListResult> arrayList = new ArrayList<MyCouponListResult>();
    Activity mActivity;

    public MyCouponAdapter(Activity activity, ArrayList<MyCouponListResult> list) {
        inflater = LayoutInflater.from(activity);
        this.arrayList = list;
        mActivity = activity;
    }

    // avoid arrayList is null
    @Override
    public int getCount() {
        if (arrayList == null) return 0;
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.item_mycoupon, parent, false);

        ViewHolder viewHolder = new ViewHolder(
                (TextView) convertView.findViewById(R.id.tvExchangeDate),
                (TextView) convertView.findViewById(R.id.tvStoreName),
//                (TextView) convertView.findViewById(R.id.tvDistance_white),
                (TextView) convertView.findViewById(R.id.tvSubtitle),
                (TextView) convertView.findViewById(R.id.tvPoint),
                (TextView) convertView.findViewById(R.id.tvDate),
                (TextView) convertView.findViewById(R.id.tvUsedTime),
                (LinearLayout) convertView.findViewById(R.id.tvExpire),
                (LinearLayout) convertView.findViewById(R.id.tvUsedTimeLayout),
                (ImageView) convertView.findViewById(R.id.img));


        convertView.setTag(viewHolder);
        viewHolder.tvExchangeDate.setText(String.format(
                mActivity.getResources().getString(R.string.txt_exchange_time), Util.formatDate(arrayList.get(position).getExchange_dateitme())
        ));
        viewHolder.tvStoreName.setText(arrayList.get(position).getStore_name());
//        viewHolder.tvDistance.setText(Util.distanceMtoKM(arrayList.get(position).getDistance()));
        viewHolder.tvSubTitle.setText(arrayList.get(position).getTitle());
        viewHolder.tvPoint.setText(arrayList.get(position).getPoints());
        viewHolder.tvDate.setText(String.format(mActivity.getResources().getString(R.string.txt_end_usetime),
                Util.formatDate(arrayList.get(position).getUsed_end_datetime())));
        viewHolder.tvUsedTime.setText(String.format(mActivity.getResources().getString(R.string.txt_use_time),
                Util.formatDateTime(arrayList.get(position).getUsed_datetime())));

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((BaseInterface) mActivity).clickItem(arrayList.get(position).getEx_coupon_id());
            }
        });

        final DownLoadHandler loadHandler = new DownLoadHandler(viewHolder.imageView);

        //開始讀取圖片
        new Thread() {
            @Override
            public void run() {
                Message msg = new Message();

                try {
                    URL url = new URL(arrayList.get(position).getThumbnail_url());
                    InputStream is = (InputStream) url.getContent();
                    if (is != null) {
                        System.gc();
                        //DownLoad Image
                        is = new BitmapHandler.FlushedInputStream(is);
                        msg.obj = BitmapFactory.decodeStream(is);
                        is.close();
                        System.gc();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (msg.obj != null)
                    loadHandler.sendMessage(msg);

            }
        }.start();

        if (Util.formatDateTime(arrayList.get(position).getUsed_datetime()).equals("0")) {
            viewHolder.tvUseTimeLayout.setVisibility(View.GONE);
            if (range(Long.valueOf(arrayList.get(position).getUsed_end_datetime())) <= 7) {
                viewHolder.tvExpire.setVisibility(View.VISIBLE);
            } else {
                viewHolder.tvExpire.setVisibility(View.GONE);
            }
        } else {
            viewHolder.tvUseTimeLayout.setVisibility(View.VISIBLE);
            viewHolder.tvExpire.setVisibility(View.GONE);
        }


        return convertView;
    }

    private class ViewHolder {
        TextView
                tvExchangeDate,
                tvStoreName,
                tvDistance,
                tvSubTitle,
                tvPoint,
                tvDate,
                tvUsedTime;
        LinearLayout
                tvExpire,
                tvUseTimeLayout;
        ImageView
                imageView;

        public ViewHolder(TextView tvExchangeDate, TextView tvStoreName, TextView tvSubTitle,
                          TextView tvPoint, TextView tvDate, TextView tvUsedTime,
                          LinearLayout tvExpire, LinearLayout tvUseTimeLayout, ImageView imageView) {
            this.tvExchangeDate = tvExchangeDate;
            this.tvStoreName = tvStoreName;
            this.tvSubTitle = tvSubTitle;
            this.tvPoint = tvPoint;
            this.tvDate = tvDate;
            this.tvUsedTime = tvUsedTime;
            this.tvExpire = tvExpire;
            this.tvUseTimeLayout = tvUseTimeLayout;
            this.imageView = imageView;
        }
    }


    public long range(Long time) {
        //取得兩個時間的Unix時間
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

        Date dt1 = null, dt2 = null;
        try {
            dt1 = sdf.parse("2012/12/31 11:49:00");
            dt2 = sdf.parse("2013/01/10 11:49:00");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Long ut1 = dt1.getTime();



        Long ut2 = System.currentTimeMillis();
        //相減獲得兩個時間差距的毫秒
        TLog.i("Ernest", "ut2: " + ut2);
        TLog.i("Ernest", "time: " + time * 1000);
        Long timeP = ut2 - (time * 1000);//毫秒差
        Long day = timeP / (1000 * 60 * 60 * 24);//日差
        TLog.i("Ernest", "day 天: " + Math.abs(day));
        return Math.abs(day);
    }

}
