package com.acloud.app.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.acloud.app.R;
import com.acloud.app.inferface.BaseInterface;
import com.acloud.app.model.FavoriteStruct;
import com.acloud.app.util.BitmapHandler;
import com.acloud.app.util.DownLoadHandler;
import com.acloud.app.util.Util;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class FavoriteAdapter extends BaseAdapter {
    private List<FavoriteStruct.Data> list;
    private Activity activity;
    private LayoutInflater layoutInflater;
    private Bitmap defaultImg = null;

    /**
     * @param activity   activity
     * @param defaultImg 預設圖片
     */
    public FavoriteAdapter(
            Activity activity,
            Bitmap defaultImg) {
        //初始化承接資料
        this.activity = activity;
        layoutInflater = LayoutInflater.from(activity);
        this.list = new ArrayList<FavoriteStruct.Data>();

        this.defaultImg = defaultImg;
    }

    @Override
    public int getCount() {
        //回傳物件數量
        return list.size();
    }

    @Override
    public Object getItem(int location) {
        //回傳物件
        return list.get(location);
    }

    @Override
    public long getItemId(int position) {
        //回傳物件位置
        return position;
    }

    /**
     * @param data 一筆資料
     */
    public void add(FavoriteStruct.Data data) {
        list.add(data);
    }

    /**
     * @param itemsToAdd 一系列料
     */
    public void add(List<FavoriteStruct.Data> itemsToAdd) {
        list.addAll(itemsToAdd);
        //往item加一堆
    }

    /**
     * @param itemsToAdd 重新載入的資料
     */
    public void reAdd(List<FavoriteStruct.Data> itemsToAdd) {
        list = itemsToAdd;

    }

    /**
     * @param itrmId 移除資料用來比對的ID
     */
    public void remove(String itrmId) {
        for (int n = 0; n < list.size(); n++) {
            if (list.get(n).getStore_id().equals(itrmId)) {
                list.remove(n);
                break;
            }
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;

        convertView = layoutInflater.inflate(R.layout.item_collection_store, null);
        //設定item layout
        viewHolder = new ViewHolder(
                (LinearLayout) convertView.findViewById(R.id.box),
                (TextView) convertView.findViewById(R.id.tvName),
                (TextView) convertView.findViewById(R.id.tvAppr),
                (TextView) convertView.findViewById(R.id.tvApp),
                (TextView) convertView.findViewById(R.id.tvCollNum),
                (TextView) convertView.findViewById(R.id.tvPrice),
                (TextView) convertView.findViewById(R.id.tvMall),
                (TextView) convertView.findViewById(R.id.tvProduce),
                (ImageView) convertView.findViewById(R.id.ivProduce),
                (ImageView) convertView.findViewById(R.id.ivHeart)
        );

        convertView.setTag(viewHolder);

        viewHolder.box.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // 點擊事件
                ((BaseInterface) activity).clickItem(list.get(position).getStore_id());
            }
        });

        viewHolder.box.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View arg0) {

                // 長按事件
                ((BaseInterface) activity).longClickItem(list.get(position).getStore_id());
                return true;
            }
        });
        final DownLoadHandler handler = new DownLoadHandler(viewHolder.ivProduce);


        if (!list.get(position).getThumbani_url().equals("")) {

            new Thread() {

                @Override
                public void run() {
                    Message msg = new Message();

                    try {
                        URL url = new URL(list.get(position).getThumbani_url());
                        InputStream is = (InputStream) url.getContent();
                        if (is != null) {
                            System.gc();
                            //DownLoad Image    BitmapHandler class
                            is = new BitmapHandler.FlushedInputStream(is);
                            msg.obj = BitmapFactory.decodeStream(is);
                            is.close();
                            System.gc();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (msg.obj != null)
                        handler.sendMessage(msg);

                }
            }.start();
        } else {

            // 載入預設圖片
            viewHolder.ivProduce.setImageBitmap(defaultImg);
        }


        viewHolder.tvName.setText(list.get(position).getStore_name());
        viewHolder.tvAppr.setText(String.format(activity.getResources().getString(R.string.txt_appra),Util.takeDecimals(list.get(position).getRating())));
        viewHolder.tvApp.setText(list.get(position).getRating_num());
        viewHolder.tvCollNum.setText(String.format(activity.getResources().getString(R.string.txt_collection_num), list.get(position).getFav_num()));
        viewHolder.tvPrice.setText(list.get(position).getAvg_price());
        viewHolder.tvMall.setText(list.get(position).getSd_name());
        viewHolder.tvProduce.setText(list.get(position).getSca_name());


        //開始讀取圖片


        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    //設定view holder
    private class ViewHolder {

        LinearLayout
                box;

        TextView
                tvName,
                tvAppr,
                tvApp,
                tvCollNum,
                tvPrice,
                tvMall,
                tvProduce;

        ImageView ivProduce, ivHeart;

        ViewHolder(LinearLayout box, TextView tvName, TextView tvAppr, TextView tvApp, TextView tvCollNum, TextView tvPrice, TextView tvMall, TextView tvProduce, ImageView ivProduce, ImageView ivHeart) {
            this.box = box;
            this.tvName = tvName;
            this.tvAppr = tvAppr;
            this.tvApp = tvApp;
            this.tvCollNum = tvCollNum;
            this.tvPrice = tvPrice;
            this.tvMall = tvMall;
            this.tvProduce = tvProduce;
            this.ivProduce = ivProduce;
            this.ivHeart = ivHeart;
        }
    }


}

