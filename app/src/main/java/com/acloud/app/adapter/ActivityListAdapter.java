package com.acloud.app.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.acloud.app.R;
import com.acloud.app.inferface.BaseInterface;
import com.acloud.app.model.ActivityListResult;
import com.acloud.app.util.Util;

import java.util.ArrayList;

/**
 * Created by skywind on 2015/8/19.
 */
public class ActivityListAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private Activity mActivity;
    ArrayList<ActivityListResult> arrarList = new ArrayList<ActivityListResult>();
    public ActivityListAdapter(Activity context, ArrayList<ActivityListResult> list) {
        inflater = LayoutInflater.from(context);
        mActivity = context;
        this.arrarList = list;
    }

    @Override
    public int getCount() {
        return arrarList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.item_activity, parent, false);
        final ViewHolder viewHolder = new ViewHolder();
        viewHolder.tvStoreName = (TextView) convertView.findViewById(R.id.tvStoreName);
        viewHolder.tvSubtitle = (TextView) convertView.findViewById(R.id.tvSubtitle);
        viewHolder.tvPoints = (TextView) convertView.findViewById(R.id.tvPoint);
//        viewHolder.tvDistance = (TextView) convertView.findViewById(R.id.tvDistance_white);
        viewHolder.tvDate = (TextView) convertView.findViewById(R.id.tvDate);
        viewHolder.tvStatus = (TextView) convertView.findViewById(R.id.tvStatus);
        viewHolder.imgStatus = (ImageView) convertView.findViewById(R.id.img);


        viewHolder.tvStoreName.setText(arrarList.get(position).getStore_name());
        viewHolder.tvSubtitle.setText(arrarList.get(position).getTitle());
        viewHolder.tvPoints.setText(String.format(mActivity.getString(R.string.txt_activity_point), arrarList.get(position).getPoints()));
//        viewHolder.tvDistance.setText(Util.distanceMtoKM(arrarList.get(position).getDistance()));
        viewHolder.tvDate.setText(String.format(mActivity.getString(R.string.txt_start_date),
                Util.formatDate(arrarList.get(position).getStart_datetime())));
        int status = Integer.valueOf(arrarList.get(position).getStatus());
        switch (status) {
            case 0: //未進行
                viewHolder.imgStatus.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.comingsoon));
                viewHolder.tvStatus.setText(mActivity.getResources().getString(R.string.txt_activity_comingsoon));
                break;
            case 1: //進行中
                viewHolder.imgStatus.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.playing));
                viewHolder.tvStatus.setText(mActivity.getResources().getString(R.string.txt_activity_playing));
                break;
            case 2: //已完成
                viewHolder.imgStatus.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.complete));
                viewHolder.tvStatus.setText(mActivity.getResources().getString(R.string.txt_activity_complete));
                break;
        }
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((BaseInterface) mActivity).clickItem(arrarList.get(position).getAid());
            }
        });
        return convertView;
    }


    @Override
    public long getItemId(int i) {
        return 0;
    }

    private class ViewHolder {
        TextView tvStoreName, tvSubtitle, tvPoints, tvDistance, tvDate, tvStatus;
        ImageView imgStatus;
    }
}
