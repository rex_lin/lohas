package com.acloud.app.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.acloud.app.R;

/**
 * Created by skywind-10 on 2015/5/12.
 */
public class OperationPageAdapter extends PagerAdapter {
    private LayoutInflater inflater;
    ImageView imageView;
    int[] images;
    public OperationPageAdapter(Context context, int[] images) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.images = images;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View convertView = inflater.inflate(R.layout.item_opertaion, container, false);
        imageView = (ImageView) convertView.findViewById(R.id.imageView);
        imageView.setImageResource(images[position]);
        container.addView(convertView);
        return convertView;
    }

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
