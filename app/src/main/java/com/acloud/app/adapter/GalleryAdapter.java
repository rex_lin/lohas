package com.acloud.app.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;

import com.acloud.app.util.BitmapHandler;

import java.io.InputStream;
import java.net.URL;
import java.util.Hashtable;
import java.util.List;

public class GalleryAdapter extends PagerAdapter {
    private Activity activity;
    ;
    private Hashtable<Integer, ImageView> cache;
    private List<String> urlList;
    private boolean noClickEvent;

    public GalleryAdapter(Activity activity, List<String> urlList) {
        this(activity, urlList, false);
    }


    public GalleryAdapter(Activity activity, List<String> urlList, boolean noClickEvent) {
        this.activity = activity;
        this.urlList = urlList;
        this.noClickEvent = noClickEvent;
        cache = new Hashtable<Integer, ImageView>();
    }

    @Override
    public int getCount() {
        if (urlList == null)
            return 0;
        return urlList.size();
    }

    @Override
    public Object instantiateItem(final View collection, final int position) {

        if (cache.get(position) != null) {//塞cache
            ((ViewPager) collection).addView(cache.get(position));

        } else {//下載圖片
            final ImageView imageView = new ImageView(activity.getApplicationContext());
            imageView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
            imageView.setAdjustViewBounds(true);
            cache.put(position, imageView);

            final Handler updateIconHandler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    if (msg != null) {
                        try {
                            cache.get(position).setImageBitmap((Bitmap) msg.obj);
                            ((ViewPager) collection).addView(cache.get(position));
                        } catch (NullPointerException e){
                            e.printStackTrace();
                        } catch (IllegalStateException e){
                            e.printStackTrace();
                        }
                    }
                }
            };

            //開始讀取圖片
            new Thread() {
                @Override
                public void run() {
                    Message msg = new Message();

                    try {
                        URL url = new URL(urlList.get(position));
                        InputStream is = (InputStream) url.getContent();
                        if (is != null) {
                            System.gc();
                            //DownLoad Image
                            is = new BitmapHandler.FlushedInputStream(is);
                            msg.obj = BitmapFactory.decodeStream(is);
                            is.close();
                            System.gc();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (msg.obj != null)
                        updateIconHandler.sendMessage(msg);

                }
            }.start();
        }

        return cache.get(position);
    }

    @Override
    public void destroyItem(View collection, int position, Object view) {
        ((ViewPager) collection).removeView(cache.remove(position));
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == (object);
    }

}
