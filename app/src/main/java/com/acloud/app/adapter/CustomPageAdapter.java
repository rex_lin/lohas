package com.acloud.app.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.acloud.app.R;
import com.acloud.app.util.BitmapHandler;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Vector;

/**
 * Created by skywind-10 on 2015/5/12.
 */
public class CustomPageAdapter extends PagerAdapter {
    private LayoutInflater inflater;
    ArrayList<String> date = new ArrayList<>();
    ArrayList<String> subtitle = new ArrayList<>();
    ArrayList<String> content = new ArrayList<>();
    ArrayList<String> imgUrl = new ArrayList<>();
    ImageView imageView;
    public CustomPageAdapter(Context context,ArrayList<String> date, ArrayList<String> subtitle, ArrayList<String> content, ArrayList<String> imgUrl) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.date = date;
        this.subtitle = subtitle;
        this.content = content;
        this.imgUrl = imgUrl;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View convertView = inflater.inflate(R.layout.item_activity_dialog, container, false);
        TextView tvTitle = (TextView) convertView.findViewById(R.id.tvSubtitle);
        TextView tvDate = (TextView) convertView.findViewById(R.id.tvDate);
        TextView tvContent = (TextView) convertView.findViewById(R.id.tvContent);
        imageView = (ImageView)convertView.findViewById(R.id.img);

        tvDate.setText(date.get(position));
        tvTitle.setText(subtitle.get(position));
        tvContent.setText(content.get(position));

        //開始讀取圖片
        new Thread() {
            @Override
            public void run() {
                Message msg = new Message();

                try {
                    URL url = new URL(imgUrl.get(position));
                    InputStream is = (InputStream) url.getContent();
                    if (is != null) {
                        System.gc();
                        //DownLoad Image    BitmapHandler class
                        is = new BitmapHandler.FlushedInputStream(is);
                        msg.obj = BitmapFactory.decodeStream(is);
                        is.close();
                        System.gc();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (msg.obj != null)
                    updateIconHandler.sendMessage(msg);
            }
        }.start();
        container.addView(convertView);
        return convertView;
    }
    final Handler updateIconHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg != null) {
                imageView.setImageBitmap((Bitmap) msg.obj);
            }
        }
    };

    @Override
    public int getCount() {
        return date.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
