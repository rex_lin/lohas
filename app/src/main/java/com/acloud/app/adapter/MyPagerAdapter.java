package com.acloud.app.adapter;

import android.app.Activity;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import java.util.Vector;

/**
 * 我的優惠劵
 */
public class MyPagerAdapter extends PagerAdapter {
    Activity activity;
    private Vector<View> pages;

    private final String[] TITLES = {"未使用","已使用","已過期"};

    public MyPagerAdapter(Activity activity,Vector<View> pages ) {
        this.activity = activity;
        this.pages = pages;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return TITLES[position];
    }

    @Override
    public int getCount() {
        return TITLES.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View page = pages.get(position);
        container.addView(page);


        return page;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

}

