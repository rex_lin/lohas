package com.acloud.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.acloud.app.R;
import com.acloud.app.model.MenuObject;

import java.util.ArrayList;

/**
 * Created by skywind on 2015/8/21.
 */
public class MenuAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    ArrayList<ArrayList<MenuObject>> list;
    int posistion;


    Context context;
    ItemClick itemClick;

    public interface ItemClick{
        public void itemclick(String itemid,int i);
    }
    public MenuAdapter(Context context, ArrayList<ArrayList<MenuObject>> list, ItemClick itemClick) {
        super();
        inflater = LayoutInflater.from(context);
        this.list = list;
        this.context = context;
        this.itemClick = itemClick;
    }

    public MenuAdapter(Context context, int posistion, ArrayList<ArrayList<MenuObject>> list, ItemClick itemClick) {
        super();
        inflater = LayoutInflater.from(context);
        this.list = list;
        this.posistion = posistion;
        this.context = context;
        this.itemClick = itemClick;
    }

    @Override
    public int getCount() {
        return list.get(posistion).size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View contertView, ViewGroup parent) {
        contertView = inflater.inflate(R.layout.item_drawer_row, parent, false);
        TextView textview = (TextView) contertView.findViewById(R.id.item_name);
        textview.setText(list.get(posistion).get(i).getTitile());

        contertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClick.itemclick(list.get(posistion).get(i).getId(), i);

            }
        });
        contertView.setBackground(context.getResources().getDrawable(R.drawable.filterright_selector));
        return contertView;
    }

}
