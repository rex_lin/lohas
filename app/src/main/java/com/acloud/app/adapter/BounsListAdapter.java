package com.acloud.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.acloud.app.R;
import com.acloud.app.fragment.BounsRecordFragment;
import com.acloud.app.tool.Pub;
import com.acloud.app.util.PrefConstant;
import com.acloud.app.util.Util;

import java.util.ArrayList;


public class BounsListAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    ArrayList<String> title, content, points, action_datetime;
    Activity activity;

    public BounsListAdapter(Activity activity, ArrayList<String> title, ArrayList<String> content,
                            ArrayList<String> points, ArrayList<String> action_datetime) {
        inflater = LayoutInflater.from(activity);
        this.title = title;
        this.content = content;
        this.points = points;
        this.action_datetime = action_datetime;
        this.activity = activity;
    }

    // avoid title is null
    @Override
    public int getCount() {
        if (title == null) return 0;
        return title.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position - 1;
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.item_bonus, parent, false);
        ViewHolder viewHolder = new ViewHolder();
        viewHolder.tvDate = (TextView) convertView.findViewById(R.id.tvDate);
        viewHolder.tvPoint = (TextView) convertView.findViewById(R.id.tvPoint);
        viewHolder.tvTitleName = (TextView) convertView.findViewById(R.id.tvTitleName);
        viewHolder.tvSubTitle = (TextView) convertView.findViewById(R.id.tvSubTitle);
        viewHolder.btnShare = (Button) convertView.findViewById(R.id.btnShare);

        viewHolder.tvTitleName.setText(title.get(position));
        viewHolder.tvSubTitle.setText(content.get(position));

        viewHolder.tvDate.setText(Util.formatDate(action_datetime.get(position)));
        if (points.get(position).contains("-")) {
            viewHolder.tvPoint.setTextColor(Color.parseColor("#FF9A40"));
            viewHolder.tvPoint.setText(points.get(position) + activity.getResources().getString(R.string.txt_point));
        } else {
            viewHolder.tvPoint.setTextColor(Color.parseColor("#5EB3CB"));
            viewHolder.tvPoint.setText("+" + points.get(position) + activity.getResources().getString(R.string.txt_point));
        }
        viewHolder.btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (points.get(position).contains("-")) {
                    Util.fbshare(activity, PrefConstant.getString(activity, Pub.VALUE_NAME, "") +
                            "在趣逢甲，購買了" + content.get(position) + "券，快跟我一起享優惠拿好康。");
                } else {
                    Util.fbshare(activity, "恭喜" + PrefConstant.getString(activity, Pub.VALUE_NAME, "") +
                            "在趣逢甲，輕鬆完成了" + content.get(position) + "，獲得 " +
                            points.get(position) + activity.getResources().getString(R.string.txt_point) +
                            "，還有更多活動等著你，讓你好禮拿不完!");

                }
                //TODO Facebook Share
            }
        });
        return convertView;
    }

    private class ViewHolder {
        TextView tvDate, tvPoint, tvTitleName, tvSubTitle;
        Button btnShare;
    }
}