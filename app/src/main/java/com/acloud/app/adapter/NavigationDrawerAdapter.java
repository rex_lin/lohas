package com.acloud.app.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ActionMenuView;
import android.widget.BaseExpandableListAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.acloud.app.NavigationDrawerCallbacks;
import com.acloud.app.NavigationItem;
import com.acloud.app.R;
import com.acloud.app.tool.TLog;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by skywind on 2015/11/30.
 */
public class NavigationDrawerAdapter extends BaseExpandableListAdapter {
    Activity activity;
    List<NavigationItem> groupItems = new ArrayList<NavigationItem>();
    List<NavigationItem> childItems = new ArrayList<NavigationItem>();


    public  NavigationDrawerAdapter(Activity a)
    {
        activity = a;
        groupItems = getGroup();
        childItems = getChild();

    }


    @Override
    public int getGroupCount() {
        return groupItems.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return childItems.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groupItems.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return childItems.get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        View v = LayoutInflater.from(activity).inflate(R.layout.item_drawer_row, null);
        TextView textView = (TextView) v.findViewById(R.id.item_name);
        textView.setText(groupItems.get(groupPosition).getText());
        switch (groupPosition){
            case 1:
                int margin = (int) activity.getResources().getDimension(R.dimen.drawer_child2);
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) textView.getLayoutParams();
                params.setMargins(margin, 0, 0, 0);
                textView.setLayoutParams(params);
                if(isExpanded){
                    textView.setCompoundDrawablesWithIntrinsicBounds(groupItems.get(groupPosition).getDrawable(), null, activity.getResources().getDrawable(R.drawable.drawer_arrow_down), null);
                }else {
                    textView.setCompoundDrawablesWithIntrinsicBounds(groupItems.get(groupPosition).getDrawable(), null, activity.getResources().getDrawable(R.drawable.drawer_arrow_left), null);
                }
                break;
            default:
                textView.setCompoundDrawablesWithIntrinsicBounds(groupItems.get(groupPosition).getDrawable(), null, null, null);
                break;
        }
        return v;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if(groupPosition == 1){
            View v = LayoutInflater.from(activity).inflate(R.layout.item_drawer_row,null);
            TextView textView = (TextView) v.findViewById(R.id.item_name);
            int marginLeft = (int) activity.getResources().getDimension(R.dimen.drawer_child);
            int margin = (int) activity.getResources().getDimension(R.dimen.drawer_child2);
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) textView.getLayoutParams();
            params.setMargins(marginLeft, 0, margin, 0);
            textView.setLayoutParams(params);
            textView.setText(childItems.get(childPosition).getText());

            textView.setCompoundDrawablesWithIntrinsicBounds(childItems.get(childPosition).getDrawable(), null, null, null);
            return v;
        }

        return null;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }




    public List<NavigationItem> getGroup() {
        List<NavigationItem> items = new ArrayList<NavigationItem>();
        items.add(new NavigationItem(activity.getResources().getString(R.string.drawer_item0), activity.getResources().getDrawable(R.drawable.drawer_home)));
        items.add(new NavigationItem(activity.getResources().getString(R.string.drawer_expand_item1), activity.getResources().getDrawable(R.drawable.drawer_street)));
        items.add(new NavigationItem(activity.getResources().getString(R.string.drawer_item11), activity.getResources().getDrawable(R.drawable.drawer_coupon)));
        items.add(new NavigationItem(activity.getResources().getString(R.string.drawer_item12), activity.getResources().getDrawable(R.drawable.drawer_chat)));
        items.add(new NavigationItem(activity.getResources().getString(R.string.drawer_item13), activity.getResources().getDrawable(R.drawable.drawer_buy)));
        items.add(new NavigationItem(activity.getResources().getString(R.string.drawer_item10), activity.getResources().getDrawable(R.drawable.drawer_setting)));
        items.add(new NavigationItem(activity.getResources().getString(R.string.drawer_item2), activity.getResources().getDrawable(R.drawable.drawer_member)));
        items.add(new NavigationItem(activity.getResources().getString(R.string.drawer_item10_3), activity.getResources().getDrawable(R.drawable.drawer_password)));
        items.add(new NavigationItem(activity.getResources().getString(R.string.drawer_item10_2), activity.getResources().getDrawable(R.drawable.drawer_logout)));
//        items.add(new NavigationItem(activity.getResources().getString(R.string.drawer_item4), activity.getResources().getDrawable(R.drawable.drawer_mypoint)));
//        items.add(new NavigationItem(activity.getResources().getString(R.string.drawer_item5), activity.getResources().getDrawable(R.drawable.drawer_activity)));
//        items.add(new NavigationItem(activity.getResources().getString(R.string.drawer_item6), activity.getResources().getDrawable(R.drawable.drawer_coupon)));
//        items.add(new NavigationItem(activity.getResources().getString(R.string.drawer_item6_2), activity.getResources().getDrawable(R.drawable.drawer_buy)));
//        items.add(new NavigationItem(activity.getResources().getString(R.string.drawer_item8), activity.getResources().getDrawable(R.drawable.drawer_giftserial)));
//        items.add(new NavigationItem(activity.getResources().getString(R.string.drawer_item9), activity.getResources().getDrawable(R.drawable.drawer_about)));
        return items;
    }

    public List<NavigationItem> getChild() {
        List<NavigationItem> items = new ArrayList<NavigationItem>();
        items.add(new NavigationItem(activity.getResources().getString(R.string.drawer_item1), activity.getResources().getDrawable(R.drawable.drawer_store)));
        items.add(new NavigationItem(activity.getResources().getString(R.string.drawer_item3), activity.getResources().getDrawable(R.drawable.drawer_favorite)));
        return items;
    }
}
