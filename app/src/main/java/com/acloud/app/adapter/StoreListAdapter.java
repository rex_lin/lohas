package com.acloud.app.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.acloud.app.R;
import com.acloud.app.inferface.BaseInterface;
import com.acloud.app.model.StoreListResult;
import com.acloud.app.util.BitmapHandler;
import com.acloud.app.util.PrefConstant;
import com.acloud.app.util.Util;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;


public class StoreListAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    Activity mContext;
    ArrayList<StoreListResult> arrayList = new ArrayList<StoreListResult>();

    public StoreListAdapter(Activity context, ArrayList<StoreListResult> arrayList) {
        inflater = LayoutInflater.from(context);
        mContext = context;
        this.arrayList = arrayList;
    }

    // avoid date is null
    @Override
    public int getCount() {
        if (arrayList == null) return 0;
        return arrayList.size();
    }

    public void add(ArrayList<StoreListResult> list){
        arrayList.addAll(list);
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder = new ViewHolder();
        convertView = inflater.inflate(R.layout.item_main_store, parent, false);
        viewHolder.storeImg = (ImageView) convertView.findViewById(R.id.imgStore);
        viewHolder.storeName = (TextView) convertView.findViewById(R.id.tvStoreName);
        viewHolder.favoriteImg = (ImageView) convertView.findViewById(R.id.imgFavorite);
        viewHolder.favoriteNum = (TextView) convertView.findViewById(R.id.tvFavoriteNum);
        viewHolder.avgCost = (TextView) convertView.findViewById(R.id.tvAvgCost);
        viewHolder.storeCategory = (TextView) convertView.findViewById(R.id.tvStoreCategory);
//        viewHolder.Distance = (TextView) convertView.findViewById(R.id.tvDistance_white);
        viewHolder.evaluate = (TextView)convertView.findViewById(R.id.tvEvaluate);
        viewHolder.evaluateNum = (TextView)convertView.findViewById(R.id.tvEvaluateNum);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((BaseInterface) mContext).clickItem(arrayList.get(position).getStore_id());
            }
        });

        convertView.setTag(convertView);

        viewHolder.storeName.setText(arrayList.get(position).getStore_name());
        viewHolder.favoriteNum.setText(arrayList.get(position).getFav_num());
        viewHolder.avgCost.setText(arrayList.get(position).getAvg_price());
        viewHolder.storeCategory.setText(arrayList.get(position).getSd_name() + "/" +
                arrayList.get(position).getSca_name());
//        viewHolder.Distance.setText(Util.distanceMtoKM(arrayList.get(position).getDistance()));
        if (arrayList.get(position).getRating().equals("0")){
            viewHolder.evaluate.setText(mContext.getResources().getString(R.string.txt_none));
        }else{
            viewHolder.evaluate.setText(String.format("%.01f", Float.valueOf(arrayList.get(position).getRating()))+
                    mContext.getResources().getString(R.string.txt_score));
            viewHolder.evaluateNum.setText(arrayList.get(position).getRating_num());
        }


        final Handler updateIconHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (msg != null) {
                    viewHolder.storeImg.setImageBitmap((Bitmap) msg.obj);
                }
            }
        };

        //開始讀取圖片
        new Thread() {
            @Override
            public void run() {
                Message msg = new Message();

                try {
                    URL url = new URL(arrayList.get(position).getThumbnail_url());
                    InputStream is = (InputStream) url.getContent();
                    if (is != null) {
                        System.gc();
                        //DownLoad Image    BitmapHandler class
                        is = new BitmapHandler.FlushedInputStream(is);
                        msg.obj = BitmapFactory.decodeStream(is);
                        is.close();
                        System.gc();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (msg.obj != null)
                    updateIconHandler.sendMessage(msg);

            }
        }.start();

        if (PrefConstant.isFavStoreExist(mContext, arrayList.get(position).getStore_id()))
            viewHolder.favoriteImg.setImageResource(R.drawable.ico_heart_pink);
        else
            viewHolder.favoriteImg.setImageResource(R.drawable.ico_heart_normal);

        return convertView;
    }

    private class ViewHolder {
        LinearLayout item;
        ImageView storeImg, favoriteImg;
        TextView storeName, favoriteNum, avgCost, storeCategory, Distance,
                evaluate, evaluateNum;
    }
}