package com.acloud.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.acloud.app.MainActivity;
import com.acloud.app.R;
import com.acloud.app.inferface.BaseInterface;
import com.acloud.app.model.CouponListResult;
import com.acloud.app.tool.TLog;
import com.acloud.app.util.BitmapHandler;
import com.acloud.app.util.LocatUtil;
import com.acloud.app.util.Util;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;


public class CouponListAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private Context mActivity;
    ArrayList<CouponListResult> arrayList = new ArrayList<CouponListResult>();

    LocatUtil locatUtil;
    Location lc;

    public CouponListAdapter(Activity context, ArrayList<CouponListResult> list) {
        inflater = LayoutInflater.from(context);
        mActivity = context;
        this.arrayList = list;
        locatUtil = ((MainActivity) mActivity).getLocatUtil();
        lc = locatUtil.getUpdatedLocation();
    }

    // avoid arrayList is null
    @Override
    public int getCount() {
        if (arrayList == null) return 0;
        return arrayList.size();
    }
    public void add(ArrayList<CouponListResult> list){
        arrayList.addAll(list);
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.item_coupon, parent, false);
        final ViewHolder viewHolder = new ViewHolder();
        viewHolder.tvDate = (TextView) convertView.findViewById(R.id.tvDate);
        viewHolder.tvPoint = (TextView) convertView.findViewById(R.id.tvPoint);
        viewHolder.tvStoreName = (TextView) convertView.findViewById(R.id.tvStoreName);
        viewHolder.tvSubTitle = (TextView) convertView.findViewById(R.id.tvSubtitle);
//        viewHolder.tvDistance = (TextView) convertView.findViewById(R.id.tvDistance_white);
        viewHolder.imgStroe = (ImageView) convertView.findViewById(R.id.img);

        viewHolder.tvStoreName.setText(arrayList.get(position).getStore_name());
        viewHolder.tvSubTitle.setText(arrayList.get(position).getTitle());
        viewHolder.tvPoint.setText(arrayList.get(position).getPoints());
//        viewHolder.tvDistance.setText(Util.GetDistance((lc != null) ? lc.getLatitude() : 0.0,
//                (lc != null) ? lc.getLongitude() : 0.0, Double.valueOf(arrayList.get(position).getLat())
//                , Double.valueOf(arrayList.get(position).getLng())) + "km");
        viewHolder.tvDate.setText(Util.formatDate(arrayList.get(position).getEnd_datetime()));
        convertView.setTag(viewHolder);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((BaseInterface) mActivity).clickItem(arrayList.get(position).getCoupon_id());
            }
        });
        final Handler updateIconHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (msg != null) {
                    viewHolder.imgStroe.setImageBitmap((Bitmap) msg.obj);
                }
            }
        };

        //開始讀取圖片
        new Thread() {
            @Override
            public void run() {
                Message msg = new Message();

                try {
                    URL url = new URL(arrayList.get(position).getThumbnail_url());
                    InputStream is = (InputStream) url.getContent();
                    if (is != null) {
                        System.gc();
                        //DownLoad Image
                        is = new BitmapHandler.FlushedInputStream(is);
                        msg.obj = BitmapFactory.decodeStream(is);
                        is.close();
                        System.gc();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (msg.obj != null)
                    updateIconHandler.sendMessage(msg);

            }
        }.start();
        return convertView;
    }

    private class ViewHolder {
        LinearLayout item;
        TextView tvDate, tvPoint, tvStoreName, tvSubTitle, tvDistance;
        ImageView imgStroe;
        Button btnShare;
    }
}