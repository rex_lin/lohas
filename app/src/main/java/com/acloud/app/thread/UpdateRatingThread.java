package com.acloud.app.thread;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.acloud.app.model.CommonStruct;
import com.acloud.app.tool.Pub;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class UpdateRatingThread extends BaseThread {
    private final String requestUrl = Pub.API_STORE_UPDATERATING;
    private List<NameValuePair> params = new ArrayList<NameValuePair>();

    private Handler handler;


    public UpdateRatingThread(Context context, String uid, String authtoken, String store_id, String rating, String lat, String lng, Handler handler) {
        super(context);
        this.handler = handler;
        params.add(new BasicNameValuePair("apikey", Pub.APIKEY));
        params.add(new BasicNameValuePair("uid", uid));
        params.add(new BasicNameValuePair("authtoken", authtoken));
        params.add(new BasicNameValuePair("store_id", store_id));
        params.add(new BasicNameValuePair("rating", rating));
        params.add(new BasicNameValuePair("lat", lat));
        params.add(new BasicNameValuePair("lng", lng));

    }

    @Override
    public void run() {

        CommonStruct rtValue = null;

        HttpPost httpPost = new HttpPost(requestUrl);
        try {
            UrlEncodedFormEntity entity = new UrlEncodedFormEntity(params, HTTP.UTF_8);
            httpPost.setEntity(entity);
            String jsonString;

            jsonString = this.requestData(httpPost);

            if (jsonString != null && jsonString.length() > 0) {
                //字串解析
                JSONObject jsonObj = new JSONObject(jsonString);
                rtValue = new CommonStruct(jsonObj.getString("syscode"), jsonObj.getString("sysmsg"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (handler != null) {
            Message msg = handler.obtainMessage();
            if (rtValue != null) {
                Bundle bundle = new Bundle();
                bundle.putSerializable(Pub.RETURN_VALUE_KEY, (Serializable) rtValue);
                msg.what = 1;
                msg.setData(bundle);
                handler.sendMessage(msg);
            } else {
                handler.sendEmptyMessage(0);
            }
        }
    }
}
