package com.acloud.app.thread;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.acloud.app.model.FavoriteStruct;
import com.acloud.app.tool.Pub;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class FavoriteListThread extends BaseThread {
    private final String requestUrl = Pub.API_STORE_FAVORITESLIST;
    private List<NameValuePair> params = new ArrayList<NameValuePair>();

    private Handler handler;


    public FavoriteListThread(Context context, String uid, String authtoken, Handler handler) {
        super(context);
        this.handler = handler;
        params.add(new BasicNameValuePair("apikey", Pub.APIKEY));
        params.add(new BasicNameValuePair("uid", uid));
        params.add(new BasicNameValuePair("authtoken", authtoken));
        params.add(new BasicNameValuePair("index", "0"));
        params.add(new BasicNameValuePair("size", "20"));
    }


    public FavoriteListThread(Context context, String uid, String authtoken, String index, Handler handler) {
        super(context);
        this.handler = handler;
        params.add(new BasicNameValuePair("apikey", Pub.APIKEY));
        params.add(new BasicNameValuePair("uid", uid));
        params.add(new BasicNameValuePair("authtoken", authtoken));
        params.add(new BasicNameValuePair("index", index));
        params.add(new BasicNameValuePair("size", "20"));

    }


    @Override
    public void run() {

        FavoriteStruct rtValue = null;
        HttpPost httpPost = new HttpPost(requestUrl);
        try {
            UrlEncodedFormEntity entity = new UrlEncodedFormEntity(params, HTTP.UTF_8);
            httpPost.setEntity(entity);
            String jsonString;

            jsonString = this.requestData(httpPost);

            if (jsonString != null && jsonString.length() > 0) {
                //字串解析
                JSONObject jsonObj = new JSONObject(jsonString);
                rtValue = new FavoriteStruct(jsonObj.getString("syscode"), jsonObj.getString("sysmsg"));
                JSONArray jsonArr = jsonObj.getJSONArray("list");

                if (jsonArr != null) {
                    for (int n = 0; n < jsonArr.length(); n++) {
                        try {
                            rtValue.getList().add(
                                    new FavoriteStruct.Data(
                                            jsonArr.getJSONObject(n).getString("store_id"),
                                            jsonArr.getJSONObject(n).getString("store_name"),
                                            jsonArr.getJSONObject(n).getString("fav_num"),
                                            jsonArr.getJSONObject(n).getString("rating"),
                                            jsonArr.getJSONObject(n).getString("rating_num"),
                                            jsonArr.getJSONObject(n).getString("avg_price"),
                                            jsonArr.getJSONObject(n).getString("thumbnail_url"),
                                            jsonArr.getJSONObject(n).getString("sd_id"),
                                            jsonArr.getJSONObject(n).getString("sd_name"),
                                            jsonArr.getJSONObject(n).getString("sca_id"),
                                            jsonArr.getJSONObject(n).getString("sca_name")
                                    )
                            );
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        if (handler != null) {
            Message msg = handler.obtainMessage();
            if (rtValue != null) {
                Bundle bundle = new Bundle();
                bundle.putSerializable(Pub.RETURN_VALUE_KEY, (Serializable) rtValue);
                msg.what = 1;
                msg.setData(bundle);
                handler.sendMessage(msg);
            } else {
                handler.sendEmptyMessage(0);
            }
        }
    }
}
