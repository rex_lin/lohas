package com.acloud.app.thread;

import android.content.Context;
import android.net.ConnectivityManager;

import com.acloud.app.tool.Pub;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.ByteArrayBuffer;

import java.io.InputStream;
import java.security.KeyStore;
import java.util.List;
import java.util.zip.GZIPInputStream;


public abstract class BaseThread implements Runnable{
	@SuppressWarnings("unused")
    private Thread thread=null;
	private ConnectivityManager connManager;
	private int timeoutConnection = 30000;
	private int timeoutSocket = 30000;
	protected int statusCode;

	protected int connectionStatus = 0;		//連線狀態：-1.網路連線異常or連線逾時

	public BaseThread(Context context){
		connManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
	}
	
	public void start(){
        thread = new Thread(this);
        thread.start();
	}

    public void stop(){
        if(thread!=null&&thread.isAlive()){
            thread.interrupt();
			try {
				thread.stop();
			} catch (UnsupportedOperationException e){
				e.printStackTrace();
			}

        }
    }

    protected String getUriPath(String serviceName,String pathVariables[]){
        return getUriPath(serviceName,pathVariables,"");
    }

    protected String getUriPath(String serviceName,String pathVariables[],String passKey){
        return getUriPath(serviceName,pathVariables,null,passKey);
    }

	protected String getUriPath(String serviceName,String pathVariables[],List<NameValuePair> params,String passKey){

        StringBuffer sbUri = new StringBuffer(Pub.API_SERVER);

        sbUri.append(serviceName);

		if(pathVariables != null){
			int pathVariablesCount = pathVariables.length;

			for(int i=0;i<pathVariablesCount;i++){
				sbUri.append("/");
				sbUri.append(pathVariables[i]);
			}
		}

        if(!passKey.equals("")) {
            sbUri.append("/");
            sbUri.append(passKey);
        }

		if(params != null){
			sbUri.append("?");
			sbUri.append(URLEncodedUtils.format(params, "utf-8"));
		}
		
		return sbUri.toString();
	}
	
	protected String requestData(HttpUriRequest request){
		HttpParams httpParameters = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
		HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
		//HttpClient client = new DefaultHttpClient(httpParameters);
		HttpClient client = getNewHttpClient(httpParameters);
		
		String retValue = "";
		statusCode = -1;
		InputStream instream = null;
		try {			
			//request.addHeader("Accept-Encoding", "gzip");
			request.addHeader("Accept", "application/json");
			request.addHeader("User-Agent", System.getProperty("http.agent"));
			
			HttpResponse response = client.execute(request);
			statusCode = response.getStatusLine().getStatusCode();
			if (statusCode == HttpStatus.SC_OK) {  
		    	instream = response.getEntity().getContent(); 
		    	if(instream != null){	
		    		Header contentEncoding = response.getFirstHeader("Content-Encoding");
		    		//回傳如果為gzip形態，解壓縮
				    if (contentEncoding != null && contentEncoding.getValue().equalsIgnoreCase("gzip")) {
				    	instream = new GZIPInputStream(instream);
				    }
				    try {
			    		byte[] data = new byte[1024];
			    		int n;  
				    	ByteArrayBuffer buf = new ByteArrayBuffer(8192);
				    	while ((n = instream.read(data)) != -1)  {
				    		buf.append(data, 0, n);
				    	}
				    		
				    	retValue = new String(buf.toByteArray(), HTTP.UTF_8);  					    	
				    	buf.clear();
				    	buf = null;
			    	}catch(Exception ex){
			    		ex.printStackTrace();
			    		
			    	}
		    	}
		    }  
		} catch (Exception e) {
			connectionStatus = -1;
			e.printStackTrace();
		} finally{			
    		try{
    			if(instream != null)
    				instream.close();
    		}catch(Exception ex){				    			
    			ex.printStackTrace();
    		}
    	}
		return retValue;
	}

	

	private HttpClient getNewHttpClient(HttpParams params) {
	    try {
	        KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
	        trustStore.load(null, null);

	        SSLSocketFactory sf = new SSLSocketFactory(trustStore);
	        sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

	        if(params == null)
	        	params = new BasicHttpParams();
	        HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
	        HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

	        SchemeRegistry registry = new SchemeRegistry();
	        registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), Pub.SERVER_PORT));
	        registry.register(new Scheme("https", sf, Pub.SERVER_PORT_SSL));

	        ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);

	        return new DefaultHttpClient(ccm, params);
	    } catch (Exception e) {
	        return new DefaultHttpClient();
	    }
	}
}

