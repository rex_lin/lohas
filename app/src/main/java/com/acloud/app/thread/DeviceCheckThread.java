package com.acloud.app.thread;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.acloud.app.model.CommonStruct;
import com.acloud.app.tool.Pub;
import com.acloud.app.tool.TLog;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * 檢查Device 需要登出
 */
public class DeviceCheckThread extends BaseThread {
    private final String requestUrl = Pub.API_CHECK_DEVICELOGIN;
    private List<NameValuePair> params = new ArrayList<NameValuePair>();

    private Handler handler;


    public DeviceCheckThread(Context context, String uid, String authtoken, String last_push_token, Handler handler) {
        super(context);
        this.handler = handler;
        params.add(new BasicNameValuePair("apikey", Pub.APIKEY));
        params.add(new BasicNameValuePair("uid", uid));
        params.add(new BasicNameValuePair("authtoken", authtoken));
        params.add(new BasicNameValuePair("last_push_token", last_push_token));
        TLog.i("Ernest" , "last_push_token : " + last_push_token);
    }

    @Override
    public void run() {
        CommonStruct rtValue = null;
        HttpPost httpPost = new HttpPost(requestUrl);
        try {
            UrlEncodedFormEntity entity = new UrlEncodedFormEntity(params, HTTP.UTF_8);
            httpPost.setEntity(entity);
            String jsonString;

            jsonString = this.requestData(httpPost);

            if (jsonString != null && jsonString.length() > 0) {
                //字串解析
                JSONObject jsonObj = new JSONObject(jsonString);
                rtValue = new CommonStruct(jsonObj.getString("syscode"), jsonObj.getString("sysmsg"));
                JSONObject JsonObj = jsonObj.getJSONObject("data");
                if (JsonObj != null) {
                    rtValue.setJsonTestObj(JsonObj);
                }
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (handler != null) {
            Message msg = handler.obtainMessage();
            if (rtValue != null) {
                Bundle bundle = new Bundle();
                bundle.putSerializable(Pub.RETURN_VALUE_KEY, (Serializable) rtValue);
                msg.what = 1;
                msg.setData(bundle);
                handler.sendMessage(msg);
            } else {
                handler.sendEmptyMessage(0);
            }
        }
    }
}
