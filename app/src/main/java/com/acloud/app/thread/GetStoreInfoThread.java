package com.acloud.app.thread;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.acloud.app.model.StoreDetailStruct;
import com.acloud.app.tool.Pub;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class GetStoreInfoThread extends BaseThread {
    private final String requestUrl = Pub.API_STOREINFO;
    private List<NameValuePair> params = new ArrayList<NameValuePair>();

    private Handler handler;


    public GetStoreInfoThread(Context context, String uid, String authtoken, String store_id, String lat, String lng, Handler handler) {
        super(context);
        this.handler = handler;
        params.add(new BasicNameValuePair("apikey", Pub.APIKEY));
        params.add(new BasicNameValuePair("uid", uid));
        params.add(new BasicNameValuePair("authtoken", authtoken));
        params.add(new BasicNameValuePair("store_id", store_id));
        params.add(new BasicNameValuePair("lat", lat));
        params.add(new BasicNameValuePair("lng", lng));

    }

    @Override
    public void run() {

        StoreDetailStruct rtValue = null;

        HttpPost httpPost = new HttpPost(requestUrl);
        try {
            UrlEncodedFormEntity entity = new UrlEncodedFormEntity(params, HTTP.UTF_8);
            httpPost.setEntity(entity);
            String jsonString;

            jsonString = this.requestData(httpPost);

            if (jsonString != null && jsonString.length() > 0) {
                //字串解析
                JSONObject jsonObj = new JSONObject(jsonString);
                rtValue = new StoreDetailStruct(jsonObj.getString("syscode"), jsonObj.getString("sysmsg"));
                JSONObject jsonDataObj = jsonObj.getJSONObject("data");

                if (jsonDataObj != null) {
                    rtValue.setStore_id(jsonDataObj.getString("store_id"));
                    rtValue.setStore_name(jsonDataObj.getString("store_name"));
                    rtValue.setRating(jsonDataObj.getString("rating"));
                    rtValue.setRating_num(jsonDataObj.getString("rating_num"));
                    rtValue.setAddress(jsonDataObj.getString("address"));
                    rtValue.setLat(jsonDataObj.getString("lat"));
                    rtValue.setLng(jsonDataObj.getString("lng"));
                    rtValue.setDistance(jsonDataObj.getString("distance"));
                    rtValue.setPhone(jsonDataObj.getString("phone"));
                    rtValue.setUser_rating(jsonDataObj.getString("user_rating"));
                    rtValue.setFav_num(jsonDataObj.getString("fav_num"));
                    rtValue.setAvg_price(jsonDataObj.getString("avg_price"));
                    rtValue.setBusiness_hours(jsonDataObj.getString("business_hours"));
                    rtValue.setContent(jsonDataObj.getString("content"));
                    rtValue.setSd_id(jsonDataObj.getString("sd_id"));
                    rtValue.setSd_name(jsonDataObj.getString("sd_name"));
                    rtValue.setSca_id(jsonDataObj.getString("sca_id"));
                    rtValue.setSca_name(jsonDataObj.getString("sca_name"));

                    JSONArray jsonImgArr = jsonDataObj.getJSONArray("images");

                    if (jsonImgArr != null) {
                        for (int n = 0; n < jsonImgArr.length(); n++) {
                            rtValue.getImages().add(jsonImgArr.get(n).toString());
                        }
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        if (handler != null) {
            Message msg = handler.obtainMessage();
            if (rtValue != null) {
                Bundle bundle = new Bundle();
                bundle.putSerializable(Pub.RETURN_VALUE_KEY, (Serializable) rtValue);
                msg.what = 1;
                msg.setData(bundle);
                handler.sendMessage(msg);
            } else {
                handler.sendEmptyMessage(0);
            }
        }
    }
}
