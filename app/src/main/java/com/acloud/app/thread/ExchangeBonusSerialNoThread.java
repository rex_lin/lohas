package com.acloud.app.thread;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.acloud.app.model.CommonStruct;
import com.acloud.app.tool.Pub;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by skywind-10 on 2015/8/10.
 */
public class ExchangeBonusSerialNoThread extends BaseThread {
    private final String requestUrl = Pub.API_EXCHANGE_BONUS_SERIAL_NO;
    private List<NameValuePair> params = new ArrayList<NameValuePair>();

    private Handler handler;


    public ExchangeBonusSerialNoThread(Context context, String uid, String authtoken, String bonus_serial_no, String lat, String lng, Handler handler) {
        super(context);
        this.handler = handler;
        params.add(new BasicNameValuePair(Pub.VALUE_APIKEY, Pub.APIKEY));
        params.add(new BasicNameValuePair(Pub.VALUE_UID, uid));
        params.add(new BasicNameValuePair(Pub.VALUE_AUTHTOKEN, authtoken));
        params.add(new BasicNameValuePair(Pub.VALUE_BOUNUS_SERIAL_NO, bonus_serial_no));
        params.add(new BasicNameValuePair(Pub.VALUE_LAT, lat));
        params.add(new BasicNameValuePair(Pub.VALUE_LNG, lng));
    }

    @Override
    public void run() {

        CommonStruct rtValue = null;
        try {


            HttpPost httpPost = new HttpPost(requestUrl);
            UrlEncodedFormEntity entity = new UrlEncodedFormEntity(params, HTTP.UTF_8);
            httpPost.setEntity(entity);
            String jsonString;

            jsonString = this.requestData(httpPost);
            if (jsonString != null && jsonString.length() > 0) {
                //字串解析
                JSONObject jsonObj = new JSONObject(jsonString);
                rtValue = new CommonStruct(jsonObj.getString("syscode"), jsonObj.getString("sysmsg"));

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (handler != null) {
            Message msg = handler.obtainMessage();
            if (rtValue != null) {
                Bundle bundle = new Bundle();
                bundle.putSerializable(Pub.RETURN_VALUE_KEY, (Serializable) rtValue);
                msg.what = 1;
                msg.setData(bundle);
                handler.sendMessage(msg);
            } else {
                handler.sendEmptyMessage(0);
            }
        }

    }
}
