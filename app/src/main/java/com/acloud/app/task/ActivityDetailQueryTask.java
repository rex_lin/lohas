package com.acloud.app.task;

import android.app.Activity;
import android.os.AsyncTask;

import com.acloud.app.MainActivity;
import com.acloud.app.dialog.CustomDialog;
import com.acloud.app.dialog.CustomDialogOrange;
import com.acloud.app.dialog.CustomNetworkDialog;
import com.acloud.app.fragment.ActivityDetailFragment;
import com.acloud.app.http.HttpActivityInfo;
import com.acloud.app.util.ActivityDetailManager;

/**
 * Created by skywind on 2015/8/19.
 */
public class ActivityDetailQueryTask extends AsyncTask {

    Activity mActivity;
    String uid, authtoken, lat, lng, aid,type,sort_type;
    HttpActivityInfo activityInfo;

    public ActivityDetailQueryTask(Activity activity, String uid, String authtoken, String aid, String lat, String lng) {
        mActivity = activity;
        this.uid = uid;
        this.authtoken = authtoken;
        this.lat = lat;
        this.lng = lng;
        this.aid = aid;
        this.type=type;
        this.sort_type=sort_type;
    }

    @Override
    protected Object doInBackground(Object[] objects) {
        activityInfo = new HttpActivityInfo(mActivity, ActivityDetailManager.getInfo(
                uid,
                authtoken,
                aid,
                lat,
                lng
        ));
        activityInfo.call();


        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        if (activityInfo.ismSuccess()) {
            ((MainActivity) mActivity).changeBackFragment(new ActivityDetailFragment());
        } else {
            CustomNetworkDialog networkDialog = new CustomNetworkDialog(mActivity);
            networkDialog.initNetworkDialog(null, null);
            networkDialog.show();
        }
    }
}

