package com.acloud.app.task;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Handler;
import android.widget.Toast;

import com.acloud.app.R;
import com.acloud.app.dialog.CustomCouponDialog;
import com.acloud.app.dialog.CustomDialogOrange;
import com.acloud.app.dialog.CustomDialogPink;
import com.acloud.app.fragment.MyCouponDetailFragment;
import com.acloud.app.http.HttpMyCouponUsed;
import com.acloud.app.tool.TLog;
import com.acloud.app.tool.Pub;
import com.acloud.app.tool.WebService;
import com.acloud.app.util.MyCouponDetailManager;
import com.acloud.app.util.PrefConstant;
import com.acloud.app.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

/**
 * 優惠卷兌換
 */
public class MyCouponExchangeTask extends AsyncTask {
    private Activity mActivity;
    private String jsonString;
    private String syscode;


    private String ex_coupon_id;
    private String verify_code;
    private String lat;
    private String lng;
    private String sysmsg;

    boolean isSuccess;
    Handler handler;
    CustomDialogPink customDialogPink;

    public MyCouponExchangeTask(Activity activity, String ex_coupon_id, String verify_coude, String lat, String lng,
                                CustomDialogPink dialog2, Handler handler) {
        mActivity = activity;
        this.ex_coupon_id = ex_coupon_id;
        this.verify_code = verify_coude;
        this.lat = lat;
        this.lng = lng;
        this.customDialogPink = dialog2;
        this.handler = handler;
    }


    @Override
    protected Object doInBackground(Object[] objects) {
        HttpMyCouponUsed used = new HttpMyCouponUsed(mActivity, WebService.getMyCoupon_Use(
                PrefConstant.getString(mActivity, Pub.VALUE_UID, ""),
                PrefConstant.getString(mActivity, Pub.VALUE_AUTHTOKEN, ""),
                ex_coupon_id,
                verify_code,
                lat,
                lng
        ));
        jsonString = used.call();
        parserJson(jsonString);
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    public String daytochinese() {
        String char1 = "";
        Calendar c = Calendar.getInstance();
        char1 = (c.get(Calendar.MONTH) + 1) + "" + "月" +
                c.get(Calendar.DAY_OF_MONTH) + "" + "日週" + Util.week(c.get(Calendar.DAY_OF_WEEK))
                + " " +
                c.get(Calendar.HOUR_OF_DAY) + "點" + c.get(Calendar.MINUTE) + "分";
        return char1;
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        if (syscode != null && syscode.equals("200")) {
            TLog.i("MyCouponExchangeTask : ", "sucess");
            isSuccess = true;


            CustomCouponDialog couponDialog = new CustomCouponDialog(mActivity, MyCouponDetailManager.getMyCouponDetailResult().getStore_name(),
                    daytochinese(),
                    MyCouponDetailManager.getMyCouponDetailResult().getImage_url(),
                    MyCouponDetailManager.getMyCouponDetailResult().getTitle(),
                    MyCouponDetailManager.getMyCouponDetailResult().getContent());
            couponDialog.initCouponDialog();
            couponDialog.show();
            customDialogPink.dismiss();
            handler.sendEmptyMessage(MyCouponDetailFragment.isSuccess);

        } else if (syscode != null && syscode.equals("414")) {
            isSuccess = false;
            //TODO 優惠劵兌換
//            Toast.makeText(mActivity, "錯誤碼 : " + syscode + "錯誤 : " + sysmsg, Toast.LENGTH_SHORT).show();
            CustomDialogOrange errorDialog = new CustomDialogOrange(mActivity, mActivity.getString(R.string.txt_error_msg_syscode414));
            errorDialog.initErrorDialog();
            errorDialog.show();
            customDialogPink.dismiss();
            handler.sendEmptyMessage(MyCouponDetailFragment.isError);
        } else {
//            Toast.makeText(mActivity, "錯誤碼 : " + syscode + "錯誤 : " + sysmsg, Toast.LENGTH_SHORT).show();
            CustomDialogOrange errorDialog = new CustomDialogOrange(mActivity, mActivity.getString(R.string.txt_exchange_fail));
            errorDialog.initErrorDialog();
            errorDialog.show();
            customDialogPink.dismiss();
            handler.sendEmptyMessage(MyCouponDetailFragment.isError);
        }
    }

    void parserJson(String message) {
        try {
            JSONObject object = new JSONObject(message);
            syscode = object.optString(Pub.VALUE_SYSCODE);
            sysmsg = object.optString(Pub.VALUE_SYSMSG);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public boolean isSysScccess() {
        return syscode.equals("200");
    }

    public String getSysCode() {
        return syscode;
    }

    public String getSysmsg() {
        return sysmsg;
    }
}
