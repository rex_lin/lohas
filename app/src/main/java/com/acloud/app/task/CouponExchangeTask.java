package com.acloud.app.task;

import android.app.Activity;
import android.os.AsyncTask;
import android.view.View;
import android.widget.Toast;

import com.acloud.app.R;
import com.acloud.app.dialog.CustomDialogPink;
import com.acloud.app.dialog.CustomDialogOrange;
import com.acloud.app.http.HttpCouponExchange;
import com.acloud.app.tool.TLog;
import com.acloud.app.tool.Pub;
import com.acloud.app.util.CouponDetailManager;
import com.acloud.app.util.PrefConstant;
import com.acloud.app.util.Util;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * 優惠卷兌換
 */
public class CouponExchangeTask extends AsyncTask {
    private Activity mActivity;
    private String
            mCoupon_id,
            mCoupon_name,
            jsonString,
            syscode,
            sysmsg;

    boolean isSuccess;

    public CouponExchangeTask(Activity activity, String coupon_id, String coupon_name) {
        mActivity = activity;
        mCoupon_id = coupon_id;
        mCoupon_name = coupon_name;
    }

    @Override
    protected Object doInBackground(Object[] objects) {
        HttpCouponExchange exchange = new HttpCouponExchange(mActivity,
                CouponDetailManager.getCouponDetail(
                        PrefConstant.getString(mActivity, Pub.VALUE_UID, ""),
                        PrefConstant.getString(mActivity, Pub.VALUE_AUTHTOKEN, ""),
                        mCoupon_id,
                        "0",
                        "0"
                ));
        jsonString = exchange.call();
        parserJson(jsonString);
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        if (syscode != null && syscode.equals("200")) {
            TLog.i("CouponExchangeTask : ", "sucess");
            isSuccess = true;

            final CustomDialogPink dialog = new CustomDialogPink(mActivity, mActivity.getResources().getString(R.string.txt_exchange_success),
                    String.format(mActivity.getResources().getString(R.string.txt_exchange_success_msg), mCoupon_name),
                    mActivity.getResources().getString(R.string.btn_nextime),
                    mActivity.getResources().getString(R.string.btn_share));
            dialog.initProgressDialog(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    Toast.makeText(mActivity, mActivity.getString(R.string.txt_feature_not_complete), Toast.LENGTH_SHORT).show();
                    Util.fbshare(mActivity,String.format(mActivity.getResources().getString(R.string.txt_exchange_success_msg), mCoupon_name));
                    //TODO Facebook share
                }
            });
            dialog.show();

        } else if (syscode != null && syscode.equals("409")) {
            isSuccess = false;
            CustomDialogOrange errordialog = new CustomDialogOrange(mActivity, mActivity.getString(R.string.txt_exchange_fail));
            errordialog.initProgressDialog();
            errordialog.show();
        } else {
//            Toast.makeText(mActivity, "錯誤碼 : " + syscode + "錯誤 : " + sysmsg, Toast.LENGTH_SHORT).show();
            CustomDialogOrange customDialog = new CustomDialogOrange(mActivity, mActivity.getString(R.string.txt_fail));
            customDialog.initErrorDialog();
            customDialog.show();
        }
    }

    void parserJson(String message) {
        try {
            JSONObject object = new JSONObject(message);
            syscode = object.optString(Pub.VALUE_SYSCODE);
            sysmsg = object.optString(Pub.VALUE_SYSMSG);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
