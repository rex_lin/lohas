package com.acloud.app.task;

import android.app.Activity;
import android.app.Application;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;

import com.acloud.app.MyApplication;
import com.acloud.app.dialog.CustomProgressDialog;
import com.acloud.app.http.HttpGetMemberInfo;
import com.acloud.app.http.HttpHuanuageLogin;
import com.acloud.app.http.HttpLogin;
import com.acloud.app.http.HttpPushTokenUpdate;
import com.acloud.app.model.MemberInfo;
import com.acloud.app.tool.Pub;
import com.acloud.app.tool.TLog;
import com.acloud.app.tool.WebService;
import com.acloud.app.util.GCMClientManager;
import com.acloud.app.util.PrefConstant;

import org.apache.http.NameValuePair;

import java.util.ArrayList;

/**
 * Created by skywind-10 on 2015/7/7.
 */
public class GetMemberInfoTask  {
    Activity mActivity;
    HttpGetMemberInfo httpGetMemberInfo;
    String mToken;
    CustomProgressDialog customProgressDialog;
    Handler mHandler;

    public GetMemberInfoTask(Activity activity,String token,Handler handler) {
        mActivity = activity;
        mToken = token;
        customProgressDialog = new CustomProgressDialog(activity);
        mHandler = handler;
        LoginThread loginThread = new LoginThread();
        loginThread.start();
        try {
            loginThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public class LoginThread extends Thread {


        @Override
        public void run() {
            super.run();
//            String message = null;
//            int code = -1;
//getProfile
            httpGetMemberInfo = new HttpGetMemberInfo(mActivity , mToken);
            httpGetMemberInfo.call();

            MemberInfo memberInfo = httpGetMemberInfo.getMemberInfo();
            TLog.d("ooxx",this.getClass().getName() +" "+ "doInBackground : " + memberInfo.getEmail());
            MyApplication.emil = memberInfo.getEmail();
            HttpHuanuageLogin login = new HttpHuanuageLogin(mActivity, WebService.getHuanuageLoginList(memberInfo.getId(), memberInfo.getEmail(), mToken));
            login.call();

//            TLog.d("ooxx", this.getClass().getName() + " " + "login Code: " + login.getSysCode());
//            TLog.d("ooxx",this.getClass().getName() +" "+ "login SysSuccess: " + login.getIsSysSuccess());
            //Update push token

            HttpPushTokenUpdate update = new HttpPushTokenUpdate(mActivity,
                    WebService.getUid_AuthToken_AppId_PushToken_PushServer(
                            PrefConstant.getString(mActivity, Pub.VALUE_UID, ""),
                            PrefConstant.getString(mActivity, Pub.VALUE_AUTHTOKEN, ""),
                            mActivity.getPackageName(),
                            PrefConstant.getString(mActivity, GCMClientManager.PROPERTY_REG_ID, ""),
                            "gcm",
                            "0",
                            "0"
                    ));
            String updateotken = update.call();
            TLog.d("ooxx", this.getClass().getName() + " " + "updateotken : " + updateotken);

            Message msg = new Message();
            msg.what = 0;
            msg.obj = updateotken;
            mHandler.sendMessage(msg);


        }
    }

//    @Override
//    protected void onPostExecute(Object o) {
//        super.onPostExecute(o);
//
//    }
//
//    @Override
//    protected Object doInBackground(Object[] objects) {
//        //getProfile
//        httpGetMemberInfo = new HttpGetMemberInfo(mActivity , mToken);
//        httpGetMemberInfo.call();
//
//        MemberInfo memberInfo = httpGetMemberInfo.getMemberInfo();
//        TLog.d("ooxx",this.getClass().getName() +" "+ "doInBackground : " + memberInfo.getEmail());
//        MyApplication.emil = memberInfo.getEmail();
//        HttpHuanuageLogin login = new HttpHuanuageLogin(mActivity, WebService.getHuanuageLoginList(memberInfo.getId(), memberInfo.getEmail(), mToken));
//        login.call();
//
//        TLog.d("ooxx", this.getClass().getName() + " " + "login Code: " + login.getSysCode());
//        TLog.d("ooxx",this.getClass().getName() +" "+ "login SysSuccess: " + login.getIsSysSuccess());
//        //Update push token
//
//        HttpPushTokenUpdate update = new HttpPushTokenUpdate(mActivity,
//                WebService.getUid_AuthToken_AppId_PushToken_PushServer(
//                        PrefConstant.getString(mActivity, Pub.VALUE_UID, ""),
//                        PrefConstant.getString(mActivity, Pub.VALUE_AUTHTOKEN, ""),
//                        mActivity.getPackageName(),
//                        PrefConstant.getString(mActivity, GCMClientManager.PROPERTY_REG_ID, ""),
//                        "gcm",
//                        "0",
//                        "0"
//                ));
//        String updateotken = update.call();
//        TLog.d("ooxx", this.getClass().getName() + " " + "updateotken : " + updateotken);
//
//        return null;
//    }
//
//    @Override
//    protected void onProgressUpdate(Object[] values) {
//        super.onProgressUpdate(values);
//    }
//
//    @Override
//    protected void onPreExecute() {
//    }
}