package com.acloud.app.task;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Handler;

import com.acloud.app.dialog.CustomDialog;
import com.acloud.app.dialog.CustomProgressDialog;
import com.acloud.app.http.HttpStoreCouponList;
import com.acloud.app.tool.Pub;
import com.acloud.app.util.PrefConstant;
import com.acloud.app.util.StoreCouponManager;

/**
 * Created by skywind-10 on 2015/7/15.
 */
public class StoreDetailCouponTask extends AsyncTask {
    Activity mActivity;
    String mStore_id;
    Handler handler;
    CustomProgressDialog customDialog;
    HttpStoreCouponList httpStoreCouponList;

    public StoreDetailCouponTask(Activity activity, String store_id, Handler handler) {
        mActivity = activity;
        mStore_id = store_id;
        this.handler = handler;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        customDialog = new CustomProgressDialog(mActivity);
        customDialog.initProgressDialog();
        customDialog.show();
    }

    @Override
    protected Object doInBackground(Object[] objects) {
        httpStoreCouponList = new HttpStoreCouponList(mActivity,
                StoreCouponManager.getStoreCoupon(
                        PrefConstant.getString(mActivity, Pub.VALUE_UID, ""),
                        PrefConstant.getString(mActivity, Pub.VALUE_AUTHTOKEN, ""),
                        mStore_id
                ));
        httpStoreCouponList.call();
        return null;
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        handler.sendEmptyMessage(0);
        customDialog.dismiss();


    }
}

