package com.acloud.app.task;

import android.app.Activity;
import android.net.Uri;
import android.os.AsyncTask;
import android.view.View;
import android.widget.Toast;

import com.acloud.app.R;
import com.acloud.app.dialog.CustomDialogPink;
import com.acloud.app.http.HttpActivityFinish;
import com.acloud.app.tool.Pub;
import com.acloud.app.tool.TLog;
import com.acloud.app.util.ActivityDetailManager;
import com.acloud.app.util.PrefConstant;
import com.acloud.app.util.Util;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * 活動完成
 */
public class ActivityExchangeTask extends AsyncTask {

    Activity
            mActivity;
    String
            aid,
            lat,
            lng,
            jsonString,
            syscode,
            sysmsg,
            obtain_bonus;

    HttpActivityFinish
            httpActivityFinish;
    public ActivityExchangeTask(Activity activity, String aid, String lat, String lng) {
        this.mActivity = activity;
        this.aid = aid;
        this.lat = lat;
        this.lng = lng;
    }

    @Override
    protected Object doInBackground(Object[] objects) {
        httpActivityFinish = new HttpActivityFinish(mActivity,
                ActivityDetailManager.getInfo(
                        PrefConstant.getString(mActivity, Pub.VALUE_UID, ""),
                        PrefConstant.getString(mActivity, Pub.VALUE_AUTHTOKEN, ""),
                        aid,
                        lat,
                        lng
                ));
        jsonString = httpActivityFinish.call();
        parserJson(jsonString);
        return null;
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);

        final CustomDialogPink dialog = new CustomDialogPink(mActivity,
                mActivity.getResources().getString(R.string.txt_finish_activity),
                String.format(mActivity.getResources().getString(R.string.txt_profile_write_success), obtain_bonus),
                mActivity.getResources().getString(R.string.btn_nextime),
                mActivity.getResources().getString(R.string.btn_share)
        );

        dialog.initProgressDialog(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Util.fbshare(mActivity,String.format(mActivity.getString(R.string.txt_facebook_shared_bonus), obtain_bonus));
            }
        });

        dialog.show();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    private void parserJson(String message) {
        JSONObject object = null;
        try {
            object = new JSONObject(message);
            syscode = object.optString("syscode");
            sysmsg = object.optString("sysmsg");

            object = object.getJSONObject("data");
            obtain_bonus = object.optString("obtain_bonus");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
