package com.acloud.app.task;

import android.app.Activity;
import android.os.AsyncTask;

import com.acloud.app.MainActivity;
import com.acloud.app.dialog.CustomDialog;
import com.acloud.app.dialog.CustomDialogOrange;
import com.acloud.app.dialog.CustomNetworkDialog;
import com.acloud.app.fragment.MyCouponDetailFragment;
import com.acloud.app.http.HttpMyCouponInfo;
import com.acloud.app.util.MyCouponDetailManager;

/**
 * CouponDetail
 */
public class MyCouponDetailTask extends AsyncTask {
    Activity mActivity;
    String mEx_coupon_id, uid, authtoken, lat, lng;
    HttpMyCouponInfo httpMyCouponInfo;
    public MyCouponDetailTask(Activity activity, String uid, String authtoken, String ex_coupon_id, String lat, String lng) {
        mActivity = activity;
        this.uid = uid;
        this.authtoken = authtoken;
        this.mEx_coupon_id = ex_coupon_id;
        this.lat = lat;
        this.lng = lng;
    }

    @Override
    protected Object doInBackground(Object[] objects) {
         httpMyCouponInfo = new HttpMyCouponInfo(mActivity, MyCouponDetailManager.getMyCounponDetail(
                uid,
                authtoken,
                mEx_coupon_id,
                lat,
                lng

        ));
        httpMyCouponInfo.call();
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        if (!httpMyCouponInfo.isSuccess()){
            CustomNetworkDialog networkDialog = new CustomNetworkDialog(mActivity);
            networkDialog.initNetworkDialog(null, null);
            networkDialog.show();
            return;
        }

        ((MainActivity) mActivity).changeBackFragment(new MyCouponDetailFragment());
    }
}
