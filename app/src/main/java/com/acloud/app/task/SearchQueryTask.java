package com.acloud.app.task;

import android.app.Activity;
import android.app.ProgressDialog;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;

import com.acloud.app.MainActivity;
import com.acloud.app.dialog.CustomDialog;
import com.acloud.app.dialog.CustomDialogOrange;
import com.acloud.app.dialog.CustomNetworkDialog;
import com.acloud.app.fragment.SearchResultFragment;
import com.acloud.app.http.HttpStoreList;
import com.acloud.app.tool.Pub;
import com.acloud.app.util.LocatUtil;
import com.acloud.app.util.PrefConstant;
import com.acloud.app.util.StoreListManager;

import org.apache.http.NameValuePair;

import java.util.ArrayList;

/**
 * Created by skywind-10 on 2015/7/13.
 */
public class SearchQueryTask extends AsyncTask {
    ProgressDialog
            bar;

    Activity
            mActivity;

    ArrayList<NameValuePair>
            list;

    Handler
            handler;

    HttpStoreList
            httpStoreList;

    String
            querytxt;

    boolean refresh;

    public SearchQueryTask(Activity activity, String querytxt, String lat, String lng, boolean refresh, Handler handler) {
        mActivity = activity;
        setNVP(lat, lng, "0", "0", "0", "0", "1", false, querytxt, "", "");
        this.querytxt = querytxt;
        this.refresh = refresh;
        this.handler = handler;
    }

    public SearchQueryTask(Activity activity, String querytxt, String lat, String lng, String sdid, String smid,
                           String scid, String scaid, String sortType, Handler handler,
                           String price_range_start, String price_range_end, boolean refresh) {
        mActivity = activity;
        this.handler = handler;
        this.querytxt = querytxt;
        setNVP(lat, lng, sdid, smid, scid, scaid, sortType, false, querytxt, price_range_start, price_range_end);
        this.refresh = refresh;
    }


    @Override
    protected void onPreExecute() {
        bar = new ProgressDialog(mActivity);
        bar.setMessage("Loading");
        bar.show();
    }


    @Override
    protected Object doInBackground(Object[] objects) {
        httpStoreList = new HttpStoreList(mActivity, list);
        httpStoreList.call();
        return null;
    }


    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        bar.dismiss();
        if (!httpStoreList.isSuccess()) {
            CustomNetworkDialog networkDialog = new CustomNetworkDialog(mActivity);
            networkDialog.initNetworkDialog(null, null);
            networkDialog.show();
            return;
        }
        if (refresh) {
            Fragment f = new SearchResultFragment();
            Bundle b = new Bundle();
            b.putString(Pub.VALUE_QUERYTEXT, querytxt);
            f.setArguments(b);
            ((MainActivity) mActivity).changeFragment(f);
        } else {
            handler.sendEmptyMessage(SearchResultFragment.SUCCESS);
        }


    }

    public void setNVP(String lat, String lng, String sdid, String smid, String scid, String scaid, String sort_type, boolean update, String keyword,
                       String price_range_start, String price_range_end) {
        list = StoreListManager.getStore_List(
                PrefConstant.getString(mActivity, Pub.VALUE_UID, ""),
                PrefConstant.getString(mActivity, Pub.VALUE_AUTHTOKEN, ""),
                "0",
                "20",
                keyword,
                sdid,
                smid,
                scid,
                scaid,
                sort_type,
                price_range_start,
                price_range_end,
                lat,
                lng);

    }
}
