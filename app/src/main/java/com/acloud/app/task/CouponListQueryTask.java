package com.acloud.app.task;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Handler;

import com.acloud.app.MainActivity;
import com.acloud.app.dialog.CustomDialog;
import com.acloud.app.dialog.CustomDialogOrange;
import com.acloud.app.dialog.CustomNetworkDialog;
import com.acloud.app.dialog.CustomProgressDialog;
import com.acloud.app.fragment.CouponListFragment;
import com.acloud.app.http.HttpCheckUserVerify;
import com.acloud.app.http.HttpCouponList;
import com.acloud.app.http.HttpCurrentBonusGet;
import com.acloud.app.tool.Pub;
import com.acloud.app.tool.TLog;
import com.acloud.app.tool.WebService;
import com.acloud.app.util.CouponListManager;
import com.acloud.app.util.PrefConstant;
import com.acloud.app.util.UserProfileManager;
import com.acloud.app.util.UserVerifyCheckManager;

import org.apache.http.NameValuePair;

import java.util.ArrayList;

/**
 * 　CouponList
 */
public class CouponListQueryTask extends AsyncTask {
    CustomProgressDialog mDialog;
    Activity mActivity;
    String index, size, keyword, sdid, smid, scid, scaid, sort_type, lat, lng;
    ArrayList<NameValuePair> list;
    HttpCouponList couponList;
    boolean isScroll, unupdate;
    Handler handler;

    public CouponListQueryTask(Activity activity, CustomProgressDialog dialog, String lat, String lng) {
        mActivity = activity;
        mDialog = dialog;
        this.lat = lat;
        this.lng = lng;
        setNVP();
    }

//    public CouponListQueryTask(Activity activity, CustomDialog dialog, String index, String size, String keyword, String sdid, String smid, String scid, String scaid, String sort_type,
//                               String lat, String lng) {
//        mActivity = activity;
//        mDialog = dialog;
//        this.index = index;
//        this.size = size;
//        this.keyword = keyword;
//        this.sdid = sdid;
//        this.smid = smid;
//        this.scid = scid;
//        this.scaid = scaid;
//        this.sort_type = sort_type;
//        this.lat = lat;
//        this.lng = lng;
//        setNVP(index, size, keyword, sdid, smid, scid, scaid, sort_type);
//    }

    public CouponListQueryTask(Activity activity, CustomProgressDialog dialog, String index, String size, String keyword, String sdid, String smid, String scid, String scaid, String sort_type,
                               String lat, String lng, Handler handler, boolean isScroll, boolean unupdate) {
        mActivity = activity;
        mDialog = dialog;
        this.index = index;
        this.size = size;
        this.keyword = keyword;
        this.sdid = sdid;
        this.smid = smid;
        this.scid = scid;
        this.scaid = scaid;
        this.sort_type = sort_type;
        this.lat = lat;
        this.lng = lng;
        setNVP(index, size, keyword, sdid, smid, scid, scaid, sort_type);
        this.isScroll = isScroll;
        this.handler = handler;
        this.unupdate = unupdate;
    }


    @Override
    protected Object doInBackground(Object[] objects) {
        //取得優惠卷列表
        TLog.i("Ernest", "couponlist nvp : " + list);
        couponList = new HttpCouponList(mActivity, list);
        couponList.call();

        //取得目前點數
        HttpCurrentBonusGet bonusGet = new HttpCurrentBonusGet(mActivity,
                WebService.getUid_AuthToken(
                        PrefConstant.getString(mActivity, Pub.VALUE_UID, ""),
                        PrefConstant.getString(mActivity, Pub.VALUE_AUTHTOKEN, "")));
        bonusGet.call();
        //檢查使用者驗證狀態

        if (!UserProfileManager.loginStatus(mActivity))
            return null;

        HttpCheckUserVerify userVerify = new HttpCheckUserVerify(mActivity,
                WebService.getUid_AuthToken(PrefConstant.getString(mActivity, Pub.VALUE_UID, ""),
                        PrefConstant.getString(mActivity, Pub.VALUE_AUTHTOKEN, "")));
        userVerify.call();

        return null;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mDialog = new CustomProgressDialog(mActivity);
        mDialog.initProgressDialog();
        if (mDialog != null && !mDialog.isShowing()) {
            mDialog.show();
        }
    }

    @Override
    protected void onPostExecute(Object o) {
        mDialog.dismiss();

        if (!couponList.isSuccess()) {
            CustomNetworkDialog networkDialog = new CustomNetworkDialog(mActivity);
            networkDialog.initNetworkDialog(null, null);
            networkDialog.show();
            return;
        }
        if (UserProfileManager.loginStatus(mActivity))
            UserVerifyCheckManager.checkVerifyStatus(mActivity);

        if (isScroll) {
            handler.sendEmptyMessage(CouponListFragment.isSuccess);
            return;

        }
        if (unupdate) {
            handler.sendEmptyMessage(CouponListFragment.isSort);
        } else {
            ((MainActivity) mActivity).changeFragment(new CouponListFragment());
        }
        super.onPostExecute(o);
    }

    public void setNVP() {
        list = CouponListManager.getCoupon_List(
                PrefConstant.getString(mActivity, Pub.VALUE_UID, ""),
                PrefConstant.getString(mActivity, Pub.VALUE_AUTHTOKEN, ""),
                "0",
                "20",
                "",
                "0",
                "0",
                "0",
                "0",
                "1",
                lat,
                lng
        );
    }

    public void setNVP(String index, String size, String keyword, String sdid, String smid, String scid, String scaid,
                       String sort_type) {
        list = CouponListManager.getCoupon_List(
                PrefConstant.getString(mActivity, Pub.VALUE_UID, ""),
                PrefConstant.getString(mActivity, Pub.VALUE_AUTHTOKEN, ""),
                index,
                size,
                keyword,
                sdid,
                smid,
                scid,
                scid,
                sort_type,
                lat,
                lng
        );
    }
}
