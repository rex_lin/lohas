package com.acloud.app.task;

import android.app.Activity;
import android.os.AsyncTask;

import com.acloud.app.MainActivity;
import com.acloud.app.dialog.CustomDialog;
import com.acloud.app.dialog.CustomDialogOrange;
import com.acloud.app.dialog.CustomNetworkDialog;
import com.acloud.app.dialog.CustomProgressDialog;
import com.acloud.app.fragment.CouponDetailFragment;
import com.acloud.app.fragment.StoreCouponDetailFragment;
import com.acloud.app.http.HttpCouponInfo;
import com.acloud.app.util.CouponDetailManager;

/**
 * 　取得優惠卷列表Task
 */
public class CouponDetailQueryTask extends AsyncTask {
    CustomProgressDialog mDialog;
    Activity mActivity;
    String uid, authtoken, coupon_id, lat, lng;
    String from;
    HttpCouponInfo httpCouponInfo;

    public CouponDetailQueryTask(Activity activity, CustomProgressDialog dialog, String uid, String authtoken,
                                 String coupon_id, String lat, String lng, String from) {
        mActivity = activity;
        mDialog = dialog;
        this.uid = uid;
        this.authtoken = authtoken;
        this.coupon_id = coupon_id;
        this.lat = lat;
        this.lng = lng;
        this.from = from;
    }


    @Override
    protected Object doInBackground(Object[] objects) {
        httpCouponInfo = new HttpCouponInfo(mActivity,
                CouponDetailManager.getCouponDetail(uid, authtoken, coupon_id, lat, lng));
        httpCouponInfo.call();

        return null;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mDialog = new CustomProgressDialog(mActivity);
        mDialog.initProgressDialog();
        if (mDialog != null && !mDialog.isShowing()) {
            mDialog.show();
        }
    }

    @Override
    protected void onPostExecute(Object o) {
        mDialog.dismiss();
        if (!httpCouponInfo.isSuccess()){
            CustomNetworkDialog networkDialog = new CustomNetworkDialog(mActivity);
            networkDialog.initNetworkDialog(null, null);
            networkDialog.show();
            return;
        }

        if (from.equals("store")) {
            ((MainActivity) mActivity).changeBackFragment(new StoreCouponDetailFragment());
        } else {
            ((MainActivity) mActivity).changeBackFragment(new CouponDetailFragment());
        }

        super.onPostExecute(o);
    }
}
