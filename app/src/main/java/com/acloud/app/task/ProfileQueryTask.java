package com.acloud.app.task;

import android.app.Activity;
import android.os.AsyncTask;

import com.acloud.app.MainActivity;
import com.acloud.app.dialog.CustomDialog;
import com.acloud.app.dialog.CustomDialogOrange;
import com.acloud.app.dialog.CustomNetworkDialog;
import com.acloud.app.dialog.CustomProgressDialog;
import com.acloud.app.fragment.ProfileFragment;
import com.acloud.app.http.HttpCheckUserVerify;
import com.acloud.app.http.HttpUserProfileGet;
import com.acloud.app.model.UserProfileResult;
import com.acloud.app.tool.Pub;
import com.acloud.app.tool.WebService;
import com.acloud.app.util.PrefConstant;
import com.acloud.app.util.UserProfileManager;
import com.acloud.app.util.UserVerifyCheckManager;

/**
 * Created by skywind-10 on 2015/7/7.
 */
public class ProfileQueryTask extends AsyncTask {
    CustomProgressDialog mDialog;
    Activity mActivity;
    HttpUserProfileGet getProfile;
    public ProfileQueryTask(Activity activity, CustomProgressDialog dialog) {
        mDialog = dialog;
        mActivity = activity;
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        mDialog.dismiss();

        if (getProfile.isSuccess()) {
            if (getProfile.isSysSucess()) {
                //從UserProfileResult 取得解析完的Profile結果
                UserProfileResult userProfile = getProfile.getUserProfileResult();
                //將結果丟到Manager
                UserProfileManager.setUserProfile(mActivity, userProfile);
            } else {
                //TODO syscode400
            }
        } else {
            CustomNetworkDialog networkDialog = new CustomNetworkDialog(mActivity);
            networkDialog.initNetworkDialog(null, null);
            networkDialog.show();
            return;
        }

        if (UserProfileManager.loginStatus(mActivity))
            UserVerifyCheckManager.checkVerifyStatus(mActivity);


        ((MainActivity) mActivity).changeFragment(new ProfileFragment());

    }

    @Override
    protected Object doInBackground(Object[] objects) {
        //getProfile
         getProfile = new HttpUserProfileGet(mActivity,
                WebService.getUid_AuthToken(
                        PrefConstant.getString(mActivity, Pub.VALUE_UID, ""),
                        PrefConstant.getString(mActivity, Pub.VALUE_AUTHTOKEN, "")));
        getProfile.call();



        if (!UserProfileManager.loginStatus(mActivity))
            return null;

        HttpCheckUserVerify userVerify = new HttpCheckUserVerify(mActivity,
                WebService.getUid_AuthToken(PrefConstant.getString(mActivity, Pub.VALUE_UID, ""),
                        PrefConstant.getString(mActivity, Pub.VALUE_AUTHTOKEN, "")));
        userVerify.call();
        return null;
    }

    @Override
    protected void onProgressUpdate(Object[] values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPreExecute() {
        mDialog = new CustomProgressDialog(mActivity);
        mDialog.initProgressDialog();
        mDialog.show();
    }
}