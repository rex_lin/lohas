package com.acloud.app.task;

import android.app.Activity;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Handler;

import com.acloud.app.MainActivity;
import com.acloud.app.dialog.CustomNetworkDialog;
import com.acloud.app.dialog.CustomProgressDialog;
import com.acloud.app.fragment.ActivityFragment;
import com.acloud.app.http.HttpActivityList;
import com.acloud.app.http.HttpCurrentBonusGet;
import com.acloud.app.http.HttpMenuShoppingDistrict;
import com.acloud.app.http.HttpMenuStoreCategory;
import com.acloud.app.http.HttpMenuVersionGet;
import com.acloud.app.model.ShoppingDistrictMenuResult;
import com.acloud.app.model.StoreCategoryMenuResult;
import com.acloud.app.tool.Pub;
import com.acloud.app.tool.TLog;
import com.acloud.app.tool.WebService;
import com.acloud.app.util.ActivityListManager;
import com.acloud.app.util.LocatUtil;
import com.acloud.app.util.PrefConstant;
import com.acloud.app.util.ShoppingDistrictMenuManager;
import com.acloud.app.util.StoreCategoryMenuManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by skywind on 2015/8/19.
 */
public class ActivityListQueryTask extends AsyncTask {

    Activity mActivity;
    LocatUtil locatUtil;
    Location lc;
    HttpActivityList activityList;
    CustomProgressDialog loadingDialog;
    String gettype,getsort_type;
    Handler handler;
    boolean menuupdate;//判斷商圈選單更新
    String from;


    public ActivityListQueryTask(Activity activity,String type,String sort_type) {
        super();
        mActivity = activity;
        locatUtil = ((MainActivity)activity).getLocatUtil();
        gettype=type;
        getsort_type=sort_type;
        lc = locatUtil.getUpdatedLocation();

    }
    public ActivityListQueryTask(Activity activity,String type,String sort_type, Handler handler, String from) {
        super();
        mActivity = activity;
        locatUtil = ((MainActivity)activity).getLocatUtil();
        gettype=type;
        getsort_type=sort_type;
        lc = locatUtil.getUpdatedLocation();
        this.handler = handler;
        this.from = from;
    }

    @Override
    protected Object doInBackground(Object[] objects) {
        activityList = new HttpActivityList(mActivity,
                ActivityListManager.getList(
                        PrefConstant.getString(mActivity, Pub.VALUE_UID, "0"),
                        PrefConstant.getString(mActivity, Pub.VALUE_AUTHTOKEN, "0"),
                        lc != null ? lc.getLatitude() + "" : "0",
                        lc != null ? lc.getLongitude() + "" : "0",
                        gettype,
                        getsort_type));
        //取得目前點數
        HttpCurrentBonusGet bonusGet = new HttpCurrentBonusGet(mActivity,
                WebService.getUid_AuthToken(
                        PrefConstant.getString(mActivity, Pub.VALUE_UID, ""),
                        PrefConstant.getString(mActivity, Pub.VALUE_AUTHTOKEN, "")));
        bonusGet.call();

        activityList.call();
        HttpMenuVersionGet menuVersionGet = new HttpMenuVersionGet(mActivity,
                WebService.getUid_AuthToken(
                        PrefConstant.getString(mActivity, Pub.VALUE_UID, ""),
                        PrefConstant.getString(mActivity, Pub.VALUE_AUTHTOKEN, "")
                ));
        menuupdate = menuVersionGet.call();
        if (menuupdate) {
            //商圈選單
            HttpMenuShoppingDistrict shoppingDistrict = new HttpMenuShoppingDistrict(mActivity, ShoppingDistrictMenuManager.getMenu(
                    PrefConstant.getString(mActivity, Pub.VALUE_UID, ""),
                    PrefConstant.getString(mActivity, Pub.VALUE_AUTHTOKEN, "")
            ));
            shoppingDistrict.call();

            HttpMenuStoreCategory menuStoreCategory = new HttpMenuStoreCategory(mActivity, StoreCategoryMenuManager.getMenu(
                    PrefConstant.getString(mActivity, Pub.VALUE_UID, ""),
                    PrefConstant.getString(mActivity, Pub.VALUE_AUTHTOKEN, "")
            ));
            menuStoreCategory.call();
        }

        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        loadingDialog = new CustomProgressDialog(mActivity);
        loadingDialog.initProgressDialog(null);
        loadingDialog.show();
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        new parserJsonCategory().parserJson(PrefConstant.getString(mActivity, Pub.VALUE_MENU_STORE_CATEGORY, ""));
        new parserJsonShopping().parserJson(PrefConstant.getString(mActivity, Pub.VALUE_MENU_SHOPPING_DISTRICT, ""));

        if (from == "0"){
            loadingDialog.dismiss();
            handler.sendEmptyMessage(1);
            return;
        }

        if (activityList.isSuccess()) {
            loadingDialog.dismiss();
            if (from == "1"){
                handler.sendEmptyMessage(2);
                return;
            }
            ((MainActivity) mActivity).changeFragment(new ActivityFragment());

        } else {
            loadingDialog.dismiss();
            CustomNetworkDialog networkDialog = new CustomNetworkDialog(mActivity);
            networkDialog.initNetworkDialog(null, null);
            networkDialog.show();
        }
    }

    private class parserJsonCategory {
        String syscode;
        String sysmsg;
        String update_datetime;
        ArrayList<StoreCategoryMenuResult> results = new ArrayList<StoreCategoryMenuResult>();
        StoreCategoryMenuResult.scaResult scaResult = new StoreCategoryMenuResult.scaResult();

        void parserJson(String message) {
            JSONObject object = null;
            JSONObject object1 = null;
            JSONArray array = null;
            try {
                object = new JSONObject(message);
                syscode = object.optString("syscode");
                sysmsg = object.optString("sysmsg");
                if (syscode.equals("200")) {
                    object1 = object.optJSONObject("data");
                    update_datetime = object1.optString("update_datetime");
                    array = object.optJSONArray("list");
                    for (int i = 0; i < array.length(); i++) {
                        StoreCategoryMenuResult result = new StoreCategoryMenuResult();
                        JSONObject object2 = (JSONObject) array.get(i);
                        result.setSc_id(object2.optString("sc_id"));
                        result.setSc_name(object2.optString("sc_name"));


                        JSONArray jsonArray = object2.optJSONArray("sca_list");
                        ArrayList<StoreCategoryMenuResult.scaResult> scaResultsArray = new ArrayList<StoreCategoryMenuResult.scaResult>();
                        if (jsonArray != null) {
                            for (int j = 0; j < jsonArray.length(); j++) {
                                JSONObject object3 = (JSONObject) jsonArray.get(j);
                                scaResult = null;
                                scaResult = new StoreCategoryMenuResult.scaResult();

                                scaResult.setSca_id(object3.optString("sca_id"));
                                scaResult.setSca_name(object3.optString("sca_name"));
                                scaResultsArray.add(scaResult);
                            }
                        }
                        result.setScaResultArrayList(scaResultsArray);
                        results.add(result);

                    }
                    StoreCategoryMenuManager.setList(results);

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private class parserJsonShopping {
        String syscode;
        String sysmsg;

        ShoppingDistrictMenuResult.smResult smResult = new ShoppingDistrictMenuResult.smResult();
        ArrayList<ShoppingDistrictMenuResult> results = new ArrayList<ShoppingDistrictMenuResult>();

        void parserJson(String message) {
            JSONObject object = null;
            JSONObject object1 = null;
            JSONArray array = null;
            try {
                object = new JSONObject(message);
                syscode = object.optString("syscode");
                sysmsg = object.optString("sysmsg");
                if (syscode.equals("200")) {
                    object1 = object.getJSONObject("data");
                    String update_datetime = object1.optString("update_datetime");
                    //TODO 更新資料時間
                    TLog.i("Ernest", "update_datetime : " + update_datetime); //更新資料時間
                    array = object.optJSONArray("list");
                    for (int i = 0; i < array.length(); i++) {
                        ShoppingDistrictMenuResult result = new ShoppingDistrictMenuResult();
                        JSONObject object2 = (JSONObject) array.get(i);
                        result.setSd_id(object2.optString("sd_id"));
                        result.setSd_name(object2.optString("sd_name"));

                        JSONArray jsonArray = object2.optJSONArray("sm_list");
                        ArrayList<ShoppingDistrictMenuResult.smResult> smResultArray = new ArrayList<ShoppingDistrictMenuResult.smResult>();
                        if(jsonArray != null){

                            for (int j = 0; j < jsonArray.length(); j++) {
                                JSONObject object3 = (JSONObject) jsonArray.get(j);
                                smResult = null;
                                smResult = new ShoppingDistrictMenuResult.smResult();
                                smResult.setSm_id(object3.optString("sm_id"));
                                smResult.setSm_name(object3.optString("sm_name"));
                                smResultArray.add(smResult);
                            }
                            result.setSmResultArrayList(smResultArray);

                            results.add(result);
                        }
                    }
                    ShoppingDistrictMenuManager.setList(results);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    }
}
