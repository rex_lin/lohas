package com.acloud.app.task;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Handler;

import com.acloud.app.MainActivity;
import com.acloud.app.dialog.CustomDialog;
import com.acloud.app.dialog.CustomDialogOrange;
import com.acloud.app.dialog.CustomNetworkDialog;
import com.acloud.app.dialog.CustomProgressDialog;
import com.acloud.app.fragment.StoreFragment;
import com.acloud.app.http.HttpCheckUserVerify;
import com.acloud.app.http.HttpMenuShoppingDistrict;
import com.acloud.app.http.HttpMenuStoreCategory;
import com.acloud.app.http.HttpMenuVersionGet;
import com.acloud.app.http.HttpStoreList;
import com.acloud.app.model.ShoppingDistrictMenuResult;
import com.acloud.app.model.StoreCategoryMenuResult;
import com.acloud.app.tool.Pub;
import com.acloud.app.tool.TLog;
import com.acloud.app.tool.WebService;
import com.acloud.app.util.PrefConstant;
import com.acloud.app.util.ShoppingDistrictMenuManager;
import com.acloud.app.util.StoreCategoryMenuManager;
import com.acloud.app.util.StoreListManager;
import com.acloud.app.util.UserProfileManager;
import com.acloud.app.util.UserVerifyCheckManager;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by skywind-10 on 2015/7/7.
 */
public class StoreListQueryTask extends AsyncTask {
    Activity mActivity;
    CustomProgressDialog mDialog;
    HttpCheckUserVerify userVerify;
    String lat, lng;
    ArrayList<NameValuePair> list;
    boolean update = true;
//    boolean menuupdate;//判斷商圈選單更新
    Handler handler;
    boolean isScroll;
    HttpStoreList storeList;

    public StoreListQueryTask(Activity activity, CustomProgressDialog dialog, String lat, String lng) {
        mActivity = activity;
        mDialog = dialog;
        this.lat = lat;
        this.lng = lng;
        setNVP();
    }

    /**
     * 使用在排序時
     * @param activity
     * @param dialog
     * @param lat
     * @param lng
     * @param sdid
     * @param smid
     * @param scid
     * @param scaid
     * @param sorttype
     * @param update
     * @param handler
     */
    public StoreListQueryTask(Activity activity, CustomProgressDialog dialog, String lat, String lng, String sdid, String smid, String scid, String scaid
            , String sorttype, boolean update, Handler handler) {
        mActivity = activity;
        mDialog = dialog;
        this.lat = lat;
        this.lng = lng;
        this.handler = handler;
        setNVP(sdid, smid, scid, scaid, sorttype, update);
    }

    /**
     * 使用在ListViewScroll時
     * @param activity
     * @param dialog
     * @param lat
     * @param lng
     * @param sdid
     * @param smid
     * @param scid
     * @param scaid
     * @param sorttype
     * @param update
     * @param handler
     * @param index
     * @param scroll
     */
    public StoreListQueryTask(Activity activity, CustomProgressDialog dialog, String lat, String lng, String sdid, String smid, String scid, String scaid
            , String sorttype, boolean update, Handler handler, String index, boolean scroll) {
        mActivity = activity;
        mDialog = dialog;
        this.lat = lat;
        this.lng = lng;
        this.handler = handler;
        setNVP(sdid, smid, scid, scaid, sorttype, update, index);
        isScroll = scroll;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mDialog = new CustomProgressDialog(mActivity);
        mDialog.initProgressDialog();
        if (!mDialog.isShowing())
            mDialog.show();
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        mDialog.dismiss();
        if (!storeList.isSuccess()) {
            CustomNetworkDialog networkDialog = new CustomNetworkDialog(mActivity);
            networkDialog.initNetworkDialog(null, null);
            networkDialog.show();
        }

        // 檢查使用者驗證狀態
        if (UserProfileManager.loginStatus(mActivity))
            UserVerifyCheckManager.checkVerifyStatus(mActivity);
//        new parserJsonCategory().parserJson(PrefConstant.getString(mActivity, Pub.VALUE_MENU_STORE_CATEGORY, ""));
//        new parserJsonShopping().parserJson(PrefConstant.getString(mActivity, Pub.VALUE_MENU_SHOPPING_DISTRICT, ""));

        if (isScroll) {
            handler.sendEmptyMessage(1);
            return;
        }

        if (update) {
            ((MainActivity) mActivity).changeFragment(new StoreFragment());
        } else {
            handler.sendEmptyMessage(0);
        }

    }

    @Override
    protected Object doInBackground(Object[] objects) {
        storeList = new HttpStoreList(mActivity, list);
        storeList.call();
        // 檢查使用者驗證狀態
        if (UserProfileManager.loginStatus(mActivity)) {
            userVerify = new HttpCheckUserVerify(mActivity,
                    WebService.getUid_AuthToken(PrefConstant.getString(mActivity, Pub.VALUE_UID, ""),
                            PrefConstant.getString(mActivity, Pub.VALUE_AUTHTOKEN, "")));
            userVerify.call();
        }

        return null;
    }

    //預設第一次Loading載入的資料
    public void setNVP() {
        list = StoreListManager.getStore_List(
                PrefConstant.getString(mActivity, Pub.VALUE_UID, ""),
                PrefConstant.getString(mActivity, Pub.VALUE_AUTHTOKEN, ""),
                "0",
                "20",
                "",
                "0",
                "0",
                "0",
                "0",
                "1",
                "",
                "",
                lat,
                lng);

    }

    //有篩選條件時載入
    public void setNVP(String sdid, String smid, String scid, String scaid, String sort_type, boolean update) {
        this.update = update;
        list = StoreListManager.getStore_List(
                PrefConstant.getString(mActivity, Pub.VALUE_UID, ""),
                PrefConstant.getString(mActivity, Pub.VALUE_AUTHTOKEN, ""),
                "0",
                "20",
                "",
                sdid,
                smid,
                scid,
                scaid,
                sort_type,
                "",
                "",
                lat,
                lng);

    }

    //有篩選條件時載入 換頁
    public void setNVP(String sdid, String smid, String scid, String scaid, String sort_type, boolean update, String index) {
        this.update = update;
        list = StoreListManager.getStore_List(
                PrefConstant.getString(mActivity, Pub.VALUE_UID, ""),
                PrefConstant.getString(mActivity, Pub.VALUE_AUTHTOKEN, ""),
                index,
                "20",
                "",
                sdid,
                smid,
                scid,
                scaid,
                sort_type,
                "",
                "",
                lat,
                lng);

    }
    }