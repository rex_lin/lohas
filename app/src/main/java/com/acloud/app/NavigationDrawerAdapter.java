//package com.acloud.app;
//
//import android.content.Context;
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.MotionEvent;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.TextView;
//
//import com.acloud.app.tool.TLog;
//
//import java.util.List;
//
///**
// * NavigationDrawer
// */
//public class NavigationDrawerAdapter extends RecyclerView.Adapter<NavigationDrawerAdapter.ViewHolder> {
//
//    private List<NavigationItem> mData;
//    private NavigationDrawerCallbacks mNavigationDrawerCallbacks;
//    private int mSelectedPosition;
//    private int mTouchedPosition = -1;
//    private Context context;
//    public NavigationDrawerAdapter(List<NavigationItem> data) {
//        mData = data;
//    }
//
//    public NavigationDrawerCallbacks getNavigationDrawerCallbacks() {
//        return mNavigationDrawerCallbacks;
//    }
//
//    public void setNavigationDrawerCallbacks(NavigationDrawerCallbacks navigationDrawerCallbacks) {
//        mNavigationDrawerCallbacks = navigationDrawerCallbacks;
//    }
//
//    @Override
//    public NavigationDrawerAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
//        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_drawer_row, viewGroup, false);
//        final ViewHolder viewholder = new ViewHolder(v);
//        viewholder.itemView.setOnTouchListener(
//                new View.OnTouchListener() {
//                    @Override
//                    public boolean onTouch(View view, MotionEvent motionEvent) {
//                        switch (motionEvent.getAction()) {
//                            case MotionEvent.ACTION_DOWN:
//                                TLog.i("Ernest" , "onTouch ACTION_DOWN: " );
//                                touchPosition(viewholder.getAdapterPosition());
//                                return false;
//                            case MotionEvent.ACTION_CANCEL:
//                                TLog.i("Ernest" , "onTouch ACTION_CANCEL: " );
//                                touchPosition(-1);
//                                return false;
//                            case MotionEvent.ACTION_MOVE:
//                                TLog.i("Ernest" , "onTouch ACTION_MOVE: " );
//                                return false;
//                            case MotionEvent.ACTION_UP:
//                                TLog.i("Ernest" , "onTouch ACTION_UP: " );
//                                touchPosition(-1);
//                                return false;
//                        }
//                        return true;
//                    }
//                }
//        );
////        viewholder.itemView.setOnClickListener(
////                new View.OnClickListener() {
////                    @Override
////                    public void onClick(View v) {
////                        TLog.i("Ernest onClick", "onClick i  : " + v);
////                        if (mNavigationDrawerCallbacks != null)
////                            mNavigationDrawerCallbacks.onNavigationDrawerItemSelected(viewholder.getAdapterPosition());
////                    }
////                }
////        );
//        viewholder.textView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                TLog.i("Ernest onClick", "onClick i  : " + view);
//                if (mNavigationDrawerCallbacks != null)
//                    mNavigationDrawerCallbacks.onNavigationDrawerItemSelected(viewholder.getAdapterPosition());
//            }
//        });
//
//
//        return viewholder;
//    }
//
//    @Override
//    public void onBindViewHolder(NavigationDrawerAdapter.ViewHolder viewHolder, final int i) {
//        //Adapter getView
//
//        viewHolder.textView.setText(mData.get(i).getText());
//        //if you wanted to set image
//        viewHolder.textView.setCompoundDrawablesWithIntrinsicBounds(mData.get(i).getDrawable(), null, null, null);
//        TLog.i("onBindViewHolder", "onBindViewHolder i  : " + i);
//
//        //控制NavigationDrawer的背景色
//        if (mSelectedPosition == i || mTouchedPosition == i) {
//            viewHolder.itemView.setBackgroundColor(viewHolder.itemView.getContext().getResources().getColor(R.color.backgorund4_2));
//        } else {
//            viewHolder.itemView.setBackgroundColor(viewHolder.itemView.getContext().getResources().getColor(R.color.background3));
//        }
//    }
//
//    private void touchPosition(int position) {
//        TLog.i("Ernest", "touchPosition : " + position);
//        int lastPosition = mTouchedPosition;
//        mTouchedPosition = position;
//        if (lastPosition >= 0)
//            notifyItemChanged(lastPosition);
//        if (position >= 0)
//            notifyItemChanged(position);
//    }
//
//    public void selectPosition(int position) {
//        TLog.i("Ernest", "selectPosition : " + position);
//        int lastPosition = mSelectedPosition;
//        mSelectedPosition = position;
//        notifyItemChanged(lastPosition);
//        notifyItemChanged(position);
//    }
//
//    @Override
//    public int getItemCount() {
//        return mData != null ? mData.size() : 0;
//    }
//
//    public static class ViewHolder extends RecyclerView.ViewHolder {
//        public TextView textView;
//
//        public ViewHolder(View itemView) {
//            super(itemView);
//            textView = (TextView) itemView.findViewById(R.id.item_name);
//        }
//    }
//}
