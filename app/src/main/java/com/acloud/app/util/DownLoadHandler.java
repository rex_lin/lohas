package com.acloud.app.util;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.widget.ImageView;

/**
 * Created by skywind-10 on 2015/7/23.
 */
public class DownLoadHandler extends Handler {
    ImageView imageView;

    public DownLoadHandler(ImageView view) {
        imageView = view;
    }

    @Override
    public void handleMessage(Message msg) {
        imageView.setImageBitmap((Bitmap) msg.obj);
    }
}