package com.acloud.app.util;

import com.acloud.app.model.LoginResult;

/**
 * 管理Login
 */
public class LoginManager {

    static LoginResult result = new LoginResult();

    public static LoginResult getResult() {
        return result;
    }

    public static void setResult(LoginResult result) {
        LoginManager.result = result;
    }
}
