package com.acloud.app.util;

import com.acloud.app.model.MyCouponDetailResult;
import com.acloud.app.tool.Pub;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;

/**
 * 管理coupondetail
 */
public class MyCouponDetailManager {

    public static MyCouponDetailResult getMyCouponDetailResult() {
        return myCouponDetailResult;
    }

    public static void setMyCouponDetailResult(MyCouponDetailResult myCouponDetailResult) {
        MyCouponDetailManager.myCouponDetailResult = myCouponDetailResult;
    }

    static MyCouponDetailResult myCouponDetailResult = new MyCouponDetailResult();

    public static ArrayList<NameValuePair> getMyCounponDetail(String uid, String authtoken, String ex_coupon_id, String lat, String lng) {
        ArrayList<NameValuePair> arrayList = new ArrayList<NameValuePair>();
        arrayList.add(new BasicNameValuePair(Pub.VALUE_APIKEY, Pub.APIKEY));
        arrayList.add(new BasicNameValuePair(Pub.VALUE_UID, uid));
        arrayList.add(new BasicNameValuePair(Pub.VALUE_AUTHTOKEN, authtoken));
        arrayList.add(new BasicNameValuePair(Pub.VALUE_EX_COUPON_ID, ex_coupon_id));
        arrayList.add(new BasicNameValuePair(Pub.VALUE_LAT, lat));
        arrayList.add(new BasicNameValuePair(Pub.VALUE_LNG, lng));

        return arrayList;
    }
}
