package com.acloud.app.util;

import android.app.Activity;
import android.os.Handler;
import android.widget.Toast;

import com.acloud.app.MainActivity;
import com.acloud.app.fragment.RecommendedFragment;
import com.acloud.app.fragment.VerifyDialogFragment;
import com.acloud.app.model.UserVerifyResult;


/**
 * 使用者驗證
 */
public class UserVerifyCheckManager {

    static UserVerifyResult result = new UserVerifyResult();

    public static UserVerifyResult getResult() {
        return result;
    }

    public static void setResult(UserVerifyResult result) {
        UserVerifyCheckManager.result = result;
    }

    /**
     * 0 = 尚未驗證成功
     * 1 = 已驗證通過
     */
    public static void checkVerifyStatus(final Activity activity) {
        if (result == null) return;

        if (result.getIs_verify() != null && result.getIs_verify().equals("0")) {
            VerifyDialogFragment fragment = new VerifyDialogFragment().Instance();
            try {
                fragment.show(((MainActivity) activity).getSupportFragmentManager(), "");
                fragment.setCancelable(false);
            } catch (IllegalStateException e){
                e.printStackTrace();
            }
        }
        if (result.getIs_verify() != null && result.getIs_verify().equals("1")) {
            if (result.getRecommended_serial_no().equals("")) {
                //TODO 檢查推薦人
            }
        }
    }
}
