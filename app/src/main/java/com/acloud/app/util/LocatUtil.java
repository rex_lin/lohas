package com.acloud.app.util;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import com.acloud.app.tool.TLog;

public class LocatUtil {

    Context ctx;
    Location updatedLocation = null;
    LocationListener gpsLocationListener, networkLocationListener;
    LocationManager locationManager;

    public LocatUtil(Context context) {
        ctx = context;
        gpsLocationListener = new android.location.LocationListener() {
            public void onLocationChanged(Location location) {
                if (location.getAccuracy() < 100) {
                    updatedLocation = location;
                    TLog.d("Ernest", "onLocationChange");
                }

            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {


            }

            public void onProviderDisabled(String provider) {

            }
        };

        networkLocationListener = new android.location.LocationListener() {
            public void onLocationChanged(Location location) {

                if (location.getAccuracy() <= 100) {

                    updatedLocation = location;
                    TLog.d("Ernest", "onLocationChange");
                }


            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {

            }

            public void onProviderDisabled(String provider) {


            }
        };


        locationManager = (LocationManager) ctx.getSystemService(ctx.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000, 10, gpsLocationListener);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 2000, 10, networkLocationListener);

    }


    public Location getUpdatedLocation() {
        return updatedLocation;
    }

    public void stopUpdates() {
        locationManager.removeUpdates(gpsLocationListener);
        locationManager.removeUpdates(networkLocationListener);
    }

    public boolean isOpenGps() {

        LocationManager locationManager
                = (LocationManager) ctx.getSystemService(Context.LOCATION_SERVICE);
        // 通過GPS衛星定位，定位級別可以精確到街（通過24顆衛星定位，在室外和空曠的地方定位準確、速度快）
        boolean gps = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        // 通過WLAN或移動網路(3G/2G)確定的位置（也稱作AGPS，輔助GPS定位。主要用於在室內或遮蓋物（建築群或茂密的深林等）密集的地方定位）
        boolean network = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (gps || network) {
            return true;
        }

        return false;
    }
}
