package com.acloud.app.util;

import com.acloud.app.model.ActivityListBannerResult;
import com.acloud.app.model.ActivityListResult;
import com.acloud.app.tool.Pub;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;

/**
 * Created by skywind on 2015/8/19.
 */
public class ActivityListManager {

    public static ArrayList<NameValuePair> getList_Default(String uid, String authtoken, String lat, String lng) {
        ArrayList<NameValuePair> list = new ArrayList<>();
        list.add(new BasicNameValuePair(Pub.VALUE_APIKEY, Pub.APIKEY));
        list.add(new BasicNameValuePair(Pub.VALUE_UID, uid));
        list.add(new BasicNameValuePair(Pub.VALUE_AUTHTOKEN, authtoken));
        list.add(new BasicNameValuePair(Pub.VALUE_INDEX, "0"));
        list.add(new BasicNameValuePair(Pub.VALUE_SIZE, "20"));
        list.add(new BasicNameValuePair(Pub.VALUE_TYPE, "0"));
        list.add(new BasicNameValuePair(Pub.VALUE_SORT_TYPE, "20"));
        list.add(new BasicNameValuePair(Pub.VALUE_LAT, lat));
        list.add(new BasicNameValuePair(Pub.VALUE_LNG, lng));
        return list;
    }

    public static ArrayList<NameValuePair> getList(String uid, String authtoken, String lat, String lng, String type , String sort_type) {
        ArrayList<NameValuePair> list = new ArrayList<>();
        list.add(new BasicNameValuePair(Pub.VALUE_APIKEY, Pub.APIKEY));
        list.add(new BasicNameValuePair(Pub.VALUE_UID, uid));
        list.add(new BasicNameValuePair(Pub.VALUE_AUTHTOKEN, authtoken));
        list.add(new BasicNameValuePair(Pub.VALUE_INDEX, "0"));
        list.add(new BasicNameValuePair(Pub.VALUE_SIZE, "20"));
        list.add(new BasicNameValuePair(Pub.VALUE_TYPE, type));
        list.add(new BasicNameValuePair(Pub.VALUE_SORT_TYPE, sort_type));
        list.add(new BasicNameValuePair(Pub.VALUE_LAT, lat));
        list.add(new BasicNameValuePair(Pub.VALUE_LNG, lng));
        return list;
    }

    public static ArrayList<ActivityListResult> getResultArrayList() {
        return resultArrayList;
    }

    public static void setResultArrayList(ArrayList<ActivityListResult> resultArrayList) {
        ActivityListManager.resultArrayList = resultArrayList;
    }

    public static ArrayList<ActivityListBannerResult> getResultsBannerArrayList() {
        return resultsBannerArrayList;
    }

    public static void setResultsBannerArrayList(ArrayList<ActivityListBannerResult> resultsBannerArrayList) {
        ActivityListManager.resultsBannerArrayList = resultsBannerArrayList;
    }

    static ArrayList<ActivityListResult> resultArrayList = new ArrayList<ActivityListResult>();
    static ArrayList<ActivityListBannerResult> resultsBannerArrayList = new ArrayList<ActivityListBannerResult>();

}
