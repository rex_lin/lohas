package com.acloud.app.util;

import com.acloud.app.model.ActivityDetailResult;
import com.acloud.app.tool.Pub;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;

/**
 * Created by skywind on 2015/8/19.
 */
public class ActivityDetailManager {
    public static ActivityDetailResult getDetailResult() {
        return detailResult;
    }

    public static void setDetailResult(ActivityDetailResult detailResult) {
        ActivityDetailManager.detailResult = detailResult;
    }

    static ActivityDetailResult detailResult = new ActivityDetailResult();

    public static ArrayList<NameValuePair> getInfo(String uid, String authtoken, String aid, String lat, String lng) {
        ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
        list.add(new BasicNameValuePair(Pub.VALUE_APIKEY, Pub.APIKEY));
        list.add(new BasicNameValuePair(Pub.VALUE_UID, uid));
        list.add(new BasicNameValuePair(Pub.VALUE_AUTHTOKEN, authtoken));
        list.add(new BasicNameValuePair(Pub.VALUE_AID, aid));
        list.add(new BasicNameValuePair(Pub.VALUE_LAT, lat));
        list.add(new BasicNameValuePair(Pub.VALUE_LNG, lng));
        return list;
    }
}
