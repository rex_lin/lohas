package com.acloud.app.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.acloud.app.R;


public class ApiErrorCode {
	public static final String TAG = "ErrorCode";

	public static final String
	code_normal="00",//	正常
	code_dbConnectError="90",//	資料庫連接錯誤	
	code_otherError="99";//	其他異常錯誤
	
	public static int getErrorMsgID(String code){
		
    	if(code.equals(ApiErrorCode.code_normal))
		{
    		return R.string.api_err_msg_00;
		}
		else if(code.equals(ApiErrorCode.code_dbConnectError))
		{
			return R.string.api_err_msg_90;
		}
    	
    	return R.string.api_err_msg_99;
    }
	
	public static boolean isConnected(Context c){
		  ConnectivityManager cm = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
		  NetworkInfo networkInfo = cm.getActiveNetworkInfo();
		  if (networkInfo != null && networkInfo.isConnected()) {
		    return true;
		  }
		  return false;
	}
	
}
