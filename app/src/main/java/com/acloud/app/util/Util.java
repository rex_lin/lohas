package com.acloud.app.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.telephony.SmsManager;

import com.acloud.app.MainActivity;
import com.acloud.app.MyApplication;
import com.acloud.app.tool.Pub;
import com.acloud.app.tool.TLog;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.zip.GZIPInputStream;

public class Util {
    public static final String TAG = "UTIL";

    static CallbackManager cm;

    /**
     * 取小數點1位
     */
    public static String takeDecimals(String str) {
        float number;
        if (str.equals("") || str == null) {
            number = 0;
        } else {
            number = Float.valueOf(str);
        }
        return String.format("%.01f", number);
    }


    /**
     * facebook shared dialog,no catch call back
     *
     * @param activity
     * @param Content
     */
    public static void fbshare(Activity activity, String Content) {
        FacebookSdk.sdkInitialize(activity.getApplicationContext());
        cm = CallbackManager.Factory.create();
        ShareDialog sd;
        sd = new ShareDialog(activity);
        sd.registerCallback(cm, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                TLog.i("Ernest", "onSuccess");
            }

            @Override
            public void onCancel() {
                TLog.i("Ernest", "onCancel");
            }

            @Override
            public void onError(FacebookException e) {

            }
        });
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    .setContentTitle("趣逢甲")
                    .setContentDescription(Content)
//                    .setContentUrl(Uri.parse("https://play.google.com/store/apps/details?id=com.acloud.app&hl=zh-TW"))
                    .setContentUrl(Uri.parse("http://ppt.cc/HiSzb")) //Url
                    .build();
            //TODO  FBshared
            sd.show(linkContent);
        }
    }

    /**
     * 轉換周
     *
     * @param week
     * @return
     */
    public static String week(int week) {
        String w = "";
        if (week == 1) {
            w = "日";
        }
        if (week == 2) {
            w = "一";
        }
        if (week == 3) {
            w = "二";
        }

        if (week == 4) {
            w = "三";
        }
        if (week == 5) {
            w = "四";
        }
        if (week == 6) {
            w = "五";
        }
        if (week == 7) {
            w = "六";
        }
        return w;
    }

    /**
     * 2015/07/08
     *
     * @param unixtime
     * @return
     */
    public static String formatDate(String unixtime) {
        if (unixtime == null || unixtime.equals("0"))
            return "0";

        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
        Date curDate = new Date(Long.valueOf(unixtime) * 1000);
        String date = format.format(curDate);
        return date;
    }

    public static String formatDateTime(String unixtime) {
        if (unixtime == null || unixtime.equals("0"))
            return "0";

        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd hh:mm");
        Date curDate = new Date(Long.valueOf(unixtime) * 1000);
        String date = format.format(curDate);
        return date;
    }

    /**
     * 計算公尺轉公里
     *
     * @param m
     * @return
     */
    public static String distanceMtoKM(String m) {
        if (m == null) m = "0";
        int m1 = 0;
        try {
            m1 = Integer.valueOf(m);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        if (m != null && m.length() > 4) {
            m1 = m1 / 1000;
            return String.valueOf(m1) + "km";
        } else {
            return String.valueOf(m1) + "m";
        }
    }

    /**
     * 計算距離公式
     */
    private static double EARTH_RADIUS = 6378.137;

    private static double rad(double d) {
        return d * Math.PI / 180.0;
    }

    /**
     * 計算距離公式
     */
    public static double GetDistance(double lat1, double lng1, double lat2, double lng2) {
        double radLat1 = rad(lat1);
        double radLat2 = rad(lat2);
        double a = radLat1 - radLat2;
        double b = rad(lng1) - rad(lng2);
        double s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2) +
                Math.cos(radLat1) * Math.cos(radLat2) * Math.pow(Math.sin(b / 2), 2)));
        s = s * EARTH_RADIUS;
        s = Math.round(s * 10000) / 10000;
        return s;
    }

    public static float setDecimal(int digit, float value) {
        float d = 1;
        while (digit > 0) {
            d *= 10;
            digit--;
        }
        return ((int) (value * d) / d);
    }

    /**
     * return bmi
     */
    public static float getBmi(float wt, float hei) {
        if (wt < 0) {
            return 0;
        } else {
            float bmi = wt / hei / hei * 10000f;
            return setDecimal(1, bmi);
        }
    }

    //---------------------------------------------------------------------------------- latuitude & longitude & address

    /**
     * @param distance 距離, 單位為公尺
     */
    public static int mapCalculateSpan(double distance) {
        double R = 6371; //earth radius in kilometer
        int span = 0;
        span = (int) Math.round(distance * 1E6 / R);
        return span;
    }

    /**
     * 取得經緯度座標陣列 [緯度, 經度]</br>
     *
     * @return double[latitudeE6, longitudeE6], or null if address not available.
     */
    public static double[] getLongitudeLatitude(Context context, String addressStr) {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        List<Address> addresses = null;
        Address add = null;
        try {
            addresses = geocoder.getFromLocationName(addressStr, 1);

            if (addresses == null || addresses.isEmpty()) {
                MyApplication.logE(TAG, "address not available");
            } else {
                add = addresses.get(0);
                double geolat = add.getLatitude() * 1E6;
                double geolon = add.getLongitude() * 1E6;
                return new double[]{geolat, geolon};
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @return return city string or null if no matches were found or there is no backend service available.
     */
    public static String getCityByLocation(Context context, Double longitude, Double latitude) throws IOException {
        // 建立Geocoder物件: Android 8 以上模疑器測式會失敗
        Geocoder gc = new Geocoder(context, Locale.getDefault()); // 地區:台灣
        // 自經緯度取得地址
        List<Address> lstAddress = gc.getFromLocation(latitude, longitude, 1);
        if (lstAddress != null && lstAddress.size() > 0) {
            return lstAddress.get(0).getAdminArea();
        }
        return "";
    }

    /**
     * @return return city string or null if no matches were found or there is no backend service available.
     */
    public static String getAreaByLocation(Context context, Double longitude, Double latitude) throws IOException {
        // 建立Geocoder物件: Android 8 以上模疑器測式會失敗
        Geocoder gc = new Geocoder(context, Locale.getDefault()); // 地區:台灣
        // 自經緯度取得地址
        List<Address> lstAddress = gc.getFromLocation(latitude, longitude, 1);
        if (lstAddress != null && lstAddress.size() > 0) {
            return lstAddress.get(0).getLocality();
        }
        return "";
    }

    /**
     * @return return city string or null if no matches were found or there is no backend service available.
     */
    public static String getAddressByLocation(Context context, Double longitude, Double latitude) throws IOException {
        // 建立Geocoder物件: Android 8 以上模疑器測式會失敗
        Geocoder gc = new Geocoder(context, Locale.getDefault()); // 地區:台灣
        // 自經緯度取得地址
        List<Address> lstAddress = gc.getFromLocation(latitude, longitude, 1);
        if (lstAddress != null && lstAddress.size() > 0) {
            StringBuffer sb = new StringBuffer();
            String tempString;
            // 市
            if ((tempString = lstAddress.get(0).getAdminArea()) != null) {
                sb.append(tempString);
            }
            // 區
            if ((tempString = lstAddress.get(0).getLocality()) != null) {
                sb.append(tempString);
            }
            // 路
            if ((tempString = lstAddress.get(0).getThoroughfare()) != null) {
                sb.append(tempString);
            }
            // 號
            if ((tempString = lstAddress.get(0).getFeatureName()) != null) {
                sb.append(tempString);
            }
            return lstAddress.get(0).getLocality();
        }
        return "";
    }


    //---------------------------------------------------------------------------------- file

    /**
     * @return return System.currentTimeMillis if file not exist
     */
    public static long getCatchDataLastModify(Context context, String fileName) {
        File file = new File(context.getCacheDir(), fileName);
        if (file.lastModified() == 0) {
            return System.currentTimeMillis();
        }
        return file.lastModified();
    }

    public static File getCatchFile(Context context, String fileName) {
        return new File(context.getCacheDir(), fileName);
    }

    /**
     * @param fileName file name
     */
    public static String getCachFileName(String fileName, String account) {
        return md5Encode(fileName + account);
    }

    public static String getCachFileName(String link) {
        return md5Encode(link);
    }

    public static Bitmap downloadImage(Context context, String link, int maxWidth) throws IOException {
        MyApplication.logE("UTIL", "url=[" + link + "]");

        File file = new File(context.getCacheDir(), downloadToCatchDir(context, link));
        if (file.exists()) {
            System.gc();
            Bitmap bit = BitmapFactory.decodeFile(file.getAbsolutePath(), BitmapHandler.getOpts(file, maxWidth));
            System.gc();
            return bit;
        } else {
            MyApplication.logE("UTIL", "image file not exist");
            return null;
        }
    }

    public static String downloadToCatchDir(Context context, String link) throws IOException {
        URL url = null;
        InputStream is = null;
        FileOutputStream fos = null;
        File f = null;
        String fileName = md5Encode(link);

        f = getCatchFile(context, fileName);
        fos = new FileOutputStream(f);

        url = new URL(link);
        is = (InputStream) url.getContent();

        byte[] buff = new byte[100];
        int len = 0;
        while ((len = is.read(buff)) > 0) {
            fos.write(buff, 0, len);
        }

        try {
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return fileName;
    }

    public static boolean saveToCatchDir(Context context, String fileName, String account, Bitmap bit) throws FileNotFoundException {
        FileOutputStream fos = null;
        File f = null;
        boolean result = false;

        f = getCatchFile(context, getCachFileName(fileName, account));
        fos = new FileOutputStream(f);
        result = bit.compress(CompressFormat.PNG, 100, fos);

        try {
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static Bitmap getCatchImageBitmap(Context context, String fileName, int maxWidth) throws IOException {
        FileInputStream fis = null;
        InputStream is = null;
        Bitmap bit = null;
        File f = null;
        System.gc();

        f = getCatchFile(context, fileName);
        fis = new FileInputStream(f);
        is = new BitmapHandler.FlushedInputStream(fis);
        bit = BitmapFactory.decodeStream(is, null, BitmapHandler.getOpts(f, maxWidth));

        try {
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.gc();

        return bit;
    }

    public static Bitmap getCatchImageBitmap(Context context, String fileName, String account, int maxWidth) throws Exception {
        return getCatchImageBitmap(context, getCachFileName(fileName, account), maxWidth);
    }


    public static void writeDataToCatch(Context context, String fileName, String data) {
        MyApplication.logE("UTIL", "fileName = [" + fileName + "]");
        MyApplication.logE("UTIL", "write data[" + data + "]");
        File file = new File(context.getCacheDir(), fileName);
        FileWriter fw = null;
        ;
        BufferedWriter br = null;
        try {
            fw = new FileWriter(file);
            br = new BufferedWriter(fw);
            br.write(data);
            br.close();
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void appendDataToCatch(Context context, String fileName, String data) {
        File file = new File(context.getCacheDir(), fileName);
        FileWriter fw = null;
        ;
        BufferedWriter br = null;
        try {
            fw = new FileWriter(file);
            br = new BufferedWriter(fw);
            br.append(data);
            br.close();
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String readDataFromCatch(Context context, String fileName) {
        File file = new File(context.getCacheDir(), fileName);
        MyApplication.logE("UTIL", "read path[" + file + "]");
        FileReader fw = null;
        ;
        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();
        if (!file.exists()) {
            MyApplication.logE("UTIL", String.format("file[%s] not exist", fileName));
            return "";
        }
        try {
            fw = new FileReader(file);
            br = new BufferedReader(fw);
            String str = "";
            while ((str = br.readLine()) != null) {
                sb.append(str);
            }
            br.close();
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }


    //---------------------------------------------------------------------------------- time

    /**
     * http://docs.oracle.com/javase/1.4.2/docs/api/java/text/SimpleDateFormat.html </br>
     * Pattern support Year, Month, Number, General time zone, RFC 822 time zone</br>
     * - y (Year)</br>
     * - M (Month in year)</br>
     * - w (Week in year)</br>
     * - W (Week in month)</br>
     * - D (Day in year)</br>
     * - d (Day in month)</br>
     * - F (Day of week in month)</br>
     * - H (Hour in day (0-23))</br>
     * - k (Hour in day (1-24))</br>
     * - K (Hour in am/pm (0-11))</br>
     * - h (Hour in am/pm (1-12))</br>
     * - m (Minute in hour)</br>
     * - s (Second in minute)</br>
     * - S (Millisecond)</br>
     * - z (General time zone)</br>
     * - Z (RFC 822 time zone)</br></br>
     * Pattern not support Text. </br>
     * - G (Era designator) </br>
     * - E (Day in week) </br>
     * - a (Am/pm marker) </br>
     *
     * @param timeString time string, ex: 2013-10-31 18:26:34
     * @param pattern    pattern, ex: yyyy-MM-dd HH:mm:ss
     */
    public static long strToTimeMill(String timeString, String pattern) throws ParseException {
        return new SimpleDateFormat(pattern).parse(timeString).getTime();
    }


    public static String getTimeStr(SimpleDateFormat defaultFormat, long timeMill, String hour, String minute, String before) {
        long diff = Calendar.getInstance().getTimeInMillis() - timeMill;
        if (diff < 86400000) {

            if (diff < 3600000) {
                return "" + ((int) (diff % 3600000 / 60000)) + minute + before;
            }

            return "" + ((int) (diff % 86400000 / 3600000)) + hour + ((int) (diff % 3600000 / 60000)) + minute + before;
        } else {
            return defaultFormat.format(new Date(timeMill));
        }
    }

    /**
     * support  [year, month, date] | [year, month, date, hour, minute] | [year, month, date, hour, minute, seconds]
     *
     * @param t time string array
     */
    public static Calendar getCalendar(String[] t) {
        Calendar cal = Calendar.getInstance();

        if (t.length == 6) {
            cal.set(Calendar.YEAR, Integer.parseInt(t[0]));
            cal.set(Calendar.MONTH, Integer.parseInt(t[1]) - 1);
            cal.set(Calendar.DATE, Integer.parseInt(t[2]));
            cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(t[3]));
            cal.set(Calendar.MINUTE, Integer.parseInt(t[4]));
            cal.set(Calendar.SECOND, Integer.parseInt(t[5]));
            cal.set(Calendar.MILLISECOND, 0);
        } else if (t.length == 5) {
            cal.set(Calendar.YEAR, Integer.parseInt(t[0]));
            cal.set(Calendar.MONTH, Integer.parseInt(t[1]) - 1);
            cal.set(Calendar.DATE, Integer.parseInt(t[2]));
            cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(t[3]));
            cal.set(Calendar.MINUTE, Integer.parseInt(t[4]));
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
        } else if (t.length == 3) {
            cal.set(Calendar.YEAR, Integer.parseInt(t[0]));
            cal.set(Calendar.MONTH, Integer.parseInt(t[1]) - 1);
            cal.set(Calendar.DATE, Integer.parseInt(t[2]));
        }

        return cal;
    }

    public static Calendar getDayStart(long timeMill) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timeMill);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        return cal;
    }

    /**
     * support  [year, month, date] | [year, month, date, hour, minute] | [year, month, date, hour, minute, seconds]
     *
     * @param t time string array
     */
    public static long getTimeMill(String[] t) {
        return getCalendar(t).getTimeInMillis();
    }

    public static long[] getMonthTimeMillRange(Calendar cal) {
        Calendar c = (Calendar) cal.clone();
        c.set(Calendar.DATE, 1);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        long timeStartMill = c.getTimeInMillis();

        c.add(Calendar.MONTH, 1);
        c.add(Calendar.DATE, -1);
        c.set(Calendar.HOUR_OF_DAY, 23);
        c.set(Calendar.MINUTE, 59);
        c.set(Calendar.SECOND, 59);
        long timeEndMill = c.getTimeInMillis();

        return new long[]{timeStartMill, timeEndMill};
    }

    public static long[] getMonthTimeMillRange(long timeMill) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timeMill);

        return getMonthTimeMillRange(cal);
    }


    public static long[] getDayTimeMillRange(Calendar cal) {
        Calendar c = (Calendar) cal.clone();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        long timeStartMill = c.getTimeInMillis();

        c.set(Calendar.HOUR_OF_DAY, 23);
        c.set(Calendar.MINUTE, 59);
        c.set(Calendar.SECOND, 59);
        long timeEndMill = c.getTimeInMillis();

        return new long[]{timeStartMill, timeEndMill};
    }

    public static long[] getWeekTimeMillRange(Calendar cal) {
        Calendar c = (Calendar) cal.clone();
        c.set(Calendar.HOUR_OF_DAY, 23);
        c.set(Calendar.MINUTE, 59);
        c.set(Calendar.SECOND, 59);
        long timeEndMill = c.getTimeInMillis();

        c.add(Calendar.DATE, -7);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 1);
        c.set(Calendar.SECOND, 0);
        long timeStartMill = c.getTimeInMillis();

        return new long[]{timeStartMill, timeEndMill};
    }

    public static long[] getDayTimeMillRange(long timeMill) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timeMill);

        return getDayTimeMillRange(cal);
    }

    public static int getHour(Calendar cal) {
        return cal.get(Calendar.HOUR_OF_DAY);
    }

    public static int getMinute(Calendar cal) {
        return cal.get(Calendar.MINUTE);
    }

    public static boolean isBetween(Calendar cal, Calendar limitCalStart, Calendar limitCalEnd) {
        return cal.before(limitCalEnd) && cal.after(limitCalStart);
    }

    /**
     * @param cal Calendar
     * @return 日期設成原本月份最後一天的Calendar
     */
    public static Calendar getLastDay(Calendar cal) {
        cal.set(Calendar.DATE, 1);
        cal.add(Calendar.MONTH, 1);
        cal.add(Calendar.DATE, -1);
        return cal;
    }

    /**
     * 補到最後
     *
     * @param sb    要被補的字串
     * @param digit 位數
     * @param ch    要補進去的字
     */
    public static String append(StringBuffer sb, int digit, String ch) {
        while (sb.length() < digit) {
            sb.append(ch);
        }
        return sb.toString();
    }

    /**
     * GMT秒數轉為當地秒數
     */
    public static Long GMTSecToLocalSec(Long GMT_SEC) {
        return GMT_SEC + TimeZone.getDefault().getRawOffset();
    }

    /**
     * //當地秒數轉GMT秒數
     */
    public static Long LocalSecToGMTSec(Long LOCAL_SEC) {
        return LOCAL_SEC - TimeZone.getDefault().getRawOffset();
    }

    /**
     * yyyy	年
     * MM 月
     * dd 日
     * HH 時
     * mm 分
     * ss 秒
     */
    public static Long stringToTime(String Time, String Format) {
        Date dt = new Date();

        SimpleDateFormat sdf = new SimpleDateFormat(Format);
        try {
            dt = sdf.parse(Time);
            return dt.getTime();
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * yyyy	年
     * MM 月
     * dd 日
     * HH 時
     * mm 分
     * ss 秒
     */
    public static String timeToString(Long sec, String Format) {
        Date date = new Date();
        date.setTime(sec);
        SimpleDateFormat sdf = new SimpleDateFormat(Format);
        String time = sdf.format(date);
        return time;
    }

    //---------------------------------------------------------------------------------- byte


    public static String md5Encode(String stringP) {
        String resultString = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            resultString = byteArrayToHexString(md.digest(stringP.getBytes()));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return resultString;
    }

    public static String byteArrayToHexString(byte[] byteP) {
        StringBuffer resultSb = new StringBuffer();
        for (int i = 0; i < byteP.length; i++) {
            resultSb.append(byteToHexString(byteP[i]));
        }
        return resultSb.toString();
    }

    public static int getIntValue(byte b) {
        return b & 0xff;
    }

    public static String getHexString(byte[] buff) {
        String str = "";
        for (byte b : buff) {
            str += Util.byteToHex(b) + ",";
        }
        return str;
    }

    public static String byteToBinaryString(byte b) {
        Integer intData = Integer.parseInt(byteToHex(b), 16);
        String Data_0 = Integer.toBinaryString(intData);
        while (Data_0.length() != 8) {
            Data_0 = "0" + Data_0;
        }
        return Data_0;
    }


    public static int getSum(byte... bytes) {
        int sum = 0;
        for (byte b : bytes) {
            sum += getIntValue(b);
        }
        return sum;
    }


    private static final String hexDigitsChar = "0123456789abcdefghijklmnopqustuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public static String byteToHexString(byte byteP) {
        int n = byteP;
        if (n < 0)
            n += 256;
        int d1 = n / 16;
        int d2 = n % 16;

        return hexDigitsChar.charAt(d1) + "" + hexDigitsChar.charAt(d2);
//		return hexDigits[d1] + hexDigits[d2];
    }

    /**
     * Convenience method to convert a byte array to a hex string.
     *
     * @param data the byte[] to convert
     * @return String the converted byte[]
     */
    public static String bytesToHex(byte[] data) {
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < data.length; i++) {
            buf.append(byteToHex(data[i]).toUpperCase());
        }
        return (buf.toString());
    }

    /**
     * method to convert a byte to a hex string.
     *
     * @param data the byte to convert
     * @return String the converted byte
     */
    public static String byteToHex(byte data) {
        StringBuffer buf = new StringBuffer();
        buf.append(toHexChar((data >>> 4) & 0x0F));
        buf.append(toHexChar(data & 0x0F));
        return buf.toString();
    }

    /**
     * Convenience method to convert an int to a hex char.
     *
     * @param i the int to convert
     * @return char the converted char
     */
    public static char toHexChar(int i) {
        if ((0 <= i) && (i <= 9)) {
            return (char) ('0' + i);
        } else {
            return (char) ('a' + (i - 10));
        }
    }

    /**
     * 51 → 81 </br>
     * 2B → 43 </br>
     *
     * @param str hex string
     */
    public static byte hexStrToByte(String str) {
        if (str.length() == 1) {
            return (byte) Character.digit(str.charAt(0), 16);
        } else {
            return (byte) ((Character.digit(str.charAt(str.length() - 2), 16) << 4)
                    + Character.digit(str.charAt(str.length() - 1), 16));
        }
    }

    public static int bytesToBinaryStringToInt(byte... b) {
        String binary = "";
        for (byte bb : b) {
            binary += byteToBinaryString(bb);
        }
        return Integer.parseInt(binary, 2);
    }

    public static byte intToHexToByte(int value) {
        return hexStrToByte(Integer.toHexString(value));
    }

    public static byte intToHexToByte_1digit(int value) {
        String hexValue = Integer.toHexString(value);

        if (hexValue.length() > 2) {
            hexValue = hexValue.substring(hexValue.length() - 2);
        }
        return hexStrToByte(hexValue);
    }

    public static byte[] intToHexToByte_2digit(int value) {
        String hexValue = Integer.toHexString(value);
        String hexValueH = "";
        String hexValueL = "";

        if (hexValue.length() > 4) {
            hexValue = hexValue.substring(hexValue.length() - 4);
        }

        switch (hexValue.length()) {
            case 4:
                hexValueH = hexValue.substring(0, 2);
                hexValueL = hexValue.substring(2);
                break;
            case 3:
                hexValueH = "0" + hexValue.substring(0, 1);
                hexValueL = hexValue.substring(1);
                break;
            case 2:
                hexValueH = "00";
                hexValueL = hexValue;
                break;
            case 1:
                hexValueH = "00";
                hexValueL = "0" + hexValue;
                break;
        }

        byte yH = hexStrToByte(hexValueH);
        byte yL = hexStrToByte(hexValueL);
        return new byte[]{yH, yL};
    }


    public static String bytesToHexString(byte[] bytes) {
        // http://stackoverflow.com/questions/332079
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                sb.append('0');
            }
            sb.append(hex);
        }
        return sb.toString();
    }

    public static final String RESULT_KEY_HASH = "hash";
    public static final String RESULT_KEY_TIMESTAMP = "gettimestamp";
    public static final String RESULT_KEY_BOUNDARY = "boundary";
    public static final String RESULT_KEY_POST_TIMESTAMP = "posttimestamp";

    public static HashMap<String, String> hashCodeForUrl(String account, String passwd) {
        final HashMap<String, String> result = new HashMap<String, String>();
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date tempDate = new Date();
        String timeStamp = format.format(tempDate);
        String timeString = timeStamp.replaceAll(" ", "%20");
        //String timeString ="2013/07/03%2014:34:18";
        String hashString = "", boundaryHashString = "";
        String stringToDigest = account + passwd + timeStamp;

        byte[] bytesToDigest = stringToDigest.getBytes();
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            messageDigest.update(bytesToDigest);
            byte[] digest = messageDigest.digest();
            hashString = bytesToHexString(digest);
            MyApplication.logE(TAG, "SHA-256 Hash: " + hashString);
            MyApplication.logE(TAG, "stringToDigest: " + stringToDigest);
        } catch (Exception e) {
            e.printStackTrace();
        }

        String boundaryStringToDigest = account + timeStamp;
        byte[] boundaryBytesToDigest = boundaryStringToDigest.getBytes();

        try {
            MessageDigest boundaryMessageDigest = MessageDigest.getInstance("SHA-256");
            boundaryMessageDigest.update(boundaryBytesToDigest);
            byte[] boundaryDigest = boundaryMessageDigest.digest();
            boundaryHashString = bytesToHexString(boundaryDigest);
            MyApplication.logE(TAG, "SHA-256 Hash: " + boundaryHashString);
        } catch (Exception e) {
            e.printStackTrace();
        }

        result.put(RESULT_KEY_HASH, hashString);
        result.put(RESULT_KEY_TIMESTAMP, timeString);
        result.put(RESULT_KEY_POST_TIMESTAMP, timeStamp);
        result.put(RESULT_KEY_BOUNDARY, boundaryHashString);
        return result;
    }

    //---------------------------------------------------------------------------------- network

    public static String getPostContent(String... param) {
        StringBuffer sb = new StringBuffer("");
        for (int i = 0; i < param.length; i++) {
            if (i % 2 == 0) {
                if (sb.length() > 0) {
                    sb.append("&");
                }
                sb.append(param[i]).append("=");
            } else {
                sb.append(param[i]);
            }
        }
        return sb.toString();
    }

    public static boolean hasNet(Context context) {
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }


//	POST /MerchantApps/service/salesClient/login/a625dd84264992cf1aee45463e1d440e HTTP/1.1
//	Host: apps.cbftechnology.com
//	Content-Type: application/x-www-form-urlencoded; charset=utf-8
//	Accept-Encoding: gzip, deflate
//	Content-Length: 47
//	Accept-Language: zh-Hant;q=1, ja;q=0.9, zh-Hans;q=0.8, en;q=0.7, fr;q=0.6, de;q=0.5
//	Accept: application/json
//	Connection: keep-alive
//	User-Agent: mitt/1.0 (iPhone Simulator; iOS 7.1; Scale/2.00)

    public static String doPost(String postUrl, String postContent, int timeout) {

        MyApplication.logE(TAG, "post url=[" + postUrl + "]");
        MyApplication.logE(TAG, "post content=[" + postContent + "]");

        String result = "";

        URL url = null;
        HttpURLConnection httpconn = null;

        OutputStream streamOut = null;
        OutputStreamWriter streamWriter = null;
        BufferedWriter bufferWriter = null;

        BufferedReader bufferReader = null;
        InputStreamReader streamReader = null;
        InputStream streamIn = null;

        try {
            url = new URL(postUrl);
            httpconn = (HttpURLConnection) url.openConnection();

            if (timeout < 1000) {
                httpconn.setConnectTimeout(timeout * 1000);
                httpconn.setReadTimeout(timeout * 1000);
            } else {
                httpconn.setConnectTimeout(timeout);
                httpconn.setReadTimeout(timeout);
            }


            HttpURLConnection.setFollowRedirects(true);
            httpconn.setDoOutput(true);
            httpconn.setDoInput(true);
            httpconn.setRequestMethod("POST");
            httpconn.setUseCaches(false);
            httpconn.setInstanceFollowRedirects(true);
            httpconn.setRequestProperty(
                    "Accept",
                    "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
            httpconn.setRequestProperty(
                    "User-Agent",
                    "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.116 Safari/537.36");
            httpconn.setRequestProperty(
                    "Accept-Encoding",
                    "gzip,deflate,sdch"
            );
            httpconn.setRequestProperty(
                    "Accept-Language",
                    "zh-TW,zh;q=0.8,en-US;q=0.6,en;q=0.4"
            );
            httpconn.setRequestProperty(
                    "Content-Length",
                    "" + postContent.getBytes().length
            );
            // int total = httpconn.getContentLength();

			/* write post content */
            try {
                streamOut = httpconn.getOutputStream();
                streamWriter = new OutputStreamWriter(streamOut);
                bufferWriter = new BufferedWriter(streamWriter);
                bufferWriter.write(postContent);
                bufferWriter.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }

			/* read post result */
            int total = httpconn.getContentLength();


            // sendUpdateIntent(account, updateType, 1, total);
            if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                streamIn = new GZIPInputStream(httpconn.getInputStream());
                streamReader = new InputStreamReader(streamIn);
                bufferReader = new BufferedReader(streamReader, 8192);

                char buff[] = new char[32];
                StringBuffer sb = new StringBuffer();
                int readLen = 0;
                while ((readLen = bufferReader.read(buff)) > 0) {
                    sb.append(buff, 0, readLen);
                }


                result = sb.toString();

                bufferReader.close();
                bufferReader = null;
                streamReader.close();
                streamReader = null;
                streamIn.close();
                streamIn = null;
            } else {

                result = "";
            }
            bufferWriter.close();
            bufferWriter = null;
            streamWriter.close();
            streamWriter = null;
            streamOut.close();
            streamOut = null;
        } catch (IOException e) {
            e.printStackTrace();
            MyApplication.logE(TAG, "connection=[" + e.toString() + "]");
        }

        if (bufferWriter != null) {
            try {
                bufferWriter.close();
                bufferWriter = null;
            } catch (IOException e) {
                e.printStackTrace();
                MyApplication.logE(TAG, "bufferWriter=[" + e.toString() + "]");
            }
        }

        if (streamWriter != null) {
            try {
                streamWriter.close();
                streamWriter = null;
            } catch (IOException e) {
                e.printStackTrace();
                MyApplication.logE(TAG, "streamWriter=[" + e.toString() + "]");
            }
        }

        if (streamOut != null) {
            try {
                streamOut.close();
                streamOut = null;
            } catch (IOException e) {
                e.printStackTrace();
                MyApplication.logE(TAG, "streamOut=[" + e.toString() + "]");
            }
        }

        if (bufferReader != null) {
            try {
                bufferReader.close();
                bufferReader = null;
            } catch (IOException e) {
                e.printStackTrace();
                MyApplication.logE(TAG, "bufferReader=[" + e.toString() + "]");
            }
        }

        if (streamReader != null) {
            try {
                streamReader.close();
                streamReader = null;
            } catch (IOException e) {
                e.printStackTrace();
                MyApplication.logE(TAG, "streamReader=[" + e.toString() + "]");
            }
        }

        if (streamIn != null) {
            try {
                streamIn.close();
                streamIn = null;
            } catch (IOException e) {
                e.printStackTrace();
                MyApplication.logE(TAG, "streamIn=[" + e.toString() + "]");
            }
        }

        MyApplication.logE(TAG, "post result=[" + result + "]");

        return result;
    }


    public static boolean sendSMS(
            String phoneNumber,
            String msg) {

        MyApplication.logE("sendSMS", "phoneNumber[" + phoneNumber + "]");

        MyApplication.logE("sendSMS", "msg[" + msg + "]");


        if (phoneNumber.equals("") || msg.equals("")) {
            return false;
        } else {
            try {

                SmsManager smsManager = SmsManager.getDefault();
                smsManager.sendTextMessage(phoneNumber, null, msg, null, null);

                return true;

            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

    }

    public static String getSMS_Code(
            String SMS_CODE,
            String IMEI,
            String UserPassWord,
            List stringList) {


        String SMS = "*#" + SMS_CODE + "#" + IMEI + "#" + UserPassWord + "#";

        for (int n = 0; n < stringList.size(); n++) {

            SMS += stringList.get(n).toString() + "#";
        }


        return SMS;

    }

    public static boolean checkStringByteLong(final String messageInput, final int maxL) {

        byte[] b = messageInput.getBytes();
        String s1 = new String(b);
        MyApplication.logE("checkStringByteLong", s1);

        if (b.length <= maxL) {//全英文
            return true;
        }


        return false;
    }


}

