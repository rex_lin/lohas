package com.acloud.app.util;

import com.acloud.app.model.CouponListResult;
import com.acloud.app.tool.Pub;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;

/**
 * CouponList相關
 */
public class CouponListManager {


    static ArrayList<CouponListResult> resultArrayList = new ArrayList<CouponListResult>();

    static String CurrentBouns;

    public static ArrayList<NameValuePair> getCoupon_List(String uid, String authtoken,
                                                          String index, String size, String keyword,
                                                          String sdid, String smid,
                                                          String scid, String scaid,
                                                          String sort_type,
                                                          String lat,
                                                          String lng) {
        ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
        list.add(new BasicNameValuePair(Pub.VALUE_APIKEY, Pub.APIKEY));
        list.add(new BasicNameValuePair(Pub.VALUE_UID, uid));
        list.add(new BasicNameValuePair(Pub.VALUE_AUTHTOKEN, authtoken));
        list.add(new BasicNameValuePair(Pub.VALUE_INDEX, index));
        list.add(new BasicNameValuePair(Pub.VALUE_SIZE, size));
        list.add(new BasicNameValuePair(Pub.VALUE_KEYWORD, keyword));
        list.add(new BasicNameValuePair(Pub.VALUE_SDID, sdid));
        list.add(new BasicNameValuePair(Pub.VALUE_SMID, smid));
        list.add(new BasicNameValuePair(Pub.VALUE_SCID, scid));
        list.add(new BasicNameValuePair(Pub.VALUE_SCAID, scaid));
        list.add(new BasicNameValuePair(Pub.VALUE_SORT_TYPE, sort_type));
        list.add(new BasicNameValuePair(Pub.VALUE_LAT, lat));
        list.add(new BasicNameValuePair(Pub.VALUE_LNG, lng));
        return list;
    }

    public static ArrayList<CouponListResult> getResultArrayList() {
        return resultArrayList;
    }

    public static void setResultArrayList(ArrayList<CouponListResult> resultArrayList) {
        CouponListManager.resultArrayList = resultArrayList;
    }

    public static String getCurrentBouns() {
        return CurrentBouns;
    }

    public static void setCurrentBouns(String currentBouns) {
        CurrentBouns = currentBouns;
    }
}
