package com.acloud.app.util;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.media.ExifInterface;
import android.util.DisplayMetrics;

import com.acloud.app.MyApplication;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class BitmapHandler {
	
	public static BitmapFactory.Options getOpts(File f, int maxSide){
		BitmapFactory.Options opts = new BitmapFactory.Options();
		
		opts.inPreferredConfig = Bitmap.Config.RGB_565;
		try {
			opts.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(new FileInputStream(f), null, opts);
			
			int sampleSize = 1;
			while(opts.outWidth/sampleSize > maxSide && opts.outHeight/sampleSize > maxSide){
				sampleSize++;
			}
			opts.inSampleSize = sampleSize;
			opts.inJustDecodeBounds = false;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return opts;
	}
	
	/**
	 * decode image file to bitmap
	 * @param f	image file
	 * @return bitmap , return null if file not exist
	 * */
	public static Bitmap decodeBitmap(File f, int maxSide){
	
		try {
			return BitmapFactory.decodeStream(new FileInputStream(f), null, getOpts(f, maxSide));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Bitmap decodeBitmap(File f, int w, int h){
		if(w == 0 || h == 0){
			return null;
		}
		
		try {
			return BitmapFactory.decodeStream(new FileInputStream(f), null, getOpts(f, MyApplication.screenSizePxS/2));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Bitmap getBitmapFromAsset(Context context, String strName) {
	    AssetManager assetManager = context.getAssets();

	    InputStream istr;
	    Bitmap bitmap = null;
	    try {
	        istr = assetManager.open(strName);
	        bitmap = BitmapFactory.decodeStream(istr);
	    } catch (IOException e) {
	        return null;
	    }

	    return bitmap;
	}
	
	public static BitmapFactory.Options getOpts(byte[] b, int maxSide){
		BitmapFactory.Options opts = new BitmapFactory.Options();
		
		opts.inPreferredConfig = Bitmap.Config.RGB_565;
		opts.inJustDecodeBounds = true;
		BitmapFactory.decodeByteArray(b, 0, b.length, opts);
		
		int sampleSize = 1;
		while(opts.outWidth/sampleSize > maxSide && opts.outHeight/sampleSize > maxSide){
			sampleSize++;
		}
		opts.inSampleSize = sampleSize;
		opts.inJustDecodeBounds = false;	
			
		return opts;
	}
	
	public static Bitmap decodeFile(byte[] b, int maxSide){
	    try {
	        return BitmapFactory.decodeByteArray(b, 0, b.length, getOpts(b, maxSide));
	    } catch (Exception e) {
	    	e.printStackTrace();
	    }
	    return null;
	}
	
	/**
	 * @param ratio	width:height
	 * @return bitmap. Return null if ratio == 0
	 * */
	public static Bitmap getRectangleImg(Bitmap image, float ratio){
		System.gc();
		if(ratio == 0){
			return null;
		}
	    try {
	    	int width=0,height=0 ,retX=0, retY=0;
	        
	        if(image.getWidth()>image.getHeight()){
	        	height = image.getHeight();
	        	width = (int)(height/ratio);
	        	retX = (int) ((image.getWidth() - width ) / 2f);
	        }
	        else{
	        	width = image.getWidth();
	        	height=(int)(width*ratio);
	        	retY = (int) ((image.getHeight() - height ) / 2f);
	        }
	        
	        if(retX < 0 || retY < 0 
	        		|| retX + width > image.getWidth() 
	        		|| retY + height > image.getHeight()){
	        	MyApplication.logD("BitmapHandler", "image size error while resize to 1:1.3");
	        	MyApplication.logD("BitmapHandler", "image H=["+image.getWidth()+"] W=["+image.getHeight()+"]");
	        	MyApplication.logD("BitmapHandler", "image retX=["+retX+"] retY=["+retY+"] width=["+width+"] height=["+height+"]");
	        	MyApplication.logD("BitmapHandler", "return original image bitmap");
	        	return image;
	        }
    		return Bitmap.createBitmap(
    				image, 
    				retX, 
    				retY, 			
    				width, 
    				height, 
    				null, 
    				false);
    		
	    } catch (Exception e) {
	    	e.printStackTrace();
	    }
	    
	    return null;
	}
	
	public static Bitmap getSquareImage(Bitmap image, int side){
		Bitmap pic;

    	if(image.getWidth()>image.getHeight()){
    		pic = Bitmap.createBitmap(image, (image.getWidth()-image.getHeight())/2, 0, image.getHeight(), image.getHeight());
    	}
    	else{
    		pic = Bitmap.createBitmap(image, 0, (image.getHeight()-image.getWidth())/2, image.getWidth(), image.getWidth());
    	}
    	image.recycle();
    	System.gc();
    	if(side>0){
			pic = Bitmap.createScaledBitmap(pic, side, side, true);
    	}
    	
		return pic;
	}
	
	public static Bitmap setProportion(Bitmap image, int proportionH,int proportionW){
		Bitmap pic=null;
		

		if(proportionW > proportionH)
		{//結果要是橫向
			if(image.getWidth()>image.getHeight())
			{//來源橫向z
				int newSide = Math.round((image.getHeight()/proportionH)*proportionW);
				pic = Bitmap.createBitmap(
							image, 
							image.getWidth() > newSide ? Math.round((image.getWidth()-newSide)/2):0, 
							0, 
							newSide, 
							image.getHeight()
						);
			}
			else
			{//來源縱向
				int newSide = Math.round((image.getWidth()/proportionW)*proportionH);
				
				pic = Bitmap.createBitmap(
						image, 
						0, 
						image.getHeight()>newSide?Math.round((image.getHeight()-newSide)/2):0, 
						image.getWidth(), 
						newSide
					);
			}
		}else if(proportionH > proportionW)
		{//結果要是縱向
			if(image.getWidth()>image.getHeight())
			{//來源橫向
				int newSide = Math.round((image.getHeight()/proportionH)*proportionW);
				pic = Bitmap.createBitmap(
						image, 
						image.getWidth()>newSide? Math.round((image.getWidth()-newSide)/2):0, 
						0, 
						newSide, 
						image.getHeight()
					);
			}
			else
			{//來源縱向
				int newSide = Math.round((image.getWidth()/proportionW)*proportionH);
				pic = Bitmap.createBitmap(
						image, 
						0, 
						image.getHeight() > newSide ? Math.round((image.getHeight()-newSide)/2):0 , 
						image.getWidth(), 
						newSide
					);
			}
		}else
		{//結果要是正方
	    	if(image.getWidth()>image.getHeight())
	    	{//來源橫向
	    		pic = Bitmap.createBitmap(
	    				image, 
	    				(image.getWidth()-image.getHeight())/2, 
	    				0, 
	    				image.getHeight(), 
	    				image.getHeight());
	    	}
	    	else
	    	{//來源縱向
	    		pic = Bitmap.createBitmap(
	    				image, 
	    				0, 
	    				(image.getHeight()-image.getWidth())/2, 
	    				image.getWidth(),
	    				image.getWidth());
	    	}
		}
		 	
		return pic;
	}

	public static Bitmap getSquarePic(Activity activity, Bitmap image){
		DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        
        int dst = dm.widthPixels;
        if(dm.widthPixels>dm.heightPixels){
        	dst = dm.heightPixels;
        }
        else{
        	dst = dm.widthPixels;
        }
		Bitmap pic;

    	if(image.getWidth()>image.getHeight()){
    		pic = Bitmap.createBitmap(image, (image.getWidth()-image.getHeight())/2, 0, image.getHeight(), image.getHeight());
    		image.recycle();
        	System.gc();
    		pic = Bitmap.createScaledBitmap(pic, dst, dst, true);
    	}
    	else{
    		pic = Bitmap.createBitmap(image, 0, (image.getHeight()-image.getWidth())/2, image.getWidth(), image.getWidth());
    		image.recycle();
        	System.gc();
    		pic = Bitmap.createScaledBitmap(pic, dst, dst, true);
    	}
		return pic;
	}
	public static Bitmap decodeFile(File f){
	    try {
	        
	        BitmapFactory.Options o = new BitmapFactory.Options();
	        o.inJustDecodeBounds = true;
	        BitmapFactory.decodeStream(new FileInputStream(f),null,o);

	        final int REQUIRED_SIZE=640;

	        int width_tmp=o.outWidth, height_tmp=o.outHeight;
	        int scale=1;
	        while(true){
	            if(width_tmp/2<=REQUIRED_SIZE || height_tmp/2<=REQUIRED_SIZE)
	                break;
	            width_tmp/=2;
	            height_tmp/=2;
	            scale*=2;
	        }


	        BitmapFactory.Options o2 = new BitmapFactory.Options();
	        o2.inSampleSize=scale;
	        return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
	    } catch (FileNotFoundException e) {}
	    return null;
	}

	
	public static Bitmap resetImageOrientation(File imageFile){
		Bitmap bitmap=null;
		int height=0,width=0,rotate = 0;
		Matrix mat = new Matrix();

		
		//檢查方向
	    try {
	    	 //activity.getContentResolver().notifyChange(imageUri, null);
	    	 
	         //File imageFile = new File(imagePath);
	         
	        bitmap = decodeFile(imageFile);
	        
			width = bitmap.getWidth();
			height = bitmap.getHeight();
	         
	         ExifInterface exif = new ExifInterface(
	                 imageFile.getAbsolutePath());
	         
	         int orientation = exif.getAttributeInt(
	                 ExifInterface.TAG_ORIENTATION,
	                 ExifInterface.ORIENTATION_NORMAL);
/** _________ ︻_
 * |			|
 * |			|
 * | 			|
 * |____________|
 *  0度, 相機正上方為拍照案件
 *  順時鐘轉
 **/
	        
	         switch (orientation) {
	         	case ExifInterface.ORIENTATION_ROTATE_270:
	         		//拍照按鈕位於左側
	         		rotate = 270;
	         		break;
	         	case ExifInterface.ORIENTATION_ROTATE_180:
	         		//拍照按鈕位於下方
	            	rotate = 180;
	            	break;
	         	case ExifInterface.ORIENTATION_ROTATE_90:
	         		//拍照按鈕位於右側
	         		rotate = 90;
	         		
	         		
	         		break;
	         }
	         
	 
	         mat.postRotate(rotate);

	    } catch (Exception e) {
	         e.printStackTrace();

	    }

	    
	    
		return Bitmap.createBitmap(
				bitmap, 
				0, 
				0, 
				width, 
				height,
				mat, 
				false);
	}
	public static Bitmap resetImageOrientation(File imageFile,Bitmap bitmap){
		int height=0,width=0,rotate = 0;
		Matrix mat = new Matrix();

		
		//檢查方向
	    try {

			width = bitmap.getWidth();
			height = bitmap.getHeight();
	         
	         ExifInterface exif = new ExifInterface(
	                 imageFile.getAbsolutePath());
	         
	         int orientation = exif.getAttributeInt(
	                 ExifInterface.TAG_ORIENTATION,
	                 ExifInterface.ORIENTATION_NORMAL);
/** _________ ︻_
 * |			|
 * |			|
 * | 			|
 * |____________|
 *  0度, 相機正上方為拍照案件
 *  順時鐘轉
 **/
	        
	         switch (orientation) {
	         	case ExifInterface.ORIENTATION_ROTATE_270:
	         		//拍照按鈕位於左側
	         		rotate = 270;
	         		break;
	         	case ExifInterface.ORIENTATION_ROTATE_180:
	         		//拍照按鈕位於下方
	            	rotate = 180;
	            	break;
	         	case ExifInterface.ORIENTATION_ROTATE_90:
	         		//拍照按鈕位於右側
	         		rotate = 90;
	         		
	         		
	         		break;
	         }
	         
	 
	         mat.postRotate(rotate);

	    } catch (Exception e) {
	         e.printStackTrace();

	    }
		return Bitmap.createBitmap(
				bitmap, 
				0, 
				0, 
				width, 
				height,
				mat, 
				false);
	}

	
	public static byte[] readBytes(FileInputStream inputStream){
	    ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
	  
	    try {
		    byte[] buffer = new byte[inputStream.available()];
		    int len = 0;
			while ((len = inputStream.read(buffer)) != -1) {
			    byteBuffer.write(buffer, 0, len);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	    return byteBuffer.toByteArray();

	}

	public static class FlushedInputStream extends FilterInputStream {
		public FlushedInputStream(InputStream inputStream) {
		    super(inputStream);
		}

		@Override
		public long skip(long n) throws IOException {
		    long totalBytesSkipped = 0L;
		    while (totalBytesSkipped < n) {
		        long bytesSkipped = in.skip(n - totalBytesSkipped);
		        if (bytesSkipped == 0L) {
		              int bytes = read();
		              if (bytes < 0) {
		                  break; 
		              } else {
		                  bytesSkipped = 1;
		              }
		       }
		        totalBytesSkipped += bytesSkipped;
		    }
		    return totalBytesSkipped;
		}
	}
	//圖片轉圓
	public static Bitmap toRoundBitmap(Bitmap bitmap) {
		if(bitmap != null)
		{
	
	        int width = bitmap.getWidth();
	        int height = bitmap.getHeight();
	        float roundPx;
	        float left,top,right,bottom,dst_left,dst_top,dst_right,dst_bottom;
	        if (width <= height) {
	                roundPx = width / 2;
	                top = 0;
	                bottom = width;
	                left = 0;
	                right = width;
	                height = width;
	                dst_left = 0;
	                dst_top = 0;
	                dst_right = width;
	                dst_bottom = width;
	        } else {
	                roundPx = height / 2;
	                float clip = (width - height) / 2;
	                left = clip;
	                right = width - clip;
	                top = 0;
	                bottom = height;
	                width = height;
	                dst_left = 0;
	                dst_top = 0;
	                dst_right = height;
	                dst_bottom = height;
	        }
	         
	        Bitmap output = Bitmap.createBitmap(width,
	                        height, Bitmap.Config.ARGB_8888);
	        Canvas canvas = new Canvas(output);
	         
	        final int color = 0xff424242;
	        final Paint paint = new Paint();
	        final Rect src = new Rect((int)left, (int)top, (int)right, (int)bottom);
	        final Rect dst = new Rect((int)dst_left, (int)dst_top, (int)dst_right, (int)dst_bottom);
	        final RectF rectF = new RectF(dst);
	
	        paint.setAntiAlias(true);
	         
	        canvas.drawARGB(0, 0, 0, 0);
	        paint.setColor(color);
	        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
	
	        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
	        canvas.drawBitmap(bitmap, src, dst, paint);
	        return output;
		}
		else
			return null;
    }


    /**
     * 轉換 dp to px
     * @param dp
     * @param context
     * @return pixel
     */
    public static float convertDpToPixel(float dp, Context context){
        float px = dp * getDensity(context);
        return px;
    }

    /**
     * 轉換 px to dp
     * @param px
     * @param context
     * @return dp
     */
    public static float convertPixelToDp(float px, Context context){
        float dp = px / getDensity(context);
        return dp;
    }

    /**
     * 取得螢幕密度
     * 120dpi = 0.75
     * 160dpi = 1 (default)
     * 240dpi = 1.5
	 * 320dpi = 2.0
	 *
     * @param context
     * @return
     */
    public static float getDensity(Context context){
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return metrics.density;
    }
	
	
}
