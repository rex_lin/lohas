package com.acloud.app.util;

import android.app.Activity;
import android.content.Context;

import com.acloud.app.model.UserProfileResult;
import com.acloud.app.tool.Pub;

/**
 * ProfileManager 從SharedPrefence取得資料
 */

public class UserProfileManager {

    public static void setUserProfile(Context context, UserProfileResult result) {
        PrefConstant.setString(context, Pub.VALUE_UID, result.getId());
        PrefConstant.setString(context, Pub.VALUE_SERIAL_NO, result.getSerialNo());
        PrefConstant.setString(context, Pub.VALUE_EMAIL, result.getEmail());
        PrefConstant.setString(context, Pub.VALUE_BONUS, result.getBonus());
        PrefConstant.setString(context, Pub.VALUE_NAME, result.getName());
        PrefConstant.setString(context, Pub.VALUE_PHONE, result.getPhone());
        PrefConstant.setString(context, Pub.VALUE_BIRTHDAY, result.getBirthday());
        PrefConstant.setString(context, Pub.VALUE_GENDER, result.getGender());
        PrefConstant.setString(context, Pub.VALUE_MEMO, result.getMemo());
        PrefConstant.setString(context, Pub.VALUE_PUSH_SWITCH, result.getPushSwitch());
        PrefConstant.setString(context, Pub.VALUE_PUSHTOEKN, result.getPushToken());
    }

    public static UserProfileResult getUserProfile(Context context) {
        UserProfileResult result = new UserProfileResult();
        result.setId(PrefConstant.getString(context, Pub.VALUE_UID, ""));
        result.setSerialNo(PrefConstant.getString(context, Pub.VALUE_SERIAL_NO, ""));
        result.setEmail(PrefConstant.getString(context, Pub.VALUE_EMAIL, ""));
        result.setBonus(PrefConstant.getString(context, Pub.VALUE_BONUS, ""));
        result.setName(PrefConstant.getString(context, Pub.VALUE_NAME, ""));
        result.setPhone(PrefConstant.getString(context, Pub.VALUE_PHONE, ""));
        result.setBirthday(PrefConstant.getString(context, Pub.VALUE_BIRTHDAY, ""));
        result.setGender(PrefConstant.getString(context, Pub.VALUE_GENDER, ""));
        result.setMemo(PrefConstant.getString(context, Pub.VALUE_MEMO, ""));
        result.setPushSwitch(PrefConstant.getString(context, Pub.VALUE_PUSH_SWITCH, ""));
        result.setPushToken(PrefConstant.getString(context, Pub.VALUE_PUSHTOEKN, ""));
        return result;
    }

//    public static boolean loginStatus(Activity activity) {
//        if (PrefConstant.getString(activity, Pub.VALUE_UID, "").equals(""))
//            return false;
//        else
//            return true;
//    }

    public static boolean loginStatus(Activity activity) {
        if (PrefConstant.getString(activity, Pub.VALUE_UID, "").equals(""))
            return false;
        else
            return true;
    }
}
