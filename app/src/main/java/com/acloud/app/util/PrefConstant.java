package com.acloud.app.util;


import android.content.Context;
import android.content.SharedPreferences;

import com.acloud.app.tool.Pub;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

/**
 * SharedPreference
 */
public class PrefConstant {

	
	/** sharedPreference name */
	public static final String PreferenceName = "Profile";
	
	public static SharedPreferences sp = null;

	private static SharedPreferences getInstances(Context context){
		if(sp == null){
			sp = context.getSharedPreferences(PreferenceName, 1);
		}
		return sp;
	}

    public static String getString(Context context,String keyName , String defValue){
		return getInstances(context).getString(keyName, defValue);
	}
	
	public static boolean setString(Context context,String keyName , String value){
		return getInstances(context).edit().putString(keyName, value).commit();
	}
	
	public static boolean getBoolean(Context context,String keyName , boolean defValue){
		return getInstances(context).getBoolean(keyName, defValue);
	}
	
	public static boolean setBoolean(Context context,String keyName , boolean value){
		return getInstances(context).edit().putBoolean(keyName, value).commit();
	}
	
	public static int getInt(Context context,String keyName , int defValue){
		return getInstances(context).getInt(keyName, defValue);
	}

	public static boolean setInt(Context context,String keyName , int value){
		return getInstances(context).edit().putInt(keyName, value).commit();
	}
	
	public static long getLong(Context context,String keyName , long defValue){
		return getInstances(context).getLong(keyName, defValue);
	}
	
	public static boolean setLong(Context context,String keyName , long value){
		return getInstances(context).edit().putLong(keyName, value).commit();
	}
	
	public static float getFloat(Context context,String keyName , float defValue){
		return getInstances(context).getFloat(keyName, defValue);
	}
	
	public static boolean setLong(Context context,String keyName , float value){
		return getInstances(context).edit().putFloat(keyName, value).commit();
	}

    //儲存收藏資料到本機暫存
    public static boolean setFavStoreList(Context context,JSONArray addValue){
        return getInstances(context).edit().putString(Pub.TEMP_FAV, addValue.toString()).commit();
    }

    //存入一筆收藏資料到本機暫存
    public static boolean setFavStore(Context context,String store_id){
        //讀出舊資料
        String ArrData = getInstances(context).getString(Pub.TEMP_FAV, "[]");
        try {
            //將既有值轉為JSONArray
            JSONArray nowValue =new JSONArray(ArrData);

            //如果已經有這筆資料 , 則更新
            boolean update=false;
            for(int i = 0 ;i<nowValue.length();i++){
                if(nowValue.getString(i).equals(store_id)){
                    nowValue.put(i,store_id);
                    update=true;
                    break;
                }
            }

            //如果沒此資料,則新增
            if(!update){
                nowValue.put(store_id);
            }

            if(nowValue!=null){
                //更新回存字串
                ArrData = nowValue.toString();
            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return getInstances(context).edit().putString(Pub.TEMP_FAV, ArrData).commit();
    }

    //讀出收藏列表
    public static List<String> getFavStoreList(Context context){
        List<String> retValue = new ArrayList<String>();
        try {
            JSONArray jsonArr = new JSONArray(getInstances(context).getString(Pub.TEMP_FAV, "[]"));
            //MyApplication.logE("getRemindNoteToList [jsonArr]",jsonArr.toString());

            for(int n = 0 ; n < jsonArr.length();n++){
                retValue.add(jsonArr.getString(n));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return retValue;
    }

    //讀出一筆備忘
    public static String getFavStore(Context context,int n){
        String retValue = null;
        try {
            JSONArray jsonArr = new JSONArray(getInstances(context).getString(Pub.TEMP_FAV, "[]"));
            if(n < jsonArr.length()){
                retValue = jsonArr.getString(n);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return retValue;
    }

    public static String getFavStore(Context context,String store_id){
        String retValue = null;
        try {
            JSONArray jsonArr = new JSONArray(getInstances(context).getString(Pub.TEMP_FAV, "[]"));
            for(int n =0 ;n < jsonArr.length();n++){

                if(store_id.equals(jsonArr.getString(n))) {
                    retValue = jsonArr.getString(n);
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return retValue;
    }

    public static boolean isFavStoreExist(Context context,String store_id){
        try {
            JSONArray jsonArr = new JSONArray(getInstances(context).getString(Pub.TEMP_FAV, "[]"));
            for(int n =0 ;n < jsonArr.length();n++){
                if(store_id.equals(jsonArr.getString(n))) {
                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    //移除一筆備忘
    public static boolean removeFavStore(Context context,String store_id){
        boolean retValue =false;
        try {
            JSONArray jsonArr = new JSONArray(getInstances(context).getString(Pub.TEMP_FAV, "[]"));
            JSONArray newJsonArr = new JSONArray();

            for(int n =0 ;n < jsonArr.length();n++){
                if(!store_id.equals(jsonArr.getString(n)))
                {
                    newJsonArr.put(jsonArr.getString(n));
                }
            }
            retValue=getInstances(context).edit().putString(Pub.TEMP_FAV, newJsonArr.toString()).commit();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return retValue;
    }
}
