package com.acloud.app.util;

import com.acloud.app.model.CouponDetailResult;
import com.acloud.app.tool.Pub;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;

/**
 * Coupon詳細頁內容
 */
public class CouponDetailManager {

    static CouponDetailResult couponDetailResult = new CouponDetailResult();

    public static ArrayList<NameValuePair> getCouponDetail(String uid, String authtoken,
                                                           String coupon_id,String lat,String lng){
        ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
        list.add(new BasicNameValuePair(Pub.VALUE_APIKEY, Pub.APIKEY));
        list.add(new BasicNameValuePair(Pub.VALUE_UID, uid));
        list.add(new BasicNameValuePair(Pub.VALUE_AUTHTOKEN, authtoken));
        list.add(new BasicNameValuePair(Pub.VALUE_COUPON_ID, coupon_id));
        list.add(new BasicNameValuePair(Pub.VALUE_LAT, lat));
        list.add(new BasicNameValuePair(Pub.VALUE_LNG, lng));
        return list;
    };

    /**
     * 取得CouponDetail Result
     * @return
     */
    public static CouponDetailResult getCouponDetailResult() {
        return couponDetailResult;
    }

    public static void setCouponDetailResult(CouponDetailResult couponDetailResult) {
        CouponDetailManager.couponDetailResult = couponDetailResult;
    }
}
