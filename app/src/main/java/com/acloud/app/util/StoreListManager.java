package com.acloud.app.util;

import com.acloud.app.model.StoreListResult;
import com.acloud.app.tool.Pub;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;

/**
 * 管理和StroreList相關
 */
public class StoreListManager {

    static ArrayList<StoreListResult> resultArrayList = new ArrayList<StoreListResult>();

    public static ArrayList<NameValuePair> getStore_List
            (String uid, String authtoken, String index, String size, String keyword,
             String sd_id, String sm_id, String sc_id, String sca_id, String sort_type, String price_ranage_start,
             String price_ranage_end, String lat, String lng) {

        ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
        list.add(new BasicNameValuePair(Pub.VALUE_APIKEY, Pub.APIKEY));
        list.add(new BasicNameValuePair(Pub.VALUE_UID, uid));
        list.add(new BasicNameValuePair(Pub.VALUE_AUTHTOKEN, authtoken));
        list.add(new BasicNameValuePair(Pub.VALUE_INDEX, index));
        list.add(new BasicNameValuePair(Pub.VALUE_SIZE, size));
        list.add(new BasicNameValuePair(Pub.VALUE_KEYWORD, keyword));
        list.add(new BasicNameValuePair(Pub.VALUE_SDID, sd_id));
        list.add(new BasicNameValuePair(Pub.VALUE_SMID, sm_id));
        list.add(new BasicNameValuePair(Pub.VALUE_SCID, sc_id));
        list.add(new BasicNameValuePair(Pub.VALUE_SCAID, sca_id));
        list.add(new BasicNameValuePair(Pub.VALUE_SORT_TYPE, sort_type));
        list.add(new BasicNameValuePair(Pub.VALUE_PRICE_RANGE_START, price_ranage_start));
        list.add(new BasicNameValuePair(Pub.VALUE_PRICE_RANGE_END, price_ranage_end));
        list.add(new BasicNameValuePair(Pub.VALUE_LAT, lat));
        list.add(new BasicNameValuePair(Pub.VALUE_LNG, lng));
        return list;
    }

    public static ArrayList<StoreListResult> getResultArrayList() {
        return resultArrayList;
    }

    public static void setResultArrayList(ArrayList<StoreListResult> resultArrayList) {
        StoreListManager.resultArrayList = resultArrayList;
    }
}
