package com.acloud.app.util;

import com.acloud.app.model.StoreCategoryMenuResult;
import com.acloud.app.tool.Pub;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;

/**
 *
 */
public class StoreCategoryMenuManager {
    public static ArrayList<StoreCategoryMenuResult> getList() {
        return list;
    }

    public static void setList(ArrayList<StoreCategoryMenuResult> list) {
        StoreCategoryMenuManager.list = list;
    }

    static ArrayList<StoreCategoryMenuResult> list = new ArrayList<>();

    public static ArrayList<NameValuePair> getMenu(String uid, String authtoken) {
        ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
        list.add(new BasicNameValuePair(Pub.VALUE_APIKEY, Pub.APIKEY));
        list.add(new BasicNameValuePair(Pub.VALUE_UID, uid));
        list.add(new BasicNameValuePair(Pub.VALUE_AUTHTOKEN, authtoken));
        return list;
    }


}
