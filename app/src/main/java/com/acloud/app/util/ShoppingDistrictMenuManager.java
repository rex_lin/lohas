package com.acloud.app.util;

import com.acloud.app.model.ShoppingDistrictMenuResult;
import com.acloud.app.tool.Pub;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;

/**
 * Created by skywind-10 on 2015/8/11.
 */
public class ShoppingDistrictMenuManager {
    public static ArrayList<ShoppingDistrictMenuResult> getList() {
        return list;
    }

    public static void setList(ArrayList<ShoppingDistrictMenuResult> list) {
        ShoppingDistrictMenuManager.list = list;
    }

    static ArrayList<ShoppingDistrictMenuResult> list = new ArrayList<>();

    public static ArrayList<NameValuePair> getMenu(String uid,String authtoken){
        ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
        list.add(new BasicNameValuePair(Pub.VALUE_APIKEY, Pub.APIKEY));
        list.add(new BasicNameValuePair(Pub.VALUE_UID, uid));
        list.add(new BasicNameValuePair(Pub.VALUE_AUTHTOKEN,authtoken));
       return list;
    }


}
