package com.acloud.app.util;

import com.acloud.app.model.MyCouponListResult;
import com.acloud.app.tool.Pub;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;

/**
 *
 */
public class MyCouponListManager {

    static ArrayList<MyCouponListResult> resultArrayList_unused = new ArrayList<MyCouponListResult>();
    static ArrayList<MyCouponListResult> resultArrayList_used = new ArrayList<MyCouponListResult>();
    static ArrayList<MyCouponListResult> resultArrayList_expired= new ArrayList<MyCouponListResult>();

    public static ArrayList<NameValuePair> getMyCouponList (String uid,String authtoken, String index,String size,
                                                            String type, String lat,String lng){
        ArrayList<NameValuePair> list  = new ArrayList<NameValuePair>();
        list.add(new BasicNameValuePair(Pub.VALUE_APIKEY, Pub.APIKEY));
        list.add(new BasicNameValuePair(Pub.VALUE_UID, uid));
        list.add(new BasicNameValuePair(Pub.VALUE_AUTHTOKEN,authtoken));
        list.add(new BasicNameValuePair(Pub.VALUE_INDEX, index));
        list.add(new BasicNameValuePair(Pub.VALUE_SIZE, size));
        list.add(new BasicNameValuePair(Pub.VALUE_TYPE, type));
        list.add(new BasicNameValuePair(Pub.VALUE_LAT, lat));
        list.add(new BasicNameValuePair(Pub.VALUE_LNG, lng));

        return list;
    }

    public static ArrayList<MyCouponListResult> getResultArrayList_unused() {
        return resultArrayList_unused;
    }

    public static void setResultArrayList_unused(ArrayList<MyCouponListResult> resultArrayList_unused) {
        MyCouponListManager.resultArrayList_unused = resultArrayList_unused;
    }

    public static ArrayList<MyCouponListResult> getResultArrayList_used() {
        return resultArrayList_used;
    }

    public static void setResultArrayList_used(ArrayList<MyCouponListResult> resultArrayList_used) {
        MyCouponListManager.resultArrayList_used = resultArrayList_used;
    }

    public static ArrayList<MyCouponListResult> getResultArrayList_expired() {
        return resultArrayList_expired;
    }

    public static void setResultArrayList_expired(ArrayList<MyCouponListResult> resultArrayList_expired) {
        MyCouponListManager.resultArrayList_expired = resultArrayList_expired;
    }

}

