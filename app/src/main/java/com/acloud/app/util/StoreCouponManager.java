package com.acloud.app.util;

import com.acloud.app.model.StoreDetailCouponResult;
import com.acloud.app.tool.Pub;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;

/**
 * 管理商店明細內的優惠卷
 */
public class StoreCouponManager {
    public static ArrayList<StoreDetailCouponResult> getResultArrayList() {
        return resultArrayList;
    }

    public static void setResultArrayList(ArrayList<StoreDetailCouponResult> resultArrayList) {
        StoreCouponManager.resultArrayList = resultArrayList;
    }

    static ArrayList<StoreDetailCouponResult> resultArrayList = new ArrayList<StoreDetailCouponResult>();


    public static ArrayList<NameValuePair> getStoreCoupon(String uid, String authtoken, String store_id) {
        ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
        list.add(new BasicNameValuePair(Pub.VALUE_APIKEY, Pub.APIKEY));
        list.add(new BasicNameValuePair(Pub.VALUE_UID, uid));
        list.add(new BasicNameValuePair(Pub.VALUE_AUTHTOKEN, authtoken));
        list.add(new BasicNameValuePair(Pub.VALUE_STOREID, store_id));
        return list;
    }
}
