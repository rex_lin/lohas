package com.acloud.app.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;

import com.acloud.app.MyApplication;

public class GetLocationIntentService extends IntentService implements LocationListener{
	
	public static final String GET_LOCATION="ACTION_GET_LOCATION";

	public static final String FEEDBACK_LOCATION="ACTION_FEEDBACK_LOCATION";
	
	private LocationManager locationMganage;
	private Location location = null;
	private Thread loopT=null;
	private boolean keepRun=true;
	
	public GetLocationIntentService() {
		super("GetLocationIntentService");
	}

	@Override  
    public IBinder onBind(Intent intent) {   
   
        return super.onBind(intent);   
    }   
    
    @Override  
    public void onCreate() {   

    	locationMganage = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
    	
    	Criteria criteria = new Criteria();
    	criteria.setAccuracy(Criteria.ACCURACY_COARSE);
		String provider = locationMganage.getBestProvider(criteria, true);
        if (provider == null) {
        	MyApplication.logE("GetLocationIntentService", "provider is null");
            return ;
        }
        else
        {
        
        	locationMganage.requestLocationUpdates(provider, 1000, 0, this);

        	location = locationMganage.getLastKnownLocation(provider);

        	this.onLocationChanged(location);
        }
  
        super.onCreate();   
    }   
    
    @Override  
    public void onDestroy() {   
 
        super.onDestroy();   
    }   
    
    @Override  
    public void onStart(Intent intent, int startId) {   
    	
        super.onStart(intent, startId);   
    }   
    
    @Override  
    public int onStartCommand(Intent intent, int flags, int startId) {   
    
        return super.onStartCommand(intent, flags, startId);   
    }   
    
    @Override  
    public void setIntentRedelivery(boolean enabled) {   
 
        super.setIntentRedelivery(enabled);   
    }   
  
    @Override  
    protected void onHandleIntent(Intent intent) {  
    	
    	MyApplication.logE("GetLocationIntentService", "onHandleIntent");
    	String action = intent.getStringExtra("ACTION");  
    	
    	if(action.equals(GET_LOCATION))
    		getlocation();


    }  
    
    private void getlocation()
    {
    	MyApplication.logE("GetLocationIntentService", "getlocation");
    }

	@Override
	public void onLocationChanged(Location location) {
		MyApplication.logE("GetLocationIntentService", "onLocationChanged");
		if(location!=null)
		{
	        Intent broadcastIntent = new Intent();  
	        broadcastIntent.setAction(FEEDBACK_LOCATION);  
	        broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);  
	        broadcastIntent.putExtra("lat", location.getLatitude());
	        broadcastIntent.putExtra("lng", location.getLongitude());
	        sendBroadcast(broadcastIntent);  
	        keepRun=false;	
	        MyApplication.logE("GetLocationIntentService", "sendBroadcast");
		}
        
	}

	@Override
	public void onProviderDisabled(String provider) {
		 // 當 location provider 無效時
	}

	@Override
	public void onProviderEnabled(String provider) {
		// 當 location provider 有效時
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// 當 location provider 改變時
	}

}
