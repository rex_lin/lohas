package com.acloud.app;

public interface NavigationDrawerCallbacks {
    void onNavigationDrawerItemSelected(int position);
}
