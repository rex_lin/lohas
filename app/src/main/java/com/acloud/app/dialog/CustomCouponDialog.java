package com.acloud.app.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.preference.DialogPreference;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.acloud.app.R;
import com.acloud.app.tool.TLog;
import com.acloud.app.util.BitmapHandler;

import java.io.InputStream;
import java.net.URL;


public class CustomCouponDialog {
    private Activity activity;
    public Dialog dialog;
    private View view;
    LayoutInflater inflater;
    String mTitle,
            mDate,
            mImgUrl,
            mCouponname,
            mContent;
    RelativeLayout imgMarquee;

    public CustomCouponDialog(Activity c, String storetitle, String date, String imgUrl, String couponname,
                              String content) {
        this.activity = c;
        inflater = c.getLayoutInflater();
        mTitle = storetitle;
        mDate = date;
        mImgUrl = imgUrl;
        mCouponname = couponname;
        mContent = content;
        this.view = inflater.inflate(R.layout.custom_coupon_dialog, null);
    }


    public Dialog getDialog() {
        return dialog;
    }

    public View findView(int id) {
        return getDialog().getWindow().findViewById(id);
    }

    public boolean isShowing() {
        return dialog.isShowing();
    }

    public void show() {
        dialog.show();
    }

    public void dismiss() {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
    }


    /**
     * 初始化ProgressDialog
     */
    public CouponDialog initCouponDialog() {
        return new CouponDialog();
    }

    public class CouponDialog {
        private CouponDialog() {
            dialog = new Dialog(activity, R.style.TranslucentThemeSelector) {
                @Override
                public void onCreate(Bundle savedInstanceState) {
                    super.onCreate(savedInstanceState);
                    DisplayMetrics dm = new DisplayMetrics();
                    activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
                    LayoutParams params = getWindow().getAttributes();
                    params.width = dm.widthPixels;
                    params.height = dm.heightPixels - getStatusBarHeight();
                    TLog.i("TAG", " params.height" + params.height + " ; " + getStatusBarHeight());
                    getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);

                }
            };
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(view);
            //是否可由實體按鍵關閉
            dialog.setCancelable(false);

            //標題
            ((TextView) view.findViewById(R.id.tvStoreName)).setVisibility(TextView.VISIBLE);
            ((TextView) view.findViewById(R.id.tvStoreName)).setText(mTitle);

            //日期
            ((TextView) view.findViewById(R.id.tvDate)).setVisibility(TextView.VISIBLE);
            ((TextView) view.findViewById(R.id.tvDate)).setText(mDate);

            //優惠卷名稱
            ((TextView) view.findViewById(R.id.tvSubtitle)).setVisibility(TextView.VISIBLE);
            ((TextView) view.findViewById(R.id.tvSubtitle)).setText(String.format(activity.getString(R.string.txt_exchange_coupon_title),
                    mCouponname));

            ((ImageView) view.findViewById(R.id.imgStore)).setVisibility(TextView.VISIBLE);
//

            //提示說明文
            ((TextView) view.findViewById(R.id.tvContent)).setVisibility(TextView.VISIBLE);
            ((TextView) view.findViewById(R.id.tvContent)).setText(String.format(activity.getString(R.string.txt_coupon_content), mContent));

            //跑馬燈秒數
            ((TextView) view.findViewById(R.id.tvTimer)).setVisibility(TextView.VISIBLE);

            //關閉Button
            ((ImageButton)view.findViewById(R.id.btnClose)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder alertdialog = new AlertDialog.Builder(activity);
                    alertdialog.setTitle(activity.getString(R.string.btn_close));
                    alertdialog.setMessage(activity.getString(R.string.txt_coupon_dialog_close_msg));
                    alertdialog.setPositiveButton(activity.getString(R.string.btn_ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface log, int which) {
                            dialog.dismiss();

                        }
                    });
                    alertdialog.setNegativeButton(activity.getString(R.string.btn_cancel), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface log, int which) {

                        }
                    });
                    alertdialog.show();
                }
            });
            //開始讀取圖片
            new Thread() {
                @Override
                public void run() {
                    Message msg = new Message();

                    try {
                        URL url = new URL(mImgUrl);
                        InputStream is = (InputStream) url.getContent();
                        if (is != null) {
                            System.gc();
                            //DownLoad Image    BitmapHandler class
                            is = new BitmapHandler.FlushedInputStream(is);
                            msg.obj = BitmapFactory.decodeStream(is);
                            is.close();
                            System.gc();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (msg.obj != null)
                        updateIconHandler.sendMessage(msg);
                }
            }.start();
            imgMarquee = (RelativeLayout) view.findViewById(R.id.Marquee);

            //倒數計時
            System.gc();
            new CountDownTimer(120000, 1000) {
                @Override
                public void onTick(long l) {
                    ((TextView) view.findViewById(R.id.tvTimer)).setText("" + l / 1000);
                }

                @Override
                public void onFinish() {
                    dialog.dismiss();
                }
            }.start();

            DisplayMetrics dm = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(dm);

            /**
             * 跑馬燈動畫
             */                                         //設置移動距離
            Animation am = new TranslateAnimation(0.0f, -3000.0f, 0.0f, 0.0f);

            //調整移動速度
            am.setDuration(10000);
            //設置線性移動,才不會產生移動完速度放慢問題
            am.setInterpolator(new LinearInterpolator());
            //setRepeatCount (int repeatCount) 設定重複次數 -1為無限次數 0
            am.setRepeatCount(-1);
            //將動畫參數設定到圖片並開始執行動畫
            imgMarquee.startAnimation(am);
            //Layout設定Margin = -2000dp是為了讓圖片移動不會露出邊緣
        }
    }

    final Handler updateIconHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg != null) {
                ((ImageView) view.findViewById(R.id.imgStore)).setImageBitmap((Bitmap) msg.obj);
            }
        }
    };

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = activity.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = activity.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

}
