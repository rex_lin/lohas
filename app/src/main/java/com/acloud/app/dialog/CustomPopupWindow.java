package com.acloud.app.dialog;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.acloud.app.R;
import com.acloud.app.adapter.MenuAdapter;
import com.acloud.app.adapter.MenuStoreAdapter;
import com.acloud.app.fragment.StoreFragment;
import com.acloud.app.model.MenuObject;
import com.acloud.app.model.ShoppingDistrictMenuResult;
import com.acloud.app.model.StoreCategoryMenuResult;
import com.acloud.app.tool.Pub;
import com.acloud.app.util.ShoppingDistrictMenuManager;
import com.acloud.app.util.StoreCategoryMenuManager;

import java.util.ArrayList;

/**
 * 店家頁彈出排序頁
 */
public class CustomPopupWindow {
    static PopupWindow
            mMainWindows,
            mCenterWindows,
            mRightWindows,
            mFliterWindows;
    private static Handler statichandler;

    private int mScreenWidth;
    private int mScreenHeight;
    //    Context mContext;
    Activity mActivity;
    Handler handler;
    MenuAdapter menuAdapter;


    ArrayList<MenuObject> mainCategoryArray = new ArrayList<>(); //主要分類
    ArrayList<MenuObject> mainDistrictArray = new ArrayList<>();//主要商圈
    ArrayList<ArrayList<MenuObject>> minorCategoryArray = new ArrayList<ArrayList<MenuObject>>(); //次要屬性
    ArrayList<ArrayList<MenuObject>> minorDistrictArray = new ArrayList<ArrayList<MenuObject>>();//次要商場

    //商圈
    ArrayList<ShoppingDistrictMenuResult> districtMenuResults = ShoppingDistrictMenuManager.getList();
    //分類
    ArrayList<StoreCategoryMenuResult> categoryMenuResults = StoreCategoryMenuManager.getList();

    ListView
            mainCategoryListView, //主要分類ListView
            minorCategoryListView, //次要屬性ListView
            mainDistrictListView, //主要商圈ListView
            minorDistrictListView; //次要商場ListView

    TextView
            btnDistance,
            btnEvaluate,
            btnCollotion,
            btnPrice;

    MenuStoreAdapter
            mDistrictAdapter,
            mCategoryAdapter;

    View
            popupWindow_category,
            popupWindow_district,
            popupWindow_sort;

    public CustomPopupWindow(Activity activity) {
        mActivity = activity;
    }

    public CustomPopupWindow(Activity activity, Handler handler) {
        mActivity = activity;
        this.handler = handler;
        this.statichandler = handler;
    }

    /**
     * 初始化預設選項　ex:全部分類,全部屬性,全部商圈,全部商場
     */
    private void initDefaultValue() {
        mainCategoryArray.add(new MenuObject("0", mActivity.getString(R.string.sort_all_category))); //預設選項 主要分類
        ArrayList<MenuObject> defaultMenu = new ArrayList<>();
        defaultMenu.add(new MenuObject("0", mActivity.getString(R.string.sort_all_property))); //預設選項
        minorCategoryArray.add(defaultMenu);

        mainDistrictArray.add(new MenuObject("0", mActivity.getString(R.string.sort_all_district)));
        ArrayList<MenuObject> defaultMenu_2 = new ArrayList<>();
        defaultMenu_2.add(new MenuObject("0", mActivity.getString(R.string.sort_all_market)));
        minorDistrictArray.add(defaultMenu_2);
    }

    private void initUI(LayoutInflater layoutInflater) {
        popupWindow_category = layoutInflater.inflate(R.layout.window_filter_category, null);
        popupWindow_district = layoutInflater.inflate(R.layout.window_filter_storegroup, null);
        popupWindow_sort = layoutInflater.inflate(R.layout.window_filter_sort, null);
        mScreenWidth = mActivity.getWindowManager().getDefaultDisplay().getWidth();
        mScreenHeight = mActivity.getWindowManager().getDefaultDisplay().getHeight();

        //視窗長寬高
        mMainWindows = new PopupWindow(popupWindow_category, mScreenWidth, mScreenHeight);
        mCenterWindows = new PopupWindow(popupWindow_district, mScreenWidth, mScreenHeight);
        mRightWindows = new PopupWindow(popupWindow_sort, mScreenWidth, mScreenHeight);

        minorCategoryListView = (ListView) popupWindow_category.findViewById(R.id.minorCategoryListView);
        mainCategoryListView = (ListView) popupWindow_category.findViewById(R.id.mainCategoryListView);
        minorDistrictListView = (ListView) popupWindow_district.findViewById(R.id.minorDistrictListview);
        mainDistrictListView = (ListView) popupWindow_district.findViewById(R.id.mainDistrictListView);

        btnDistance = (TextView) popupWindow_sort.findViewById(R.id.btnDistance);
        btnEvaluate = (TextView) popupWindow_sort.findViewById(R.id.btnEvaluate);
        btnCollotion = (TextView) popupWindow_sort.findViewById(R.id.btnCollotion);
        btnPrice = (TextView) popupWindow_sort.findViewById(R.id.btnPrice);

        btnDistance.setOnClickListener(viewOnClickListener);
        btnEvaluate.setOnClickListener(viewOnClickListener);
        btnCollotion.setOnClickListener(viewOnClickListener);
        btnPrice.setOnClickListener(viewOnClickListener);
    }

    public void initWindow() {
        LayoutInflater layoutInflater = LayoutInflater.from(mActivity);
        initUI(layoutInflater);
        initDefaultValue();
        initCategoryAdapter();
        initMainStoreAdapter();

        //加入分類,屬性內容
        for (int k = 0; k < categoryMenuResults.size(); k++) {
            ArrayList<MenuObject> arrayListCategory = new ArrayList<>();

            mainCategoryArray.add(new MenuObject(categoryMenuResults.get(k).getSc_id(),
                    categoryMenuResults.get(k).getSc_name()));

            arrayListCategory.add(new MenuObject("0", mActivity.getString(R.string.sort_all_property)));

            //加入屬性
            for (int j = 0; j < categoryMenuResults.get(k).getScaResultArrayList().size(); j++) {
                arrayListCategory.add(new MenuObject(categoryMenuResults.get(k).getScaResultArrayList().get(j).getSca_id(),
                        categoryMenuResults.get(k).getScaResultArrayList().get(j).getSca_name()));
            }
            minorCategoryArray.add(arrayListCategory);
        }
        mainCategoryListView.setAdapter(mCategoryAdapter);

        //加入商圈,商場內容
        for (int k = 0; k < districtMenuResults.size(); k++) {
            ArrayList<MenuObject> arrayListDistrict = new ArrayList<>();

            mainDistrictArray.add(new MenuObject(districtMenuResults.get(k).getSd_id(),
                    districtMenuResults.get(k).getSd_name()));

            arrayListDistrict.add(new MenuObject("0", mActivity.getString(R.string.sort_all_market)));

            //加入商場
            for (int j = 0; j < districtMenuResults.get(k).getSmResultArrayList().size(); j++) {
                arrayListDistrict.add(new MenuObject(districtMenuResults.get(k).getSmResultArrayList().get(j).getSm_id(),
                        districtMenuResults.get(k).getSmResultArrayList().get(j).getSm_name()));
            }
            minorDistrictArray.add(arrayListDistrict);
        }
        mainDistrictListView.setAdapter(mDistrictAdapter);
    }

    /**
     * 分類,屬性
     */
    private void initCategoryAdapter() {
        mCategoryAdapter = new MenuStoreAdapter(mActivity, mainCategoryArray, new MenuStoreAdapter.ItemClick() {
            @Override
            public void itemclick(String itemid, final int i) {
                menuAdapter = new MenuAdapter(mActivity, i, minorCategoryArray, new MenuAdapter.ItemClick() {
                    @Override
                    public void itemclick(String itemid, int j) {
                        Message message = new Message();
                        message.what = StoreFragment.SORT_CATEGORY;
                        message.arg1 = Integer.valueOf(mainCategoryArray.get(i).getId());
                        message.arg2 = Integer.valueOf(itemid);
                        handler.sendMessage(message);
                        mainCategoryArray.get(i).getId();
                        StoreFragment.setTvCategory(minorCategoryArray.get(i).get(j).getTitile());
                    }
                });
                //切換背景色
                try {
                    for (int j = 0; j < mainCategoryArray.size(); j++) {
                        mainCategoryListView.getChildAt(j).setBackground(mActivity.getResources().getDrawable(R.color.background3));
                    }
                    mainCategoryListView.getChildAt(i).setBackground(mActivity.getResources().getDrawable(R.drawable.choose_left));
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                //更改文字
                StoreFragment.setTvCategory(mainCategoryArray.get(i).getTitile());
                //動態更新次要屬性內容
                minorCategoryListView.setAdapter(menuAdapter);
            }
        });
    }

    /**
     * 商圈,商場
     */
    private void initMainStoreAdapter() {
        mDistrictAdapter = new MenuStoreAdapter(mActivity, mainDistrictArray, new MenuStoreAdapter.ItemClick() {
            @Override
            public void itemclick(String itemid, final int i) {
                menuAdapter = new MenuAdapter(mActivity, i, minorDistrictArray, new MenuAdapter.ItemClick() {
                    @Override
                    public void itemclick(String itemid, int j) {
                        Message message = new Message();
                        message.what = StoreFragment.SORT_DISTRICT;
                        message.arg1 = Integer.valueOf(mainDistrictArray.get(i).getId());
                        message.arg2 = Integer.valueOf(itemid);
                        handler.sendMessage(message);
                        mainDistrictArray.get(i).getId();
                        StoreFragment.setTvCategoryDistrict(minorDistrictArray.get(i).get(j).getTitile());
                    }
                });
                //切換背景色
                try {
                    for (int j = 0; j < mainDistrictArray.size(); j++) {
                        mainDistrictListView.getChildAt(j).setBackground(mActivity.getResources().getDrawable(R.color.background3));
                    }
                    mainDistrictListView.getChildAt(i).setBackground(mActivity.getResources().getDrawable(R.drawable.choose_left));
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                //更改文字
                StoreFragment.setTvCategoryDistrict(mainDistrictArray.get(i).getTitile());
                //動態更新次要商場內容
                minorDistrictListView.setAdapter(menuAdapter);
            }
        });
    }

    private View.OnClickListener viewOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Message msg = new Message();
            switch (v.getId()) {
                case R.id.btnDistance:
                    msg.what = StoreFragment.SORTBY_TYPE;
                    msg.arg1 = Pub.SORT_DISTANCE;
                    StoreFragment.setTvSort(btnDistance.getText().toString());
                    handler.sendMessage(msg);
                    break;
                case R.id.btnEvaluate:
                    msg.what = StoreFragment.SORTBY_TYPE;
                    msg.arg1 = Pub.SORT_EVALUATE;
                    handler.sendMessage(msg);
                    StoreFragment.setTvSort(btnEvaluate.getText().toString());
                    break;
                case R.id.btnCollotion:
                    msg.what = StoreFragment.SORTBY_TYPE;
                    msg.arg1 = Pub.SORT_COLLATION;
                    handler.sendMessage(msg);
                    StoreFragment.setTvSort(btnCollotion.getText().toString());
                    break;
                case R.id.btnPrice:
                    msg.what = StoreFragment.SORTBY_TYPE;
                    msg.arg1 = Pub.SORT_PRICE;
                    handler.sendMessage(msg);
                    StoreFragment.setTvSort(btnPrice.getText().toString());
                    break;
            }
        }
    };

    /**
     * 關閉全部視窗
     */
    public static void closeAllPopupWindow() {
        if (mMainWindows != null && mMainWindows.isShowing()) {
            mMainWindows.dismiss();
        }
        if (mCenterWindows != null && mCenterWindows.isShowing()) {
            mCenterWindows.dismiss();
        }
        if (mRightWindows != null && mRightWindows.isShowing()) {
            mRightWindows.dismiss();
        }
        if (mFliterWindows != null && mFliterWindows.isShowing()) {
            mFliterWindows.dismiss();
        }
        if (statichandler != null)
            statichandler.sendEmptyMessage(StoreFragment.CLOSE_ALL);
    }

    /**
     * 顯示左邊分類視窗
     *
     * @param view
     */
    public void showLeftWindow(View view) {
        if (mCenterWindows.isShowing()) {
            mCenterWindows.dismiss();
            mMainWindows.showAsDropDown(view, 0, 2);
            return;
        }
        if (mRightWindows.isShowing()) {
            mRightWindows.dismiss();
            mMainWindows.showAsDropDown(view, 0, 2);
            return;
        }
        if (mMainWindows.isShowing()) {
            mMainWindows.dismiss();
            return;
        } else {
            mMainWindows.showAsDropDown(view, 0, 2);
        }
    }

    /**
     * 顯示中間商圈視窗
     *
     * @param view
     */
    public void showCenterWindow(View view) {
        if (mMainWindows.isShowing()) {
            mMainWindows.dismiss();
            mCenterWindows.showAsDropDown(view, 0, 2);
            return;
        }
        if (mRightWindows.isShowing()) {
            mRightWindows.dismiss();
            mCenterWindows.showAsDropDown(view, 0, 2);
            return;
        }
        if (mCenterWindows.isShowing()) {
            mCenterWindows.dismiss();
            return;
        } else {
            mCenterWindows.showAsDropDown(view, 0, 2);
        }
    }

    /**
     * 顯示右邊排序視窗
     *
     * @param view
     */
    public void showRightWindow(View view) {
        if (mMainWindows.isShowing()) {
            mMainWindows.dismiss();
            mRightWindows.showAsDropDown(view, 0, 2);
            return;
        }
        if (mCenterWindows.isShowing()) {
            mCenterWindows.dismiss();
            mRightWindows.showAsDropDown(view, 0, 2);
            return;
        }
        if (mRightWindows.isShowing()) {
            mRightWindows.dismiss();
            return;
        } else {
            mRightWindows.showAsDropDown(view, 0, 2);
        }
    }
}