package com.acloud.app.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.acloud.app.R;
import com.acloud.app.util.XpeceDrawableWrapper;


public class CustomProgressDialog {
    private Activity activity;
    public Dialog dialog;
    private View view;
    LayoutInflater inflater;
    RatingBar ratingBar; //評分星數

    public CustomProgressDialog(Activity c) {
        this.activity = c;
        inflater = c.getLayoutInflater();
        this.view = inflater.inflate(R.layout.custom_progress_dialog, null);
    }


    public Dialog getDialog() {
        return dialog;
    }

    public View findView(int id) {
        return getDialog().getWindow().findViewById(id);
    }

    public boolean isShowing() {
        return dialog.isShowing();
    }

    public void show() {
        dialog.show();
    }

    public void dismiss() {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
    }

    public RatingBar getRatingBar() {
        return ratingBar;
    }

    /**
     * 初始化ProgressDialog
     */
    public ProgressDialog initProgressDialog() {
        return new ProgressDialog(null, null);
    }

    /**
     * 初始化ProgressDialog
     *
     * @param negClick 取消按鈕點擊事件
     */
    public ProgressDialog initProgressDialog(View.OnClickListener negClick) {
        return new ProgressDialog(null, negClick);
    }

    /**
     * 初始化ProgressDialog
     *
     * @param posClick 確認按鈕點擊事件
     * @param negClick 取消按鈕點擊事件
     */
    public ProgressDialog initProgressDialog(View.OnClickListener posClick, View.OnClickListener negClick) {
        return new ProgressDialog(posClick, negClick);
    }

    public class ProgressDialog {
        private ProgressDialog(View.OnClickListener posClick, View.OnClickListener negClick) {
            dialog = new Dialog(activity, R.style.TranslucentThemeSelector) {
                @Override
                public void onCreate(Bundle savedInstanceState) {
                    super.onCreate(savedInstanceState);

                    DisplayMetrics dm = new DisplayMetrics();
                    activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
                    LayoutParams params = getWindow().getAttributes();
                    params.width = dm.widthPixels;
                    params.height = dm.heightPixels;
                    getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);

                }
            };

            dialog.setContentView(view);
            //是否可由實體按鍵關閉
            dialog.setCancelable(false);
            //標題icon
            ((ImageView) view.findViewById(R.id.ivIcon)).setVisibility(ImageView.GONE);

            //標題
            ((TextView) view.findViewById(R.id.tvTitle)).setVisibility(TextView.VISIBLE);
            ((TextView) view.findViewById(R.id.tvTitle)).setText(R.string.txt_authing_title);

            //提示說明文
            ((TextView) view.findViewById(R.id.tvMsg)).setVisibility(TextView.VISIBLE);
            ((TextView) view.findViewById(R.id.tvMsg)).setText(R.string.txt_authing_msg);

            //列表
            ((ListView) view.findViewById(R.id.list)).setVisibility(ListView.GONE);

            //progressBar
            ((ProgressBar) view.findViewById(R.id.progressBar)).setVisibility(ProgressBar.VISIBLE);

            //左按鍵
            ((Button) view.findViewById(R.id.btnCancel)).setVisibility(Button.VISIBLE);
            //((Button) layout.findViewById(R.id.btnCancel)).setText(negBtnStr);
            if (negClick != null) {
                ((Button) view.findViewById(R.id.btnCancel)).setOnClickListener(negClick);
            } else {
                ((Button) view.findViewById(R.id.btnCancel)).setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        }
                );
            }

            //右按鍵
            ((Button) view.findViewById(R.id.btnAccept)).setVisibility(Button.GONE);
            //((Button) view.findViewById(R.id.btnAccept)).setText(posBtnStr);
            if (posClick != null) {
                ((Button) view.findViewById(R.id.btnCancel)).setOnClickListener(posClick);
            } else {
                ((Button) view.findViewById(R.id.btnAccept)).setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        }
                );
            }
        }
    }

}
