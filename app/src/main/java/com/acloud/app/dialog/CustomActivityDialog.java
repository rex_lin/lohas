package com.acloud.app.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.acloud.app.R;
import com.acloud.app.adapter.CustomPageAdapter;
import com.acloud.app.tool.TLog;
import com.acloud.app.util.ActivityListManager;
import com.acloud.app.util.BitmapHandler;

import java.util.ArrayList;

/**
 * 活動廣告頁
 */
public class CustomActivityDialog {
    private Activity activity;
    public Dialog dialog;
    private View view;
    LayoutInflater inflater;
    RelativeLayout imgMarquee;
    CustomPageAdapter customPageAdapter;
    LinearLayout points;

    public CustomActivityDialog(Activity c) {
        this.activity = c;
        inflater = c.getLayoutInflater();
        this.view = inflater.inflate(R.layout.custom_activity_dialog, null);

    }


    public Dialog getDialog() {
        return dialog;
    }

    public View findView(int id) {
        return getDialog().getWindow().findViewById(id);
    }

    public boolean isShowing() {
        return dialog.isShowing();
    }

    public void show() {
        dialog.show();
    }

    public void dismiss() {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
    }


    /**
     * 初始化ActivityDialog
     */
    public ActivityDialog initActivityDialog() {
        return new ActivityDialog();
    }

    public class ActivityDialog {
        private ActivityDialog() {
            dialog = new Dialog(activity, R.style.TranslucentThemeSelector) {
                @Override
                public void onCreate(Bundle savedInstanceState) {
                    super.onCreate(savedInstanceState);
                    DisplayMetrics dm = new DisplayMetrics();
                    activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
                    LayoutParams params = getWindow().getAttributes();
                    params.width = dm.widthPixels;
                    params.height = dm.heightPixels - getStatusBarHeight();
                    TLog.i("TAG", " params.height" + params.height + " ; " + getStatusBarHeight());
                    getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);

                }
            };
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(view);
            //是否可由實體按鍵關閉
            dialog.setCancelable(true);

            ArrayList<String> date = new ArrayList<>();
            ArrayList<String> imageUrl = new ArrayList<>();
            ArrayList<String> content = new ArrayList<>();
            ArrayList<String> title = new ArrayList<>();
            for (int j = 0; j < ActivityListManager.getResultsBannerArrayList().size(); j++) {
                date.add(ActivityListManager.getResultsBannerArrayList().get(j).getStart_datetime() + "~" +
                        ActivityListManager.getResultsBannerArrayList().get(j).getEnd_datetime());
                imageUrl.add(ActivityListManager.getResultsBannerArrayList().get(j).getImage_url());
                content.add(ActivityListManager.getResultsBannerArrayList().get(j).getContent());
                title.add(ActivityListManager.getResultsBannerArrayList().get(j).getTitile());
            }
            customPageAdapter = new CustomPageAdapter(activity, date, title, content, imageUrl);
            ((ViewPager) view.findViewById(R.id.viewpager)).setAdapter(customPageAdapter);
            points = (LinearLayout) view.findViewById(R.id.points);
            imgMarquee = (RelativeLayout) view.findViewById(R.id.Marquee);


            ((ViewPager) view.findViewById(R.id.viewpager)).setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    updatePointAndText();
                }

                @Override
                public void onPageSelected(int position) {

                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
            initPoints();

            ImageButton imgBtnCancel = (ImageButton) view.findViewById(R.id.imgBtnCancel);
            imgBtnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
        }
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = activity.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = activity.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    private void initPoints() {
        for (int i = 0; i < ActivityListManager.getResultsBannerArrayList().size(); i++) {
            View view = new View(activity);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams((int) BitmapHandler.convertDpToPixel(5, activity),
                    (int) BitmapHandler.convertDpToPixel(5, activity));
            if (i != 0) {
                params.leftMargin = 15;
            }
            view.setLayoutParams(params);
            view.setBackgroundResource(R.drawable.selector_points_pink);
            points.addView(view);
        }
    }

    private void updatePointAndText() {
        int currentItem = ((ViewPager) view.findViewById(R.id.viewpager)).getCurrentItem() % ActivityListManager.getResultsBannerArrayList().size();
        TLog.i("Ernest", "imgGallery currentItem : " + currentItem);
        for (int i = 0; i < points.getChildCount(); i++) {
            points.getChildAt(i).setEnabled(currentItem == i);
        }
    }
}
