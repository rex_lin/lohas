package com.acloud.app.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.acloud.app.R;


public class CustomDialogOrange {
    private Activity activity;
    public Dialog dialog;
    private View view;
    LayoutInflater inflater;
    String msg;
    public CustomDialogOrange(Activity c) {
        this.activity = c;
        inflater = c.getLayoutInflater();
        this.view = inflater.inflate(R.layout.custom_dialog_orange, null);
    }
    public CustomDialogOrange(Activity c, String msg) {
        this.activity = c;
        inflater = c.getLayoutInflater();
        this.view = inflater.inflate(R.layout.custom_dialog_orange, null);
        this.msg = msg;
    }



    public Dialog getDialog() {
        return dialog;
    }

    public View findView(int id) {
        return getDialog().getWindow().findViewById(id);
    }

    public boolean isShowing() {
        return dialog.isShowing();
    }

    public void show() {
        dialog.show();
    }

    public void dismiss() {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
    }


    /**
     * 初始化ProgressDialog
     */
    public ProgressDialog initProgressDialog() {
        return new ProgressDialog(null, null);
    }

    /**
     * 初始化ProgressDialog
     *
     * @param posClick 確定點擊事件
     */
    public ProgressDialog initProgressDialog(View.OnClickListener posClick) {
        return new ProgressDialog(posClick, null);
    }

    /**
     * 初始化ProgressDialog
     *
     * @param posClick 確認按鈕點擊事件
     * @param negClick 取消按鈕點擊事件
     */
    public ProgressDialog initProgressDialog(View.OnClickListener posClick, View.OnClickListener negClick) {
        return new ProgressDialog(posClick, negClick);
    }

    public class ProgressDialog {
        private ProgressDialog(View.OnClickListener posClick, View.OnClickListener negClick) {
            dialog = new Dialog(activity, R.style.TranslucentThemeSelector) {
                @Override
                public void onCreate(Bundle savedInstanceState) {
                    super.onCreate(savedInstanceState);

                    DisplayMetrics dm = new DisplayMetrics();
                    activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
                    LayoutParams params = getWindow().getAttributes();
                    params.width = dm.widthPixels;
                    params.height = dm.heightPixels;
                    getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);

                }
            };

            dialog.setContentView(view);
            //是否可由實體按鍵關閉
            dialog.setCancelable(false);
            //標題icon
            ((ImageView) view.findViewById(R.id.ivIcon)).setVisibility(ImageView.GONE);

            //標題
            ((TextView) view.findViewById(R.id.tvTitle)).setVisibility(TextView.VISIBLE);
            ((TextView) view.findViewById(R.id.tvTitle)).setText(R.string.txt_exchange_coupon_title);

            //提示說明文
            ((TextView) view.findViewById(R.id.tvMsg)).setVisibility(TextView.VISIBLE);
            ((TextView) view.findViewById(R.id.tvMsg)).setText(activity.getResources().getString(R.string.txt_point_notenough));

            //列表
            ((ListView) view.findViewById(R.id.list)).setVisibility(ListView.GONE);

            //progressBar
            ((ProgressBar) view.findViewById(R.id.progressBar)).setVisibility(ProgressBar.GONE);

            //左按鍵
            ((Button) view.findViewById(R.id.btnCancel)).setVisibility(Button.GONE);
//            ((Button) layout.findViewById(R.id.btnCancel)).setText(negBtnStr);
            if (negClick != null) {
                ((Button) view.findViewById(R.id.btnCancel)).setOnClickListener(negClick);
            } else {
                ((Button) view.findViewById(R.id.btnAccept)).setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        }
                );
            }

            //右按鍵
            ((Button) view.findViewById(R.id.btnAccept)).setVisibility(Button.VISIBLE);
            //((Button) view.findViewById(R.id.btnAccept)).setText(posBtnStr);
            if (posClick != null) {
                ((Button) view.findViewById(R.id.btnAccept)).setOnClickListener(posClick);
            } else {
                ((Button) view.findViewById(R.id.btnAccept)).setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        }
                );
            }
        }
    }

    /**
     * 掃描失敗
     * @param posClick 確認點擊按鈕
     * @param negClick 取消點擊按鈕
     * @return
     */
    public ScanErrorDialog initScanErrorDialog(View.OnClickListener posClick, View.OnClickListener negClick) {
        return new ScanErrorDialog(posClick, negClick);
    }

    public class ScanErrorDialog {
        private ScanErrorDialog(View.OnClickListener posClick, View.OnClickListener negClick) {
            dialog = new Dialog(activity, R.style.TranslucentThemeSelector) {
                @Override
                public void onCreate(Bundle savedInstanceState) {
                    super.onCreate(savedInstanceState);

                    DisplayMetrics dm = new DisplayMetrics();
                    activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
                    LayoutParams params = getWindow().getAttributes();
                    params.width = dm.widthPixels;
                    params.height = dm.heightPixels;
                    getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);

                }
            };

            dialog.setContentView(view);
            //是否可由實體按鍵關閉
            dialog.setCancelable(false);
            //標題icon
            ((ImageView) view.findViewById(R.id.ivIcon)).setVisibility(ImageView.GONE);

            //標題
            ((TextView) view.findViewById(R.id.tvTitle)).setVisibility(TextView.VISIBLE);
            ((TextView) view.findViewById(R.id.tvTitle)).setText(R.string.title_scan_fail);

            //提示說明文
            ((TextView) view.findViewById(R.id.tvMsg)).setVisibility(TextView.VISIBLE);
            ((TextView) view.findViewById(R.id.tvMsg)).setText(activity.getResources().getString(R.string.txt_please_restart_sacn));

            //列表
            ((ListView) view.findViewById(R.id.list)).setVisibility(ListView.GONE);

            //progressBar
            ((ProgressBar) view.findViewById(R.id.progressBar)).setVisibility(ProgressBar.GONE);

            //左按鍵
            ((Button) view.findViewById(R.id.btnCancel)).setVisibility(Button.GONE);
//            ((Button) layout.findViewById(R.id.btnCancel)).setText(negBtnStr);
            if (negClick != null) {
                ((Button) view.findViewById(R.id.btnCancel)).setOnClickListener(negClick);
            } else {
                ((Button) view.findViewById(R.id.btnAccept)).setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        }
                );
            }

            //右按鍵
            ((Button) view.findViewById(R.id.btnAccept)).setVisibility(Button.VISIBLE);
            //((Button) view.findViewById(R.id.btnAccept)).setText(posBtnStr);
            if (posClick != null) {
                ((Button) view.findViewById(R.id.btnAccept)).setOnClickListener(posClick);
            } else {
                ((Button) view.findViewById(R.id.btnAccept)).setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        }
                );
            }
        }
    }

    /**
     * 初始化ErrorDialog
     * @return
     */
    public ErrorDialog initErrorDialog() {
        return new ErrorDialog(null, null);
    }

    /***
     * 初始化ErrorDailog
     * @param posClick 確認點擊按鈕
     * @param negClick 取消點擊按鈕
     * @return
     */
    public ErrorDialog initErrorDialog(View.OnClickListener posClick, View.OnClickListener negClick) {
        return new ErrorDialog(posClick, negClick);
    }

    public class ErrorDialog {
        private ErrorDialog(View.OnClickListener posClick, View.OnClickListener negClick) {
            dialog = new Dialog(activity, R.style.TranslucentThemeSelector) {
                @Override
                public void onCreate(Bundle savedInstanceState) {
                    super.onCreate(savedInstanceState);

                    DisplayMetrics dm = new DisplayMetrics();
                    activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
                    LayoutParams params = getWindow().getAttributes();
                    params.width = dm.widthPixels;
                    params.height = dm.heightPixels;
                    getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);

                }
            };

            dialog.setContentView(view);
            //是否可由實體按鍵關閉
            dialog.setCancelable(false);
            //標題icon
            ((ImageView) view.findViewById(R.id.ivIcon)).setVisibility(ImageView.GONE);

            //標題
            ((TextView) view.findViewById(R.id.tvTitle)).setVisibility(TextView.VISIBLE);
            ((TextView) view.findViewById(R.id.tvTitle)).setText(R.string.txt_fail);

            //提示說明文
            ((TextView) view.findViewById(R.id.tvMsg)).setVisibility(TextView.VISIBLE);
            ((TextView) view.findViewById(R.id.tvMsg)).setText(msg);

            //列表
            ((ListView) view.findViewById(R.id.list)).setVisibility(ListView.GONE);

            //progressBar
            ((ProgressBar) view.findViewById(R.id.progressBar)).setVisibility(ProgressBar.GONE);

            //左按鍵
            ((Button) view.findViewById(R.id.btnCancel)).setVisibility(Button.GONE);
//            ((Button) layout.findViewById(R.id.btnCancel)).setText(negBtnStr);
            if (negClick != null) {
                ((Button) view.findViewById(R.id.btnCancel)).setOnClickListener(negClick);
            } else {
                ((Button) view.findViewById(R.id.btnAccept)).setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        }
                );
            }

            //右按鍵
            ((Button) view.findViewById(R.id.btnAccept)).setVisibility(Button.VISIBLE);
            //((Button) view.findViewById(R.id.btnAccept)).setText(posBtnStr);
            if (posClick != null) {
                ((Button) view.findViewById(R.id.btnAccept)).setOnClickListener(posClick);
            } else {
                ((Button) view.findViewById(R.id.btnAccept)).setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        }
                );
            }
        }
    }

    /**
     * 初始化ErrorDialog
     * @return
     */
    public ErrorDialog initLeaveDialog() {
        return new ErrorDialog(null, null);
    }

    /***
     * 初始化ErrorDailog
     * @param posClick 確認點擊按鈕
     * @param negClick 取消點擊按鈕
     * @return
     */
    public LeaveDialog initLeaveDialog(View.OnClickListener posClick, View.OnClickListener negClick) {
        return new LeaveDialog(posClick, negClick);
    }

    public class LeaveDialog {
        private LeaveDialog(View.OnClickListener posClick, View.OnClickListener negClick) {
            dialog = new Dialog(activity, R.style.TranslucentThemeSelector) {
                @Override
                public void onCreate(Bundle savedInstanceState) {
                    super.onCreate(savedInstanceState);

                    DisplayMetrics dm = new DisplayMetrics();
                    activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
                    LayoutParams params = getWindow().getAttributes();
                    params.width = dm.widthPixels;
                    params.height = dm.heightPixels;
                    getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);

                }
            };

            dialog.setContentView(view);
            //是否可由實體按鍵關閉
            dialog.setCancelable(false);
            //標題icon
            ((ImageView) view.findViewById(R.id.ivIcon)).setVisibility(ImageView.GONE);

            //標題
            ((TextView) view.findViewById(R.id.tvTitle)).setVisibility(TextView.GONE);
            ((TextView) view.findViewById(R.id.tvTitle)).setText("");
            ((RelativeLayout)view.findViewById(R.id.rlTitle)).setVisibility(View.GONE);
            ((LinearLayout)view.findViewById(R.id.ll_title)).setVisibility(View.GONE);

            //提示說明文
            ((TextView) view.findViewById(R.id.tvMsg)).setVisibility(TextView.VISIBLE);
            ((TextView) view.findViewById(R.id.tvMsg)).setText(msg);

            //列表
            ((ListView) view.findViewById(R.id.list)).setVisibility(ListView.GONE);

            //progressBar
            ((ProgressBar) view.findViewById(R.id.progressBar)).setVisibility(ProgressBar.GONE);

            //左按鍵
            ((Button) view.findViewById(R.id.btnCancel)).setVisibility(Button.VISIBLE);
//            ((Button) layout.findViewById(R.id.btnCancel)).setText(negBtnStr);
            if (negClick != null) {
                ((Button) view.findViewById(R.id.btnCancel)).setOnClickListener(negClick);
            } else {
                ((Button) view.findViewById(R.id.btnAccept)).setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        }
                );
            }

            //右按鍵
            ((Button) view.findViewById(R.id.btnAccept)).setVisibility(Button.VISIBLE);
            //((Button) view.findViewById(R.id.btnAccept)).setText(posBtnStr);
            if (posClick != null) {
                ((Button) view.findViewById(R.id.btnAccept)).setOnClickListener(posClick);
            } else {
                ((Button) view.findViewById(R.id.btnAccept)).setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        }
                );
            }
        }
    }
}
