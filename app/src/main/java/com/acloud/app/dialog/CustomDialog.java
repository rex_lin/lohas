package com.acloud.app.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.acloud.app.R;
import com.acloud.app.util.XpeceDrawableWrapper;


public class CustomDialog {
    private Activity activity;
    public Dialog dialog;
    private View view;
    LayoutInflater inflater;
    RatingBar ratingBar; //評分星數

    public CustomDialog(Activity c) {
        this.activity = c;
        inflater = c.getLayoutInflater();
        this.view = inflater.inflate(R.layout.custom_dialog, null);
    }


    public Dialog getDialog() {
        return dialog;
    }

    public View findView(int id) {
        return getDialog().getWindow().findViewById(id);
    }

    public boolean isShowing() {
        return dialog.isShowing();
    }

    public void show() {
        dialog.show();
    }

    public void dismiss() {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
    }

    public RatingBar getRatingBar() {
        return ratingBar;
    }


    /**
     * 初始化移除收藏Dialog
     *
     * @param posClick 確認按鈕點擊事件
     * @param negClick 取消按鈕點擊事件
     */
    public RemoveFevDialog initRemoveFevDialog(View.OnClickListener posClick, View.OnClickListener negClick) {
        return new RemoveFevDialog(posClick, negClick);
    }

    public class RemoveFevDialog {
        private RemoveFevDialog(View.OnClickListener posClick, View.OnClickListener negClick) {
            dialog = new Dialog(activity, R.style.TranslucentThemeSelector) {
                @Override
                public void onCreate(Bundle savedInstanceState) {
                    super.onCreate(savedInstanceState);

                    DisplayMetrics dm = new DisplayMetrics();
                    activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
                    LayoutParams params = getWindow().getAttributes();
                    params.width = dm.widthPixels;
                    params.height = dm.heightPixels;
                    getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);

                }
            };

            dialog.setContentView(view);
            //是否可由實體按鍵關閉
            dialog.setCancelable(true);
            //標題icon
            ((ImageView) view.findViewById(R.id.ivIcon)).setVisibility(ImageView.GONE);

            //標題
            ((TextView) view.findViewById(R.id.tvTitle)).setVisibility(TextView.GONE);
            ((TextView) view.findViewById(R.id.tvTitle)).setText(R.string.txt_remove_fev_title);

            //提示說明文
            ((TextView) view.findViewById(R.id.tvMsg)).setVisibility(TextView.VISIBLE);
            ((TextView) view.findViewById(R.id.tvMsg)).setText(R.string.txt_remove_fev_hint);

            //列表
            ((ListView) view.findViewById(R.id.list)).setVisibility(ListView.GONE);

            //progressBar
            ((ProgressBar) view.findViewById(R.id.progressBar)).setVisibility(ProgressBar.GONE);

            //左按鍵
            ((Button) view.findViewById(R.id.btnCancel)).setVisibility(Button.VISIBLE);
            //((Button) layout.findViewById(R.id.btnCancel)).setText(negBtnStr);
            if (negClick != null) {
                ((Button) view.findViewById(R.id.btnCancel)).setOnClickListener(negClick);
            } else {
                ((Button) view.findViewById(R.id.btnCancel)).setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        }
                );
            }

            //右按鍵
            ((Button) view.findViewById(R.id.btnAccept)).setVisibility(Button.VISIBLE);
            //((Button) view.findViewById(R.id.btnAccept)).setText(posBtnStr);
            if (posClick != null) {
                ((Button) view.findViewById(R.id.btnAccept)).setOnClickListener(posClick);
            } else {
                ((Button) view.findViewById(R.id.btnAccept)).setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        }
                );
            }
        }
    }

    /**
     * 初始化評分星數Dialog
     *
     * @param saveClick 儲存按鈕點擊事件
     */
    public RatingStarDialog initRatingStarDialog(View.OnClickListener saveClick) {
        return new RatingStarDialog(saveClick);
    }

    public class RatingStarDialog {

        private RatingStarDialog(View.OnClickListener saveClick) {
            dialog = new Dialog(activity, R.style.TranslucentThemeSelector) {
                @Override
                public void onCreate(Bundle savedInstanceState) {
                    super.onCreate(savedInstanceState);

                    DisplayMetrics dm = new DisplayMetrics();
                    activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
                    LayoutParams params = getWindow().getAttributes();
                    params.width = dm.widthPixels;
                    params.height = dm.heightPixels;
                    getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);

                }
            };

            dialog.setContentView(view);
            //是否可由實體按鍵關閉
            dialog.setCancelable(true);
            //標題icon
            ((ImageView) view.findViewById(R.id.ivIcon)).setVisibility(ImageView.GONE);

            //標題
            ((TextView) view.findViewById(R.id.tvTitle)).setVisibility(TextView.GONE);
            ((TextView) view.findViewById(R.id.tvTitle)).setText(R.string.txt_remove_fev_title);

            //提示說明文
            ((TextView) view.findViewById(R.id.tvMsg)).setVisibility(TextView.GONE);
            ((TextView) view.findViewById(R.id.tvMsg)).setText(R.string.txt_hint_appra);

            //列表
            ((ListView) view.findViewById(R.id.list)).setVisibility(ListView.GONE);

            //progressBar
            ((ProgressBar) view.findViewById(R.id.progressBar)).setVisibility(ProgressBar.GONE);

            ((RelativeLayout) view.findViewById(R.id.box)).setVisibility(RelativeLayout.VISIBLE);

            View appraView = inflater.inflate(R.layout.inc_appra, null);
            ratingBar = ((RatingBar) appraView.findViewById(R.id.ratingBar));
            ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                @Override
                public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

                }
            });

            try {
                LayerDrawable stars = (LayerDrawable) XpeceDrawableWrapper.getDrawable(ratingBar.getProgressDrawable());
                stars.getDrawable(2).setColorFilter(Color.parseColor("#F93232"), PorterDuff.Mode.SRC_ATOP); // for filled stars
                stars.getDrawable(1).setColorFilter(Color.parseColor("#F93232"), PorterDuff.Mode.SRC_ATOP); // for half filled stars
                stars.getDrawable(0).setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY); // for empty stars
                ratingBar.setProgressDrawable(stars);
            } catch (ClassCastException e) {
                e.printStackTrace();
            }


            ((Button) appraView.findViewById(R.id.btnSave)).setOnClickListener(saveClick);

            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
            ((RelativeLayout) view.findViewById(R.id.box)).addView(appraView, lp);


            //左按鍵
            ((Button) view.findViewById(R.id.btnCancel)).setVisibility(Button.GONE);

            //右按鍵
            ((Button) view.findViewById(R.id.btnAccept)).setVisibility(Button.GONE);

        }
    }


    /**
     * 獲得點數Dialog
     *
     *
     */
    public GetPointDialog initGetPointDialog() {
        return new GetPointDialog();
    }

    public class GetPointDialog {

        private GetPointDialog() {
            dialog = new Dialog(activity, R.style.TranslucentThemeSelector) {
                @Override
                public void onCreate(Bundle savedInstanceState) {
                    super.onCreate(savedInstanceState);

                    DisplayMetrics dm = new DisplayMetrics();
                    activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
                    LayoutParams params = getWindow().getAttributes();
                    params.width = dm.widthPixels;
                    params.height = dm.heightPixels;
                    getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);

                }
            };

            dialog.setContentView(view);
            //是否可由實體按鍵關閉
            dialog.setCancelable(true);
            //標題icon
            ((ImageView) view.findViewById(R.id.ivIcon)).setVisibility(ImageView.GONE);
            ((LinearLayout) view.findViewById(R.id.top)).setVisibility(View.GONE);
            ((RelativeLayout) view.findViewById(R.id.title_background)).setBackgroundColor(Color.parseColor("#5489C0"));

            //標題
            ((TextView) view.findViewById(R.id.tvTitle)).setVisibility(TextView.VISIBLE);
            ((TextView) view.findViewById(R.id.tvTitle)).setText(R.string.txt_complete_get_point);

            //提示說明文
            ((TextView) view.findViewById(R.id.tvMsg)).setVisibility(TextView.VISIBLE);
            ((TextView) view.findViewById(R.id.tvMsg)).setText(R.string.txt_activity_100_bonus);

            //列表
            ((ListView) view.findViewById(R.id.list)).setVisibility(ListView.GONE);

            //progressBar
            ((ProgressBar) view.findViewById(R.id.progressBar)).setVisibility(ProgressBar.GONE);

            ((RelativeLayout) view.findViewById(R.id.box)).setVisibility(RelativeLayout.VISIBLE);

            View appraView = inflater.inflate(R.layout.inc_appra, null);

            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
            ((RelativeLayout) view.findViewById(R.id.box)).addView(appraView, lp);

            //左按鍵
            ((Button) view.findViewById(R.id.btnCancel)).setVisibility(Button.GONE);

            //右按鍵
            ((Button) view.findViewById(R.id.btnAccept)).setVisibility(Button.GONE);
            ((Button) view.findViewById(R.id.btnOneAccept)).setVisibility(Button.VISIBLE);
            ((Button) view.findViewById(R.id.btnOneAccept)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });

        }
    }

}
