package com.acloud.app.dialog;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.acloud.app.R;
import com.acloud.app.tool.Pub;


/**
 * Created by skywind-10 on 2015/7/20.
 */
public class CustomPopupActivityWindow {

    static PopupWindow
            mLeftWindows,
            mRightWindows;

    private int mScreenWidth;
    private int mScreenHeight;
    Activity mActivity;
    int check = 0;
    Handler handler1;

    public CustomPopupActivityWindow(Activity activity, Handler handler) {
        handler1 = handler;
        mActivity = activity;
    }


    public void initWindow() {
        LayoutInflater layoutInflater = LayoutInflater.from(mActivity);
//
        View popupWindow_sort_activity = layoutInflater.inflate(R.layout.window_filter_sort_activity, null);
        View popupWindow_category_activity = layoutInflater.inflate(R.layout.window_filter_category_activity, null);
        mScreenWidth = mActivity.getWindowManager().getDefaultDisplay().getWidth();
        mScreenHeight = mActivity.getWindowManager().getDefaultDisplay().getHeight();
        //視窗長寬高
        mLeftWindows = new PopupWindow(popupWindow_sort_activity, mScreenWidth, mScreenHeight);
//
        mRightWindows = new PopupWindow(popupWindow_category_activity, mScreenWidth, mScreenHeight);

        TextView btnPunch = (TextView) popupWindow_sort_activity.findViewById(R.id.btnPuch);
        TextView btnViewStore = (TextView) popupWindow_sort_activity.findViewById(R.id.btnViewStore);
        TextView btnQRcode = (TextView) popupWindow_sort_activity.findViewById(R.id.btnQRcode);
        TextView btnARgame = (TextView) popupWindow_sort_activity.findViewById(R.id.btnARgame);
        TextView btnALL = (TextView) popupWindow_sort_activity.findViewById(R.id.btnALL);
        TextView btnDate = (TextView) popupWindow_category_activity.findViewById(R.id.btnDateSort);
        TextView btnDistance = (TextView) popupWindow_category_activity.findViewById(R.id.btnDistanceSort);
        TextView btnBonus = (TextView) popupWindow_category_activity.findViewById(R.id.btnBonusSort);

        btnPunch.setOnClickListener(viewOnClickListener);
        btnViewStore.setOnClickListener(viewOnClickListener);
        btnQRcode.setOnClickListener(viewOnClickListener);
        btnARgame.setOnClickListener(viewOnClickListener);
        btnALL.setOnClickListener(viewOnClickListener);
        btnDate.setOnClickListener(viewOnClickListener);
        btnDistance.setOnClickListener(viewOnClickListener);
        btnBonus.setOnClickListener(viewOnClickListener);
    }

    private View.OnClickListener viewOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Message msg = new Message();
            switch (v.getId()) {
                case R.id.btnPuch:
                    msg.what = Pub.SEND_CATEGORY;
                    msg.arg1 = Pub.CATEGORY_PUNCH;
                    msg.obj = mActivity.getString(R.string.btn_punch_activity);
                    handler1.sendMessage(msg);
                    break;
                case R.id.btnViewStore:
                    msg.what = Pub.SEND_CATEGORY;
                    msg.arg1 = Pub.CATEGORY_VIEWSTORE;
                    msg.obj = mActivity.getString(R.string.txt_look_storeinfo);
                    handler1.sendMessage(msg);
                    break;
                case R.id.btnQRcode:
                    msg.what = Pub.SEND_CATEGORY;
                    msg.arg1 = Pub.CATEGORY_QRCODE;
                    msg.obj = mActivity.getString(R.string.txt_scan_qrcode);
                    handler1.sendMessage(msg);
                    break;
                case R.id.btnARgame:
                    msg.what = Pub.SEND_CATEGORY;
                    msg.arg1 = Pub.CATEGORY_ARGAME;
                    msg.obj = mActivity.getString(R.string.txt_ar_game);
                    handler1.sendMessage(msg);
                    break;
                case R.id.btnALL:
                    msg.what = Pub.SEND_CATEGORY;
                    msg.arg1 = Pub.CATEGORY_ALL;
                    msg.obj = mActivity.getString(R.string.btn_all_activity);
                    handler1.sendMessage(msg);
                    break;
                case R.id.btnDateSort:
                    msg.what = Pub.SEND_SORT;
                    msg.arg1 = Pub.SORT_ACTIVITY_DATE;
                    msg.obj = mActivity.getString(R.string.btn_new_activity);
                    handler1.sendMessage(msg);
                    break;
                case R.id.btnDistanceSort:
                    msg.what = Pub.SEND_SORT;
                    msg.arg1 = Pub.SORT_ACTIVITY_DISTANCE;
                    msg.obj = mActivity.getString(R.string.btn_distance_close);
                    handler1.sendMessage(msg);
                    break;
                case R.id.btnBonusSort:
                    msg.what = Pub.SEND_SORT;
                    msg.arg1 = Pub.SORT_ACTIVITY_BONUS;
                    msg.obj = mActivity.getString(R.string.btn_point_much);
                    handler1.sendMessage(msg);
                    break;
            }
        }
    };


    public static void closeAllPopupWindow() {
        if (mRightWindows != null && mRightWindows.isShowing()) {
            mRightWindows.dismiss();
        }
        if (mLeftWindows != null && mLeftWindows.isShowing()) {
            mLeftWindows.dismiss();
        }
    }

    /**
     * btnCategory
     *
     * @param view
     * @param change
     */
    public void showRightWindow2(View view, int change) {
        if (check == 0) {
            check = change;
        }
        if (change != check) {
            check = change;
        }
        if (mRightWindows.isShowing()) {
            mRightWindows.dismiss();
            mLeftWindows.showAsDropDown(view, 0, 2);
            return;
        }
        if (mLeftWindows.isShowing()) {
            mLeftWindows.dismiss();
            return;
        } else {
            mLeftWindows.showAsDropDown(view, 0, 2);
        }
    }

    /**
     * btnSort
     *
     * @param view
     * @param change
     */
    public void showRightWindow3(View view, int change) {
        if (check == 0) {
            check = change;
        }
        if (change != check) {
            check = change;
        }
        if (mLeftWindows.isShowing()) {
            mLeftWindows.dismiss();
            mRightWindows.showAsDropDown(view, 0, 2);
            return;
        }
        if (mRightWindows.isShowing()) {
            mRightWindows.dismiss();
            return;
        } else {
            mRightWindows.showAsDropDown(view, 0, 2);
        }
    }


}