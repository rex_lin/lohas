package com.acloud.app.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.acloud.app.R;


public class CustomNetworkDialog {
    private Activity activity;
    public Dialog dialog;
    private View view;
    LayoutInflater inflater;
    String msg;
    public CustomNetworkDialog(Activity c) {
        this.activity = c;
        inflater = c.getLayoutInflater();
        this.view = inflater.inflate(R.layout.custom_network_dialog, null);
    }
    public CustomNetworkDialog(Activity c, String msg) {
        this.activity = c;
        inflater = c.getLayoutInflater();
        this.view = inflater.inflate(R.layout.custom_network_dialog, null);
        this.msg = msg;
    }



    public Dialog getDialog() {
        return dialog;
    }

    public View findView(int id) {
        return getDialog().getWindow().findViewById(id);
    }

    public boolean isShowing() {
        return dialog.isShowing();
    }

    public void show() {
        dialog.show();
    }

    public void dismiss() {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
    }

    /**
     * 網路連線有問題
     *
     * @param posClick
     * @param negClick
     * @return
     */
    public NetworkDialog initNetworkDialog(View.OnClickListener posClick, View.OnClickListener negClick) {
        return new NetworkDialog(posClick, negClick);
    }

    public class NetworkDialog {
        public NetworkDialog(View.OnClickListener posClick, View.OnClickListener negClick) {
            dialog = new Dialog(activity, R.style.TranslucentThemeSelector) {
                @Override
                public void onCreate(Bundle savedInstanceState) {
                    super.onCreate(savedInstanceState);

                    DisplayMetrics dm = new DisplayMetrics();
                    activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
                    LayoutParams params = getWindow().getAttributes();
                    params.width = dm.widthPixels;
                    params.height = dm.heightPixels;
                    getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);

                }
            };

            dialog.setContentView(view);
            //是否可由實體按鍵關閉
            dialog.setCancelable(false);

            //提示說明文
            ((TextView) view.findViewById(R.id.tvMsg)).setVisibility(TextView.VISIBLE);
            ((TextView) view.findViewById(R.id.tvMsg)).setText(R.string.txt_network_error_msg);

            //列表
            ((ListView) view.findViewById(R.id.list)).setVisibility(ListView.GONE);

            //progressBar
            ((ProgressBar) view.findViewById(R.id.progressBar)).setVisibility(ProgressBar.GONE);


            //按鍵的上層LinearLayout

            LinearLayout llButton = ((LinearLayout) view.findViewById(R.id.ll_Button));
            llButton.setGravity(Gravity.CENTER);

            //左按鍵

            ((Button) view.findViewById(R.id.btnCancel)).setVisibility(Button.VISIBLE);

            Button btnLeft = (Button) view.findViewById(R.id.btnCancel);

            //((Button)view.findViewById(R.id.btnCancel)).setWidth(724);
            //((Button)view.findViewById(R.id.btnCancel)).setHeight(160);


            Drawable img = activity.getResources().getDrawable(R.drawable.ic_refresh);
            img.setBounds(180, 0, 240, 60);
            btnLeft.setCompoundDrawables(img, null, null, null);

            /*
            int layout_height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 160/3, view.getResources().getDisplayMetrics());
            int layout_width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 724/3, view.getResources().getDisplayMetrics());

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(layout_width, layout_height);
            layoutParams.gravity = Gravity.CENTER;

            ((Button) view.findViewById(R.id.btnCancel)).setLayoutParams(layoutParams);
            */
            /*
            try {
                Drawable leftDrawable;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    leftDrawable = view.getResources().getDrawable(R.drawable.ic_refresh, activity.getTheme());
                } else {
                    leftDrawable = view.getResources().getDrawable(R.drawable.ic_refresh);
                }

                ((Button) view.findViewById(R.id.btnCancel)).setCompoundDrawables(leftDrawable, null, null, null);
            }catch(Exception ex){
                ex.printStackTrace();
            }
            */

            if (negClick != null) {
                ((Button) view.findViewById(R.id.btnCancel)).setOnClickListener(negClick);
            } else {
                ((Button) view.findViewById(R.id.btnCancel)).setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        }
                );
            }

        }
    }

}
