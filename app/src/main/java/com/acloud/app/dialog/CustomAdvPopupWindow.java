package com.acloud.app.dialog;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.acloud.app.R;
import com.acloud.app.adapter.MenuAdapter;
import com.acloud.app.adapter.MenuStoreAdapter;
import com.acloud.app.model.MenuObject;
import com.acloud.app.model.ShoppingDistrictMenuResult;
import com.acloud.app.model.StoreCategoryMenuResult;
import com.acloud.app.tool.Pub;
import com.acloud.app.tool.TLog;
import com.acloud.app.util.ShoppingDistrictMenuManager;
import com.acloud.app.util.StoreCategoryMenuManager;

import java.util.ArrayList;

/**
 * Created by skywind-10 on 2015/7/20.
 */
public class CustomAdvPopupWindow {
    static PopupWindow
            mFliterWindows;

    private int mScreenWidth;
    private int mScreenHeight;
    //    Context mContext;
    Activity mActivity;
    Handler handler;

    LinearLayout
            sortLayout,
            categoryLayout,
            storeGroupLayout,
            priceRangeLayout;

    static int mAdjustHeight;

    boolean isHidePriceLayout = false;

    TextView
            tvCategory,
            tvDistrict,
            tvSort;

    CheckBox
            checkCategory,
            checkDistrict,
            checkSort;

    ListView
            mainCategoryListView,
            minorCategoryListView,
            mainDistrictListView,
            minorDistrictListView;

    MenuStoreAdapter
            mCategoryAdatper,
            mDistrictAdapter;

    MenuAdapter
            menuAdapter;

    EditText
            editLowPrice,
            editHeighPrice;

    ArrayList<StoreCategoryMenuResult> categoryMenuResults = StoreCategoryMenuManager.getList();
    ArrayList<ShoppingDistrictMenuResult> districtMenuResults = ShoppingDistrictMenuManager.getList();

    final ArrayList<MenuObject> mainCategoryArray = new ArrayList<>(); //主要分類
    final ArrayList<ArrayList<MenuObject>> minorCategoryArray = new ArrayList<ArrayList<MenuObject>>(); //次要屬性
    final ArrayList<MenuObject> minaDistrictArray = new ArrayList<>(); //主要商圈
    final ArrayList<ArrayList<MenuObject>> minorDistrictArray = new ArrayList<ArrayList<MenuObject>>(); //次要商場


    public CustomAdvPopupWindow(Activity activity, int adjustheight, Handler handler, boolean hidePriceLayout) {
        mActivity = activity;
        this.handler = handler;
        mAdjustHeight = adjustheight;
        isHidePriceLayout = hidePriceLayout;
    }

    /**
     * 進階篩選
     */
    public void initUI(View popupWindow) {
        mScreenWidth = mActivity.getWindowManager().getDefaultDisplay().getWidth();
        mScreenHeight = mActivity.getWindowManager().getDefaultDisplay().getHeight();
        LinearLayout btnCategory = (LinearLayout) popupWindow.findViewById(R.id.btnCategory);
        LinearLayout btnDistrict = (LinearLayout) popupWindow.findViewById(R.id.btnDistrict);
        LinearLayout btnSort = (LinearLayout) popupWindow.findViewById(R.id.btnSort);
        sortLayout = (LinearLayout) popupWindow.findViewById(R.id.sortLayout);
        categoryLayout = (LinearLayout) popupWindow.findViewById(R.id.categoryLayout);
        storeGroupLayout = (LinearLayout) popupWindow.findViewById(R.id.storeLayout);
        priceRangeLayout = (LinearLayout) popupWindow.findViewById(R.id.priceRangeLayout);
        //Sort
        TextView btnOk = (TextView) popupWindow.findViewById(R.id.btnOk);
        TextView btnDistance = (TextView) popupWindow.findViewById(R.id.btnDistance);
        TextView btnEvaluate = (TextView) popupWindow.findViewById(R.id.btnEvaluate);
        TextView btnCollation = (TextView) popupWindow.findViewById(R.id.btnCollation);
        TextView btnPrice = (TextView) popupWindow.findViewById(R.id.btnPrice);
        //Category
        tvCategory = (TextView) popupWindow.findViewById(R.id.tvCategory);
        tvDistrict = (TextView) popupWindow.findViewById(R.id.tvDistrict);
        tvSort = (TextView) popupWindow.findViewById(R.id.tvSort);
        checkCategory = (CheckBox) popupWindow.findViewById(R.id.checkCategory);
        checkDistrict = (CheckBox) popupWindow.findViewById(R.id.checkDistrict);
        checkSort = (CheckBox) popupWindow.findViewById(R.id.checkSort);
        //主要分類
        mainCategoryListView = (ListView) popupWindow.findViewById(R.id.mainCategoryListView);
        minorDistrictListView = (ListView) popupWindow.findViewById(R.id.secondDistrictListView);
        mainDistrictListView = (ListView) popupWindow.findViewById(R.id.mainDistrictListView);
        minorCategoryListView = (ListView) popupWindow.findViewById(R.id.secondCategoryListView);

        editLowPrice = (EditText)popupWindow.findViewById(R.id.editLowPrice);
        editHeighPrice = (EditText)popupWindow.findViewById(R.id.editHeighPrice);

        btnCategory.setOnClickListener(onClickListener);
        btnDistrict.setOnClickListener(onClickListener);
        btnSort.setOnClickListener(onClickListener);
        btnOk.setOnClickListener(onClickListener);
        btnDistance.setOnClickListener(onClickListener);
        btnEvaluate.setOnClickListener(onClickListener);
        btnCollation.setOnClickListener(onClickListener);
        btnPrice.setOnClickListener(onClickListener);

        TLog.i("Ernest", "adjustHeight : " + mAdjustHeight);
        mFliterWindows = new PopupWindow(popupWindow, mScreenWidth, mScreenHeight - mAdjustHeight);
        if (isHidePriceLayout) {
            priceRangeLayout.setVisibility(View.GONE);
        } else {
            mFliterWindows.setFocusable(true);
            mFliterWindows.update();
            mFliterWindows.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        }
    }

    private void initDefaultValue() {
        mainCategoryArray.add(new MenuObject("0", mActivity.getString(R.string.sort_all_category)));
        ArrayList<MenuObject> arrayList32 = new ArrayList<>();
        arrayList32.add(new MenuObject("0", mActivity.getString(R.string.sort_all_property)));
        minorCategoryArray.add(arrayList32);

        minaDistrictArray.add(new MenuObject("0", mActivity.getString(R.string.sort_all_district)));
        ArrayList<MenuObject> arrayList33 = new ArrayList<>();
        arrayList33.add(new MenuObject("0", mActivity.getString(R.string.sort_all_market)));
        minorDistrictArray.add(arrayList33);
    }

    public void initFliterWindow() {
        LayoutInflater layoutInflater = LayoutInflater.from(mActivity);
        View popupWindow = layoutInflater.inflate(R.layout.window_filter_advanced, null);

        initUI(popupWindow);
        initDefaultValue();
        initCategoryAdapter();
        initDistrictAdapter();
        //加入主要分類,屬性
        for (int k = 0; k < categoryMenuResults.size(); k++) {
            ArrayList<MenuObject> arrayList = null;
            arrayList = new ArrayList<>();
            mainCategoryArray.add(new MenuObject(categoryMenuResults.get(k).getSc_id(),
                    categoryMenuResults.get(k).getSc_name()));

            arrayList.add(new MenuObject("0", mActivity.getString(R.string.sort_all_property)));
            //加入屬性
            for (int j = 0; j < categoryMenuResults.get(k).getScaResultArrayList().size(); j++) {
                arrayList.add(new MenuObject(categoryMenuResults.get(k).getScaResultArrayList().get(j).getSca_id(),
                        categoryMenuResults.get(k).getScaResultArrayList().get(j).getSca_name()));
            }
            minorCategoryArray.add(arrayList);
        }
        mainCategoryListView.setAdapter(mCategoryAdatper);

        //加入主要商圈,商場
        for (int k = 0; k < districtMenuResults.size(); k++) {
            ArrayList<MenuObject> arrayList = null;
            arrayList = new ArrayList<>();
            minaDistrictArray.add(new MenuObject(districtMenuResults.get(k).getSd_id(),
                    districtMenuResults.get(k).getSd_name()));

            arrayList.add(new MenuObject("0", mActivity.getString(R.string.sort_all_market)));
            //加入商場
            for (int j = 0; j < districtMenuResults.get(k).getSmResultArrayList().size(); j++) {
                arrayList.add(new MenuObject(districtMenuResults.get(k).getSmResultArrayList().get(j).getSm_id(),
                        districtMenuResults.get(k).getSmResultArrayList().get(j).getSm_name()));
            }
            minorDistrictArray.add(arrayList);
        }
        mainDistrictListView.setAdapter(mDistrictAdapter);
    }

    private void initCategoryAdapter() {
        mCategoryAdatper = new MenuStoreAdapter(mActivity, mainCategoryArray, new MenuStoreAdapter.ItemClick() {
            @Override
            public void itemclick(String itemid, final int i) {
                menuAdapter = new MenuAdapter(mActivity, i, minorCategoryArray, new MenuAdapter.ItemClick() {
                    @Override
                    public void itemclick(String itemid, int j) {
                        Message message = new Message();
                        message.what = Pub.SEND_CATEGORY;
                        message.arg1 = Integer.valueOf(mainCategoryArray.get(i).getId());
                        message.arg2 = Integer.valueOf(itemid);
                        handler.sendMessage(message);
                        mainCategoryArray.get(i).getId();
                        tvCategory.setText(minorCategoryArray.get(i).get(j).getTitile());
                    }
                });
                //切換背景色
                try {
                    for (int j = 0; j < mainCategoryArray.size(); j++) {
                        mainCategoryListView.getChildAt(j).setBackground(mActivity.getResources().getDrawable(R.color.background3));
                    }
                    mainCategoryListView.getChildAt(i).setBackground(mActivity.getResources().getDrawable(R.drawable.choose_left));
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                //更改文字
                tvCategory.setText(mainCategoryArray.get(i).getTitile());
                //動態更新次要屬性內容
                minorCategoryListView.setAdapter(menuAdapter);
            }
        });
    }

    private void initDistrictAdapter() {
        mDistrictAdapter = new MenuStoreAdapter(mActivity, minaDistrictArray, new MenuStoreAdapter.ItemClick() {
            @Override
            public void itemclick(String itemid, final int i) {
                menuAdapter = new MenuAdapter(mActivity, i, minorDistrictArray, new MenuAdapter.ItemClick() {
                    @Override
                    public void itemclick(String itemid, int j) {
                        Message message = new Message();
                        message.what = Pub.SEND_DISTRICT;
                        message.arg1 = Integer.valueOf(minaDistrictArray.get(i).getId());
                        message.arg2 = Integer.valueOf(itemid);
                        handler.sendMessage(message);
                        minaDistrictArray.get(i).getId();
                        tvDistrict.setText(minorDistrictArray.get(i).get(j).getTitile());
                    }
                });
                //切換背景色
                try {
                    for (int j = 0; j < minaDistrictArray.size(); j++) {
                        mainDistrictListView.getChildAt(j).setBackground(mActivity.getResources().getDrawable(R.color.background3));
                    }
                    mainDistrictListView.getChildAt(i).setBackground(mActivity.getResources().getDrawable(R.drawable.choose_left));
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                //更改文字
                tvDistrict.setText(minaDistrictArray.get(i).getTitile());
                //動態更新次要商場內容
                minorDistrictListView.setAdapter(menuAdapter);
            }
        });
    }

    public View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Message msg;
            switch (view.getId()) {
                case R.id.btnCategory:
                    TLog.i("Ernest", "btnCategory");
                    sortLayout.setVisibility(View.GONE);
                    storeGroupLayout.setVisibility(View.GONE);
                    categoryLayout.setVisibility(View.VISIBLE);
                    closeAllSortBox();
                    tvCategory.setTextColor(mActivity.getResources().getColor(R.color.m));
                    checkCategory.setChecked(true);
                    break;
                case R.id.btnDistrict:
                    TLog.i("Ernest", "btnDistrict");

                    sortLayout.setVisibility(View.GONE);
                    storeGroupLayout.setVisibility(View.VISIBLE);
                    categoryLayout.setVisibility(View.GONE);
                    closeAllSortBox();
                    tvDistrict.setTextColor(mActivity.getResources().getColor(R.color.m));
                    checkDistrict.setChecked(true);
                    break;
                case R.id.btnSort:
                    TLog.i("Ernest", "btnSort");
                    sortLayout.setVisibility(View.VISIBLE);
                    categoryLayout.setVisibility(View.GONE);
                    storeGroupLayout.setVisibility(View.GONE);
                    closeAllSortBox();
                    tvSort.setTextColor(mActivity.getResources().getColor(R.color.m));
                    checkSort.setChecked(true);
                    break;
                case R.id.btnOk:
                    msg = new Message();
                    msg.what = Pub.BUTTON_OK;
                    Bundle b = new Bundle();
                    b.putString(Pub.VALUE_LOW_PRICE , editLowPrice.getText().toString());
                    b.putString(Pub.VALUE_HEIGHT_PRICE , editHeighPrice.getText().toString());
                    msg.setData(b);
                    handler.sendMessage(msg);
                    break;
                case R.id.btnDistance:
                    msg = new Message();
                    msg.what = Pub.BUTTON_SORT;
                    msg.arg1 = Pub.SORT_DISTANCE;
                    handler.sendMessage(msg);
                    break;
                case R.id.btnEvaluate:
                    msg = new Message();
                    msg.what = Pub.BUTTON_SORT;
                    msg.arg1 = Pub.SORT_EVALUATE;
                    handler.sendMessage(msg);
                    break;
                case R.id.btnCollation:
                    msg = new Message();
                    msg.what = Pub.BUTTON_SORT;
                    msg.arg1 = Pub.SORT_COLLATION;
                    handler.sendMessage(msg);
                    break;
                case R.id.btnPrice:
                    msg = new Message();
                    msg.what = Pub.BUTTON_SORT;
                    msg.arg1 = Pub.SORT_PRICE;
                    handler.sendMessage(msg);
                    break;
            }
        }
    };

    public void closeAllSortBox() {
        checkCategory.setChecked(false);
        checkDistrict.setChecked(false);
        checkSort.setChecked(false);
        tvSort.setTextColor(Color.parseColor("#3E3A39"));
        tvCategory.setTextColor(Color.parseColor("#3E3A39"));
        tvDistrict.setTextColor(Color.parseColor("#3E3A39"));
    }

    /**
     * 要Show的View
     *
     * @param view
     */
    public static void showAdvancePopupwindow(View view) {
        if (mFliterWindows.isShowing()) {
            mFliterWindows.dismiss();
        } else
            mFliterWindows.showAsDropDown(view, 0, 2);
    }

    //關閉全部視窗
    public static void closeAllPopupWindow() {
        if (mFliterWindows != null && mFliterWindows.isShowing()) {
            mFliterWindows.dismiss();
        }
    }

}