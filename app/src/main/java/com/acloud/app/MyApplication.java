package com.acloud.app;


import android.app.Application;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;

import com.acloud.app.tool.TLog;
import com.crashlytics.android.Crashlytics;
import com.huanuage.basenewfoundtion.untils.Utils;

import io.fabric.sdk.android.Fabric;

public class MyApplication extends Application {
    public static final boolean IN_DEBUG = true;

    public static String emil = "";

    public static final String DbName = "CbfSalesDB";
    public static final int DbVersion = 0;

    /**
     * 照片長寬
     */
    public static int photoSide = 128;

    /**
     * screen shorter density
     */
    public static int densityDpi = 120;

    /**
     * screen shorter side length
     */
    public static int screenSizePxS = 0;
    /**
     * screen longer side length
     */
    public static int screenSizePxL = 0;
    /**
     * global text size xxs
     */
    public static int textSizePxXXS = 0;
    /**
     * global text size xs
     */
    public static int textSizePxXS = 0;
    /**
     * global text size s
     */
    public static int textSizePxS = 0;
    /**
     * global text size m
     */
    public static int textSizePxMS = 0;
    /**
     * global text size m
     */
    public static int textSizePxM = 0;
    /**
     * global text size l
     */
    public static int textSizePxL = 0;
    /**
     * global text size xl
     */
    public static int textSizePxXL = 0;
    /**
     * global text size xxl
     */
    public static int textSizePxXXL = 0;


    /**
     * 印log.d都用這個方法, 等要發佈apk的時候就可以一次把這方法內部註解掉
     */
    public static void logD(String tag, String text) {
        if (IN_DEBUG) {
            Log.d(tag, text);
        }
    }

    /**
     * 印log.e都用這個方法, 等要發佈apk的時候就可以一次把這方法內部註解掉
     */
    public static void logE(String tag, String text) {
        if (IN_DEBUG) {
            Log.e(tag, text);
        }
    }

    /**
     * 印Exception都用這個方法, 等要發佈apk的時候就可以一次把這方法內部註解掉
     */
    public static void printException(Exception e) {
        if (IN_DEBUG) {
            e.printStackTrace();
        }
    }

    public static void setTextSize(View v, int sizeInPx) {
        if (v instanceof android.widget.TextView) {
            ((android.widget.TextView) v).setTextSize(TypedValue.COMPLEX_UNIT_PX, sizeInPx);
        } else if (v instanceof android.widget.Button) {
            ((android.widget.Button) v).setTextSize(TypedValue.COMPLEX_UNIT_PX, sizeInPx);
        } else if (v instanceof android.widget.EditText) {
            ((android.widget.EditText) v).setTextSize(TypedValue.COMPLEX_UNIT_PX, sizeInPx);
        } else if (v instanceof android.widget.RadioButton) {
            ((android.widget.RadioButton) v).setTextSize(TypedValue.COMPLEX_UNIT_PX, sizeInPx);
        } else if (v instanceof android.widget.CheckBox) {
            ((android.widget.CheckBox) v).setTextSize(TypedValue.COMPLEX_UNIT_PX, sizeInPx);
        } else if (v instanceof android.widget.ToggleButton) {
            ((android.widget.ToggleButton) v).setTextSize(TypedValue.COMPLEX_UNIT_PX, sizeInPx);
        } else {

        }
    }

    Handler mLoginSuccessHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Intent intent = new Intent();
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setClass(getApplicationContext(), MainActivity.class);
            getApplicationContext().startActivity(intent);
        }
    };


    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        Utils.setLoginSuccessCallbackHandler(mLoginSuccessHandler);
        logE(getClass().getName(), "create ApplicationContext");

        if (MyApplication.screenSizePxL == 0 || MyApplication.screenSizePxS == 0) {
            setGlobalScreenSize();
        }
        setGlobalTextSize(
                32,
                30,
                28,
                26,
                24,
                22,
                20,
                18
        );

        setPhotoSide();


    }


    private void setGlobalTextSize(float textNumXXS, float textNumXS, float textNumS, float textNumMS, float textNumM, float textNumL, float textNumXL, float textNumXXL) {
        if (screenSizePxL == 0 || screenSizePxS == 0) {
            setGlobalScreenSize();
        }

        textSizePxXXS = (int) (screenSizePxS / textNumXXS);
        textSizePxXS = (int) (screenSizePxS / textNumXS);
        textSizePxS = (int) (screenSizePxS / textNumS);
        textSizePxMS = (int) (screenSizePxS / textNumM);
        textSizePxM = (int) (screenSizePxS / textNumM);
        textSizePxL = (int) (screenSizePxS / textNumL);
        textSizePxXL = (int) (screenSizePxS / textNumXL);
        textSizePxXXL = (int) (screenSizePxS / textNumXXL);
    }

    public void setGlobalScreenSize() {
        DisplayMetrics dm = getApplicationContext().getResources().getDisplayMetrics();
        if (dm.widthPixels < dm.heightPixels) {
            screenSizePxS = dm.widthPixels;
            screenSizePxL = dm.heightPixels;
        } else {
            screenSizePxS = dm.heightPixels;
            screenSizePxL = dm.widthPixels;
        }

        densityDpi = dm.densityDpi;
        TLog.e("Myapplication", "densityDpi" + densityDpi);
    }

    public void setPhotoSide() {
        DisplayMetrics dm = getApplicationContext().getResources().getDisplayMetrics();
        if (dm.widthPixels < dm.heightPixels) {

            photoSide = (dm.heightPixels / 1280) * photoSide;
        } else {
            photoSide = (dm.widthPixels / 1280) * photoSide;
        }

    }


}
