package com.acloud.app.http;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.acloud.app.model.StoreListResult;
import com.acloud.app.tool.Pub;
import com.acloud.app.tool.TLog;
import com.acloud.app.util.StoreListManager;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by skywind-10 on 2015/4/13.
 */
public class HttpStoreList extends BaseHttp {
    Context mContext;
    Activity activity;

    ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
    ArrayList<StoreListResult> listResults = new ArrayList<StoreListResult>(); //存入解析完的資料

    int code = -1;
    String responseString;
    String TAG = "HttpStoreList";
    boolean mSuccess;
    String mSysCode;
    String mSysMsg;

    public HttpStoreList(Activity activity, ArrayList<NameValuePair> list) {
        this.activity = activity;
        this.list = list;
        TLog.i("Ernest", "StoreList NVP : " + list);
        setTAG(TAG);
    }

    //http的判斷
    public boolean isSuccess() {
        return mSuccess;
    }

    public boolean isSysSuccess() {
        return mSysCode.equals("200");
    }

    public void call() {
        if (callAPI(Pub.API_STORELIST, list)) {
            parserJson(getResponseString());
            mSuccess = true;
            //將解析完成的Arraylist丟到管理CouponListManager裡面
            StoreListManager.setResultArrayList(listResults);
        } else {
            mSuccess = false;
        }
    }

    @Override
    public void setTAG(String tag) {
        super.setTAG(tag);
    }

    @Override
    public String getResponseString() {
        return super.getResponseString();
    }

    @Override
    protected boolean callAPI(String url, ArrayList<NameValuePair> list) {
        return super.callAPI(url, list);
    }


    void parserJson(String message) {
        JSONObject object = null;
        JSONArray array = null;
        try {
            object = new JSONObject(message);

            mSysCode = object.optString(Pub.VALUE_SYSCODE);
            mSysMsg = object.optString(Pub.VALUE_SYSMSG);

            if (mSysCode.equals("200")) {
                array = object.optJSONArray("list");

                for (int i = 0; i < array.length(); i++) {
                    JSONObject object1 = (JSONObject) array.get(i);
                    StoreListResult result = new StoreListResult();
                    result.setStore_id(object1.optString("store_id"));
                    result.setStore_name(object1.optString("store_name"));
                    result.setThumbnail_url(object1.optString("thumbnail_url"));
                    result.setLat(object1.optString("lat"));
                    result.setLng(object1.optString("lng"));
                    result.setDistance(object1.optString("distance"));
                    result.setRating(object1.optString("rating"));
                    result.setRating_num(object1.optString("rating_num"));
                    result.setAvg_price(object1.optString("avg_price"));
                    result.setFav_num(object1.optString("fav_num"));
                    result.setSd_id(object1.optString("sd_id"));
                    result.setSd_name(object1.optString("sd_name"));
                    result.setSca_id(object1.optString("sca_id"));
                    result.setSca_name(object1.optString("sca_name"));
                    listResults.add(result);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}



