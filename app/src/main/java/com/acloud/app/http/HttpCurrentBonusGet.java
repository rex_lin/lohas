package com.acloud.app.http;

import android.app.Activity;
import android.content.Context;

import com.acloud.app.tool.TLog;
import com.acloud.app.tool.Pub;
import com.acloud.app.util.CouponListManager;

import org.apache.http.NameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 *
 * 取得當前點數
 */
public class HttpCurrentBonusGet extends BaseHttp {
    Context mContext;
    Activity activity;
    ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
    String TAG = "HttpCurrentBonusGet";
    boolean mSuccess;
    String syscode, current_bonus;
    boolean mSyscode;

    public boolean ismSuccess() {
        return mSuccess;
    }

    public boolean ismSyscode() {
        return syscode.equals("200");
    }

    public HttpCurrentBonusGet(Activity activity, ArrayList<NameValuePair> list) {
        this.activity = activity;
        this.list = list;
        setTAG(TAG);
    }

    public void call() {
        if (callAPI(Pub.API_CURRENTBONUS_GET, list)) {
            parserJson(getResponseString());
            CouponListManager.setCurrentBouns(current_bonus);
            mSuccess = true;
        } else {
            mSuccess = false;
        }
    }

    @Override
    public void setTAG(String tag) {
        super.setTAG(tag);
    }

    @Override
    protected boolean callAPI(String url, ArrayList<NameValuePair> list) {
        return super.callAPI(url, list);
    }

    @Override
    public String getResponseString() {
        return super.getResponseString();
    }

    void parserJson(String message) {
        JSONObject object = null;

        String sysmsg;

        try {
            object = new JSONObject(message);

            syscode = object.optString(Pub.VALUE_SYSCODE);
            sysmsg = object.optString(Pub.VALUE_SYSMSG);
            object = object.getJSONObject("data");
            TLog.i(TAG, object.optString("current_bonus"));
            current_bonus = object.optString("current_bonus");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}



