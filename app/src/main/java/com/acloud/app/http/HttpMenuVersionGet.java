package com.acloud.app.http;

import android.app.Activity;
import android.content.Context;

import com.acloud.app.tool.Pub;
import com.acloud.app.tool.TLog;
import com.acloud.app.util.PrefConstant;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by skywind-10 on 2015/4/13.
 */
public class HttpMenuVersionGet extends BaseHttp {
    Context mContext;
    Activity activity;
    ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
    int code = -1;
    String responseString;
    String TAG = "HttpMenuVersionGet";
    boolean update = false;

    public HttpMenuVersionGet(Activity activity, ArrayList<NameValuePair> list) {
        this.activity = activity;
        this.list = list;
        setTAG(TAG);
    }

    public boolean call() {
        if (callAPI(Pub.API_MENU_VERSIONS, list)) {
            parserJson(getResponseString());
            return update;
        } else {
            return update;
        }
    }

    @Override
    public void setTAG(String tag) {
        super.setTAG(tag);
    }

    @Override
    public String getResponseString() {
        return super.getResponseString();
    }

    @Override
    protected boolean callAPI(String url, ArrayList<NameValuePair> list) {
        return super.callAPI(url, list);
    }

    private void parserJson(String message) {
        String
                syscode,
                sysmsg,
                key,
                updatedatetime,
                shopping_updatetime = "",
                store_updatetime = "";
        try {
            JSONObject object = new JSONObject(message);
            syscode = object.optString("syscode");
            sysmsg = object.optString("sysmsg");
            if (syscode.equals("200")) {
                JSONArray jsonArray = object.optJSONArray("list");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object1 = (JSONObject) jsonArray.get(i);
                    key = object1.optString("key");
                    updatedatetime = object1.optString("update_datetime");
                    if (key.equals(Pub.VALUE_SHOPPING_DISTRICT)) {
                        shopping_updatetime = updatedatetime;
                    }
                    if (key.equals(Pub.VALUE_STORE_CATEGORY)) {
                        store_updatetime = updatedatetime;
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (shopping_updatetime.equals(PrefConstant.getString(activity, Pub.VALUE_SHOPPING_DISTRICT, ""))) {
            update = false;
        } else {
            PrefConstant.setString(activity, Pub.VALUE_SHOPPING_DISTRICT, shopping_updatetime);
            update = true;
        }
        if (store_updatetime.equals(PrefConstant.getString(activity, Pub.VALUE_STORE_CATEGORY, ""))) {
            update = false;
        } else {
            update = true;
            PrefConstant.setString(activity, Pub.VALUE_STORE_CATEGORY, store_updatetime);
        }
        TLog.i("Ernest", "MenuVersion : " + update);
    }

}




