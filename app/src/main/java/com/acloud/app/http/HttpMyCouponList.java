package com.acloud.app.http;

import android.app.Activity;
import android.content.Context;

import com.acloud.app.model.MyCouponListResult;
import com.acloud.app.tool.Pub;
import com.acloud.app.util.MyCouponListManager;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by skywind-10 on 2015/4/13.
 */
public class HttpMyCouponList extends BaseHttp {
    Context mContext;
    Activity activity;
    ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
    String TAG = "HttpMyCouponList";

    String syscode, sysmsg;
    ArrayList<MyCouponListResult> listResults = new ArrayList<MyCouponListResult>();
    int type;

    public HttpMyCouponList(Activity activity, ArrayList<NameValuePair> list, int type) {
        this.activity = activity;
        this.list = list;
        setTAG(TAG);
        this.type = type;
    }

    public void call() {
        if (callAPI(Pub.API_MYCOUPON_LIST, list)) {
            parserJson(getResponseString());
            switch (type) {
                case 1: //未使用
                    MyCouponListManager.setResultArrayList_unused(listResults);
                    break;
                case 2: //已使用
                    MyCouponListManager.setResultArrayList_used(listResults);
                    break;
                case 3: //已過期
                    MyCouponListManager.setResultArrayList_expired(listResults);
                    break;
            }

        } else {
        }
    }

    @Override
    protected boolean callAPI(String url, ArrayList<NameValuePair> list) {
        return super.callAPI(url, list);
    }

    @Override
    public void setTAG(String tag) {
        super.setTAG(tag);
    }

    @Override
    public String getResponseString() {
        return super.getResponseString();
    }

    void parserJson(String message) {
        JSONObject object = null;
        JSONArray array = null;

        try {
            object = new JSONObject(message);
            syscode = object.optString(Pub.VALUE_SYSCODE);
            sysmsg = object.optString(Pub.VALUE_SYSMSG);
            if (syscode.equals("200")) {
                array = object.getJSONArray("list");

                for (int i = 0; i < array.length(); i++) {
                    JSONObject object1 = (JSONObject) array.get(i);
                    //用物件去接json的字串
                    MyCouponListResult result = new MyCouponListResult();
                    result.setEx_coupon_id(object1.optString("ex_coupon_id"));
                    result.setCoupon_id(object1.optString("coupon_id"));
                    result.setTitle(object1.optString("title"));
                    result.setStore_id(object1.optString("store_id"));
                    result.setStore_name(object1.optString("store_name"));
                    result.setLat(object1.optString("lat"));
                    result.setLng(object1.optString("lng"));
                    result.setDistance(object1.optString("distance"));
                    result.setThumbnail_url(object1.optString("thumbnail_url"));
                    result.setPoints(object1.optString("points"));
                    result.setUsed_start_datetime(object1.optString("used_start_datetime"));
                    result.setUsed_end_datetime(object1.optString("used_end_datetime"));
                    result.setExchange_dateitme(object1.optString("exchange_datetime"));
                    result.setUsed_datetime(object1.optString("used_datetime"));
                    result.setStatus(object1.optString("status"));
                    listResults.add(result);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}



