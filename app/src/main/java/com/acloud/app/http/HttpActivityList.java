package com.acloud.app.http;

import android.app.Activity;

import com.acloud.app.model.ActivityListBannerResult;
import com.acloud.app.model.ActivityListResult;
import com.acloud.app.tool.Pub;
import com.acloud.app.tool.TLog;
import com.acloud.app.util.ActivityListManager;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by skywind-10 on 2015/4/13.
 */
public class HttpActivityList extends BaseHttp {
    Activity activity;
    ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
    String responseString;
    String TAG = "HttpActivityList";

    public String getSyscode() {
        return syscode;
    }

    public void setSyscode(String syscode) {
        this.syscode = syscode;
    }

    public String getSysmsg() {
        return sysmsg;
    }

    public void setSysmsg(String sysmsg) {
        this.sysmsg = sysmsg;
    }

    String syscode;
    String sysmsg;
    boolean mSuccess;

    public HttpActivityList(Activity activity, ArrayList<NameValuePair> list) {
        this.activity = activity;
        this.list = list;
        setTAG(TAG);
    }

    public void call() {
        if (callAPI(Pub.API_ACTIVITY_LIST, list)) {
            parserJson(getResponseString());
            mSuccess = true;
            //將解析完成的ArrayList丟到管理的ActivityListManager裡面
            ActivityListManager.setResultArrayList(listResults);
            ActivityListManager.setResultsBannerArrayList(listBannerResults);

        } else {
            mSuccess =false;
        }
    }

    @Override
    protected boolean callAPI(String url, ArrayList<NameValuePair> list) {
        return super.callAPI(url, list);
    }

    @Override
    public void setTAG(String tag) {
        super.setTAG(tag);
    }

    @Override
    public String getResponseString() {
        return super.getResponseString();
    }

    ArrayList<ActivityListBannerResult> listBannerResults = new ArrayList<ActivityListBannerResult>();
    ArrayList<ActivityListResult> listResults = new ArrayList<ActivityListResult>();
    void parserJson(String message) {
        try {
            JSONObject object = new JSONObject(message);
            JSONArray array = null;
            syscode = object.optString("syscode");
            sysmsg = object.optString("sysmsg");
            if (syscode.equals("200")) {
                array = object.getJSONArray("banners");
                for (int i = 0; i < array.length(); i++) {
                    TLog.i(TAG, "for i" + i);
                    JSONObject object1 = (JSONObject) array.get(i);
                    //用物件去接json的字串
                    ActivityListBannerResult bannerResult = new ActivityListBannerResult();
                    bannerResult.setAid(object1.optString("aid"));
                    bannerResult.setImage_url(object1.optString("image_url"));
                    bannerResult.setContent(object1.optString("content"));
                    bannerResult.setTitile(object1.optString("title"));
                    bannerResult.setStart_datetime(object1.optString("start_datetime"));
                    bannerResult.setEnd_datetime(object1.optString("end_datetime"));
                    listBannerResults.add(bannerResult);
                }

                array = object.getJSONArray("list");
                for (int j = 0; j < array.length(); j++) {
                    TLog.i(TAG, "for j" + j);
                    JSONObject object1 = (JSONObject) array.get(j);
                    ActivityListResult listResult = new ActivityListResult();
                    listResult.setAid(object1.optString("aid"));
                    listResult.setTitle(object1.optString("title"));
                    listResult.setType(object1.optString("type"));
                    listResult.setStart_datetime(object1.optString("start_datetime"));
                    listResult.setEnd_datetime(object1.optString("end_datetime"));
                    listResult.setStatus(object1.optString("status"));
                    listResult.setStore_id(object1.optString("store_id"));
                    listResult.setStore_name(object1.optString("store_name"));
                    listResult.setDistance(object1.optString("distance"));
                    listResult.setPoints(object1.optString("points"));
                    listResult.setLat(object1.optString("lat"));
                    listResult.setLng(object1.optString("lng"));
                    listResults.add(listResult);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
    public boolean isSuccess() {
        return mSuccess;
    }

    public boolean isSysSuccess() {
        return syscode.equals("200");
    }
}



