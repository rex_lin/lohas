package com.acloud.app.http;

import com.acloud.app.tool.TLog;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;

/**
 * Created by skywind-10 on 2015/6/8.
 */
public class BaseHttp {
    int code;
    private String responseString;
    ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
    String TAG = "BaseHttp";
    String url;

    public void setTAG(String tag) {
        TAG = tag;
    }


    protected boolean callAPI(String url, ArrayList<NameValuePair> list) {
        this.url = url;
        this.list = list;
        TLog.d("ooxx", TAG + " list : " + list);
        HttpPost httpPost = new HttpPost(url);
        HttpParams httpParams = new BasicHttpParams();
        // Set the timeout in milliseconds until a connection is established.
        // The default value is zero, that means the timeout is not used.
        int timeoutConnection = 10000;
        HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
        // Set the default socket timeout (SO_TIMEOUT)
        // in milliseconds which is the timeout for waiting for data.
        int timeoutSocket = 10000;
        HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);

        HttpClient httpClient = new DefaultHttpClient(httpParams);
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(list, HTTP.UTF_8));
            HttpResponse response = httpClient.execute(httpPost);
            code = response.getStatusLine().getStatusCode();
            if (code >= 200) {
                HttpEntity entity = response.getEntity();
                responseString = EntityUtils.toString(entity, "UTF-8");

                TLog.i(TAG, "response" + responseString);
            }

        } catch (ClientProtocolException e) {
            TLog.e(TAG, "clinet protocol exception");
            return false;
        } catch (SocketTimeoutException e) {
            // TODO: handle exception
            TLog.e(TAG, "socket timeout" + timeoutSocket);
            return false;
        } catch (ConnectTimeoutException e) {
            // TODO: handle exception
            TLog.e(TAG, "connection timeout" + timeoutConnection);
            return false;
        } catch (IOException e) {
            TLog.e(TAG, "io exception");
            return false;
        }
        return true;

    }

    public String getResponseString() {
        return responseString;
    }

}
