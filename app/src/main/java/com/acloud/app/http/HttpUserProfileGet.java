package com.acloud.app.http;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;

import com.acloud.app.model.UserProfileResult;
import com.acloud.app.tool.Pub;

import org.apache.http.NameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * 取得個人資訊
 */
public class HttpUserProfileGet extends BaseHttp {
    Context mContext;
    Activity activity;
    ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
    int code = -1;
    String responseString;
    String TAG = "HttpUserProfileGet";
    Handler handler;
    private boolean mSuccess;
    private String mSyscode;
    private String mSysmsg;
    UserProfileResult mUserProfile;

    public HttpUserProfileGet(Activity activity, ArrayList<NameValuePair> list) {
        this.activity = activity;
        this.list = list;
    }
    //http的判斷
    public boolean isSuccess() {
        return mSuccess;
    }
    //SysCode的判斷
    public boolean isSysSucess() {
        return mSyscode.equals("200");
    }

    public UserProfileResult getUserProfileResult() {
        return mUserProfile;
    }

    public void call() {
        if (callAPI(Pub.API_USERINFO, list)) {
//            return getResponseString();
            mSuccess = true;
            parserJson(getResponseString());
        } else {
            mSuccess = false;
        }
    }

    @Override
    protected boolean callAPI(String url, ArrayList<NameValuePair> list) {
        return super.callAPI(url, list);
    }

    @Override
    public String getResponseString() {
        return super.getResponseString();
    }

    void parserJson(String message) {
        JSONObject object = null;

        try {
            object = new JSONObject(message);

            mSyscode = object.optString(Pub.VALUE_SYSCODE);
            mSysmsg = object.optString(Pub.VALUE_SYSMSG);
            object =  object.getJSONObject("data");
            if(mSyscode.equals("200")) {
                mUserProfile = new UserProfileResult(object);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


}



