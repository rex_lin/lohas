package com.acloud.app.http;

import android.app.Activity;
import android.content.Context;

import com.acloud.app.tool.Pub;
import com.acloud.app.util.PrefConstant;

import org.apache.http.NameValuePair;

import java.util.ArrayList;

/**
 * 商圈選單
 */
public class HttpMenuShoppingDistrict extends BaseHttp {
    Context mContext;
    Activity activity;
    ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
    int code = -1;
    String responseString;
    String TAG = "HttpMenuShoppingDistrict";

    public HttpMenuShoppingDistrict(Activity activity, ArrayList<NameValuePair> list) {
        this.activity = activity;
        this.list = list;
    }

    public void call() {
        if (callAPI(Pub.API_MENU_SHOPPINGDISTRICT, list)) {
            PrefConstant.setString(activity, Pub.VALUE_MENU_SHOPPING_DISTRICT, getResponseString());
        } else {
        }
    }

    @Override
    public void setTAG(String tag) {
        super.setTAG(tag);
    }

    @Override
    public String getResponseString() {
        return super.getResponseString();
    }

    @Override
    protected boolean callAPI(String url, ArrayList<NameValuePair> list) {
        return super.callAPI(url, list);
    }


}



