package com.acloud.app.http;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.acloud.app.tool.Pub;
import com.acloud.app.tool.TLog;

import org.apache.http.NameValuePair;

import java.util.ArrayList;

/**
 * Created by skywind-10 on 2015/4/13.
 */
public class HttpBonusListGet extends BaseHttp{
    Context mContext;
    Activity activity;
    ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
    int code = -1;
    String responseString;
    String TAG = "HttpBonusListGet";

    public HttpBonusListGet(Activity activity, ArrayList<NameValuePair> list) {
        this.activity = activity;
        this.list = list;
        TLog.i("Ernest", "BonusList : " + list);
        setTAG(TAG);
    }

    public String call() {
        if (callAPI(Pub.API_BONUSLIST_GET,list)) {
            return getResponseString();
        } else {
            return "false";
        }
    }

    @Override
    public void setTAG(String tag) {
        super.setTAG(tag);
    }

    @Override
    protected boolean callAPI(String url, ArrayList<NameValuePair> list) {
        return super.callAPI(url, list);
    }

    @Override
    public String getResponseString() {
        return super.getResponseString();
    }
}



