package com.acloud.app.http;

import android.app.Activity;

import com.acloud.app.model.MyCouponDetailResult;
import com.acloud.app.tool.Pub;
import com.acloud.app.util.MyCouponDetailManager;

import org.apache.http.NameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by skywind-10 on 2015/4/13.
 */
public class HttpMyCouponInfo extends BaseHttp {
    Activity activity;
    ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
    String TAG = "HttpMyCouponInfo";
    String syscode, sysmsg;
    MyCouponDetailResult result = new MyCouponDetailResult();
    boolean isSuccess;

    public HttpMyCouponInfo(Activity activity, ArrayList<NameValuePair> list) {
        this.activity = activity;
        this.list = list;
        setTAG(TAG);
    }

    public void call() {
        if (callAPI(Pub.API_MYCOUPON_INFO, list)) {
            parserJson(getResponseString());
            MyCouponDetailManager.setMyCouponDetailResult(result);
            isSuccess = true;
        } else {
            isSuccess =false;
        }
    }

    @Override
    protected boolean callAPI(String url, ArrayList<NameValuePair> list) {
        return super.callAPI(url, list);
    }

    @Override
    public void setTAG(String tag) {
        super.setTAG(tag);
    }

    @Override
    public String getResponseString() {
        return super.getResponseString();
    }


    void parserJson(String message) {
        JSONObject object;
        try {
            object = new JSONObject(message);
            syscode = object.optString(Pub.VALUE_SYSCODE);
            sysmsg = object.optString(Pub.VALUE_SYSCODE);
            if (syscode.equals("200")) {
                object = object.getJSONObject("data");
                result.setEx_coupon_id(object.optString("ex_coupon_id"));
                result.setCoupon_id(object.optString("coupon_id"));
                result.setTitle(object.optString("title"));
                result.setContent(object.optString("content"));
                result.setStore_id(object.optString("store_id"));
                result.setStore_name(object.optString("store_name"));
                result.setLat(object.optString("lat"));
                result.setLng(object.optString("lng"));
                result.setDistance(object.optString("distance"));
                result.setImage_url(object.optString("image_url"));
                result.setUsed_start_datetime(object.optString("used_start_datetime"));
                result.setUsed_end_datetime(object.optString("used_end_datetime"));
                result.setExchange_datetime(object.optString("exchange_dateime"));
                result.setUsed_type(object.optString("used_type"));
                result.setStatus(object.optString("status"));
                result.setVerify_code(object.optString("verify_code"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public boolean isSuccess() {
        return isSuccess;
    }
}



