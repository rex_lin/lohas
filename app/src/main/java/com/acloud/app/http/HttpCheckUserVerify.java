package com.acloud.app.http;

import android.app.Activity;
import android.content.Context;

import com.acloud.app.model.UserVerifyResult;
import com.acloud.app.tool.Pub;
import com.acloud.app.tool.TLog;
import com.acloud.app.util.UserVerifyCheckManager;

import org.apache.http.NameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * 　確認User是否驗證成功
 */
public class HttpCheckUserVerify extends BaseHttp {
    Context mContext;
    Activity activity;
    ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
    String TAG = "HttpCheckUserVerify";
    String syscode;
    String sysmsg;

    public HttpCheckUserVerify(Activity activity, ArrayList<NameValuePair> list) {
        this.activity = activity;
        this.list = list;
        setTAG(TAG);
    }

    public void call() {
        TLog.d("ooxx", this.getClass().getName() + " " + "HttpCheckUserVerify list : " + list);
        if (callAPI(Pub.API_CHECK_USERVERIFY, list)) {
            parserJson(getResponseString());
        } else {

        }
    }

    @Override
    public boolean callAPI(String url, ArrayList<NameValuePair> list) {
        return super.callAPI(url, list);
    }

    @Override
    public void setTAG(String tag) {
        super.setTAG(tag);
    }

    @Override
    public String getResponseString() {
        return super.getResponseString();
    }

    void parserJson(String message) {
        try {
            JSONObject jsonObject = new JSONObject(message);
            syscode = jsonObject.optString(Pub.VALUE_SYSCODE);
            jsonObject = jsonObject.getJSONObject("data");
            //UserVery Ojbect 用來解析Json
            UserVerifyResult result = new UserVerifyResult();
            result.setIs_verify(jsonObject.optString("is_verify"));
            result.setUid(jsonObject.optString("uid"));
            result.setAuthtoken(jsonObject.optString("authtoken"));
            result.setFirst_login(jsonObject.optString("first_login"));
            result.setIs_activity(jsonObject.optString("is_activity"));
            result.setSerial_no(jsonObject.optString("serial_no"));
            result.setRecommended_serial_no(jsonObject.optString("recommended_serial_no"));
            result.setVerifing_email(jsonObject.optString("verifing_email"));

            UserVerifyCheckManager.setResult(result);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public String getSysmsg() {
        return sysmsg;
    }

    public void setSysmsg(String sysmsg) {
        this.sysmsg = sysmsg;
    }

    public String getSyscode() {
        return syscode;
    }

    public void setSyscode(String syscode) {
        this.syscode = syscode;
    }
}



