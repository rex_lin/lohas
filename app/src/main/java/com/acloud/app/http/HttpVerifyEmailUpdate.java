package com.acloud.app.http;

import android.app.Activity;
import android.content.Context;

import com.acloud.app.tool.Pub;

import org.apache.http.NameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * 　驗證信箱修改
 */
public class HttpVerifyEmailUpdate extends BaseHttp {
    Context mContext;
    Activity activity;
    ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
    int code = -1;
    String responseString;
    String TAG = "HttpVerifyEmailUpdate";
    boolean mSuccess;
    String syscode;
    String sysmsg;

    public HttpVerifyEmailUpdate(Activity activity, ArrayList<NameValuePair> list) {
        this.activity = activity;
        this.list = list;
        setTAG(TAG);
    }

    public void call() {
        if (callAPI(Pub.API_UPDATE_VERIFYEMAIL, list)) {
            parserJson(getResponseString());
            mSuccess = true;
        } else {
            mSuccess = false;
        }
    }

    @Override
    public void setTAG(String tag) {
        super.setTAG(tag);
    }

    @Override
    public boolean callAPI(String url, ArrayList<NameValuePair> list) {
        return super.callAPI(url, list);
    }

    @Override
    public String getResponseString() {
        return super.getResponseString();
    }

    public boolean ismSuccess() {
        return mSuccess;
    }

    public boolean getIsSysSuccess() {
        return syscode.equals("200");
    }

    public String getSysCode() {
        return syscode;
    }

    void parserJson(String message) {
        JSONObject object = null;
        try {
            object = new JSONObject(message);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        syscode = object.optString("syscode");
        sysmsg = object.optString("sysmsg");
    }
}



