package com.acloud.app.http;

import android.app.Activity;
import android.content.Context;

import com.acloud.app.model.LoginResult;
import com.acloud.app.model.MemberInfo;
import com.acloud.app.tool.Pub;
import com.acloud.app.tool.TLog;
import com.acloud.app.util.LoginManager;
import com.acloud.app.util.PrefConstant;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * 登入
 */
public class HttpGetMemberInfo {
    Context mContext;
    Activity activity;
    int code = -1;
    String TAG = "HttpGetMemberInfo";
    String syscode, sysmsg;
    boolean mSuccess;
    String token;
    MemberInfo memberInfo;


    public HttpGetMemberInfo(Activity activity, String token) {
        this.activity = activity;
        this.token = token;
    }

    public void call() {
        HttpURLConnection urlConnection = null;
        try {
            URL mUrl = new URL(Pub.API_GET_MEMBER_INFO);
            urlConnection = (HttpURLConnection) mUrl.openConnection();
            urlConnection.setReadTimeout(10000);
            urlConnection.setConnectTimeout(15000);
            urlConnection.setRequestMethod("GET");
            urlConnection.setRequestProperty("Authorization", token);
            urlConnection.connect();


            int responseCode = urlConnection.getResponseCode();
            if(responseCode == 200){
                InputStream is = urlConnection.getInputStream();
                String state = getStringFromInputStream(is);
                TLog.d("ooxx", this.getClass().getName() + " " + "state : " + state);
                JSONObject jsonObject = new JSONObject(state);
                memberInfo= new MemberInfo();
                memberInfo.setStatus(jsonObject.optString("status"));
                memberInfo.setId(jsonObject.optString("id"));
                memberInfo.setEmail(jsonObject.optString("email"));
                memberInfo.setName(jsonObject.optString("name"));
                memberInfo.setPhone(jsonObject.optString("phone"));
                memberInfo.setMobile(jsonObject.optString("mobile"));
                memberInfo.setAddress(jsonObject.optString("address"));
                memberInfo.setEpId(jsonObject.optString("epId"));
                memberInfo.setEpName(jsonObject.optString("epName"));
                memberInfo.setEpLogo(jsonObject.optString("epLogo"));
                memberInfo.setEpLevel(jsonObject.optString("epLevel"));

            }
        } catch (IOException | JSONException e){
            e.printStackTrace();

        } finally {
            try {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
            } catch (Exception e) {
                e.printStackTrace(); //If you want further info on failure...
            }
        }

    }

    public MemberInfo getMemberInfo() {
        return memberInfo;
    }


    /**
     * 根据流返回一个字符串信息         *
     * @param is
     * @return
     * @throws IOException
     */
    private static String getStringFromInputStream(InputStream is)
            throws IOException {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        // 模板代码 必须熟练
        byte[] buffer = new byte[1024];
        int len = -1;
        while ((len = is.read(buffer)) != -1) {
            os.write(buffer, 0, len);
        }
        is.close();
        String state = os.toString();
        os.close();
        return state;
    }


    LoginResult loginResult = new LoginResult();

    private void parserJson(String message) {
        JSONObject object = null;
        JSONObject object1 = null;
        try {
            object = new JSONObject(message);

            syscode = object.optString(Pub.VALUE_SYSCODE);
            sysmsg = object.optString(Pub.VALUE_SYSMSG);
            object1 = object.getJSONObject("data");
            loginResult.setUid(object1.optString("uid"));
            loginResult.setAuthtoken(object1.optString("authtoken"));
            loginResult.setFirst_login(object1.optString("first_login"));
            loginResult.setIs_activity(object1.optString("is_active"));
            loginResult.setSerial_no(object1.optString("serial_no"));
            loginResult.setRecommended_serial_no(object1.optString("recommended_serial_no"));
            loginResult.setUser_grp_id(object1.optString("user_grp_id"));
            loginResult.setVerifing_email(object1.optString("verifing_email"));
            loginResult.setPush_token(object1.optString("push_token"));
            LoginManager.setResult(loginResult);
            PrefConstant.setString(activity, Pub.VALUE_UID, object1.optString("uid"));
            PrefConstant.setString(activity, Pub.VALUE_AUTHTOKEN, object1.optString("authtoken"));
            PrefConstant.setString(activity, Pub.VALUE_FIRST_LOGIN, object1.optString("first_login"));
            PrefConstant.setString(activity, Pub.VALUE_IS_ACTIVE, object1.optString("is_active"));
            PrefConstant.setString(activity, Pub.VALUE_SERIAL_NO, object1.optString("serial_no"));
            PrefConstant.setString(activity, Pub.VALUE_RECOMMENDED_SERIAL_NO, object1.optString("recommended_serial_no"));
            PrefConstant.setString(activity, Pub.VALUE_USER_GRP_ID, object1.optString("user_grp_id"));
            PrefConstant.setString(activity, Pub.VALUE_VERIFING_EMAIL, object1.optString("verifing_email"));
            PrefConstant.setString(activity, Pub.VALUE_PUSHTOEKN, object1.optString("push_token"));
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public boolean ismSuccess() {
        return mSuccess;
    }

    public boolean getIsSysSuccess() {
        return syscode.equals("200");
    }

    public String getSysCode() {
        return syscode;
    }
}



