package com.acloud.app.http;

import android.app.Activity;
import android.content.Context;

import com.acloud.app.model.StoreDetailCouponResult;
import com.acloud.app.tool.Pub;
import com.acloud.app.util.StoreCouponManager;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by skywind-10 on 2015/4/13.
 */
public class HttpStoreCouponList extends BaseHttp {
    Context mContext;
    Activity activity;
    ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
    String TAG = "HttpStoreCouponList";
    String syscode, sysmsg;
    ArrayList<StoreDetailCouponResult> listResults = new ArrayList<StoreDetailCouponResult>();
    boolean isSuccess;

    public HttpStoreCouponList(Activity activity, ArrayList<NameValuePair> list) {
        this.activity = activity;
        this.list = list;
        setTAG(TAG);
    }

    public void call() {
        if (callAPI(Pub.API_STORE_COUPONLIST, list)) {
            parserJson(getResponseString());
            StoreCouponManager.setResultArrayList(listResults);
            isSuccess = true;
        } else {
            isSuccess = false;
        }
    }

    @Override
    public void setTAG(String tag) {
        super.setTAG(tag);
    }

    @Override
    public String getResponseString() {
        return super.getResponseString();
    }

    @Override
    protected boolean callAPI(String url, ArrayList<NameValuePair> list) {
        return super.callAPI(url, list);
    }



    void parserJson(String message) {
        JSONObject object = null;
        JSONArray array = null;

        try {
            object = new JSONObject(message);
            syscode = object.optString(Pub.VALUE_SYSCODE);
            sysmsg = object.optString(Pub.VALUE_SYSMSG);
            if (syscode.equals("200")) {
                array = object.getJSONArray("list");

                for (int i = 0; i < array.length(); i++) {
                    JSONObject object1 = (JSONObject) array.get(i);
                    //用物件去接json的字串
                    StoreDetailCouponResult result = new StoreDetailCouponResult();
                    result.setCoupon_id(object1.optString("coupon_id"));
                    result.setTitle(object1.optString("title"));
                    listResults.add(result);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getSyscode() {
        return syscode;
    }

    public boolean isSuccess() {
        return isSuccess;
    }
}



