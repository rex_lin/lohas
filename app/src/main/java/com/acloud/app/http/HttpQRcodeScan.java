package com.acloud.app.http;

import android.app.Activity;
import android.content.Context;

import com.acloud.app.tool.Pub;
import com.acloud.app.tool.TLog;

import org.apache.http.NameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by skywind-10 on 2015/4/13.
 */
public class HttpQRcodeScan extends BaseHttp {
    Context mContext;
    Activity activity;
    ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
    String TAG = "HttpQRcodeScan";

    public String getSyscode() {
        return syscode;
    }

    String syscode;

    public String getSysmsg() {
        return sysmsg;
    }

    String sysmsg;

    public String getObtain_bonus() {
        return obtain_bonus;
    }

    String obtain_bonus;

    public boolean ismSuccess() {
        return mSuccess;
    }

    boolean mSuccess;

    public HttpQRcodeScan(Activity activity, ArrayList<NameValuePair> list) {
        this.activity = activity;
        this.list = list;
        setTAG(TAG);
    }

    public void call() {
        if (callAPI(Pub.API_QRCODE_SCAN, list)) {
            parserJson(getResponseString());
            mSuccess = true;
        } else {
            mSuccess = false;
        }
    }

    @Override
    public String getResponseString() {
        return super.getResponseString();
    }

    @Override
    public void setTAG(String tag) {
        super.setTAG(tag);
    }

    @Override
    protected boolean callAPI(String url, ArrayList<NameValuePair> list) {
        return super.callAPI(url, list);
    }

    void parserJson(String message) {
        try {

            JSONObject jsonObject = new JSONObject(message);
            syscode = jsonObject.optString("syscode");
            sysmsg = jsonObject.optString("sysmsg");
            jsonObject = jsonObject.getJSONObject("data");
            obtain_bonus = jsonObject.optString("obtain_bonus");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}



