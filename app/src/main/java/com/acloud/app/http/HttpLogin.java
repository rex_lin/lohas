package com.acloud.app.http;

import android.app.Activity;
import android.content.Context;

import com.acloud.app.model.LoginResult;
import com.acloud.app.tool.Pub;
import com.acloud.app.util.LoginManager;
import com.acloud.app.util.PrefConstant;

import org.apache.http.NameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * 登入
 */
public class HttpLogin extends BaseHttp {
    Context mContext;
    Activity activity;
    ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
    int code = -1;
    String TAG = "HttpLogin";
    String syscode, sysmsg;
    boolean mSuccess;

    public HttpLogin(Activity activity, ArrayList<NameValuePair> list) {
        this.activity = activity;
        this.list = list;
        setTAG(TAG);
    }

    public void call() {
        if (callAPI(Pub.API_LOGIN, list)) {
            parserJson(getResponseString());
            mSuccess = true;
        } else {
            mSuccess = false;
        }
    }

    @Override
    public void setTAG(String tag) {
        super.setTAG(tag);
    }

    @Override
    protected boolean callAPI(String url, ArrayList<NameValuePair> list) {
        return super.callAPI(url, list);
    }

    @Override
    public String getResponseString() {
        return super.getResponseString();
    }

    LoginResult loginResult = new LoginResult();

    private void parserJson(String message) {
        JSONObject object = null;
        JSONObject object1 = null;
        try {
            object = new JSONObject(message);

            syscode = object.optString(Pub.VALUE_SYSCODE);
            sysmsg = object.optString(Pub.VALUE_SYSMSG);
            object1 = object.getJSONObject("data");
            loginResult.setUid(object1.optString("uid"));
            loginResult.setAuthtoken(object1.optString("authtoken"));
            loginResult.setFirst_login(object1.optString("first_login"));
            loginResult.setIs_activity(object1.optString("is_active"));
            loginResult.setSerial_no(object1.optString("serial_no"));
            loginResult.setRecommended_serial_no(object1.optString("recommended_serial_no"));
            loginResult.setUser_grp_id(object1.optString("user_grp_id"));
            loginResult.setVerifing_email(object1.optString("verifing_email"));
            loginResult.setPush_token(object1.optString("push_token"));
            LoginManager.setResult(loginResult);
            PrefConstant.setString(activity, Pub.VALUE_UID, object1.optString("uid"));
            PrefConstant.setString(activity, Pub.VALUE_AUTHTOKEN, object1.optString("authtoken"));
            PrefConstant.setString(activity, Pub.VALUE_FIRST_LOGIN, object1.optString("first_login"));
            PrefConstant.setString(activity, Pub.VALUE_IS_ACTIVE, object1.optString("is_active"));
            PrefConstant.setString(activity, Pub.VALUE_SERIAL_NO, object1.optString("serial_no"));
            PrefConstant.setString(activity, Pub.VALUE_RECOMMENDED_SERIAL_NO, object1.optString("recommended_serial_no"));
            PrefConstant.setString(activity, Pub.VALUE_USER_GRP_ID, object1.optString("user_grp_id"));
            PrefConstant.setString(activity, Pub.VALUE_VERIFING_EMAIL, object1.optString("verifing_email"));
            PrefConstant.setString(activity, Pub.VALUE_PUSHTOEKN, object1.optString("push_token"));
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public boolean ismSuccess() {
        return mSuccess;
    }

    public boolean getIsSysSuccess() {
        return syscode.equals("200");
    }

    public String getSysCode() {
        return syscode;
    }
}



