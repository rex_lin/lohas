package com.acloud.app.http;

import android.app.Activity;
import android.content.Context;

import com.acloud.app.model.ActivityDetailResult;
import com.acloud.app.tool.Pub;
import com.acloud.app.util.ActivityDetailManager;

import org.apache.http.NameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by skywind-10 on 2015/4/13.
 */
public class HttpActivityInfo extends BaseHttp {
    Context mContext;
    Activity activity;
    ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
    String TAG = "HttpActivityInfo";

    String syscode;
    String sysmsg;

    boolean mSuccess;
    public HttpActivityInfo(Activity activity, ArrayList<NameValuePair> list) {
        this.activity = activity;
        this.list = list;
    }

    public void call() {
        if (callAPI(Pub.API_ACTIVITY_INFO, list)) {
            parserJson(getResponseString());
            mSuccess = true;
        } else {
            mSuccess =false;
        }
    }

    @Override
    public void setTAG(String tag) {
        super.setTAG(tag);
    }

    @Override
    public String getResponseString() {
        return super.getResponseString();
    }

    @Override
    protected boolean callAPI(String url, ArrayList<NameValuePair> list) {
        return super.callAPI(url, list);
    }

    void parserJson(String message) {
        JSONObject object = null;
        try {
            object = new JSONObject(message);
            syscode = object.optString("syscode");
            sysmsg = object.optString("sysmsg");
            if (syscode.equals("200")){
                object = object.getJSONObject("data");
                ActivityDetailResult result = new ActivityDetailResult();
                result.setAid(object.optString("aid"));
                result.setTitle(object.optString("title"));
                result.setType(object.optString("type"));
                result.setImage_url(object.optString("image_url"));
                result.setContent(object.optString("content"));
                result.setStart_datetime(object.optString("start_datetime"));
                result.setEnd_datetime(object.optString("end_datetime"));
                result.setStatus(object.optString("status"));
                result.setStore_id(object.optString("store_id"));
                result.setStore_name(object.optString("store_name"));
                result.setLat(object.optString("lat"));
                result.setLng(object.optString("lng"));
                result.setDistance(object.optString("distance"));
                result.setPoints(object.optString("points"));
                result.setFb_place_id(object.optString("fb_place_id"));
                ActivityDetailManager.setDetailResult(result);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public String getSyscode() {
        return syscode;
    }

    public boolean ismSuccess() {
        return mSuccess;
    }
}



