package com.acloud.app.http;

import android.app.Activity;
import android.content.Context;

import com.acloud.app.model.CouponListResult;
import com.acloud.app.tool.Pub;
import com.acloud.app.util.CouponListManager;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by skywind-10 on 2015/4/13.
 */
public class HttpCouponList extends BaseHttp {
    Context mContext;
    Activity activity;
    ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
    ArrayList<CouponListResult> listResults = new ArrayList<CouponListResult>();//存入解析完的資料
    String TAG = "HttpCouponList";
    String syscode, sysmsg;
    boolean isSuccess;
    public HttpCouponList(Activity activity, ArrayList<NameValuePair> list) {
        this.activity = activity;
        this.list = list;
        setTAG(TAG);
    }

    public void call() {
        if (callAPI(Pub.API_COUPON_LIST, list)) {
            parserJson(getResponseString());
            isSuccess = true;
            //將解析完成的Arraylist丟到管理CouponListManager裡面
            CouponListManager.setResultArrayList(listResults);
        } else {
            isSuccess = false;
        }
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public boolean isSysSuccess() {
        return syscode.equals("200");
    }


    @Override
    public void setTAG(String tag) {
        super.setTAG(tag);
    }

    @Override
    public String getResponseString() {
        return super.getResponseString();
    }

    @Override
    protected boolean callAPI(String url, ArrayList<NameValuePair> list) {
        return super.callAPI(url, list);
    }

    void parserJson(String message) {
        JSONObject object = null;
        JSONArray array = null;

        try {
            object = new JSONObject(message);
            syscode = object.optString(Pub.VALUE_SYSCODE);
            sysmsg = object.optString(Pub.VALUE_SYSMSG);
            if (syscode.equals("200")) {
                array = object.getJSONArray("list");

                for (int i = 0; i < array.length(); i++) {
                    JSONObject object1 = (JSONObject)array.get(i);
                    //用物件去接json的字串
                    CouponListResult result = new CouponListResult();
                    result.setStore_name(object1.optString("store_name"));
                    result.setCoupon_id(object1.optString("coupon_id"));
                    result.setStore_id(object1.optString("store_id"));
                    result.setDistance(object1.optString("distance"));
                    result.setLat(object1.optString("lat"));
                    result.setLng(object1.optString("lng"));
                    result.setTitle(object1.optString("title"));
                    result.setPoints(object1.optString("points"));
                    result.setStart_datetime(object1.optString("start_datetime"));
                    result.setEnd_datetime(object1.optString("end_datetime"));
                    result.setUsed_start_datetime(object1.optString("used_start_datetime"));
                    result.setUsed_end_datetime(object1.optString("used_end_datetime"));
                    result.setThumbnail_url(object1.optString("thumbnail_url"));
                    listResults.add(result);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}



