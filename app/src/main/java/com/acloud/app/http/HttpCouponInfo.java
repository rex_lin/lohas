package com.acloud.app.http;

import android.app.Activity;
import android.content.Context;

import com.acloud.app.model.CouponDetailResult;
import com.acloud.app.tool.Pub;
import com.acloud.app.util.CouponDetailManager;

import org.apache.http.NameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by skywind-10 on 2015/4/13.
 */
public class HttpCouponInfo extends BaseHttp {
    Context mContext;
    Activity activity;
    ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
    String TAG = "HttpCouponInfo";
    String syscode;
    String sysmsg;
    CouponDetailResult result = new CouponDetailResult();
    boolean isSuccess;

    public HttpCouponInfo(Activity activity, ArrayList<NameValuePair> list) {
        this.activity = activity;
        this.list = list;
        setTAG(TAG);
    }

    public void call() {
        if (callAPI(Pub.API_COUPON_INFO, list)) {
            parserJson(getResponseString());
            CouponDetailManager.setCouponDetailResult(result);
            isSuccess = true;
        } else {
            isSuccess = false;
        }
    }

    @Override
    public void setTAG(String tag) {
        super.setTAG(tag);
    }

    @Override
    public String getResponseString() {
        return super.getResponseString();
    }

    @Override
    protected boolean callAPI(String url, ArrayList<NameValuePair> list) {
        return super.callAPI(url, list);
    }

    void parserJson(String message) {
        JSONObject object;
        try {
            object = new JSONObject(message);
            syscode = object.optString(Pub.VALUE_SYSCODE);
            sysmsg = object.optString(Pub.VALUE_SYSCODE);
            if (syscode.equals("200")) {
                object = object.getJSONObject("data");

                result.setCoupon_id(object.optString("coupon_id"));
                result.setStore_id(object.optString("store_id"));
                result.setStore_name(object.optString("store_name"));
                result.setDistance(object.optString("distance"));
                result.setTitle(object.optString("title"));
                result.setContent(object.optString("content"));
                result.setPoints(object.optString("points"));
                result.setImage_url(object.optString("image_url"));
                result.setStart_datetime(object.optString("start_datetime"));
                result.setEnd_datetime(object.optString("end_datetime"));
                result.setUsed_start_datetime(object.optString("used_start_datetime"));
                result.setUsed_end_datetime(object.optString("used_end_datetime"));
                result.setExchange_limit_amount(object.optString("exchange_limit_amount"));
                result.setRemaining_amount(object.optString("remaining_amount"));
                result.setAllow_repeat(object.optString("allow_repeat"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public boolean isSuccess() {
        return isSuccess;
    }
}



