package com.acloud.app.http;

import android.app.Activity;
import android.content.Context;

import com.acloud.app.tool.Pub;

import org.apache.http.NameValuePair;

import java.util.ArrayList;

/**
 *
 */
public class HttpLoginVerifyEmail extends BaseHttp {
    Context mContext;
    Activity activity;
    ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
    int code = -1;
    String responseString;
    String TAG = "HttpLoginVerifyEmail";

    public HttpLoginVerifyEmail(Activity activity, ArrayList<NameValuePair> list) {
        this.activity = activity;
        this.list = list;
        setTAG(TAG);
    }

    public String call() {
        if (callAPI(Pub.API_VERIFYEMAIL,list)) {
            return getResponseString();
        } else {
            return "false";
        }
    }

    @Override
    public void setTAG(String tag) {
        super.setTAG(tag);
    }

    @Override
    public String getResponseString() {
        return super.getResponseString();
    }

    @Override
    protected boolean callAPI(String url, ArrayList<NameValuePair> list) {
        return super.callAPI(url, list);
    }
}



