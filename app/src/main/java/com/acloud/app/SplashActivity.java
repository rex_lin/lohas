package com.acloud.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.acloud.app.adapter.OperationPageAdapter;
import com.acloud.app.tool.Pub;
import com.acloud.app.tool.TLog;
import com.acloud.app.util.BitmapHandler;
import com.acloud.app.util.PrefConstant;
import com.acloud.app.util.Util;
import com.huanuage.basenewfoundtion.LoginActivity;

/**
 * Created by skywind-10 on 2015/7/20.
 */
public class SplashActivity extends Activity {

    private String TAG = "SplashActivity";
    Handler handler = new Handler();
    Runnable runnable;
    LinearLayout points;
    ViewPager viewPager;
    int[] Images = new int[]{R.drawable.operation1, R.drawable.operation2, R.drawable.operation3, R.drawable.operation4,
            R.drawable.operation5};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        runnable = new Runnable() {
            @Override
            public void run() {
                TLog.d("ooxx",this.getClass().getName() +" "+ "run");
//                if (PrefConstant.getString(SplashActivity.this, Pub.VALUE_FIRST_USE, "").equals("")) {
//                    setContentView(R.layout.activity_operation);
//                    initOpertaionView();
//                    PrefConstant.setString(SplashActivity.this, Pub.VALUE_FIRST_USE, "1");
//                } else {
//                    Intent i = new Intent();
//                    i.setClass(SplashActivity.this, MainActivity.class);
//                    startActivity(i);
//                    finish();
//                }
                if (PrefConstant.getString(SplashActivity.this, Pub.VALUE_FIRST_USE, "").equals("")) {
                    setContentView(R.layout.activity_operation);
                    initOpertaionView();
                    PrefConstant.setString(SplashActivity.this, Pub.VALUE_FIRST_USE, "1");
                } else {
                    Intent i = new Intent();
                    i.setClass(SplashActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }

            }
        };
        handler.postDelayed(runnable, 4000);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        TLog.i("Ernest", " onPause");
        handler.removeCallbacks(runnable);
    }

    @Override
    protected void onStop() {
        super.onStop();
        TLog.i("Ernest", " onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        TLog.i("Ernest", " onDestroy");
    }

    private void initOpertaionView() {
        viewPager = (ViewPager) findViewById(R.id.viewPagerOperation);
        viewPager.setAdapter(new OperationPageAdapter(SplashActivity.this, Images));
        points = (LinearLayout) findViewById(R.id.points);
        final Button btn = (Button) findViewById(R.id.btnOk);
        final TextView txtSkip = (TextView) findViewById(R.id.txtSkip);
        initPoints();
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                updatePointAndText();
                if (viewPager.getCurrentItem() == Images.length - 1) {
                    btn.setVisibility(View.VISIBLE);
                    txtSkip.setVisibility(View.INVISIBLE);
                } else {
                    btn.setVisibility(View.INVISIBLE);
                    txtSkip.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setClass(SplashActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        });
        txtSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setClass(SplashActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        });
    }


    private void initPoints() {
        for (int i = 0; i < Images.length; i++) {
            View view = new View(SplashActivity.this);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    (int) BitmapHandler.convertDpToPixel(15, SplashActivity.this)
                    , (int) BitmapHandler.convertDpToPixel(15, SplashActivity.this));
            if (i != 0) {
                params.leftMargin = 15;
            }
            view.setLayoutParams(params);
            view.setBackgroundResource(R.drawable.selector_points_pink);
            points.addView(view);
        }
    }

    private void updatePointAndText() {
        int currentItem = viewPager.getCurrentItem() % Images.length;
        TLog.i("Ernest", "imgGallery currentItem : " + currentItem);
        for (int i = 0; i < points.getChildCount(); i++) {
            points.getChildAt(i).setEnabled(currentItem == i);
        }
    }
}
