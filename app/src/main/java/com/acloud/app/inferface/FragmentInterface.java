package com.acloud.app.inferface;


import android.os.Handler;

public interface FragmentInterface {
    public Object getObject(int n);
    public void  startOtherActive(Handler h);
    public void startOtherActive(Handler h,Object obj) ;
    public void  clickAccept(Object obj);
    public void  clickAccept(Object obj,String item_position);
    public void  clickCancel();
}
