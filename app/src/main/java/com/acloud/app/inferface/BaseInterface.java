package com.acloud.app.inferface;


import android.support.v4.app.Fragment;

public interface BaseInterface {

    public void showErrorDialog(String code);
    public void clickItem(final String itemId);
    public void longClickItem(final String itemId);
    public void changeFragment(Fragment fragment);
    public void changeBackFragment(Fragment fragment);

}
