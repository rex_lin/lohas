package com.acloud.app.tool;

/**
 * Created by skywind-10 on 2015/6/5.
 */
public class Pub {
    public static final boolean isDebug = true;
    //TODO delete Debug data
    public static final String GCM_PROJECT_NUMBER = "205333972564";
    public static final int SERVER_PORT = 80;
    public static final int SERVER_PORT_SSL = 80;
    public static final String APIKEY = "8q6G2YMOPtpuRM5SoFvY14J93Z95D5Iu";
    public static final String RETURN_VALUE_KEY = "rtValue";

//    public static final String API_SERVER = "http://h02.myflysoft.com:8017/";
    public static final String API_SERVER = "http://skywindstg.cloudapp.net/";
    public static final String API_STROE_SERVER = "http://shop.lohas-tst.huanuage-sky.com/shop.aspx";

    public static final String API_GET_MEMBER_INFO = "http://lohas-tst.huanuage-sky.com:8888/api/SkyWind/MemberInfo";
    public static final String API_LOGIN = API_SERVER + "api/User/FacebookLogin";
    public static final String API_HUANUAGELOGIN = API_SERVER + "api/User/HuanuageLogin";
    public static final String API_VERIFYEMAIL = API_SERVER + "api/User/FacebookLoginVerifyEmail";
    public static final String API_RESENT_VERFYEMAIL = API_SERVER + "api/User/ResendVerifyEmail";
    public static final String API_UPDATE_VERIFYEMAIL = API_SERVER + "api/User/UpdateVerifyEmail";
    public static final String API_CHECK_USERVERIFY = API_SERVER + "api/User/CheckUserVerify";

    public static final String API_USERINFO = API_SERVER + "api/User/GetUserProfile";
    public static final String API_USERINFO_UPDATE = API_SERVER + "api/User/UpdateUserProfile";
    public static final String API_USEREMAIL_UPDATE = API_SERVER + "api/User/UpdateEmail";
    public static final String API_CHECK_DEVICELOGIN = API_SERVER + "api/User/CheckSomeDeviceLogin";
    public static final String API_CURRENTBONUS_GET = API_SERVER + "api/User/GetCurrentBonus";

    public static final String API_BONUSLIST_GET = API_SERVER + "api/User/GetBonusRecordList";
    public static final String API_RECOMMENDED_UPDATE = API_SERVER + "api/User/SettingRecommended";

    public static final String API_EXCHANGE_BONUS_SERIAL_NO = API_SERVER + "api/User/ExchangeBonusSerialNo";
    public static final String API_SWITCH_PUSH = API_SERVER + "api/User/SwitchPush";
    public static final String API_UPDATE_PUSHTOKEN = API_SERVER + "api/Push/UpdateToken";

    public static final String API_STORELIST = API_SERVER + "api/Store/List";
    public static final String API_STOREINFO = API_SERVER + "api/Store/Info";
    public static final String API_STORE_COUPONLIST = API_SERVER + "api/Store/CouponList";
    public static final String API_STORE_BROWSE = API_SERVER + "api/Store/PageView";
    public static final String API_STORE_UPDATERATING = API_SERVER + "api/StoreRating/UpdateRating";
    public static final String API_STORE_UPDATEFAVORITES = API_SERVER + "api/StoreFavorites/UpdateFavorites";

    public static final String API_STORE_FAVORITESLIST = API_SERVER + "api/StoreFavorites/List";
    public static final String API_MENU_VERSIONS = API_SERVER + "api/Menus/GetMenuVersions";

    public static final String API_MENU_SHOPPINGDISTRICT = API_SERVER + "api/Menus/ShoppingDistrictMenu";
    public static final String API_MENU_STORECATEGORY = API_SERVER + "api/Menus/StoreCategoryMenu";
    public static final String API_COUPON_LIST = API_SERVER + "api/Coupon/List";
    public static final String API_COUPON_INFO = API_SERVER + "api/Coupon/Info";
    public static final String API_COUPON_EXCHANGE = API_SERVER + "api/Coupon/ExchangeCoupon";

    public static final String API_MYCOUPON_LIST = API_SERVER + "api/MyCoupon/List";
    public static final String API_MYCOUPON_INFO = API_SERVER + "api/MyCoupon/Info";
    public static final String API_MYCOUPON_USED = API_SERVER + "api/MyCoupon/UseMyCoupon";

    public static final String API_ACTIVITY_LIST = API_SERVER + "api/Activity/List";
    public static final String API_ACTIVITY_INFO = API_SERVER + "api/Activity/Info";
    public static final String API_ACTIVITY_FINISH = API_SERVER + "api/Activity/FinishActivityProcess";
    public static final String API_QRCODE_SCAN = API_SERVER + "api/Activity/ScanQRCodeActivityProcess";

    public static final String API_CHECK_RATING = API_SERVER + "api/StoreRating/CheckRating";
    public static final String API_POINT_RATING = API_SERVER + "api/StoreRating/PointRating";

    public static final String API_GET_FAV_STORE_IDS = API_SERVER + "api/StoreFavorites/GetFavStoreIds";


    public static final String URL_ONLINE_SHOP = "http://shop.lohas-tst.huanuage-sky.com/shop.aspx";
    public static final String URL_HAPPY_FUN_WEB = "http://event.lohas-tst.huanuage-sky.com/web/index.php?r=baoming";
    public static final String URL_ABOUT_US= "http://skywindstg.cloudapp.net/aboutus.aspx";

    public static final String VALUE_APIKEY = "apikey";
    public static final String VALUE_EMAIL = "email";
    public static final String VALUE_FBID = "fbid";
    public static final String VALUE_ACCESS_TOKEN = "access_token";
    public static final String VALUE_UID = "uid";
    public static final String VALUE_VERIFYEMAIL = "verify_email";
    public static final String VALUE_AUTHTOKEN = "authtoken";
    public static final String VALUE_NAME = "name";
    public static final String VALUE_PHONE = "phone";
    public static final String VALUE_BIRTHDAY = "birthday";
    public static final String VALUE_GENDER = "gender";
    public static final String VALUE_MEMO = "memo";
    public static final String VALUE_LAT = "lat";
    public static final String VALUE_LNG = "lng";
    public static final String VALUE_LAST_PUSH_TOKEN = "last_push_token";
    public static final String VALUE_INDEX = "index";
    public static final String VALUE_SIZE = "size";
    public static final String VALUE_RECOMMENDED_SERIAL_NO = "recommended_serial_no";

    public static final String VALUE_BOUNUS_SERIAL_NO = "bounus_serial_no";
    public static final String VALUE_PUSH_SWITCH = "push_switch";
    public static final String VALUE_APPID = "app_id";
    public static final String VALUE_PUSHTOEKN = "push_token";
    public static final String VALUE_PUSHSERVER = "push_server";
    public static final String VALUE_KEYWORD = "keyword";
    public static final String VALUE_SDID = "sd_id";
    public static final String VALUE_SMID = "sm_id";
    public static final String VALUE_SCID = "sc_id";
    public static final String VALUE_SCAID = "sca_id";
    public static final String VALUE_SORT_TYPE = "sort_type";
    public static final String VALUE_PRICE_RANGE_START = "price_range_start";
    public static final String VALUE_PRICE_RANGE_END = "price_range_end";
    public static final String VALUE_STOREID = "store_id";
    public static final String VALUE_RATING = "rating";
    public static final String VALUE_ACTION = "action";
    public static final String VALUE_COUPON_ID = "coupon_id";
    public static final String VALUE_TYPE = "type";
    public static final String VALUE_EX_COUPON_ID = "ex_coupon_id";
    public static final String VALUE_VERIFY_CODE = "verify_code";
    public static final String VALUE_AID = "aid";
    public static final String VALUE_ACTIVITY_ID = "activity_id";
    public static final String VALUE_QRCODE_ID = "qr_code_id";
    public static final String VALUE_SYSCODE = "syscode";
    public static final String VALUE_SYSMSG = "sysmsg";
    public static final String VALUE_FIRST_LOGIN = "first_login";
    public static final String VALUE_IS_ACTIVE = "is_active";
    public static final String VALUE_SERIAL_NO = "serial_no";
    public static final String VALUE_USER_GRP_ID = "user_grp_id";
    public static final String VALUE_VERIFING_EMAIL = "verifing_email";
    public static final String VALUE_BONUS = "bonus";
    public static final String VALUE_SHOPPING_DISTRICT = "shopping_district";
    public static final String VALUE_STORE_CATEGORY = "store_category";
    public static final String VALUE_MENU_SHOPPING_DISTRICT = "menu_shopping_district";
    public static final String VALUE_MENU_STORE_CATEGORY = "menu_store_category";
    public static final String VALUE_FIRST_USE = "first_use";
    public static final String VALUE_QUERYTEXT = "querytext";
    public static final String VALUE_HEIGHT_PRICE = "HeighPrice";
    public static final String VALUE_LOW_PRICE = "LowPrice";

    public static final String ACTION_REMOVE = "remove";
    public static final String ACTION_ADD = "add";

    public static final String SP_PROFILE = "profile";

    public static final String TEMP_FAV = "temp_fav_data";
    public static final String IS_LOGIN = "is_login";

    public static final String APPURL = "https://play.google.com/store/apps/details?id=com.acloud.app&hl=zh-TW";

    public static final int SORT_DISTANCE = 1;
    public static final int SORT_EVALUATE = 2;
    public static final int SORT_COLLATION = 3;
    public static final int SORT_PRICE =4;
    public static final int BUTTON_OK = 0;
    public static final int BUTTON_SORT = 1;
    public static final int SEND_CATEGORY = 2;
    public static final int SEND_DISTRICT = 3;
    public static final int SEND_SORT =4;
    public static final int CATEGORY_ALL = 0;
    public static final int CATEGORY_PUNCH = 1;
    public static final int CATEGORY_VIEWSTORE = 2;
    public static final int CATEGORY_QRCODE = 3;
    public static final int CATEGORY_ARGAME = 4;
    public static final int SORT_ACTIVITY_DATE = 1;
    public static final int SORT_ACTIVITY_DISTANCE = 2;
    public static final int SORT_ACTIVITY_BONUS = 3;
}
