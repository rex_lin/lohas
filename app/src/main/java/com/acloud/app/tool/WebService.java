package com.acloud.app.tool;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;

/**
 * Call API, NameValuePair
 */
public class WebService {

    public static ArrayList<NameValuePair> getFBIDEmail(String fbid, String email, String access_token) {
        ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
        list.add(new BasicNameValuePair(Pub.VALUE_APIKEY, Pub.APIKEY));
        list.add(new BasicNameValuePair(Pub.VALUE_FBID, fbid));
        list.add(new BasicNameValuePair(Pub.VALUE_ACCESS_TOKEN, access_token));
        list.add(new BasicNameValuePair(Pub.VALUE_EMAIL, email));
        return list;
    }

    public static ArrayList<NameValuePair> getHuanuageLoginList(String uid, String email, String access_token) {
        ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
        list.add(new BasicNameValuePair(Pub.VALUE_APIKEY, Pub.APIKEY));
        list.add(new BasicNameValuePair(Pub.VALUE_UID, uid));
        list.add(new BasicNameValuePair(Pub.VALUE_ACCESS_TOKEN, access_token));
        list.add(new BasicNameValuePair(Pub.VALUE_EMAIL, email));
        TLog.d("ooxx","getHuanuageLoginList : " + list);
        return list;
    }



    public static ArrayList<NameValuePair> getUid_AuthToken(String uid, String authtoken) {
        ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
        list.add(new BasicNameValuePair(Pub.VALUE_APIKEY, Pub.APIKEY));
        list.add(new BasicNameValuePair(Pub.VALUE_UID, uid));
        list.add(new BasicNameValuePair(Pub.VALUE_AUTHTOKEN, authtoken));

        return list;
    }

    /**
     * Update User Email
     * @param uid
     * @param authtoken
     * @param email
     * @param lat
     * @param lng
     * @return
     */
    public static ArrayList<NameValuePair> getUid_AuthToken_Email(
            String uid, String authtoken,String email,String lat,String lng) {
        ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
        list.add(new BasicNameValuePair(Pub.VALUE_APIKEY, Pub.APIKEY));
        list.add(new BasicNameValuePair(Pub.VALUE_UID, uid));
        list.add(new BasicNameValuePair(Pub.VALUE_AUTHTOKEN, authtoken));
        list.add(new BasicNameValuePair(Pub.VALUE_EMAIL, email));
        list.add(new BasicNameValuePair(Pub.VALUE_LAT, lat));
        list.add(new BasicNameValuePair(Pub.VALUE_LNG, lng));
        return list;
    }
    /**
     * UpdateVerifyEmail
     * @param uid
     * @param authtoken
     * @param verifyemail
     * @return
     */
    public static ArrayList<NameValuePair> getUid_AuthToken_VERIFYEMAIL(String uid,
                                                                        String authtoken, String verifyemail) {
        ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
        list.add(new BasicNameValuePair(Pub.VALUE_APIKEY, Pub.APIKEY));
        list.add(new BasicNameValuePair(Pub.VALUE_UID, uid));
        list.add(new BasicNameValuePair(Pub.VALUE_AUTHTOKEN, authtoken));
        list.add(new BasicNameValuePair(Pub.VALUE_VERIFYEMAIL, verifyemail));
        return list;
    }

    /**
     * UserProfile
     * @param uid
     * @param authtoken
     * @param name
     * @param phone
     * @param birthday
     * @param gender
     * @param memo
     * @param lat
     * @param lng
     * @return
     */
    public static ArrayList<NameValuePair> getUser_Info(String uid, String authtoken,
                                                        String name, String phone, String birthday,
                                                        String gender, String memo, String lat, String lng) {
        ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
        list.add(new BasicNameValuePair(Pub.VALUE_APIKEY, Pub.APIKEY));
        list.add(new BasicNameValuePair(Pub.VALUE_UID, uid));
        list.add(new BasicNameValuePair(Pub.VALUE_AUTHTOKEN, authtoken));
        list.add(new BasicNameValuePair(Pub.VALUE_NAME, name));
        list.add(new BasicNameValuePair(Pub.VALUE_PHONE, phone));
        list.add(new BasicNameValuePair(Pub.VALUE_BIRTHDAY, birthday));
        list.add(new BasicNameValuePair(Pub.VALUE_GENDER, gender));
        list.add(new BasicNameValuePair(Pub.VALUE_MEMO, memo));
        list.add(new BasicNameValuePair(Pub.VALUE_LAT, lat));
        list.add(new BasicNameValuePair(Pub.VALUE_LNG, lng));
        return list;
    }

    /**
     * getBonusRecord
     * @param uid
     * @param authtoken
     * @param index
     * @param size
     * @return
     */
    public static ArrayList<NameValuePair> getUid_AuthToken_INDEX_SIZE(String uid,
                                                                       String authtoken, String index, String size) {
        ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
        list.add(new BasicNameValuePair(Pub.VALUE_APIKEY, Pub.APIKEY));
        list.add(new BasicNameValuePair(Pub.VALUE_UID, uid));
        list.add(new BasicNameValuePair(Pub.VALUE_AUTHTOKEN, authtoken));
        list.add(new BasicNameValuePair(Pub.VALUE_INDEX, index));
        list.add(new BasicNameValuePair(Pub.VALUE_SIZE, size));
        return list;
    }

    public static ArrayList<NameValuePair> getUid_AuthToken_RecommandedSerialNo(
            String uid, String authtoken, String serialno, String lat, String lng) {
        ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
        list.add(new BasicNameValuePair(Pub.VALUE_APIKEY, Pub.APIKEY));
        list.add(new BasicNameValuePair(Pub.VALUE_UID, uid));
        list.add(new BasicNameValuePair(Pub.VALUE_AUTHTOKEN, authtoken));
        list.add(new BasicNameValuePair(Pub.VALUE_RECOMMENDED_SERIAL_NO, serialno));
        list.add(new BasicNameValuePair(Pub.VALUE_LAT, lat));
        list.add(new BasicNameValuePair(Pub.VALUE_LNG, lng));
        return list;
    }

    /**
     * Update PushToken
     * @param uid
     * @param authtoken
     * @param appid
     * @param pushtoken
     * @param pushserver
     * @param lat
     * @param lng
     * @return
     */
    public static ArrayList<NameValuePair> getUid_AuthToken_AppId_PushToken_PushServer
            (String uid, String authtoken, String appid, String pushtoken,
             String pushserver, String lat, String lng) {
        ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
        list.add(new BasicNameValuePair(Pub.VALUE_APIKEY, Pub.APIKEY));
        list.add(new BasicNameValuePair(Pub.VALUE_UID, uid));
        list.add(new BasicNameValuePair(Pub.VALUE_AUTHTOKEN, authtoken));
        list.add(new BasicNameValuePair(Pub.VALUE_APPID, appid));
        list.add(new BasicNameValuePair(Pub.VALUE_PUSHTOEKN, pushtoken));
        list.add(new BasicNameValuePair(Pub.VALUE_PUSHSERVER, pushserver));
        list.add(new BasicNameValuePair(Pub.VALUE_LAT, lat));
        list.add(new BasicNameValuePair(Pub.VALUE_LAT, lng));
        return list;
    }

    public static ArrayList<NameValuePair> getMyCoupon_Use
            (String uid, String authtoken, String ex_coupon_id, String verify_code,
             String lat, String lng) {

        ArrayList<NameValuePair> list = new ArrayList<NameValuePair>();
        list.add(new BasicNameValuePair(Pub.VALUE_APIKEY, Pub.APIKEY));
        list.add(new BasicNameValuePair(Pub.VALUE_UID, uid));
        list.add(new BasicNameValuePair(Pub.VALUE_AUTHTOKEN, authtoken));
        list.add(new BasicNameValuePair(Pub.VALUE_VERIFY_CODE, verify_code));
        list.add(new BasicNameValuePair(Pub.VALUE_EX_COUPON_ID, ex_coupon_id));
        list.add(new BasicNameValuePair(Pub.VALUE_LAT, lat));
        list.add(new BasicNameValuePair(Pub.VALUE_LNG, lng));
        return list;
    }
    }
