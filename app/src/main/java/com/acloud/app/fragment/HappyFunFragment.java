package com.acloud.app.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.acloud.app.MainActivity;
import com.acloud.app.R;
import com.acloud.app.superclass.BaseFragment;
import com.acloud.app.tool.Pub;

/**
 * Setting Fragment
 */
public class HappyFunFragment extends BaseFragment {
    WebView webView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((MainActivity) getActivity()).setTitle(getString(R.string.drawer_item13));
        View rootView = inflater.inflate(R.layout.fragment_happy_fun, container, false);
        initUI(rootView);

        setAction();
        return rootView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    private void initUI(View view) {
        webView = (WebView) view.findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.loadUrl(Pub.URL_HAPPY_FUN_WEB);
        webView.requestFocus();
        webView.setWebViewClient(new WebViewClient());
    }

    private void setAction() {
    }


    public boolean canGoBack() {
        boolean canGoBack = webView.canGoBack();
        if(canGoBack){
            webView.goBack();
        }
        return canGoBack;
    }


}
