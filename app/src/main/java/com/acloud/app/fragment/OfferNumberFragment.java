package com.acloud.app.fragment;

import android.app.Activity;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.acloud.app.MainActivity;
import com.acloud.app.R;
import com.acloud.app.model.CommonStruct;
import com.acloud.app.superclass.BaseFragment;
import com.acloud.app.thread.ExchangeBonusSerialNoThread;
import com.acloud.app.tool.Pub;
import com.acloud.app.util.LocatUtil;
import com.acloud.app.util.PrefConstant;

/**
 * 優惠序號
 */
public class OfferNumberFragment extends BaseFragment {

    String TAG = "ShareFragment";
    Location location;
    LocatUtil locatUtil;
    Activity mActivity;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((MainActivity) getActivity()).setTitle(getString(R.string.txt_number));
        View rootView = inflater.inflate(R.layout.fragment_number
                , container, false);
        initUI(rootView);
        locatUtil = ((MainActivity) getActivity()).getLocatUtil();
        location = locatUtil.getUpdatedLocation();
        return rootView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    private void initUI(View view) {
        final EditText editText = (EditText) view.findViewById(R.id.editText);
        TextView sendBtn = (TextView) view.findViewById(R.id.tvSend);

        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new ExchangeBonusSerialNoThread(mActivity,
                        PrefConstant.getString(mActivity, Pub.VALUE_UID, ""),
                        PrefConstant.getString(mActivity, Pub.VALUE_AUTHTOKEN, ""),
                        editText.getText().toString(),
                        location != null ? location.getLatitude() + "" : "0",
                        location != null ? location.getLongitude() + "" : "0",
                        new Handler() {
                            @Override
                            public void handleMessage(Message msg) {
                                super.handleMessage(msg);
                                switch (msg.what) {
                                    case 1:
                                        if ( ((CommonStruct) msg.getData().getSerializable(Pub.RETURN_VALUE_KEY)).getApi().equals("200")){
                                            //TODO success
                                            Toast.makeText(mActivity, getString(R.string.txt_input_number_success),Toast.LENGTH_SHORT).show();
                                        }else if ( ((CommonStruct) msg.getData().getSerializable(Pub.RETURN_VALUE_KEY)).getApi().equals("400")){
                                            Toast.makeText(mActivity, getString(R.string.txt_input_number_error),Toast.LENGTH_SHORT).show();
                                        }
                                        break;
                                    case 0:
                                        break;
                                }
                            }
                        }
                ).start();
            }
        });
    }
}
