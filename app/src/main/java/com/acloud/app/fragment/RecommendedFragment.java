package com.acloud.app.fragment;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.acloud.app.MainActivity;
import com.acloud.app.R;
import com.acloud.app.http.HttpRecommendedUpdate;
import com.acloud.app.tool.Pub;
import com.acloud.app.tool.WebService;
import com.acloud.app.util.PrefConstant;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * 輸入推薦人頁
 */
public class RecommendedFragment extends Fragment implements View.OnClickListener {

    String TAG = "RecommendedFragment";
    Activity mActivity;
    EditText editText;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_recommended, container, false);
        TextView tvSend = (TextView) rootView.findViewById(R.id.tvSend);
        TextView tvPass = (TextView) rootView.findViewById(R.id.tvPass);
        editText = (EditText) rootView.findViewById(R.id.editText);
        tvSend.setOnClickListener(this);
        tvPass.setOnClickListener(this);
        ((MainActivity) mActivity).setTitle(R.string.title_recommend);
        return rootView;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvSend:
                QueryRecommendedTask mQueryRecommendedTask = new QueryRecommendedTask();
                mQueryRecommendedTask.execute();
                break;
            case R.id.tvPass:
                if (PrefConstant.getString(mActivity, Pub.VALUE_FIRST_LOGIN, "").equals("1")) {
                    ((MainActivity) mActivity).changeFragment(new ProfileSendFragment());
                } else {
                    ((MainActivity) mActivity).getmNavigationDrawerFragment().onNavigationDrawerItemSelected(MainActivity.MAIN_PAGE);
                }

                break;
        }
    }

    public class QueryRecommendedTask extends AsyncTask {
        String jsonString;
        String syscode, sysmsg;

        public QueryRecommendedTask() {
        }

        @Override
        protected void onPostExecute(Object o) {
            if (syscode.equals("200")) {
                if (PrefConstant.getString(mActivity, Pub.VALUE_FIRST_LOGIN, "").equals("1")) {
                    ((MainActivity) getActivity()).changeFragment(new ProfileSendFragment());
                } else {
                    ((MainActivity) mActivity).getmNavigationDrawerFragment().onNavigationDrawerItemSelected(MainActivity.MAIN_PAGE);
                    //TODO 流程會有問題
                }
            } else if (syscode.equals("400")) {
                Toast.makeText(mActivity, "參數錯誤 : " + sysmsg, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(mActivity, "錯誤code: " + syscode + " msg : " + sysmsg, Toast.LENGTH_SHORT).show();
            }
            super.onPostExecute(o);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            HttpRecommendedUpdate recommendedUpdateh = new HttpRecommendedUpdate(getActivity(),
                    WebService.getUid_AuthToken_RecommandedSerialNo(
                            PrefConstant.getString(mActivity, Pub.VALUE_UID, ""),
                            PrefConstant.getString(mActivity, Pub.VALUE_AUTHTOKEN, ""),
                            editText.getText().toString(),
                            PrefConstant.getString(mActivity, Pub.VALUE_LAT, "0"),
                            PrefConstant.getString(mActivity, Pub.VALUE_LNG, "0")
                    ));
            jsonString = recommendedUpdateh.call();
            parserJson(jsonString);
            return null;
        }

        void parserJson(String message) {
            JSONObject object = null;

            try {
                object = new JSONObject(message);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            syscode = object.optString(Pub.VALUE_SYSCODE);
            sysmsg = object.optString(Pub.VALUE_SYSMSG);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        mActivity = activity;
        super.onAttach(activity);
    }
}
