package com.acloud.app.fragment;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.acloud.app.MainActivity;
import com.acloud.app.R;
import com.acloud.app.http.HttpLoginVerifyEmail;
import com.acloud.app.inferface.BaseInterface;
import com.acloud.app.tool.TLog;
import com.acloud.app.tool.Pub;
import com.acloud.app.tool.WebService;
import com.acloud.app.util.PrefConstant;

import org.json.JSONException;
import org.json.JSONObject;


public class SendVerifyFragment extends Fragment {

    String TAG = "SendVerifyFragment";
    EditText mEditText;
    Activity mActivity;
    static String mFbid;
    static String mAccessToken;

    public static Fragment newInstance(String fbid, String accesstoken) {
        SendVerifyFragment fragment = new SendVerifyFragment();
        mFbid = fbid;
        mAccessToken = accesstoken;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_sendverify, container, false);
        TextView tvBtn = (TextView) rootView.findViewById(R.id.tvBtn);
        mEditText = (EditText) rootView.findViewById(R.id.editText);
        tvBtn.setOnClickListener(btnOnClickListener);
        return rootView;
    }

    private View.OnClickListener btnOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            QuerySendVerifyTask mQuerySendVerifyTask = new QuerySendVerifyTask(mFbid, mEditText.getText().toString(),
                    mAccessToken);
            mQuerySendVerifyTask.execute();
        }
    };

    public class QuerySendVerifyTask extends AsyncTask {
        String fbid, email;
        String jsonString;
        String syscode;
        String sysmsg;
        String accesstoken;
        public QuerySendVerifyTask(String fbid, String email,String accesstoken) {
            this.fbid = fbid;
            this.email = email;
            this.accesstoken = accesstoken;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            if (syscode == null)
                return;

            TLog.e(TAG, "syscode : " + syscode);
            if (syscode.equals("200")) {
                ((MainActivity) mActivity).getmNavigationDrawerFragment().onNavigationDrawerItemSelected(MainActivity.MAIN_PAGE);
                Toast.makeText(mActivity, "請到信箱收驗證信", Toast.LENGTH_SHORT).show();
            } else if (syscode.equals("400")) {
                Toast.makeText(mActivity, "請輸入e-mail", Toast.LENGTH_SHORT).show();
            }
            //當第一次登入是無法抓到Email再使用者輸入完Email後會跳到首頁並且通知使用者收驗證信
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            HttpLoginVerifyEmail loginVerifyEmail = new HttpLoginVerifyEmail(getActivity(),
                    WebService.getFBIDEmail(fbid, email, accesstoken));

            jsonString = loginVerifyEmail.call();
            parserJson(jsonString);

            return null;
        }

        private void parserJson(String message) {
            JSONObject object = null;
            JSONObject array = null;

            try {
                object = new JSONObject(message);

                syscode = object.optString(Pub.VALUE_SYSCODE);
                sysmsg = object.optString(Pub.VALUE_SYSMSG);
                if (syscode.equals("200")) {
                    array = object.getJSONObject("data");
                    TLog.i(TAG, array.optString("uid"));
                    TLog.i(TAG, array.optString("authtoken"));
                    TLog.i(TAG, array.optString("first_login"));
                    TLog.i(TAG, array.optString("is_active"));
                    TLog.i(TAG, array.optString("serial_no"));
                    TLog.i(TAG, array.optString("recommended_serial_no"));
                    TLog.i(TAG, array.optString("user_grp_id"));
                    TLog.i(TAG, array.optString("verifing_email"));

                    PrefConstant.setString(mActivity, Pub.VALUE_UID, array.optString("uid"));
                    PrefConstant.setString(mActivity, Pub.VALUE_AUTHTOKEN, array.optString("authtoken"));
                    PrefConstant.setString(mActivity, Pub.VALUE_FIRST_LOGIN, array.optString("first_login"));
                    PrefConstant.setString(mActivity, Pub.VALUE_IS_ACTIVE, array.optString("is_active"));
                    PrefConstant.setString(mActivity, Pub.VALUE_SERIAL_NO, array.optString("serial_no"));
                    PrefConstant.setString(mActivity, Pub.VALUE_RECOMMENDED_SERIAL_NO, array.optString("recommended_serial_no"));
                    PrefConstant.setString(mActivity, Pub.VALUE_USER_GRP_ID, array.optString("user_grp_id"));
                    PrefConstant.setString(mActivity, Pub.VALUE_VERIFING_EMAIL, array.optString("verifing_email"));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MainActivity) mActivity).setTitle(getString(R.string.tiTle_login));
    }

    @Override
    public void onAttach(Activity activity) {
        mActivity = activity;
        super.onAttach(activity);
    }
}
