package com.acloud.app.fragment;

import android.app.Activity;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.acloud.app.MainActivity;
import com.acloud.app.R;
import com.acloud.app.adapter.StoreListAdapter;
import com.acloud.app.dialog.CustomPopupWindow;
import com.acloud.app.dialog.CustomProgressDialog;
import com.acloud.app.model.StoreListResult;
import com.acloud.app.task.SearchQueryTask;
import com.acloud.app.task.StoreListQueryTask;
import com.acloud.app.tool.TLog;
import com.acloud.app.util.LocatUtil;
import com.acloud.app.util.StoreListManager;

import java.util.ArrayList;


/**
 * Created by skywind-10 on 2015/5/28.
 */
public class StoreFragment extends Fragment implements View.OnClickListener {

    ArrayList<CheckBox> mArrayListCheckBox = new ArrayList<CheckBox>();
    CustomPopupWindow popupWindow;
    LocatUtil locatUtil;
    Location lc;
    static TextView tvCategory,
            tvDistrict,
            tvSort;

    String scid, scaid, sdid, smid;
    String type;
    StoreListAdapter mSotreListAdapter;
    ListView listView;
    boolean isLoading = false; //是否下載後續資料
    ArrayList<StoreListResult> storeArray;
    Activity mActivity;
    public static final int SORTBY_TYPE = 1;
    public static final int SORT_CATEGORY = 2;
    public static final int SORT_DISTRICT = 3;
    public static final int CLOSE_ALL = 4;
    public static final int UPDATE = 5;


    Handler uiHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            closeAllSortBox();

            switch (msg.what) {
                case 0:
                    break;
                case SORTBY_TYPE:
                    //SortType
                    TLog.i("Ernest", "Handler" + msg.arg1);
                    StoreListQueryTask task = new StoreListQueryTask(mActivity,
                            new CustomProgressDialog(mActivity),
                            lc != null ? lc.getLatitude() + "" : "0",
                            lc != null ? lc.getLongitude() + "" : "0",
                            sdid,
                            smid,
                            scid,
                            scaid,
                            msg.arg1 + "",
                            false,
                            listHandler);
                    type = msg.arg1 + "";
                    task.execute();
                    popupWindow.closeAllPopupWindow();

                    break;
                case SORT_CATEGORY:
                    StoreListQueryTask task1 = new StoreListQueryTask(mActivity,
                            new CustomProgressDialog(mActivity),
                            lc != null ? lc.getLatitude() + "" : "0",
                            lc != null ? lc.getLongitude() + "" : "0",
                            sdid,
                            smid,
                            msg.arg1 + "",
                            msg.arg2 + "",
                            type,
                            false,
                            listHandler);
                    task1.execute();
                    popupWindow.closeAllPopupWindow();
                    TLog.i("Ernest", "Handler scid" + msg.arg1);
                    TLog.i("Ernest", "Handler scaid" + msg.arg2);
                    scid = msg.arg1 + "";
                    scaid = msg.arg2 + "";
                    break;
                case SORT_DISTRICT:
                    StoreListQueryTask task2 = new StoreListQueryTask(mActivity,
                            new CustomProgressDialog(mActivity),
                            lc != null ? lc.getLatitude() + "" : "0",
                            lc != null ? lc.getLongitude() + "" : "0",
                            msg.arg1 + "",
                            msg.arg2 + "",
                            scid,
                            scaid,
                            type,
                            false,
                            listHandler);
                    task2.execute();
                    popupWindow.closeAllPopupWindow();
                    TLog.i("Ernest", "Handler sdid" + msg.arg1);
                    TLog.i("Ernest", "Handler smid" + msg.arg2);
                    sdid = msg.arg1 + "";
                    smid = msg.arg2 + "";
                    break;
                case CLOSE_ALL:
                    closeAllSortBox();
                    break;
                case UPDATE:
                    StoreListQueryTask task3 = new StoreListQueryTask(mActivity,
                            new CustomProgressDialog(mActivity),
                            lc != null ? lc.getLatitude() + "" : "0",
                            lc != null ? lc.getLongitude() + "" : "0",
                            sdid,
                            smid,
                            scid,
                            scaid,
                            type,
                            false,
                            listHandler);
                    task3.execute();
                    break;
            }
        }
    };

    private Handler listHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    mSotreListAdapter = new StoreListAdapter(mActivity, StoreListManager.getResultArrayList());
                    listView.setAdapter(mSotreListAdapter);
                    break;
            }
        }

    };


    @Override
    public void onStart() {
        super.onStart();
//        uiHandler.sendEmptyMessage(SORTBY_TYPE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View rootView = inflater.inflate(R.layout.fragment_store, container, false);
        initUI(rootView);
        locatUtil = ((MainActivity) mActivity).getLocatUtil();
        lc = locatUtil.getUpdatedLocation();
        ((MainActivity) mActivity).setTitle(getString(R.string.drawer_item1));
        //init Menu
        storeArray = StoreListManager.getResultArrayList();
        popupWindow = new CustomPopupWindow(mActivity, uiHandler);
        uiHandler.sendEmptyMessage(UPDATE);
        popupWindow.initWindow();
        setHasOptionsMenu(true);
        return rootView;
    }



    private void initUI(View view) {
        LinearLayout btnCategory = (LinearLayout) view.findViewById(R.id.btnCategory);
        LinearLayout btnDistrict = (LinearLayout) view.findViewById(R.id.btnDistrict);
        LinearLayout btnSort = (LinearLayout) view.findViewById(R.id.btnSort);

        tvCategory = (TextView) view.findViewById(R.id.tvCategory);
        tvDistrict = (TextView) view.findViewById(R.id.tvDistrict);
        tvSort = (TextView) view.findViewById(R.id.tvSort);

        listView = (ListView) view.findViewById(R.id.listView);
        mArrayListCheckBox.add((CheckBox) view.findViewById(R.id.checkBox5));
        mArrayListCheckBox.add((CheckBox) view.findViewById(R.id.checkBox6));
        mArrayListCheckBox.add((CheckBox) view.findViewById(R.id.checkBox7));

        //TODO
        mSotreListAdapter = new StoreListAdapter(mActivity, StoreListManager.getResultArrayList());
        listView.setAdapter(mSotreListAdapter);

        btnCategory.setOnClickListener(this);
        btnDistrict.setOnClickListener(this);
        btnSort.setOnClickListener(this);

        for (int i = 0; i < mArrayListCheckBox.size(); i++) {
            mArrayListCheckBox.get(i).setEnabled(false);
        }

        listView.setOnScrollListener(onScrollListener);
    }

    private AbsListView.OnScrollListener onScrollListener = new AbsListView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(AbsListView absListView, int scrollState) {
            switch (scrollState) {
                case SCROLL_STATE_FLING:
                    TLog.v("Scroll State", "滾動中...");
                    break;
                case SCROLL_STATE_IDLE:
                    TLog.v("Scroll State", "滾動停止...");

                    break;
                case SCROLL_STATE_TOUCH_SCROLL:
                    TLog.v("Scroll State", "手指滾動...");
                    break;
            }
        }

        @Override
        public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            TLog.v("OnScroll", "Scroll");
            TLog.v("OnScroll", "firstVisibleItem:" + firstVisibleItem);
            TLog.v("OnScroll", "visibleItemCount:" + visibleItemCount);
            TLog.v("OnScroll", "totalItemCount:" + totalItemCount);
            //如果總數超過20且可整除20表示可能有更多資料
            if (totalItemCount > 19 && totalItemCount % 20 == 0) {
                //拉到最底部了
                if ((firstVisibleItem + visibleItemCount) == totalItemCount) {
                    if (!isLoading) {
                        isLoading = true;
                        StoreListQueryTask task1 = new StoreListQueryTask(mActivity,
                                new CustomProgressDialog(mActivity),
                                lc != null ? lc.getLatitude() + "" : "0",
                                lc != null ? lc.getLongitude() + "" : "0",
                                sdid,
                                smid,
                                scid,
                                scaid,
                                type,
                                false,
                                new Handler() {
                                    @Override
                                    public void handleMessage(Message msg) {
                                        super.handleMessage(msg);
                                        switch (msg.what) {
                                            case 0:
                                                break;
                                            case 1:
                                                TLog.i("Ernest", "Scroll : notify");
                                                mSotreListAdapter.add(StoreListManager.getResultArrayList());
                                                mSotreListAdapter.notifyDataSetChanged();
                                                isLoading = false;
                                                break;
                                        }
                                    }
                                },
                                ++totalItemCount + "",
                                true);
                        task1.execute();
                    }
                }
            }
        }
    };

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        TLog.e("StoreFragment  ", "prepare option menu");
        super.onPrepareOptionsMenu(menu);

    }

    @Override
    public void onResume() {
        super.onResume();
    }



    @Override
    public void onPause() {
        super.onPause();
    }




    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        TLog.e("StoreFragment  ", "onCreateOptionsMenu");
        inflater.inflate(R.menu.menu_global, menu);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();

        searchView.setQueryHint(getString(R.string.txt_search_hint));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                TLog.i("TAG", "query " + query);
                SearchQueryTask task = new SearchQueryTask(mActivity, query,
                        lc != null ? lc.getLatitude() + "" : "0",
                        lc != null ? lc.getLongitude() + "" : "0",
                        true,
                        null);
                task.execute();
                closeAllSortBox();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                TLog.i("TAG", "new Text : " + newText);
                return false;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    /**
     * 控制排序的顯示跟文字變色
     *
     * @param view
     */
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnCategory:
                popupWindow.showLeftWindow(view);
                if (mArrayListCheckBox.get(0).isChecked()) {
                    mArrayListCheckBox.get(0).setChecked(false);
                    tvCategory.setTextColor(getResources().getColor(R.color.b));
                } else {
                    closeAllSortBox();
                    mArrayListCheckBox.get(0).setChecked(true);
                    tvCategory.setTextColor(getResources().getColor(R.color.m));
                }
                break;
            case R.id.btnDistrict:
                popupWindow.showCenterWindow(view);
                if (mArrayListCheckBox.get(1).isChecked()) {
                    mArrayListCheckBox.get(1).setChecked(false);
                    tvDistrict.setTextColor(getResources().getColor(R.color.b));
                } else {
                    closeAllSortBox();
                    mArrayListCheckBox.get(1).setChecked(true);
                    tvDistrict.setTextColor(getResources().getColor(R.color.m));
                }
                break;
            case R.id.btnSort:
                popupWindow.showRightWindow(view);
                if (mArrayListCheckBox.get(2).isChecked()) {
                    mArrayListCheckBox.get(2).setChecked(false);
                    tvSort.setTextColor(getResources().getColor(R.color.b));
                } else {
                    closeAllSortBox();
                    mArrayListCheckBox.get(2).setChecked(true);

                    tvSort.setTextColor(getResources().getColor(R.color.m));
                }
                break;
        }
    }

    /**
     * 控制排序的文字顏色變化　- 關閉
     */
    public void closeAllSortBox() {
        mArrayListCheckBox.get(0).setChecked(false);
        mArrayListCheckBox.get(1).setChecked(false);
        mArrayListCheckBox.get(2).setChecked(false);
        tvSort.setTextColor(Color.parseColor("#3E3A39"));
        tvCategory.setTextColor(Color.parseColor("#3E3A39"));
        tvDistrict.setTextColor(Color.parseColor("#3E3A39"));
    }

    /**
     * 變換分類排序文字
     *
     * @param text
     */
    public static void setTvCategory(String text) {
        tvCategory.setText(text);
    }

    /**
     * 變換商圈排序文字
     *
     * @param text
     */
    public static void setTvCategoryDistrict(String text) {
        tvDistrict.setText(text);
    }

    /**
     * 變換排序文字
     *
     * @param text
     */
    public static void setTvSort(String text) {
        tvSort.setText(text);
    }



}