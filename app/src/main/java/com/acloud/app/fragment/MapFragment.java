package com.acloud.app.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.acloud.app.R;

/**
 * Created by skywind-10 on 2015/5/28.
 */
public class MapFragment extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_map, container, false);
        final WebView wv = (WebView) rootView.findViewById(R.id.webView);
        wv.loadUrl("file:///android_asset/index.html");
        wv.getSettings().setJavaScriptEnabled(true);
        wv.setWebChromeClient(new WebChromeClient());
        wv.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                final String centerURL = "javascript:calcRoute(" + "25.0573178" + "," + "121.5599072" + "," +
                        "25.0555489" + "," + "121.5445006" + ")";
                view.loadUrl(centerURL);
            }
        });

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }
}
