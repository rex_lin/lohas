package com.acloud.app.fragment;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.acloud.app.MainActivity;
import com.acloud.app.R;
import com.acloud.app.adapter.BounsListAdapter;
import com.acloud.app.http.HttpBonusListGet;
import com.acloud.app.http.HttpCheckUserVerify;
import com.acloud.app.http.HttpCurrentBonusGet;
import com.acloud.app.superclass.BaseFragment;
import com.acloud.app.tool.TLog;
import com.acloud.app.tool.Pub;
import com.acloud.app.tool.WebService;
import com.acloud.app.util.CouponListManager;
import com.acloud.app.util.PrefConstant;
import com.acloud.app.util.UserProfileManager;
import com.acloud.app.util.UserVerifyCheckManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * 我的點數列表
 */
public class BounsRecordFragment extends BaseFragment {

    String TAG = "BounsRecordFragment";
    boolean isLoading = false; //是否下載後續資料
    Activity mActivity;
    BounsListAdapter listAdapter;
    ArrayList<String> title = new ArrayList<String>();
    ArrayList<String> content = new ArrayList<String>();
    ArrayList<String> points = new ArrayList<String>();
    ArrayList<String> action_datetime = new ArrayList<String>();
    TextView tvPoint;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_bonuslist, container, false);
        ListView listView = (ListView) rootView.findViewById(R.id.listView);
        tvPoint = (TextView) rootView.findViewById(R.id.tvPoint);
        //要丟入Adapter的各個資訊
        listAdapter = new BounsListAdapter(getActivity(), title, content, points, action_datetime);

        Bonus bonus = new Bonus();
        bonus.execute();

        listView.setAdapter(listAdapter);
        listView.setOnScrollListener(onScrollListener);
        return rootView;
    }

    private AbsListView.OnScrollListener onScrollListener = new AbsListView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
            switch (scrollState) {
                case SCROLL_STATE_FLING:
                    TLog.i("Scroll State", "滾動中...");
                    break;
                case SCROLL_STATE_IDLE:
                    TLog.i("Scroll State", "滾動停止...");

                    break;
                case SCROLL_STATE_TOUCH_SCROLL:
                    TLog.i("Scroll State", "手指滾動...");
                    break;
            }
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            TLog.v("OnScroll", "Scroll");
            TLog.v("OnScroll", "firstVisibleItem:" + firstVisibleItem);
            TLog.v("OnScroll", "visibleItemCount:" + visibleItemCount);
            TLog.v("OnScroll", "totalItemCount:" + totalItemCount);
            //如果總數超過20且可整除20表示可能有更多資料
            if (totalItemCount > 19 && totalItemCount % 20 == 0) {
                //拉到最底部了
                if ((firstVisibleItem + visibleItemCount) == totalItemCount) {
                    if (!isLoading) {
                        isLoading = true;
                        BonusScrollTask task = new BonusScrollTask(++totalItemCount + "");
                        task.execute();
                    }
                }
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MainActivity) getActivity()).setTitle(getString(R.string.title_bouns));
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Activity activity) {
        mActivity = activity;
        super.onAttach(activity);
    }
    private class Bonus extends AsyncTask{
        String jsonString;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Object[] params) {
            if (!UserProfileManager.loginStatus(mActivity))
                return null;

            //Get List
            HttpBonusListGet bonusListGet = new HttpBonusListGet(mActivity,
                    WebService.getUid_AuthToken_INDEX_SIZE(PrefConstant.getString(mActivity, Pub.VALUE_UID, ""),
                            PrefConstant.getString(mActivity, Pub.VALUE_AUTHTOKEN, ""),
                            "0",
                            "20"
                    ));
            jsonString = bonusListGet.call();
            parserJson(jsonString);
            //Get CurrentBonus
            HttpCurrentBonusGet bonusGet = new HttpCurrentBonusGet(mActivity,
                    WebService.getUid_AuthToken(
                            PrefConstant.getString(mActivity, Pub.VALUE_UID, ""),
                            PrefConstant.getString(mActivity, Pub.VALUE_AUTHTOKEN, "")));
            bonusGet.call();
            //呼叫API 檢查使用者驗證狀態

            HttpCheckUserVerify userVerify = new HttpCheckUserVerify(mActivity,
                    WebService.getUid_AuthToken(PrefConstant.getString(mActivity, Pub.VALUE_UID, ""),
                            PrefConstant.getString(mActivity, Pub.VALUE_AUTHTOKEN, "")));
            userVerify.call();
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            //解析完資料通知Adapter更新
            listAdapter.notifyDataSetChanged();
            tvPoint.setText(CouponListManager.getCurrentBouns() != null ? CouponListManager.getCurrentBouns() : "0"
                    + mActivity.getResources().getString(R.string.txt_point));
            //檢查使用者驗證狀態
            if (UserProfileManager.loginStatus(mActivity)) {
                UserVerifyCheckManager.checkVerifyStatus(mActivity);
            } else {
                ((MainActivity)getActivity()).changeFragment(new LoginFragment());
                Toast.makeText(getActivity(), getString(R.string.txt_please_login), Toast.LENGTH_SHORT).show();
            }
        }

        void parserJson(String message) {
            JSONObject object = null;
            JSONArray array = null;
            String syscode;
            String sysmsg;

            try {
                object = new JSONObject(message);

                syscode = object.optString(Pub.VALUE_SYSCODE);
                sysmsg = object.optString(Pub.VALUE_SYSMSG);
                array = (JSONArray) object.get("list");

                TLog.i(TAG, "" + array.length());
                for (int i = 0; i < array.length(); i++) {
                    JSONObject jsonObject = array.getJSONObject(i);
                    title.add(jsonObject.optString("title"));
                    content.add(jsonObject.optString("content"));
                    points.add(jsonObject.optString("points"));
                    action_datetime.add(jsonObject.optString("action_datetime"));
                    //解析Json丟入Arraylist
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private class BonusScrollTask extends AsyncTask {
        String jsonString;
        String index;

        public BonusScrollTask(String index) {
            this.index = index;
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            //Get List
            HttpBonusListGet bonusListGet = new HttpBonusListGet(mActivity,
                    WebService.getUid_AuthToken_INDEX_SIZE(PrefConstant.getString(mActivity, Pub.VALUE_UID, ""),
                            PrefConstant.getString(mActivity, Pub.VALUE_AUTHTOKEN, ""),
                            index,
                            "20"
                    ));
            jsonString = bonusListGet.call();
            parserJson(jsonString);
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }


        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            //解析完資料通知Adapter更新
            listAdapter.notifyDataSetChanged();
            isLoading = false;
        }

        void parserJson(String message) {
            JSONObject object = null;
            JSONArray array = null;
            String syscode;
            String sysmsg;

            try {
                object = new JSONObject(message);

                syscode = object.optString(Pub.VALUE_SYSCODE);
                sysmsg = object.optString(Pub.VALUE_SYSMSG);
                array = (JSONArray) object.get("list");

                TLog.i(TAG, "" + array.length());
                for (int i = 0; i < array.length(); i++) {
                    JSONObject jsonObject = array.getJSONObject(i);
                    title.add(jsonObject.optString("title"));
                    content.add(jsonObject.optString("content"));
                    points.add(jsonObject.optString("points"));
                    action_datetime.add(jsonObject.optString("action_datetime"));
                    //解析Json丟入Arraylist
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

}
