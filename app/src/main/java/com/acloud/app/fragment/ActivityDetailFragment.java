package com.acloud.app.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.acloud.app.dialog.CustomDialogPink;
import com.acloud.app.dialog.CustomDialogOrange;
import com.acloud.app.util.UserProfileManager;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareContent;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.acloud.app.MainActivity;
import com.acloud.app.R;
import com.acloud.app.qrcodescanner;
import com.acloud.app.superclass.BaseFragment;
import com.acloud.app.task.ActivityExchangeTask;
import com.acloud.app.thread.GetStoreInfoThread;
import com.acloud.app.tool.Pub;
import com.acloud.app.tool.TLog;
import com.acloud.app.util.ActivityDetailManager;
import com.acloud.app.util.BitmapHandler;
import com.acloud.app.util.DownLoadHandler;
import com.acloud.app.util.LocatUtil;
import com.acloud.app.util.PrefConstant;
import com.acloud.app.util.Util;

import java.io.InputStream;
import java.net.URL;

/**
 * Activity  Fragment
 */
public class ActivityDetailFragment extends BaseFragment implements View.OnClickListener {

    String TAG = "ActivityFragment";
    TextView
            tvStoreName,
            tvDistance,
            tvSubtitle,
            tvBonus,
            tvDate,
            tvContent,
            tvStatus;

    ImageView
            imgStore,
            imageStatus;

    Button
            btnExchange,
            btnStore;

    CallbackManager
            cm;

    ShareDialog
            sd;

    LocatUtil
            locatUtil;

    Location
            lc;

    int
            type,
            status;

    Activity
            mActivity;

    public static int QRCODE_ITEM = 0025;
    private static final int COMMING_SOON = 0;
    private static final int PLAYING = 1;
    private static final int COMPETLE = 2;
    private static final int PUNCH = 1;
    private static final int VIEW_STORE = 2;
    private static final int QRCODE = 3;
    private static final int AR_GAME = 4;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((MainActivity) getActivity()).setTitle(getString(R.string.txt_activity_detail));
        View rootView = inflater.inflate(R.layout.fragment_activitydetail, container, false);
        initUI(rootView);
        setText();

        status = Integer.valueOf(ActivityDetailManager.getDetailResult().getStatus());
        activityStatus();

        type = Integer.valueOf(ActivityDetailManager.getDetailResult().getType());
        activityType();

        locatUtil = ((MainActivity) mActivity).getLocatUtil();
        lc = locatUtil.getUpdatedLocation();
        return rootView;
    }
    //根據活動的狀態切換
    private void activityStatus(){
        switch (status) {
            case COMMING_SOON:
                imageStatus.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.comingsoon));
                tvStatus.setText(getString(R.string.txt_activity_comingsoon));
                btnExchange.setTextColor(getResources().getColor(R.color.txt_color2));
                btnExchange.setBackground(getResources().getDrawable(R.drawable.btn_selector_gray_soldout));
                btnExchange.setEnabled(false);
                break;
            case PLAYING:
                imageStatus.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.playing));
                tvStatus.setText(getString(R.string.txt_activity_playing));
                break;
            case COMPETLE:
                imageStatus.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.complete));
                tvStatus.setText(getString(R.string.txt_activity_complete));
                btnExchange.setTextColor(getResources().getColor(R.color.txt_color2));
                btnExchange.setBackground(getResources().getDrawable(R.drawable.btn_selector_gray_soldout));
                btnExchange.setEnabled(false);
                break;
        }
    }

    //根據活動的類別切換
    private void activityType() {
        switch (type) {
            case PUNCH: //打卡活動
                btnExchange.setText(getString(R.string.txt_punchin));
                break;
            case VIEW_STORE: //觀看店家資訊
                btnExchange.setText(getString(R.string.txt_look_storeinfo));
                btnStore.setVisibility(View.INVISIBLE);
                break;
            case QRCODE: //掃描QRcode
                btnExchange.setText(R.string.txt_scan_qrcode);
                break;
            case AR_GAME: //AR game
                btnExchange.setText(getString(R.string.txt_ar_game));
                break;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(mActivity);
        cm = CallbackManager.Factory.create();
        sd = new ShareDialog(this);
        sd.registerCallback(cm, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                TLog.i("Ernest", "onSuccess");
                ActivityExchangeTask task = new ActivityExchangeTask(
                        mActivity,
                        ActivityDetailManager.getDetailResult().getAid(),
                        lc != null ? lc.getLatitude() + "" : "0",
                        lc != null ? lc.getLongitude() + "" : "0"
                );
                task.execute();
            }

            @Override
            public void onCancel() {
                TLog.i("Ernest", "onCancel");
            }

            @Override
            public void onError(FacebookException e) {

            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    private void initUI(View view) {
        tvStoreName = (TextView) view.findViewById(R.id.tvStoreName);
//        tvDistance = (TextView) view.findViewById(R.id.tvDistance_white);
        imgStore = (ImageView) view.findViewById(R.id.img);
        tvSubtitle = (TextView) view.findViewById(R.id.tvSubtitle);
        tvBonus = (TextView) view.findViewById(R.id.tvBonus);
        btnExchange = (Button) view.findViewById(R.id.btnExchange);
        btnStore = (Button) view.findViewById(R.id.btnStore);
        tvDate = (TextView) view.findViewById(R.id.tvDate);
        tvContent = (TextView) view.findViewById(R.id.tvContent);
        imageStatus = (ImageView) view.findViewById(R.id.imgStatus);
        tvStatus = (TextView) view.findViewById(R.id.tvStatusTxt);

        LinearLayout contentLayout = (LinearLayout) view.findViewById(R.id.ContentLayout);
        contentLayout.setEnabled(false);
        btnExchange.setOnClickListener(this);
        btnStore.setOnClickListener(this);
        downloadImage();
    }

    private void setText() {
        tvStoreName.setText(ActivityDetailManager.getDetailResult().getStore_name());
//        tvDistance.setText(Util.distanceMtoKM(ActivityDetailManager.getDetailResult().getDistance()));
        tvSubtitle.setText(String.format(getString(R.string.txt_activity_subtitle), ActivityDetailManager.getDetailResult().getTitle()));
        tvBonus.setText(String.format(getString(R.string.txt_activity_bonus), ActivityDetailManager.getDetailResult().getPoints()));

        tvDate.setText(String.format(getString(R.string.txt_activity_date),
                Util.formatDate(ActivityDetailManager.getDetailResult().getStart_datetime()) + "-" +
                        Util.formatDate(ActivityDetailManager.getDetailResult().getEnd_datetime())));

        tvContent.setText(ActivityDetailManager.getDetailResult().getContent());
    }


    private void downloadImage() {
        final DownLoadHandler loadHandler = new DownLoadHandler(imgStore);
        //開始讀取圖片
        new Thread() {
            @Override
            public void run() {
                Message msg = new Message();

                try {
                    URL url = new URL(ActivityDetailManager.getDetailResult().getImage_url());
                    InputStream is = (InputStream) url.getContent();
                    if (is != null) {
                        System.gc();
                        //DownLoad Image
                        is = new BitmapHandler.FlushedInputStream(is);
                        msg.obj = BitmapFactory.decodeStream(is);
                        is.close();
                        System.gc();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (msg.obj != null)
                    loadHandler.sendMessage(msg);

            }
        }.start();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnExchange:
                TLog.i(TAG, "btnExchange");
                if (!UserProfileManager.loginStatus(mActivity)) {
                    Toast.makeText(mActivity, getString(R.string.txt_please_login), Toast.LENGTH_SHORT).show();
                    return;
                }

                switch (type) {
                    case PUNCH: //打卡活動
                        if (ShareDialog.canShow(ShareLinkContent.class)) {

                            ShareContent sc = new ShareLinkContent.Builder()
                                    .setPlaceId(ActivityDetailManager.getDetailResult().getFb_place_id())
                                    .build();
                            sd.show(sc);
                        }
                        break;
                    case VIEW_STORE: //觀看店家資訊
                        showStoreInfoWithExchange();
                        break;
                    case QRCODE: //掃描QRcode
                        Intent it = new Intent();
                        it.setClass(mActivity, qrcodescanner.class);
                        startActivityForResult(it, QRCODE_ITEM);
                        break;
                    case AR_GAME: //AR game
                        break;
                }

                break;
            case R.id.btnStore:
                showStoreInfo();
                break;
        }
    }

    //顯示店家介紹並完成活動
    private void showStoreInfoWithExchange() {
        new GetStoreInfoThread(
                mActivity,
                PrefConstant.getString(mActivity, Pub.VALUE_UID, ""),
                PrefConstant.getString(mActivity, Pub.VALUE_AUTHTOKEN, ""),
                ActivityDetailManager.getDetailResult().getStore_id(),
                lc != null ? lc.getLatitude() + "" : "0",
                lc != null ? lc.getLongitude() + "" : "0",
                new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        switch (msg.what) {
                            case 1:
                                //正常
                                ActivityExchangeTask task = new ActivityExchangeTask(
                                        mActivity,
                                        ActivityDetailManager.getDetailResult().getAid(),
                                        lc != null ? lc.getLatitude() + "" : "0",
                                        lc != null ? lc.getLongitude() + "" : "0"
                                );
                                task.execute();

                                Fragment nextFragment = new StoreDetailFragment();
                                nextFragment.setArguments(msg.getData());
                                ((MainActivity) mActivity).changeBackFragment(nextFragment);
                            case 0:
                                //TODO 連線失敗提示錯誤訊息

                                break;
                        }
                    }
                }

        ).start();
    }

    //只顯示店家介紹
    private void showStoreInfo() {
        new GetStoreInfoThread(
                mActivity,
                PrefConstant.getString(mActivity, Pub.VALUE_UID, ""),

                PrefConstant.getString(mActivity, Pub.VALUE_AUTHTOKEN, ""),
                ActivityDetailManager.getDetailResult().getStore_id(),
                lc != null ? lc.getLatitude() + "" : "0",
                lc != null ? lc.getLongitude() + "" : "0",
                new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        switch (msg.what) {
                            case 1:
                                //正常

                                Fragment nextFragment = new StoreDetailFragment();
                                nextFragment.setArguments(msg.getData());
                                ((MainActivity) mActivity).changeBackFragment(nextFragment);
                            case 0:
                                //TODO 連線失敗提示錯誤訊息

                                break;
                        }
                    }
                }

        ).start();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == QRCODE_ITEM) {
            if (data == null) {
                Toast.makeText(mActivity, "請重新掃描", Toast.LENGTH_SHORT).show();
                return;
            }

            String syscode = data.getStringExtra(Pub.VALUE_SYSCODE);
            final String obtain_bonus = data.getStringExtra("obtain_bonus");
            if (syscode.equals("200")) {
                showSuccessDialog(obtain_bonus);
            } else if (syscode.equals("400") || syscode.equals("503")) {
                final CustomDialogOrange errorDailog = new CustomDialogOrange(mActivity);
                errorDailog.initScanErrorDialog(null, null);
                errorDailog.show();
            } else if (syscode.equals("422")) {
                //TODO 若為QRCode掃描活動，則回傳422，無法處理QRCode 掃描，請通知系統管理員
            } else if (syscode.equals("415")) {
                //TODO 若活動已參加過，則回傳415，你已參加過「<活動標題>」活動。
            } else if (syscode.equals("416")) {
                //TODO若活動未開始，則回傳416，「<活動標題>」還未開始。
            } else if (syscode.equals("417")) {
                //TODO若活動已結束，則回傳417，「<活動標題>」已結束。
            }
        } else {
            cm.onActivityResult(requestCode, resultCode, data);
        }
    }


    private void showSuccessDialog(final String obtain_bonus) {
        final CustomDialogPink successDialog = new CustomDialogPink(mActivity,
                mActivity.getResources().getString(R.string.txt_finish_activity),
                String.format(mActivity.getResources().getString(R.string.txt_profile_write_success), obtain_bonus),
                mActivity.getResources().getString(R.string.btn_nextime),
                mActivity.getResources().getString(R.string.btn_share)
        );

        successDialog.initProgressDialog(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Util.fbshare(mActivity, String.format(mActivity.getString(R.string.txt_facebook_shared_bonus), obtain_bonus));
            }
        });

        successDialog.show();
    }
}
