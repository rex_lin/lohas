package com.acloud.app.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.acloud.app.MainActivity;
import com.acloud.app.R;
import com.acloud.app.superclass.BaseFragment;

/**
 * Setting Fragment
 */
public class FeedbackFragment extends BaseFragment {

    String TAG = "NewTeachFragment";
    TextView txt_feedBack;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((MainActivity) getActivity()).setTitle(getString(R.string.txt_feedback));
        View rootView = inflater.inflate(R.layout.fragment_feedback, container, false);
        initUI(rootView);
        setAction();
        return rootView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    private void initUI(View view) {
        txt_feedBack = (TextView) view.findViewById(R.id.txt_feedBack);


        txt_feedBack.setText(getString(R.string.txt_feedback_content));
    }

    private void setAction() {

    }

}
