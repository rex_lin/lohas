package com.acloud.app.fragment;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.acloud.app.MainActivity;
import com.acloud.app.R;
import com.acloud.app.superclass.BaseFragment;
import com.acloud.app.tool.Pub;
import com.acloud.app.tool.TLog;
import com.acloud.app.util.PrefConstant;
import com.acloud.app.util.Util;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

/**
 * Share Fragment
 */
public class ShareFragment extends BaseFragment {

    String TAG = "ShareFragment";
    TextView tvShared;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((MainActivity) getActivity()).setTitle(getString(R.string.txt_share));
        View rootView = inflater.inflate(R.layout.fragment_share, container, false);
        initUI(rootView);
        return rootView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    private void initUI(View view) {
        TextView tvSerialNo = (TextView) view.findViewById(R.id.tvSerialNo);
        tvSerialNo.setText(PrefConstant.getString(getActivity(), Pub.VALUE_SERIAL_NO, ""));
        tvShared = (TextView) view.findViewById(R.id.tvSend);
        tvShared.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Util.fbshare(getActivity(),
                        String.format(getString(R.string.txt_facebook_shared_no)
                        ,PrefConstant.getString(getActivity(), Pub.VALUE_SERIAL_NO, "")));
            }
        });
    }
}
