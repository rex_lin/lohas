package com.acloud.app.fragment;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;

import com.acloud.app.MainActivity;
import com.acloud.app.R;
import com.acloud.app.adapter.CouponListAdapter;
import com.acloud.app.dialog.CustomAdvPopupWindow;
import com.acloud.app.dialog.CustomProgressDialog;
import com.acloud.app.task.CouponListQueryTask;
import com.acloud.app.tool.Pub;
import com.acloud.app.tool.TLog;
import com.acloud.app.util.CouponListManager;
import com.acloud.app.util.LocatUtil;

import java.util.ArrayList;

/**
 * CouponList
 */
public class CouponListFragment extends Fragment implements View.OnClickListener,
        SearchView.OnQueryTextListener, View.OnFocusChangeListener {

    String TAG = "CouponListFragment";
    TextView
            tvCurrentBonus;

    CustomAdvPopupWindow
            advfliterWindows;

    LinearLayout
            btnFilter;
    String
            type,
            scid,
            smid,
            sdid,
            scaid,
            keyword;

    LocatUtil
            locatUtil;

    Location
            lc;

    ListView
            listView;

    boolean isLoading = false; //是否下載後續資料

    CouponListAdapter
            couponListAdapter;
    Activity
            activity;

    public static final int isSuccess = 1;
    public static final int isSort = 2;
    private Handler uiHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case isSuccess:
                    couponListAdapter.add(CouponListManager.getResultArrayList());
                    couponListAdapter.notifyDataSetChanged();
                    isLoading = false;
                    break;
                case isSort:
                    couponListAdapter = new CouponListAdapter(getActivity(), CouponListManager.getResultArrayList());
                    listView.setAdapter(couponListAdapter);
                    break;
            }
        }
    };


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//        setHasOptionsMenu(true);
//        initCustomActionBar();
        View rootView = inflater.inflate(R.layout.fragment_couponlist, container, false);
        ((MainActivity) getActivity()).setTitle("");
        initUI(rootView);

        couponListAdapter = new CouponListAdapter(getActivity(), CouponListManager.getResultArrayList());
        listView.setAdapter(couponListAdapter);
        tvCurrentBonus.setText(CouponListManager.getCurrentBouns() != null ? CouponListManager.getCurrentBouns() : "0"
                + getActivity().getResources().getString(R.string.txt_point));
        locatUtil = ((MainActivity) getActivity()).getLocatUtil();
        lc = locatUtil.getUpdatedLocation();

        int height = (((MainActivity) getActivity()).getToolBarHeight() + ((MainActivity) getActivity()).getMStatusBarHeight());
        int ffloat = (int) (60 * (((MainActivity) getActivity()).getDPI()));

        advfliterWindows = new CustomAdvPopupWindow(getActivity(), (height + ffloat), new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case Pub.BUTTON_OK:
                        CouponListQueryTask QueryTask = new CouponListQueryTask(
                                getActivity(), new CustomProgressDialog(getActivity()),
                                "0",
                                "20",
                                keyword,
                                sdid,
                                smid,
                                scid,
                                scaid,
                                type,
                                lc != null ? lc.getLatitude() + "" : "0",
                                lc != null ? lc.getLongitude() + "" : "0",
                                uiHandler,
                                false,
                                true
                        );
                        QueryTask.execute();
                        advfliterWindows.closeAllPopupWindow();
                        TLog.i("Ernest", "0");
                        break;
                    case Pub.BUTTON_SORT:
                        type = msg.arg1 + "";
                        TLog.i("Ernest", "type : " + type);
                        break;
                    case Pub.SEND_CATEGORY:
                        TLog.i("Ernest", "2");
                        scid = msg.arg1 + "";
                        scaid = msg.arg2 + "";
                        break;
                    case Pub.SEND_DISTRICT:
                        sdid = msg.arg1 + "";
                        smid = msg.arg2 + "";
                        break;
                }
            }
        }, true);

        advfliterWindows.initFliterWindow();
        listView.setOnScrollListener(onScrollListener);
        TLog.i("Ernest", "onCreateView");
        return rootView;
    }

    private AbsListView.OnScrollListener onScrollListener = new AbsListView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(AbsListView absListView, int scrollState) {
            switch (scrollState) {
                case SCROLL_STATE_FLING:
                    TLog.v("Scroll State", "滾動中...");
                    break;
                case SCROLL_STATE_IDLE:
                    TLog.v("Scroll State", "滾動停止...");

                    break;
                case SCROLL_STATE_TOUCH_SCROLL:
                    TLog.v("Scroll State", "手指滾動...");
                    break;
            }
        }

        @Override
        public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            TLog.v("OnScroll", "Scroll");
            TLog.v("OnScroll", "firstVisibleItem:" + firstVisibleItem);
            TLog.v("OnScroll", "visibleItemCount:" + visibleItemCount);
            TLog.v("OnScroll", "totalItemCount:" + totalItemCount);
            if (totalItemCount > 19 && totalItemCount % 20 == 0) {
                //拉到最底部了
                if ((firstVisibleItem + visibleItemCount) == totalItemCount) {
                    if (!isLoading) {
                        isLoading = true;
                        //TODO CouponList
                        CouponListQueryTask QueryTask = new CouponListQueryTask(
                                getActivity(), new CustomProgressDialog(getActivity()),
                                ++totalItemCount + "",
                                "20",
                                keyword,
                                sdid,
                                smid,
                                scid,
                                scaid,
                                type,
                                lc != null ? lc.getLatitude() + "" : "0",
                                lc != null ? lc.getLongitude() + "" : "0",
                                uiHandler,
                                true,
                                false
                        );
                        QueryTask.execute();
                    }
                }
            }
        }
    };


    private void initUI(View view) {
        listView = (ListView) view.findViewById(R.id.listView);
        SearchView searchView = (SearchView) view.findViewById(R.id.searchView);
        tvCurrentBonus = (TextView) view.findViewById(R.id.tvCurrentPoint);
        btnFilter = (LinearLayout) view.findViewById(R.id.btnFilter);
        btnFilter.setOnClickListener(this);
        searchView.setOnQueryTextListener(this);
        searchView.setOnQueryTextFocusChangeListener(this);
        //設定SearchView的顏色
        int searchSrcTextId = getResources().getIdentifier("android:id/search_src_text", null, null);
        EditText searchEditText = (EditText) searchView.findViewById(searchSrcTextId);
        searchEditText.setTextColor(Color.parseColor("#3E3A39")); // set the text color
        searchEditText.setHintTextColor(Color.parseColor("#3E3A39")); // set the hint color

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnFilter:
                advfliterWindows.showAdvancePopupwindow(btnFilter);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        TLog.e(TAG, "onCreateOptionsMenu");
        super.onCreateOptionsMenu(menu, inflater);
    }


    private void initCustomActionBar() {
        ((MainActivity) getActivity()).getSupportActionBar().setDisplayShowCustomEnabled(true);
        LayoutInflater inflator = (LayoutInflater) getActivity().getSystemService(getActivity().
                getApplicationContext().LAYOUT_INFLATER_SERVICE);
        View view = inflator.inflate(R.layout.spinnerwound, null);

        Spinner s = (Spinner) view.findViewById(R.id.spinner);
        s.getBackground().setColorFilter(getResources().getColor(R.color.t), PorterDuff.Mode.SRC_ATOP);
        s.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 1:
                        TLog.i(TAG, "position : " + i);
                        ((MainActivity) getActivity()).changeFragment(new MyCouponFragment());
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        ArrayList spinnerlist = new ArrayList<String>();
        spinnerlist.add(getString(R.string.title_coupon_exchange));
        spinnerlist.add(getString(R.string.title_mycoupon));

        ArrayAdapter spinneradapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_dropdown_item_1line, spinnerlist);

        s.setAdapter(spinneradapter);

        ((MainActivity) getActivity()).getSupportActionBar().setCustomView(view);
        spinneradapter.notifyDataSetChanged();
    }

    @Override
    public void onResume() {
        super.onResume();
        initCustomActionBar();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //Close Custome View
        ((MainActivity) getActivity()).getSupportActionBar().setDisplayShowCustomEnabled(false);
    }

    @Override
    public void onDestroyOptionsMenu() {
        super.onDestroyOptionsMenu();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        CouponListQueryTask QueryTask = new CouponListQueryTask(
                getActivity(), new CustomProgressDialog(getActivity()),
                "0",
                "20",
                s,
                sdid,
                smid,
                scid,
                scaid,
                type,
                lc != null ? lc.getLatitude() + "" : "0",
                lc != null ? lc.getLongitude() + "" : "0",
                uiHandler,
                false,
                true
        );
        QueryTask.execute();
        keyword = s;
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        return false;
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        advfliterWindows.closeAllPopupWindow();
    }
}
