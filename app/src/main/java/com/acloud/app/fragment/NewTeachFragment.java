package com.acloud.app.fragment;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.acloud.app.MainActivity;
import com.acloud.app.R;
import com.acloud.app.adapter.OperationPageAdapter;
import com.acloud.app.superclass.BaseFragment;
import com.acloud.app.thread.SwitchPushThread;
import com.acloud.app.tool.Pub;
import com.acloud.app.tool.TLog;
import com.acloud.app.util.BitmapHandler;
import com.acloud.app.util.PrefConstant;

import static android.content.Intent.ACTION_VIEW;

/**
 * Setting Fragment
 */
public class NewTeachFragment extends BaseFragment {

    String TAG = "NewTeachFragment";
    ViewPager viewPagerOperation;
    LinearLayout points;
    int[] Images = new int[]{R.drawable.operation1, R.drawable.operation2, R.drawable.operation3, R.drawable.operation4,
            R.drawable.operation5};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((MainActivity) getActivity()).setTitle(getString(R.string.txt_novice_teaching));
        View rootView = inflater.inflate(R.layout.activity_operation, container, false);
        TextView tv = (TextView) rootView.findViewById(R.id.txtSkip);
        tv.setVisibility(View.GONE);
        initUI(rootView);

        setAction();
        return rootView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    private void initUI(View view) {
        Button btn = (Button) view.findViewById(R.id.btnOk);
        btn.setVisibility(View.GONE);
        points = (LinearLayout) view.findViewById(R.id.points);
        initPoints();

        viewPagerOperation = (ViewPager) view.findViewById(R.id.viewPagerOperation);
        viewPagerOperation.setAdapter(new OperationPageAdapter(getActivity(), Images));

    }

    private void setAction() {
        viewPagerOperation.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                updatePointAndText();
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void updatePointAndText() {
        int currentItem = viewPagerOperation.getCurrentItem() % Images.length;
        for (int i = 0; i < points.getChildCount(); i++) {
            points.getChildAt(i).setEnabled(currentItem == i);
        }
    }

    private void initPoints() {
        for (int i = 0; i < Images.length; i++) {
            View view = new View(getActivity());
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    (int) BitmapHandler.convertDpToPixel(15, getActivity())
                    , (int) BitmapHandler.convertDpToPixel(15, getActivity()));
            if (i != 0) {
                params.leftMargin = 15;
            }
            view.setLayoutParams(params);
            view.setBackgroundResource(R.drawable.selector_points_pink);
            points.addView(view);
        }
    }

}
