package com.acloud.app.fragment;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.acloud.app.MainActivity;
import com.acloud.app.R;
import com.acloud.app.superclass.BaseFragment;
import com.acloud.app.thread.SwitchPushThread;
import com.acloud.app.tool.Pub;
import com.acloud.app.tool.TLog;
import com.acloud.app.util.PrefConstant;

import static android.content.Intent.*;

/**
 * Setting Fragment
 */
public class SettingFragment extends BaseFragment {

    String TAG = "SettingFragment";
    LinearLayout btnNoviceTeach,btnFeedBack,btnQuestion,btnDiscuss,btnAbout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((MainActivity) getActivity()).setTitle(getString(R.string.txt_setting));
        View rootView = inflater.inflate(R.layout.fragment_setting, container, false);
        initUI(rootView);
        return rootView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    private void initUI(View view) {
        ToggleButton toggleBtnPush = (ToggleButton) view.findViewById(R.id.toggleBtnPush);
        if (PrefConstant.getString(getActivity(), Pub.VALUE_PUSH_SWITCH, "false").equals("true")) {
            toggleBtnPush.setChecked(true);
        } else {
            toggleBtnPush.setChecked(false);
        }


        toggleBtnPush.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, final boolean b) {
                if (b) {
                    TLog.i(TAG, "true");
                    PrefConstant.setString(getActivity(), Pub.VALUE_PUSH_SWITCH, "true");
                    new SwitchPushThread(getActivity(),
                            PrefConstant.getString(getActivity(), Pub.VALUE_UID, ""),
                            PrefConstant.getString(getActivity(), Pub.VALUE_AUTHTOKEN, ""),
                            "1",
                            new Handler() {
                                @Override
                                public void handleMessage(Message msg) {
                                    super.handleMessage(msg);
                                    switch (msg.what) {
                                        case 1:
                                            TLog.i(TAG, "SwitchPush Success");
                                            break;
                                        case 0:
                                            break;
                                    }
                                }
                            }
                    ).start();
                } else {
                    TLog.i(TAG, "false");
                    PrefConstant.setString(getActivity(), Pub.VALUE_PUSH_SWITCH, "false");
                    new SwitchPushThread(getActivity(),
                            PrefConstant.getString(getActivity(), Pub.VALUE_UID, ""),
                            PrefConstant.getString(getActivity(), Pub.VALUE_AUTHTOKEN, ""),
                            "0",
                            new Handler() {
                                @Override
                                public void handleMessage(Message msg) {
                                    super.handleMessage(msg);
                                    switch (msg.what) {
                                        case 1:
                                            TLog.i(TAG, "SwitchPush Success");
                                            break;
                                        case 0:
                                            break;
                                    }
                                }
                            }
                    ).start();
                }
            }
        });

        TextView tvVersion = (TextView)view.findViewById(R.id.tvVersion);
        tvVersion.setText(R.string.app_ver);
        btnNoviceTeach = (LinearLayout)view.findViewById(R.id.btnNoviceTeach);
        btnFeedBack = (LinearLayout)view.findViewById(R.id.btnFeedBack);
        btnQuestion = (LinearLayout)view.findViewById(R.id.btnQuestion);
        btnDiscuss = (LinearLayout)view.findViewById(R.id.btnDiscuss);
        btnAbout = (LinearLayout)view.findViewById(R.id.btnAbout);

        setAction();

    }

    private void setAction() {
        btnDiscuss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = Pub.APPURL;
                Intent i = new Intent(ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });
        btnNoviceTeach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).changeBackFragment(new NewTeachFragment());
            }
        });
        btnFeedBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).changeBackFragment(new FeedbackFragment());
            }
        });
        btnQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).changeBackFragment(new QuestionFragment());
            }
        });
        btnAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).changeBackFragment(new AboutFragment());
            }
        });

    }

}
