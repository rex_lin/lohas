package com.acloud.app.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.acloud.app.MainActivity;
import com.acloud.app.R;
import com.acloud.app.adapter.GalleryAdapter;
import com.acloud.app.adapter.StoreCouponAdapter;
import com.acloud.app.dialog.CustomDialog;
import com.acloud.app.inferface.BaseInterface;
import com.acloud.app.model.GooleMapV3Struct;
import com.acloud.app.model.StoreDetailStruct;
import com.acloud.app.superclass.BaseFragment;
import com.acloud.app.task.StoreDetailCouponTask;
import com.acloud.app.thread.UpdateCollectionThread;
import com.acloud.app.thread.UpdateRatingThread;
import com.acloud.app.tool.Pub;
import com.acloud.app.tool.TLog;
import com.acloud.app.util.LocatUtil;
import com.acloud.app.util.PrefConstant;
import com.acloud.app.util.StoreCouponManager;
import com.acloud.app.util.UserProfileManager;
import com.acloud.app.util.Util;

import java.io.Serializable;


public class StoreDetailFragment extends BaseFragment {
    Activity activity;
    android.support.v4.view.ViewPager imgGallery; //照片瀏覽

    //UI變更
    private Handler
            uiHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    break;
                case 1:
                    break;
            }
        }
    };

    TextView
            tvName,//店名
            tvAppr,//平均評價
            tvAp,//平均評價
            tvDistance,//距離
            tvAddress,//地址
            tvMyAppra,//我的評價
            tvFevNum,//收藏量
            tvPrice,//平均價格
            tvTime,//營業時間
            tvMall,//商店
            tvProduce,//品項
            tvContent;//簡介


    LinearLayout
            btnCall,
            btnMyAppra,
            btnMyFevNum,
            eventBox,
            points;

    ListView
            mCouponList;
    ImageView
            imgStar,
            imgFavorite;

    static int keepFavCount;

    //接資料
    StoreDetailStruct rtValue = null;
    LocatUtil locatUtil;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_store_detail, container, false);

        getActivity().invalidateOptionsMenu();
        ((MainActivity) activity).setTitle(getString(R.string.title_store_detail));

        tvName = (TextView) v.findViewById(R.id.tvName);
        tvAppr = (TextView) v.findViewById(R.id.tvAppr);
        tvAp = (TextView) v.findViewById(R.id.tvAp);
//        tvDistance = (TextView) v.findViewById(R.id.tvDistance);
        tvAddress = (TextView) v.findViewById(R.id.tvAddress);
        tvMyAppra = (TextView) v.findViewById(R.id.tvMyAppra);
        tvFevNum = (TextView) v.findViewById(R.id.tvFevNum);
        tvPrice = (TextView) v.findViewById(R.id.tvPrice);
        tvTime = (TextView) v.findViewById(R.id.tvTime);
        tvMall = (TextView) v.findViewById(R.id.tvMall);
        tvProduce = (TextView) v.findViewById(R.id.tvProduce);
        tvContent = (TextView) v.findViewById(R.id.tvContent);

        btnCall = (LinearLayout) v.findViewById(R.id.btnCall);
        btnMyAppra = (LinearLayout) v.findViewById(R.id.btnMyAppra);
        btnMyFevNum = (LinearLayout) v.findViewById(R.id.btnMyFevNum);
        eventBox = (LinearLayout) v.findViewById(R.id.eventBox);

        imgStar = (ImageView) v.findViewById(R.id.imgStar);
        imgFavorite = (ImageView) v.findViewById(R.id.imgFavorite);

        imgGallery = (android.support.v4.view.ViewPager) v.findViewById(R.id.imgGallery);

        mCouponList = (ListView) v.findViewById(R.id.couponList);
        points = (LinearLayout) v.findViewById(R.id.points);

        tvAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment f = new WebFragment();

                GooleMapV3Struct mapSetting = new GooleMapV3Struct();
                Location lc = locatUtil.getUpdatedLocation();
                TLog.i("Ernest DetailFragment", "Location : " + lc);
                mapSetting.setGmapLatitude(rtValue.getLat());
                mapSetting.setGmapLongitude(rtValue.getLng());
                mapSetting.setGmapPinTitle(rtValue.getStore_name());
                mapSetting.setGmapUrl("file:///android_asset/index.html");
                mapSetting.setNowLatitude(lc != null ? lc.getLatitude() + "" : "0");
                mapSetting.setNowLongitude(lc != null ? lc.getLongitude() + "" : "0");

                Bundle bundle = new Bundle();
                bundle.putSerializable(Pub.RETURN_VALUE_KEY, (Serializable) mapSetting);

                f.setArguments(bundle);
                ((BaseInterface) activity).changeBackFragment(f);
            }
        });

        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { // 打電話
                try {
                    Intent intent = new Intent();
                    intent.setAction("android.intent.action.CALL");
                    intent.setData(Uri.parse("tel:" + rtValue.getPhone()));
                    startActivity(intent);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        btnMyAppra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { // 給評價
                final CustomDialog ratingStartDialog = new CustomDialog(activity);

                ratingStartDialog.initRatingStarDialog(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // SAVE

                        if (!UserProfileManager.loginStatus(activity)) {
                            Toast.makeText(activity, getString(R.string.txt_please_login), Toast.LENGTH_SHORT).show();
                            return;
                        }

                        new UpdateRatingThread(
                                activity,
                                PrefConstant.getString(activity, Pub.VALUE_UID, ""),
                                PrefConstant.getString(activity, Pub.VALUE_AUTHTOKEN, ""),
                                rtValue.getStore_id(),
                                Math.round(ratingStartDialog.getRatingBar().getRating()) + "",
                                "0",
                                "0",
                                new Handler() {
                                    @Override
                                    public void handleMessage(Message msg) {
                                        switch (msg.what) {
                                            case 1:
                                                //成功 , 送出改變評價
                                                tvMyAppra.setText(
                                                        String.format(activity.getResources().getString(R.string.txt_myappra),
                                                                ratingStartDialog.getRatingBar().getRating() + ""));
                                                imgStar.setImageResource(R.drawable.star_pressed);
                                                break;
                                            case 0:
                                                //TODO 失敗提示錯誤訊息
                                                break;
                                        }

                                    }
                                }
                        ).start();
                        ratingStartDialog.dismiss();
                    }
                });

                ratingStartDialog.show();
            }
        });

        btnMyFevNum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { // 變更最愛狀態




                if (!UserProfileManager.loginStatus(activity)) {
                    Toast.makeText(activity, getString(R.string.txt_please_login), Toast.LENGTH_SHORT).show();
                    return;
                }



                new UpdateCollectionThread(
                        activity,
                        PrefConstant.getString(activity, Pub.VALUE_UID, ""),
                        PrefConstant.getString(activity, Pub.VALUE_AUTHTOKEN, ""),
                        rtValue.getStore_id(),
                        PrefConstant.isFavStoreExist(activity, rtValue.getStore_id()) ? Pub.ACTION_REMOVE : Pub.ACTION_ADD,
                        "0",
                        "0",
                        new Handler() {
                            @Override
                            public void handleMessage(Message msg) {
                                switch (msg.what) {
                                    case 1:
                                        if (PrefConstant.isFavStoreExist(activity, rtValue.getStore_id())) {
                                            PrefConstant.removeFavStore(activity, rtValue.getStore_id());
                                            imgFavorite.setImageResource(R.drawable.store_addtofavorite_normal);
                                            Toast.makeText(activity, getString(R.string.txt_cancel_Collection), Toast.LENGTH_SHORT).show();
                                            tvFevNum.setText(String.valueOf(keepFavCount - 1)); // add -1 if click love star to remove store from personal favorite list
                                        } else {
                                            PrefConstant.setFavStore(activity, rtValue.getStore_id());
                                            imgFavorite.setImageResource(R.drawable.store_addtofavorite_pressed);
                                            tvFevNum.setText(String.valueOf(keepFavCount + 1)); // add +1 if click love star to add store to personal favorite list
                                        }
                                        break;
                                    case 0:
                                        //TODO 失敗提示錯誤訊息
                                        break;
                                }
                            }
                        }
                ).start();
            }
        });

        imgGallery.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrollStateChanged(int arg0) {
                updatePointAndText();

            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {

            }

            @Override
            public void onPageSelected(int position) {

            }
        });

        rtValue = (StoreDetailStruct) getArguments().getSerializable(Pub.RETURN_VALUE_KEY);

        tvName.setText(rtValue.getStore_name());

        try {
            tvAppr.setText(String.format(activity.getResources().getString(R.string.txt_appra),
                    String.format("%.01f", Float.valueOf(rtValue.getRating()))));

            tvAp.setText(rtValue.getRating_num());

        } catch (NumberFormatException e){
            e.printStackTrace();
        }
//        tvDistance.setText(Util.distanceMtoKM(rtValue.getDistance()));
        tvAddress.setText(rtValue.getAddress());
        tvMyAppra.setText((rtValue.getUser_rating().equals("-1")) ?
                activity.getResources().getString(R.string.txt_appraise) :
                String.format(activity.getResources().getString(R.string.txt_myappra), rtValue.getUser_rating()));
        //set Rating image;
        if (!(rtValue.getUser_rating().equals("-1")))
            imgStar.setImageResource(R.drawable.star_pressed);

        tvFevNum.setText(rtValue.getFav_num());
        //TODO bug rtValue.getFav_num() == ""
        keepFavCount = Integer.parseInt(rtValue.getFav_num());
        Log.e("WTF", "WTF keepFavCount  :" + keepFavCount);


        tvPrice.setText(String.format(activity.getResources().getString(R.string.txt_price), rtValue.getAvg_price()));
        tvTime.setText(String.format(activity.getResources().getString(R.string.txt_time), rtValue.getBusiness_hours()));
        tvMall.setText(rtValue.getSd_name());
        tvProduce.setText(rtValue.getSca_name());
        tvContent.setText(rtValue.getContent());

        //Switch favorite image
        if (PrefConstant.isFavStoreExist(activity, rtValue.getStore_id()))
            imgFavorite.setImageResource(R.drawable.store_addtofavorite_pressed);
        else
            imgFavorite.setImageResource(R.drawable.store_addtofavorite_normal);

        //--- Screen
        int width = 0;
        Display display = activity.getWindowManager().getDefaultDisplay();
        if (android.os.Build.VERSION.SDK_INT > 12) {
            Point size = new Point();
            display.getSize(size);
            width = size.x;
        } else {
            width = display.getWidth();
        }
//        if (width > 0) {
//            int adjustHeight = (int) (((float) width / 600) * 400);
//            ViewGroup.LayoutParams bannerGalleryParams = imgGallery.getLayoutParams();
//            bannerGalleryParams.width = width;
//            bannerGalleryParams.height = adjustHeight;
//            imgGallery.setLayoutParams(bannerGalleryParams);
//        }

        GalleryAdapter adapter = new GalleryAdapter(activity, rtValue.getImages());
        imgGallery.setAdapter(adapter);
        //Store Coupon
        StoreDetailCouponTask detailCouponTask = new StoreDetailCouponTask(activity, rtValue.getStore_id(),
                new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        super.handleMessage(msg);
                        switch (msg.what) {
                            case 0:
                                mCouponList.setAdapter(new StoreCouponAdapter(activity, StoreCouponManager.getResultArrayList()));
                                setListViewHeightBasedOnChildren(mCouponList);
                                break;
                        }
                    }
                });
        detailCouponTask.execute();
        initPoints();
        return v;
    }

    private void initPoints() {
        for (int i = 0; i < rtValue.getImages().size(); i++) {
            View view = new View(activity);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(15, 15);
            if (i != 0) {
                params.leftMargin = 15;
            }
            view.setLayoutParams(params);
            view.setBackgroundResource(R.drawable.selector_points);
            points.addView(view);
        }
    }

    private void updatePointAndText() {
        int currentItem = imgGallery.getCurrentItem() % rtValue.getImages().size();
        TLog.i("Ernest", "imgGallery currentItem : " + currentItem);
        for (int i = 0; i < points.getChildCount(); i++) {
            points.getChildAt(i).setEnabled(currentItem == i);
        }
    }

    /**
     * height Dynamic
     *
     * @param listView
     */
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    @Override
    public void onResume() {
        super.onResume();
        locatUtil = ((MainActivity) activity).getLocatUtil();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}
