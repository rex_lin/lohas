package com.acloud.app.fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.acloud.app.MainActivity;
import com.acloud.app.R;
import com.acloud.app.dialog.CustomDialogOrange;
import com.acloud.app.dialog.CustomDialogPink;
import com.acloud.app.http.HttpUserProfileUpdate;
import com.acloud.app.tool.Pub;
import com.acloud.app.tool.WebService;
import com.acloud.app.util.PrefConstant;
import com.acloud.app.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;


public class ProfileSendFragment extends Fragment implements View.OnClickListener {

    String TAG = "ProfileSendFragment";
    Activity mActivity;
    EditText editMail, editName, editPhone, editNote;
    TextView tvBirthday;
    Spinner spinnerGender;
    RelativeLayout SelectDate;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_profile_write, container, false);

        ((MainActivity) mActivity).setTitle(getString(R.string.title_profile));

        initUI(rootView);

        //Set Gender spinner
        ArrayList spinnerlist = new ArrayList<String>();
        spinnerlist.add(getString(R.string.txt_boy));
        spinnerlist.add(getString(R.string.txt_girl));

        ArrayAdapter spinneradapter = new ArrayAdapter<String>(getActivity(), R.layout.item_profile_gender, spinnerlist);
        spinnerGender.setAdapter(spinneradapter);


        return rootView;
    }

    private void initUI(View view) {
        TextView tvSend = (TextView) view.findViewById(R.id.tvSend);
        TextView tvPass = (TextView) view.findViewById(R.id.tvPass);
        TextView tvID = (TextView) view.findViewById(R.id.tvID);
        TextView tvNo = (TextView) view.findViewById(R.id.tvNo);
        Button btnShare = (Button) view.findViewById(R.id.btnShare);


        editMail = (EditText) view.findViewById(R.id.editMail);
        editName = (EditText) view.findViewById(R.id.editName);
        editPhone = (EditText) view.findViewById(R.id.editPhone);
        tvBirthday = (TextView) view.findViewById(R.id.tvBirthday);

        editNote = (EditText) view.findViewById(R.id.editNote);
        spinnerGender = (Spinner) view.findViewById(R.id.spinnerGender);
        SelectDate = (RelativeLayout) view.findViewById(R.id.SelectDate);

        tvSend.setOnClickListener(this);
        btnShare.setOnClickListener(this);
        SelectDate.setOnClickListener(this);
        tvPass.setOnClickListener(this);

        tvID.setText(PrefConstant.getString(mActivity, Pub.VALUE_UID, ""));
        tvNo.setText(PrefConstant.getString(mActivity, Pub.VALUE_SERIAL_NO, ""));
        editMail.setText(PrefConstant.getString(mActivity, Pub.VALUE_EMAIL, ""));
        editName.setText(PrefConstant.getString(mActivity, Pub.VALUE_NAME, ""));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvSend:
                QuerySendProfileTask sendProfileTask = new QuerySendProfileTask();
                sendProfileTask.execute();
                break;
            case R.id.btnShare:
                Toast.makeText(getActivity(), "Share ", Toast.LENGTH_SHORT).show();
                break;
            case R.id.tvPass:
                ((MainActivity) mActivity).getmNavigationDrawerFragment().onNavigationDrawerItemSelected(MainActivity.MAIN_PAGE);
                break;
            case R.id.SelectDate:
                showDatePickerDialog();
                break;
        }
    }

    public class QuerySendProfileTask extends AsyncTask {
        String jsonString;
        String syscode;
        String sysmsg;
        String obtain_bonus;

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            if (syscode.equals("200")) {
                final CustomDialogPink dialog = new CustomDialogPink(getActivity(),
                        getActivity().getResources().getString(R.string.txt_profile_success_title),
                        String.format(getActivity().getResources().getString(R.string.txt_profile_write_success), obtain_bonus),
                        getActivity().getResources().getString(R.string.btn_nextime),
                        getActivity().getResources().getString(R.string.btn_share)
                );

                dialog.initProgressDialog(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Util.fbshare(mActivity, String.format(mActivity.getString(R.string.txt_facebook_shared_bonus), obtain_bonus));

                    }
                },new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        ((MainActivity) mActivity).getmNavigationDrawerFragment().onNavigationDrawerItemSelected(MainActivity.MAIN_PAGE);
                    }
                });

                dialog.show();
            } else {
//                Toast.makeText(getActivity(), "錯誤碼: " + syscode + " msg:" + sysmsg, Toast.LENGTH_SHORT).show();
                final CustomDialogOrange customDialog = new CustomDialogOrange(mActivity, mActivity.getString(R.string.txt_fail));
                customDialog.initErrorDialog();
                customDialog.show();
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            HttpUserProfileUpdate update = new HttpUserProfileUpdate(getActivity(),
                    WebService.getUser_Info(
                            PrefConstant.getString(mActivity, Pub.VALUE_UID, ""),
                            PrefConstant.getString(mActivity, Pub.VALUE_AUTHTOKEN, ""),
                            editName.getText().toString(),
                            editPhone.getText().toString(),
                            tvBirthday.getText().toString(),
                            (spinnerGender.getSelectedItem().toString().equals(getString(R.string.txt_boy)) ? "0" : "1"),
                            editNote.getText().toString(),
                            "0",
                            "0")
            );
            jsonString = update.call();
            //TODO Dialog show
            parserJson(jsonString);
            return null;
        }

        void parserJson(String message) {
            JSONObject object = null;
            JSONObject array = null;

            try {
                object = new JSONObject(message);

                syscode = object.optString(Pub.VALUE_SYSCODE);
                sysmsg = object.optString(Pub.VALUE_SYSMSG);
                array = object.getJSONObject("data");
                obtain_bonus = array.optString("obtain_bonus");

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    public void showDatePickerDialog() {
        // 設定初始日期
        int mYear, mMonth, mDay;

        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        // 跳出日期選擇器
        DatePickerDialog dpd = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        // 完成選擇，顯示日期
                        tvBirthday.setText(year + "/" + (monthOfYear + 1) + "/"
                                + dayOfMonth);

                    }
                }, mYear, mMonth, mDay);
        dpd.show();
    }

    @Override
    public void onAttach(Activity activity) {
        mActivity = activity;
        super.onAttach(activity);
    }

}
