package com.acloud.app.fragment;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.acloud.app.MainActivity;
import com.acloud.app.R;
import com.acloud.app.dialog.CustomDialogPink;
import com.acloud.app.superclass.BaseFragment;
import com.acloud.app.task.CouponExchangeTask;
import com.acloud.app.thread.GetStoreInfoThread;
import com.acloud.app.tool.Pub;
import com.acloud.app.tool.TLog;
import com.acloud.app.util.BitmapHandler;
import com.acloud.app.util.CouponDetailManager;
import com.acloud.app.util.CouponListManager;
import com.acloud.app.util.LocatUtil;
import com.acloud.app.util.PrefConstant;
import com.acloud.app.util.UserProfileManager;
import com.acloud.app.util.Util;

import java.io.InputStream;
import java.net.URL;

/**
 * CouponDetail
 */
public class CouponDetailFragment extends BaseFragment implements View.OnClickListener {

    String TAG = "CouponDetailFragment";
    TextView
            tvStoreName,
            tvDistance,
            tvSubTitle,
            tvDate,
            tvPoint,
            tvContent,
            tvOffer,
            tvConvert,
            tvRedeemed,
            tvRemain;

    Activity
            mActivity;
    ImageView
            imgStore;
    Button
            btnExchange;
    LinearLayout
            mRedeemedLayout;
    LocatUtil
            locatUtil;
    Location
            lc;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_coupondetail, container, false);
        initUI(rootView);
        locatUtil = ((MainActivity)getActivity()).getLocatUtil();
        lc = locatUtil.getUpdatedLocation();

        setText();

        //開始讀取圖片
        new Thread() {
            @Override
            public void run() {
                Message msg = new Message();

                try {
                    URL url = new URL(CouponDetailManager.getCouponDetailResult().getImage_url());
                    InputStream is = (InputStream) url.getContent();
                    if (is != null) {
                        System.gc();
                        //DownLoad Image    BitmapHandler class
                        is = new BitmapHandler.FlushedInputStream(is);
                        msg.obj = BitmapFactory.decodeStream(is);
                        is.close();
                        System.gc();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (msg.obj != null)
                    updateIconHandler.sendMessage(msg);

            }
        }.start();

        visibleLayout();

        return rootView;
    }
    private void visibleLayout(){
        if (Integer.valueOf(CouponDetailManager.getCouponDetailResult().getExchange_limit_amount()) == 0) {
            mRedeemedLayout.setVisibility(View.INVISIBLE);
        } else {
            mRedeemedLayout.setVisibility(View.VISIBLE);
            //判斷當剩餘數量為0
            if (Integer.valueOf(CouponDetailManager.getCouponDetailResult().getRemaining_amount()) == 0) {
                btnExchange.setText(getString(R.string.txt_soldout));
                btnExchange.setTextColor(getResources().getColor(R.color.txt_color2));
                btnExchange.setBackground(getResources().getDrawable(R.drawable.btn_selector_gray_soldout));
                btnExchange.setEnabled(false);
            }
        }
    }


    final Handler updateIconHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg != null) {
                imgStore.setImageBitmap((Bitmap) msg.obj);
            }
        }
    };


    private void setText(){
        tvStoreName.setText(CouponDetailManager.getCouponDetailResult().getStore_name());
//        tvDistance.setText(Util.distanceMtoKM(CouponDetailManager.getCouponDetailResult().getDistance()));
        tvSubTitle.setText(String.format(getString(R.string.txt_coupon_subtitle), CouponDetailManager.getCouponDetailResult().getTitle()));

        tvDate.setText(String.format(getString(R.string.txt_activity_during),
                Util.formatDate(CouponDetailManager.getCouponDetailResult().getStart_datetime()) + " - " +
                        Util.formatDate(CouponDetailManager.getCouponDetailResult().getEnd_datetime()
                        )));

        tvContent.setText(String.format(getString(R.string.txt_coupon_content), CouponDetailManager.getCouponDetailResult().getContent()));

        tvPoint.setText(CouponDetailManager.getCouponDetailResult().getPoints() + " P");
        tvOffer.setText(String.format(getString(R.string.txt_offer_valid_date),
                Util.formatDate(CouponDetailManager.getCouponDetailResult().getUsed_end_datetime())));

        tvConvert.setText(String.format(getString(R.string.txt_convertible_number),
                CouponDetailManager.getCouponDetailResult().getExchange_limit_amount()));

        tvRedeemed.setText(String.format(getString(R.string.txt_redeemed_number),
                Calculate(CouponDetailManager.getCouponDetailResult().getExchange_limit_amount(),
                        CouponDetailManager.getCouponDetailResult().getRemaining_amount())));

        tvRemain.setText(String.format(getString(R.string.txt_remaining_amount),
                CouponDetailManager.getCouponDetailResult().getRemaining_amount()));
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btnExchange:
                if (!UserProfileManager.loginStatus(mActivity)) {
                    Toast.makeText(mActivity, "請先登入", Toast.LENGTH_SHORT).show();
                    break;
                }

                final CustomDialogPink dialog = new CustomDialogPink(
                        getActivity(), getActivity().getResources().getString(R.string.txt_exchange_title),
                        String.format(getActivity().getResources().getString(R.string.txt_exchange_msg), CouponListManager.getCurrentBouns(),
                                CouponDetailManager.getCouponDetailResult().getPoints()),
                        getActivity().getResources().getString(R.string.btn_cancel),
                        getActivity().getResources().getString(R.string.btn_ok));


                dialog.initProgressDialog(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        CouponExchangeTask exchangeTask = new CouponExchangeTask(mActivity,
                                CouponDetailManager.getCouponDetailResult().getCoupon_id(),
                                CouponDetailManager.getCouponDetailResult().getTitle());
                        exchangeTask.execute();


                    }
                });
                dialog.show();
                break;
            case R.id.btnStore:
                showStoreInfo();
                break;
        }
    }
    private void showStoreInfo(){
        new GetStoreInfoThread(
                getActivity(),
                PrefConstant.getString(getActivity(), Pub.VALUE_UID, ""),

                PrefConstant.getString(getActivity(), Pub.VALUE_AUTHTOKEN, ""),
                CouponDetailManager.getCouponDetailResult().getStore_id(),
                lc != null ? lc.getLatitude() + "" : "0",
                lc != null ? lc.getLongitude() + "" : "0",
                new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        switch (msg.what) {
                            case 1:
                                //正常

                                Fragment nextFragment = new StoreDetailFragment();
                                nextFragment.setArguments(msg.getData());
                                ((MainActivity)getActivity()).changeBackFragment(nextFragment);
                            case 0:
                                //TODO 連線失敗提示錯誤訊息

                                break;
                        }
                    }
                }

        ).start();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MainActivity) getActivity()).setTitle(getString(R.string.title_coupon));
    }

    private void initUI(View view) {
        btnExchange = (Button) view.findViewById(R.id.btnExchange);
        Button btnStore = (Button) view.findViewById(R.id.btnStore);
        tvStoreName = (TextView) view.findViewById(R.id.tvStoreName);
//        tvDistance = (TextView) view.findViewById(R.id.tvDistance_white);
        tvSubTitle = (TextView) view.findViewById(R.id.tvSubtitle);
        tvDate = (TextView) view.findViewById(R.id.tvDate);
        tvPoint = (TextView) view.findViewById(R.id.tvPoint);
        tvContent = (TextView) view.findViewById(R.id.tvContent);
        imgStore = (ImageView) view.findViewById(R.id.img);
        tvOffer = (TextView) view.findViewById(R.id.tvOffer);
        tvConvert = (TextView) view.findViewById(R.id.tvConvert);
        tvRedeemed = (TextView) view.findViewById(R.id.tvRedeemed);
        tvRemain = (TextView) view.findViewById(R.id.tvRemain);

        LinearLayout contentLayout = (LinearLayout) view.findViewById(R.id.ContentLayout);
        contentLayout.setEnabled(false);
        mRedeemedLayout = (LinearLayout) view.findViewById(R.id.RedeemedLayout);



        btnExchange.setOnClickListener(this);
        btnStore.setOnClickListener(this);
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    /**
     * 計算已兌換數量
     *
     * @param total
     * @param convert
     * @return
     */
    private int Calculate(String total, String convert) {
        TLog.i(TAG, "value " + (Integer.valueOf(total) - Integer.valueOf(convert)));
        if (Math.abs(Integer.valueOf(total) - Integer.valueOf(convert)) <= 0) {
            return 0;
        } else {
            return Math.abs(Integer.valueOf(total) - Integer.valueOf(convert));
        }

    }
}
