package com.acloud.app.fragment;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.acloud.app.dialog.CustomDialogOrange;
import com.acloud.app.dialog.CustomDialogPink;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.acloud.app.MainActivity;
import com.acloud.app.R;
import com.acloud.app.http.HttpLogin;
import com.acloud.app.http.HttpPushTokenUpdate;
import com.acloud.app.inferface.BaseInterface;
import com.acloud.app.tool.TLog;
import com.acloud.app.tool.Pub;
import com.acloud.app.tool.WebService;
import com.acloud.app.util.GCMClientManager;
import com.acloud.app.util.LoginManager;
import com.acloud.app.util.PrefConstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

public class LoginFragment extends Fragment {

    CallbackManager mCallbackManager;
    AccessToken mAccessToken;
    String TAG = "LoginFragment";
    Activity mActivity;
    Location mLocation;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
        View rootView = inflater.inflate(R.layout.fragment_login, container, false);
        initLoginButton(rootView);
        return rootView;
    }

    private void initLoginButton(View view) {
        final LoginButton loginButton = (LoginButton) view.findViewById(R.id.loginbutton);
        final CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkBox);
        TextView btnProVision = (TextView) view.findViewById(R.id.btnProVision); //使用條款按鈕

        loginButton.setReadPermissions("email");
        loginButton.setBackgroundResource(R.drawable.btn_selector_login);
        loginButton.setEnabled(false);
        loginButton.setFragment(this);
        mCallbackManager = CallbackManager.Factory.create();
        TLog.i(TAG, "onCreateView");
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    loginButton.setEnabled(true);
                } else {
                    loginButton.setEnabled(false);
                }
            }
        });
        loginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                mAccessToken = loginResult.getAccessToken();
                TLog.i(TAG, "access token get : " + mAccessToken.getToken());
                GraphRequest request = GraphRequest.newMeRequest(mAccessToken, new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject jsonObject, GraphResponse graphResponse) {
                        TLog.i(LoginFragment.class.getSimpleName(), "name : " + jsonObject.optString("name"));
                        TLog.i(LoginFragment.class.getSimpleName(), "link : " + jsonObject.optString("link"));
                        TLog.i(LoginFragment.class.getSimpleName(), "id : " + jsonObject.optString("id"));
                        TLog.i(LoginFragment.class.getSimpleName(), "email : " + jsonObject.optString("email"));
                        QueryLoginTask task = new QueryLoginTask(jsonObject.optString("id"), jsonObject.optString("email"));
                        task.execute();

                    }
                });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,link");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                TLog.i(LoginFragment.class.getSimpleName(), "cancel");
            }

            @Override
            public void onError(FacebookException e) {
                TLog.i(LoginFragment.class.getSimpleName(), "error : " + e);
            }
        });


        btnProVision.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {//
                CustomDialogPink customDialog = new CustomDialogPink(mActivity, "", getString().toString(), getString(R.string.btn_ok), "");
                customDialog.initProvisionDialog(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                });
                customDialog.show();
            }
        });
    }

    public StringBuilder getString() {
        StringBuilder buf = new StringBuilder();
        BufferedReader in = null;

        try {
            String str = "";
            InputStream json = getResources().getAssets().open("provision.txt");
            in = new BufferedReader(new InputStreamReader(json, "UTF-8"));
            while ((str = in.readLine()) != null) {
                buf.append(str);
            }

            in.close();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return buf;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        ((MainActivity) getActivity()).setTitle(getString(R.string.tiTle_login));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    //Call Login Task
    public class QueryLoginTask extends AsyncTask {
        String fbid, email;
        HttpLogin login;

        public QueryLoginTask(String fbid, String email) {
            this.fbid = fbid;
            this.email = email;
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            //Login
            login = new HttpLogin(mActivity, WebService.getFBIDEmail(fbid, email, mAccessToken.getToken()));
            login.call();
            //Update push token
            HttpPushTokenUpdate update = new HttpPushTokenUpdate(mActivity,
                    WebService.getUid_AuthToken_AppId_PushToken_PushServer(
                            PrefConstant.getString(mActivity, Pub.VALUE_UID, ""),
                            PrefConstant.getString(mActivity, Pub.VALUE_AUTHTOKEN, ""),
                            mActivity.getPackageName(),
                            PrefConstant.getString(mActivity, GCMClientManager.PROPERTY_REG_ID, ""),
                            "gcm",
                            mLocation != null ? mLocation.getLatitude() + "" : "0",
                            mLocation != null ? mLocation.getLongitude() + "" : "0"
                    ));
            String updateotken = update.call();
            parserJson2(updateotken);
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Object o) {

            if (login.getSysCode() != null && login.getSysCode().equals("200")) {
                if (LoginManager.getResult().getUser_grp_id().equals("2")) {
                    ((MainActivity) mActivity).getmNavigationDrawerFragment().onNavigationDrawerItemSelected(MainActivity.MAIN_PAGE);
                } else {
                    if (PrefConstant.getString(mActivity, Pub.VALUE_RECOMMENDED_SERIAL_NO, "").equals("")) {
                        ((BaseInterface) mActivity).changeBackFragment(new RecommendedFragment());
                    } else {
                        ((MainActivity) mActivity).getmNavigationDrawerFragment().onNavigationDrawerItemSelected(MainActivity.MAIN_PAGE);
                    }
                }

                PrefConstant.setBoolean(mActivity, Pub.IS_LOGIN, true);
                PrefConstant.setString(mActivity, Pub.VALUE_LAST_PUSH_TOKEN, PrefConstant.getString(mActivity, GCMClientManager.PROPERTY_REG_ID, ""));
                //更改LoginStatus
                ((MainActivity) mActivity).changeLoginStatus();
            } else if (login.getSysCode() != null && login.getSysCode().equals("404")) {
                ((BaseInterface) mActivity).changeBackFragment(new SendVerifyFragment().newInstance(fbid, mAccessToken.getToken()));
            } else {
//                Toast.makeText(mActivity, "錯誤碼:" + login.getSysCode(), Toast.LENGTH_SHORT).show();
                final CustomDialogOrange errorDialog = new CustomDialogOrange(mActivity, mActivity.getString(R.string.txt_fail));
                errorDialog.initErrorDialog();
                errorDialog.show();
            }


            super.onPostExecute(o);
        }


        private void parserJson2(String message) {
            JSONObject object = null;
            try {
                object = new JSONObject(message);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (object != null) {
                TLog.i(TAG, "UpdateToken syscode : " + object.optString(Pub.VALUE_SYSCODE));
                TLog.i(TAG, "UpdateToken sysmsg : " + object.optString(Pub.VALUE_SYSMSG));
            }
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        mLocation = ((MainActivity) mActivity).getLocatUtil().getUpdatedLocation();
    }

    @Override
    public void onAttach(Activity activity) {
        mActivity = activity;
        super.onAttach(activity);
    }
}
