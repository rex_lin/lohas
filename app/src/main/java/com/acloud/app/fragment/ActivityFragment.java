package com.acloud.app.fragment;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.acloud.app.MainActivity;
import com.acloud.app.R;
import com.acloud.app.adapter.ActivityListAdapter;
import com.acloud.app.adapter.GalleryAdapter;
import com.acloud.app.dialog.CustomPopupActivityWindow;
import com.acloud.app.model.ActivityListBannerResult;
import com.acloud.app.superclass.BaseFragment;
import com.acloud.app.task.ActivityListQueryTask;
import com.acloud.app.tool.Pub;
import com.acloud.app.tool.TLog;
import com.acloud.app.util.ActivityListManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Activity  Fragment
 */
public class ActivityFragment extends BaseFragment implements View.OnClickListener {

    String TAG = "ActivityFragment";

    ArrayList<CheckBox> mArrayListCheckBox = new ArrayList<CheckBox>();
    CustomPopupActivityWindow popupWindow;

    int check = 0;
    static int type = 0;
    static int sort = 1;
    TextView
            tvCategory,
            tvSort;

    ActivityListAdapter activityListAdapter;
    ListView listView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((MainActivity) getActivity()).setTitle(getString(R.string.txt_activity));
        View rootView = inflater.inflate(R.layout.fragment_activity, container, false);
        initUI(rootView);
        initPopupWindowActivity();

        return rootView;
    }

    private void initPopupWindowActivity() {
        popupWindow = new CustomPopupActivityWindow(getActivity(), new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                closeAllSortBox();



                switch (msg.what) {

                    case Pub.SEND_CATEGORY:
                        //類別
                        type = msg.arg1;
                        tvCategory.setText(msg.obj.toString());
                        break;
                    case Pub.SEND_SORT:
                        //排序
                        sort = msg.arg1;
                        tvSort.setText(msg.obj.toString());
                        break;
                }
                TLog.i("Ernest ", "type : " + type + " ; sort : " + sort);
                ActivityListQueryTask queryTask = new ActivityListQueryTask(getActivity(), type + "", sort + "",
                        new Handler(){
                            @Override
                            public void handleMessage(Message msg) {
                                super.handleMessage(msg);
                                switch (msg.what){
                                    case 0:
                                        break;
                                    case 2:
                                        activityListAdapter = new ActivityListAdapter(getActivity(), ActivityListManager.getResultArrayList());
                                        listView.setAdapter(activityListAdapter);
                                        break;
                                }

                            }
                        }, "1");
                queryTask.execute();
                CustomPopupActivityWindow.closeAllPopupWindow();
            }
        });
        popupWindow.initWindow();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    public void initUI(View view) {
        listView = (ListView) view.findViewById(R.id.listView);
        activityListAdapter = new ActivityListAdapter(getActivity(), ActivityListManager.getResultArrayList());
        listView.setAdapter(activityListAdapter);
        LinearLayout btnCategory = (LinearLayout) view.findViewById(R.id.btnCategory);
        LinearLayout btnSort = (LinearLayout) view.findViewById(R.id.btnSort);
        android.support.v4.view.ViewPager imgGallery = (android.support.v4.view.ViewPager) view.findViewById(R.id.imgGallery);

        tvCategory = (TextView) view.findViewById(R.id.tvCategory);
        tvSort = (TextView) view.findViewById(R.id.tvSort);

        mArrayListCheckBox.add((CheckBox) view.findViewById(R.id.checkBox4));
        mArrayListCheckBox.add((CheckBox) view.findViewById(R.id.checkBox8));

        btnCategory.setOnClickListener(this);
        btnSort.setOnClickListener(this);

        List<ActivityListBannerResult> list = ActivityListManager.getResultsBannerArrayList();
        List<String> imgurls = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            imgurls.add(list.get(i).getImage_url());
        }
        GalleryAdapter adapter = new GalleryAdapter(getActivity(), imgurls);
        imgGallery.setAdapter(adapter);
    }

    /**
     * 控制排序畫面顯示跟變換文字顏色
     *
     * @param view
     */
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnCategory:
                check = 1;
                popupWindow.showRightWindow2(view, check);
                if (mArrayListCheckBox.get(0).isChecked()) {
                    mArrayListCheckBox.get(0).setChecked(false);
                    tvCategory.setTextColor(getResources().getColor(R.color.b));
                } else {
                    closeAllSortBox();
                    mArrayListCheckBox.get(0).setChecked(true);
                    tvCategory.setTextColor(getResources().getColor(R.color.m));
                }
                break;
            case R.id.btnSort:
                check = 2;
                popupWindow.showRightWindow3(view, check);
                if (mArrayListCheckBox.get(1).isChecked()) {
                    mArrayListCheckBox.get(1).setChecked(false);
                    tvSort.setTextColor(getResources().getColor(R.color.b));
                } else {
                    closeAllSortBox();
                    mArrayListCheckBox.get(1).setChecked(true);
                    tvSort.setTextColor(getResources().getColor(R.color.m));
                }
                break;
        }
    }

    /**
     * 控制排序的文字顏色變化
     */
    public void closeAllSortBox() {
        mArrayListCheckBox.get(0).setChecked(false);
        mArrayListCheckBox.get(1).setChecked(false);
        tvSort.setTextColor(Color.parseColor("#3E3A39"));
        tvCategory.setTextColor(Color.parseColor("#3E3A39"));
    }

}
