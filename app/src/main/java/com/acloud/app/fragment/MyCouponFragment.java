package com.acloud.app.fragment;

import android.app.Activity;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.acloud.app.dialog.CustomProgressDialog;
import com.astuetz.PagerSlidingTabStrip;
import com.acloud.app.MainActivity;
import com.acloud.app.R;
import com.acloud.app.adapter.MyCouponAdapter;
import com.acloud.app.adapter.MyPagerAdapter;
import com.acloud.app.dialog.CustomDialog;
import com.acloud.app.http.HttpCheckUserVerify;
import com.acloud.app.http.HttpMyCouponList;
import com.acloud.app.superclass.BaseFragment;
import com.acloud.app.tool.TLog;
import com.acloud.app.tool.Pub;
import com.acloud.app.tool.WebService;
import com.acloud.app.util.MyCouponListManager;
import com.acloud.app.util.PrefConstant;
import com.acloud.app.util.UserProfileManager;
import com.acloud.app.util.UserVerifyCheckManager;

import java.util.ArrayList;
import java.util.Vector;

/**
 * 我的優惠卷
 */
public class MyCouponFragment extends BaseFragment {

    String TAG = "MyCouponFragment";

    Location mLocation;
    LinearLayout rStripLayout;
    ArrayList<ListView> arraylistview = new ArrayList<ListView>();
    Vector<View> pages = new Vector<View>();
    Activity mActivity;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        mLocation = ((MainActivity)mActivity).getLocatUtil().getUpdatedLocation();
        View rootView = inflater.inflate(R.layout.fragment_mycoupon, container, false);
        MyCouponListTask myCouponListTask = new MyCouponListTask(mActivity, rootView);
        myCouponListTask.execute();


        return rootView;
    }



    private void initUI(View view) {

        rStripLayout = (LinearLayout) view.findViewById(R.id.tabstrip_layout);
        TLog.i("Enrest" , "initUI : " + mActivity);
        arraylistview.add(new ListView(mActivity));
        arraylistview.add(new ListView(mActivity));
        arraylistview.add(new ListView(mActivity));
        for (int i = 0; i < arraylistview.size(); i++) {
            pages.add(i, arraylistview.get(i));
        }

        arraylistview.get(0).setAdapter(new MyCouponAdapter(mActivity, MyCouponListManager.getResultArrayList_unused()));
        arraylistview.get(1).setAdapter(new MyCouponAdapter(mActivity, MyCouponListManager.getResultArrayList_used()));
        arraylistview.get(2).setAdapter(new MyCouponAdapter(mActivity, MyCouponListManager.getResultArrayList_expired()));

        MyPagerAdapter myPagerAdapter = new MyPagerAdapter(getActivity(), pages);
        ViewPager viewPager = (ViewPager) view.findViewById(R.id.ViewPager);
        viewPager.setAdapter(myPagerAdapter);
        myPagerAdapter.notifyDataSetChanged();
        final PagerSlidingTabStrip tabStrip = (PagerSlidingTabStrip) view.findViewById(R.id.PagerSlidingTabStrip);
        tabStrip.setShouldExpand(true);
        tabStrip.setIndicatorHeight(5);
        tabStrip.setTextColor(Color.parseColor("#9FA0A0"));
        //Bottom Color
        tabStrip.setIndicatorColor(Color.parseColor("#D94707"));
        tabStrip.setViewPager(viewPager);

        //set text color of tab as default
        rStripLayout = ((LinearLayout)tabStrip.getChildAt(0));
        TextView tv = (TextView) rStripLayout.getChildAt(0);
        tv.setTextColor(getResources().getColor(R.color.s));

        tabStrip.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                //change text color of tab if position changed
                for(int i=0; i < rStripLayout.getChildCount(); i++){
                    TextView tv = (TextView) rStripLayout.getChildAt(i);
                    if(i == position){
                        tv.setTextColor(getResources().getColor(R.color.s));
                    } else {
                        tv.setTextColor(getResources().getColor(R.color.g));
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }





    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        initCustomActionBar();
        ((MainActivity) mActivity).setTitle("");
        TLog.e(TAG, "onCreateOptionsMenu");
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void initCustomActionBar() {
        ((MainActivity) mActivity).getSupportActionBar().setDisplayShowCustomEnabled(true);
        LayoutInflater inflator = (LayoutInflater) mActivity.getSystemService(mActivity.
                getApplicationContext().LAYOUT_INFLATER_SERVICE);
        View view = inflator.inflate(R.layout.spinnerwound, null);

        Spinner s = (Spinner) view.findViewById(R.id.spinner);
        s.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                TLog.i(TAG, "position : " + i);
                switch (i) {
                    case 0:
                        TLog.i(TAG, "position : " + i);
                        ((MainActivity) mActivity).changeFragment(new CouponListFragment());
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        ArrayList spinnerlist = new ArrayList<String>();
        spinnerlist.add(getString(R.string.title_coupon_exchange));
        spinnerlist.add(getString(R.string.title_mycoupon));

        ArrayAdapter spinneradapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_dropdown_item_1line, spinnerlist);

        s.setAdapter(spinneradapter);
        s.setSelection(1);
        ((MainActivity) getActivity()).getSupportActionBar().setCustomView(view);


        spinneradapter.notifyDataSetChanged();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }


    public class MyCouponListTask extends AsyncTask {
        View rootview;
        CustomProgressDialog dialog;
        Activity activity;

        public MyCouponListTask(Activity activity, View view) {
            rootview = view;
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            dialog = new CustomProgressDialog(activity);
            dialog.initProgressDialog();
            dialog.show();
            super.onPreExecute();
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            //檢查驗證
            if (!UserProfileManager.loginStatus(mActivity))
                return null;

            HttpMyCouponList httpMyCouponList = new HttpMyCouponList(mActivity, MyCouponListManager.getMyCouponList(
                    PrefConstant.getString(getActivity(), Pub.VALUE_UID, ""),
                    PrefConstant.getString(getActivity(), Pub.VALUE_AUTHTOKEN, ""),
                    "0",
                    "20",
                    "1",
                    (mLocation != null) ? mLocation.getLatitude() + "" : "0",
                    (mLocation != null) ? mLocation.getLongitude() + "" : "0"
            ), 1);
            httpMyCouponList.call();
            HttpMyCouponList httpMyCouponList2 = new HttpMyCouponList(mActivity, MyCouponListManager.getMyCouponList(
                    PrefConstant.getString(getActivity(), Pub.VALUE_UID, ""),
                    PrefConstant.getString(getActivity(), Pub.VALUE_AUTHTOKEN, ""),
                    "0",
                    "20",
                    "2",
                    (mLocation != null) ? mLocation.getLatitude() + "" : "0",
                    (mLocation != null) ? mLocation.getLongitude() + "" : "0"
            ), 2);
            httpMyCouponList2.call();
            HttpMyCouponList httpMyCouponList3 = new HttpMyCouponList(mActivity, MyCouponListManager.getMyCouponList(
                    PrefConstant.getString(getActivity(), Pub.VALUE_UID, ""),
                    PrefConstant.getString(getActivity(), Pub.VALUE_AUTHTOKEN, ""),
                    "0",
                    "20",
                    "3",
                    (mLocation != null) ? mLocation.getLatitude() + "" : "0",
                    (mLocation != null) ? mLocation.getLongitude() + "" : "0"
            ), 3);
            httpMyCouponList3.call();


            HttpCheckUserVerify userVerify = new HttpCheckUserVerify(mActivity,
                    WebService.getUid_AuthToken(PrefConstant.getString(mActivity, Pub.VALUE_UID, ""),
                            PrefConstant.getString(mActivity, Pub.VALUE_AUTHTOKEN, "")));
            userVerify.call();
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            initUI(rootview);
            if (dialog != null)
                dialog.dismiss();

            //檢查是否有登入
            if (UserProfileManager.loginStatus(mActivity)) {
                UserVerifyCheckManager.checkVerifyStatus(mActivity);
            } else {
                Toast.makeText(getActivity(), getString(R.string.txt_please_login), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        TLog.e(TAG, "onDestroyView");
        //Close Custome View
        ((MainActivity) mActivity).getSupportActionBar().setDisplayShowCustomEnabled(false);
    }
}
