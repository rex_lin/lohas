package com.acloud.app.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.acloud.app.MainActivity;
import com.acloud.app.R;
import com.acloud.app.model.GooleMapV3Struct;
import com.acloud.app.superclass.BaseFragment;
import com.acloud.app.tool.Pub;
import com.acloud.app.util.LocatUtil;


public class WebFragment extends BaseFragment {
    //layout
    Activity activity;
    WebView webView; //網頁瀏覽
    //ver
    String MAP_URL = "";
    GooleMapV3Struct rtValue = null;
    LocatUtil locatUtil;
    Location lc;
    //UI變更
    private Handler
            uiHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    break;
                case 1:
//                                        final String init =
//                            "javascript:initialize(" +
//                                    rtValue.getNowLatitude()
//                                    + "," +
//                                    rtValue.getNowLongitude()
//                                    + "," +
//                                    rtValue.getGmapLatitude()
//                                    + "," +
//                                    rtValue.getGmapLongitude()
//                                    + ")";
//
                    final String init =
                            "javascript:initialize(" +
                                    "0"
                                    + "," +
                                    "0"
                                    + "," +
                                    rtValue.getGmapLatitude()
                                    + "," +
                                    rtValue.getGmapLongitude()
                                    + ")";


                    webView.setWebViewClient(new WebViewClient() {
                        @Override
                        public void onPageFinished(WebView view, String url) {
                            webView.loadUrl(init);

                        }
                    });
                    webView.loadUrl(rtValue.getGmapUrl());
                    ((MainActivity) getActivity()).setTitle(rtValue.getGmapPinTitle());
                    break;
            }
        }
    };


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_web, container, false);
        webView = (WebView) v.findViewById(R.id.web);
        rtValue = (GooleMapV3Struct) getArguments().getSerializable(Pub.RETURN_VALUE_KEY);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.getSettings().setJavaScriptEnabled(true);
//        webView.setAllowFileAccess(true);
        if (rtValue != null)
            uiHandler.sendEmptyMessage(1);

        return v;
    }

}
