package com.acloud.app.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.acloud.app.MainActivity;
import com.acloud.app.R;
import com.acloud.app.superclass.BaseFragment;
import com.acloud.app.tool.Pub;

/**
 * About Us Fragment
 */
public class AboutFragment extends BaseFragment{

    String TAG = "AboutFragment";
    WebView webView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((MainActivity) getActivity()).setTitle(getString(R.string.title_about));
        View rootView = inflater.inflate(R.layout.fragment_about, container, false);
        init(rootView);
        return rootView;
    }

    private void init(View view) {
        webView = (WebView) view.findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.loadUrl(Pub.URL_ABOUT_US);
        webView.requestFocus();
        webView.setWebViewClient(new WebViewClient());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    public boolean canGoBack() {
        boolean canGoBack = webView.canGoBack();
        if(canGoBack){
            webView.goBack();
        }
        return canGoBack;
    }

}
