package com.acloud.app.fragment;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;

import com.acloud.app.MainActivity;
import com.acloud.app.R;
import com.acloud.app.adapter.FavoriteAdapter;
import com.acloud.app.model.FavoriteStruct;
import com.acloud.app.superclass.BaseFragment;
import com.acloud.app.thread.FavoriteListThread;
import com.acloud.app.tool.Pub;
import com.acloud.app.tool.TLog;
import com.acloud.app.util.BitmapHandler;
import com.acloud.app.util.PrefConstant;
import com.acloud.app.view.DefaultPhoto;

import java.util.List;


public class FavoriteFragment extends BaseFragment {

    Activity activity;
    Bitmap defaultPhoto = null; //預設照片
    ListView list; //列表
    FavoriteAdapter adapter;
    boolean isLoading = false; //是否下載後續資料

    //UI變更
    private Handler
            uiHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    break;
                case 2:
                    adapter = new FavoriteAdapter(
                            activity,
                            defaultPhoto
                    );
                    list.setAdapter(adapter);
                case 1:
                    if (msg.getData().getSerializable(Pub.RETURN_VALUE_KEY) != null) {
                        isLoading = false;
                        msg.obj = ((FavoriteStruct) msg.getData().getSerializable(Pub.RETURN_VALUE_KEY)).getList();
                    }

                    adapter.add((List<FavoriteStruct.Data>) msg.obj);
                    adapter.notifyDataSetChanged();
                    break;
            }
        }
    };

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_list, container, false);
        ((MainActivity) getActivity()).setTitle(getString(R.string.title_favorite));
        //產生個對應物件尺寸的畫布
        defaultPhoto = Bitmap.createBitmap(
                (int) BitmapHandler.convertDpToPixel(120, activity),
                (int) BitmapHandler.convertDpToPixel(120, activity),
                Bitmap.Config.ARGB_8888);

        //畫上預設圖片
        DefaultPhoto drew = new DefaultPhoto(
                defaultPhoto,
                (float) defaultPhoto.getWidth(),
                (float) defaultPhoto.getHeight());

        list = (ListView) v.findViewById(R.id.list);

        list.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int scrollState) {
                switch (scrollState) {
                    case SCROLL_STATE_FLING:
                        TLog.i("Scroll State", "滾動中...");
                        break;
                    case SCROLL_STATE_IDLE:
                        TLog.i("Scroll State", "滾動停止...");

                        break;
                    case SCROLL_STATE_TOUCH_SCROLL:
                        TLog.i("Scroll State", "手指滾動...");
                        break;
                }
            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                TLog.i("OnScroll", "Scroll");
                TLog.i("OnScroll", "firstVisibleItem:" + firstVisibleItem);
                TLog.i("OnScroll", "visibleItemCount:" + visibleItemCount);
                TLog.i("OnScroll", "totalItemCount:" + totalItemCount);
                //如果總數超過20且可整除20表示可能有更多資料
                if (totalItemCount > 19 && totalItemCount % 20 == 0) {
                    //拉到最底部了
                    if ((firstVisibleItem + visibleItemCount) == totalItemCount) {
                        if (!isLoading) {
                            isLoading = true;
                            new FavoriteListThread(
                                    activity,
                                    PrefConstant.getString(activity, Pub.VALUE_UID, ""),
                                    PrefConstant.getString(activity, Pub.VALUE_AUTHTOKEN, ""),
                                    totalItemCount + "",
                                    uiHandler
                            ).start();
                        }
                    }
                }
            }
        });

        FavoriteStruct rtValue = (FavoriteStruct) getArguments().getSerializable(Pub.RETURN_VALUE_KEY);
        Message msg = new Message();
        msg.what = 2;
        msg.obj = rtValue.getList();
        uiHandler.sendMessage(msg);

        return v;
    }

    //移除物件
    public void removeListItem(String itrmId) {
        if (PrefConstant.isFavStoreExist(activity, itrmId))
            PrefConstant.removeFavStore(activity, itrmId);

        adapter.remove(itrmId);
        adapter.notifyDataSetChanged();

    }
}
