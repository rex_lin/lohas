package com.acloud.app.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.acloud.app.MainActivity;
import com.acloud.app.R;
import com.acloud.app.dialog.CustomActivityDialog;
import com.acloud.app.inferface.BaseInterface;
import com.acloud.app.qrcodescanner;
import com.acloud.app.task.ActivityListQueryTask;
import com.acloud.app.tool.Pub;
import com.acloud.app.util.ActivityListManager;

/**
 * Created by skywind on 2015/9/10.
 */
public class MainFragment extends Fragment implements View.OnClickListener {

    Activity activity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_main, container, false);
        ((MainActivity) getActivity()).setTitle(R.string.app_name);
        initUI(rootview);
//        ActivityListQueryTask queryTask = new ActivityListQueryTask(activity, "0", "0",
//                new Handler() {
//                    @Override
//                    public void handleMessage(Message msg) {
//                        super.handleMessage(msg);
//                        switch (msg.what) {
//                            case 1:
//                                if (ActivityListManager.getResultsBannerArrayList().size() > 0) {
//                                    CustomActivityDialog activityDialog = new CustomActivityDialog(activity);
//                                    activityDialog.initActivityDialog();
//                                    activityDialog.show();
//                                }
//                                break;
//                        }
//                    }
//                }, "0");
//        queryTask.execute();
//        if (Pub.isDebug){
//
//            Intent it = new Intent();
//            it.setClass(getActivity(), qrcodescanner.class);
//            startActivityForResult(it, QRCODE_ITEM);
//        }
        return rootview;
    }
    public static int QRCODE_ITEM = 0025;

    private void initUI(View view) {
        ImageView btnStore = (ImageView) view.findViewById(R.id.btnStore);
        btnStore.setOnClickListener(this);
//        ImageView btnCouponList = (ImageView) view.findViewById(R.id.btnCouponList);
//        btnCouponList.setOnClickListener(this);
//        ImageView btnActivity = (ImageView) view.findViewById(R.id.btnActivityList);
//        btnActivity.setOnClickListener(this);
        //------
        ImageView btnHomeOnlineShop = (ImageView) view.findViewById(R.id.btnHomeOnlineShop);
        btnHomeOnlineShop.setOnClickListener(this);
        ImageView btnHomeChat = (ImageView) view.findViewById(R.id.btnHomeChat);
//        btnHomeChat.setOnClickListener(this);
        ImageView btnHomeFun = (ImageView) view.findViewById(R.id.btnHomeFun);
        btnHomeFun.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnStore:
                ((BaseInterface) activity).clickItem("1");//STORE_PAGE
                break;
//            case R.id.btnActivityList:
//                ((BaseInterface) activity).clickItem("5");//ACTIVITY_LIST_PAGE
//                break;
//            case R.id.btnCouponList:
//                ((BaseInterface) activity).clickItem("6");//COUPON_LIST_PAGE
//                break;
            case R.id.btnHomeOnlineShop:
                ((BaseInterface) activity).clickItem("13");//ONLINE_SHOP
                break;
            case R.id.btnHomeChat:
                ((BaseInterface) activity).clickItem("15");//GROUP_CHAT
                break;
            case R.id.btnHomeFun:
                ((BaseInterface) activity).clickItem("14");//HAPPY_FUN
                break;
        }
    }
}
