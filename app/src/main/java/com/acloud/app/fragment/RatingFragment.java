package com.acloud.app.fragment;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.acloud.app.MainActivity;
import com.acloud.app.R;
import com.acloud.app.superclass.BaseFragment;
import com.acloud.app.thread.SwitchPushThread;
import com.acloud.app.tool.Pub;
import com.acloud.app.tool.TLog;
import com.acloud.app.util.PrefConstant;

import static android.content.Intent.ACTION_VIEW;

/**
 * Setting Fragment
 */
public class RatingFragment extends BaseFragment {

    String TAG = "RatingFragment";
    Button btnSave ;
    TextView txt_title ,txt_content;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((MainActivity) getActivity()).setTitle(getString(R.string.txt_setting));
        View rootView = inflater.inflate(R.layout.fragment_rating, container, false);
        initUI(rootView);
        setAction();
        return rootView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    private void initUI(View view) {
        btnSave = (Button) view.findViewById(R.id.btnSave);
        txt_title = (TextView) view.findViewById(R.id.txt_title);
        txt_content = (TextView) view.findViewById(R.id.txt_content);


    }

    private void setAction() {
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

}
