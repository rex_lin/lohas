package com.acloud.app.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.acloud.app.R;
import com.acloud.app.dialog.CustomDialog;
import com.acloud.app.dialog.CustomProgressDialog;
import com.acloud.app.http.HttpVerifyEmailResend;
import com.acloud.app.http.HttpVerifyEmailUpdate;
import com.acloud.app.tool.TLog;
import com.acloud.app.tool.Pub;
import com.acloud.app.tool.WebService;
import com.acloud.app.util.PrefConstant;

/**
 * Created by skywind-10 on 2015/7/9.
 */
public class VerifyDialogFragment extends DialogFragment implements View.OnClickListener {
    EditText editEmail;
    static VerifyDialogFragment fragment;
    Activity mActivity;

    public static VerifyDialogFragment Instance() {
        fragment = new VerifyDialogFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.custom_dialog_verifycheck, container);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);

        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        getDialog().getWindow().setLayout(dm.widthPixels, dm.heightPixels);


        Button btnNeg = (Button) view.findViewById(R.id.btnResend);
        Button btnNeu = (Button) view.findViewById(R.id.btnModify);
        Button btnPos = (Button) view.findViewById(R.id.btnAccept);
        TextView tvTitle = (TextView) view.findViewById(R.id.tvTitle);
        TextView tvMsg = (TextView) view.findViewById(R.id.tvMsg);
        editEmail = (EditText) view.findViewById(R.id.editMsg);

        tvTitle.setText(getString(R.string.txt_verifingdialog_title));
        tvMsg.setText(getString(R.string.txt_verifingdialog_msg));
        editEmail.setText(PrefConstant.getString(mActivity, Pub.VALUE_VERIFING_EMAIL, ""));
        btnNeg.setOnClickListener(this);
        btnNeu.setOnClickListener(this);
        btnPos.setOnClickListener(this);
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnResend:
                TLog.i("DialogFramgent", "RESEND");
                ResendTask task = new ResendTask();
                task.execute();
                break;
            case R.id.btnModify:
                TLog.i("DialogFramgent", "modify");
                ModifyTask modifyTask = new ModifyTask();
                modifyTask.execute();
                break;
            case R.id.btnAccept:
                if (fragment != null)
                    fragment.dismiss();
                TLog.i("DialogFramgent", "accept");
                break;
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    //重送驗證信
    public class ResendTask extends AsyncTask {
        HttpVerifyEmailResend resend;
        CustomProgressDialog loadingDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingDialog = new CustomProgressDialog(mActivity);
            loadingDialog.initProgressDialog();
            loadingDialog.show();
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            if (loadingDialog != null)
                loadingDialog.dismiss();

            if (!resend.ismSuccess())
                return;

            if (resend.getIsSysSuccess()) {
                if (resend.getSysCode().equals("200")) {
                    Toast.makeText(mActivity, "Success", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mActivity, "Syscode : " + resend.getSysCode(), Toast.LENGTH_SHORT).show();
                }
            } else {
                if (resend.getSysCode().equals("303")) {
                    Toast.makeText(mActivity, "信箱驗證中，請30分後再試", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mActivity, "Syscode : " + resend.getSysCode(), Toast.LENGTH_SHORT).show();
                }
            }
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            resend = new HttpVerifyEmailResend(mActivity,
                    WebService.getUid_AuthToken(PrefConstant.getString(mActivity, Pub.VALUE_UID, ""),
                            PrefConstant.getString(mActivity, Pub.VALUE_AUTHTOKEN, "")));
            resend.call();
            return null;
        }
    }

    //修改驗證信箱
    public class ModifyTask extends AsyncTask {
        HttpVerifyEmailUpdate update;
        CustomProgressDialog loadingDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingDialog = new CustomProgressDialog(mActivity);
            loadingDialog.initProgressDialog();
            loadingDialog.show();
        }

        @Override
        protected void onPostExecute(Object o) {
            if (loadingDialog != null)
                loadingDialog.dismiss();

            if (!update.ismSuccess())
                return;

            if (update.getIsSysSuccess()) {
                if (update.getSysCode().equals("200")) {
                    Toast.makeText(mActivity, "Success", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mActivity, "Syscode : " + update.getSysCode(), Toast.LENGTH_SHORT).show();
                }
            }
            super.onPostExecute(o);
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            update = new HttpVerifyEmailUpdate(mActivity,
                    WebService.getUid_AuthToken_VERIFYEMAIL(PrefConstant.getString(mActivity, Pub.VALUE_UID, ""),
                            PrefConstant.getString(mActivity, Pub.VALUE_AUTHTOKEN, ""),
                            editEmail.getText().toString()));
            update.call();
            return null;
        }
    }
}
