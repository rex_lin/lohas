package com.acloud.app.fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.acloud.app.MainActivity;
import com.acloud.app.R;
import com.acloud.app.http.HttpUserEmailUpdate;
import com.acloud.app.http.HttpUserProfileUpdate;
import com.acloud.app.tool.Pub;
import com.acloud.app.tool.TLog;
import com.acloud.app.tool.WebService;
import com.acloud.app.util.PrefConstant;
import com.acloud.app.util.UserProfileManager;
import com.facebook.FacebookSdk;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;


public class ProfileFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    String TAG = "ProfileFragment";
    Activity mActivity;
    EditText editMail, editName, editPhone, editNote;
    TextView mTvID, mTvSerialNo, tvBirthday;
    RelativeLayout SelectDate;
    Spinner spinnerGender;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Enable the option menu for the Fragment
        setHasOptionsMenu(true);
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);

        ((MainActivity) getActivity()).setTitle(getString(R.string.title_profile));

        initUI(rootView);
        //Get Profile from SharedPreference
        mTvID.setText(PrefConstant.getString(mActivity, Pub.VALUE_UID, ""));
        editMail.setText(PrefConstant.getString(mActivity, Pub.VALUE_EMAIL, ""));
        mTvSerialNo.setText(PrefConstant.getString(mActivity, Pub.VALUE_SERIAL_NO, ""));
        editName.setText(PrefConstant.getString(mActivity, Pub.VALUE_NAME, ""));
        editPhone.setText(PrefConstant.getString(mActivity, Pub.VALUE_PHONE, ""));
        tvBirthday.setText(PrefConstant.getString(mActivity, Pub.VALUE_BIRTHDAY, ""));
        editNote.setText(PrefConstant.getString(mActivity, Pub.VALUE_MEMO, ""));

        //Set Gender spinner
        ArrayList spinnerlist = new ArrayList<String>();
        spinnerlist.add(getString(R.string.txt_boy));
        spinnerlist.add(getString(R.string.txt_girl));

        ArrayAdapter spinneradapter = new ArrayAdapter<String>(getActivity(), R.layout.item_profile_gender, spinnerlist);
        spinnerGender.setAdapter(spinneradapter);

        int valueBoy = 0;
        int valueGirl = 1;
        spinnerGender.setSelection(
                (PrefConstant.getString(mActivity, Pub.VALUE_GENDER, "").equals("0") ? valueBoy : valueGirl));
        if (!UserProfileManager.loginStatus(getActivity())) {
            Toast.makeText(getActivity(), getString(R.string.txt_please_login), Toast.LENGTH_SHORT).show();
            ((MainActivity)getActivity()).changeFragment(new LoginFragment());
        }


        return rootView;
    }

    private void initUI(View view) {
        TextView btn = (TextView) view.findViewById(R.id.tvUpdate);
        Button btnShare = (Button) view.findViewById(R.id.btnShare);
        Button btnCheckEmail = (Button) view.findViewById(R.id.btnCheckEmail);
        editMail = (EditText) view.findViewById(R.id.editMail);
        editName = (EditText) view.findViewById(R.id.editName);
        editPhone = (EditText) view.findViewById(R.id.editPhone);
        tvBirthday = (TextView) view.findViewById(R.id.tvBirthday);
        spinnerGender = (Spinner) view.findViewById(R.id.spinnerGender);
        editNote = (EditText) view.findViewById(R.id.editNote);
        mTvID = (TextView) view.findViewById(R.id.tvID);
        mTvSerialNo = (TextView) view.findViewById(R.id.tvNo);
        SelectDate = (RelativeLayout)view.findViewById(R.id.SelectDate);
        spinnerGender = (Spinner) view.findViewById(R.id.spinnerGender);

        btnShare.setOnClickListener(this);
        btn.setOnClickListener(this);
        btnCheckEmail.setOnClickListener(this);
        SelectDate.setOnClickListener(this);
        spinnerGender.setOnItemSelectedListener(this);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_profile, menu);
        ImageButton btnShare = (ImageButton) menu.findItem(R.id.action_share).getActionView();
        btnShare.setBackground(null);
        btnShare.setImageResource(R.drawable.share_white);
        btnShare.setOnClickListener(this);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvUpdate:
                QueryProfileTask task = new QueryProfileTask();
                task.execute();
                break;
            case R.id.btnShare:
                ((MainActivity)mActivity).getmNavigationDrawerFragment().onNavigationDrawerItemSelected(MainActivity.SHARE_PAGE);
                break;
            case R.id.btnCheckEmail:
                QueryUpdateEmailTask updateEmailTask = new QueryUpdateEmailTask();
                updateEmailTask.execute();
                break;
            case R.id.SelectDate:
                showDatePickerDialog();
                break;
            case R.id.action_share:
                ((MainActivity) mActivity).changeFragment(new RecommendedFragment());
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        switch (view.getId()) {
            case R.id.spinnerGender:
                TLog.i(TAG, "Select : " + i);
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    /**
     * 呼叫API更新資料
     */
    public class QueryProfileTask extends AsyncTask {
        String jsonString, syscode, sysmsg;

        public QueryProfileTask() {
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            if (syscode.equals("200")) {
                Toast.makeText(mActivity, "更新成功", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(mActivity, "錯誤訊息:" + sysmsg, Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            HttpUserProfileUpdate update = new HttpUserProfileUpdate(getActivity(),
                    WebService.getUser_Info(
                            PrefConstant.getString(mActivity, Pub.VALUE_UID, ""),
                            PrefConstant.getString(mActivity, Pub.VALUE_AUTHTOKEN, ""),
                            editName.getText().toString(),
                            editPhone.getText().toString(),
                            tvBirthday.getText().toString(),
                            (spinnerGender.getSelectedItem().toString().equals(getString(R.string.txt_boy)) ? "0" : "1"), //boy:0 girl:1
                            editNote.getText().toString(),
                            "0",
                            "0"));
            //TODO確認 Lat,Lng是否要抓
            jsonString = update.call();
            parserJson(jsonString);
            return null;
        }

        void parserJson(String message) {
            try {
                JSONObject object = new JSONObject(message);
                syscode = object.getString("syscode");
                sysmsg = object.getString("sysmsg");
                object = object.getJSONObject("data");
                TLog.i(TAG, "obtain : " + object.optString("obtain_bonus"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * 更新個人Email
     */
    public class QueryUpdateEmailTask extends AsyncTask {
        String jsonString, syscode, sysmsg;

        public QueryUpdateEmailTask() {
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            if (syscode.equals("200")) {
                Toast.makeText(mActivity, "更新成功", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(mActivity, "錯誤訊息:" + sysmsg, Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            HttpUserEmailUpdate emailUpdate = new HttpUserEmailUpdate(mActivity,
                    WebService.getUid_AuthToken_Email(
                            PrefConstant.getString(mActivity, Pub.VALUE_UID, ""),
                            PrefConstant.getString(mActivity, Pub.VALUE_AUTHTOKEN, ""),
                            editMail.getText().toString(),
                            "0",
                            "0"
                    ));
            jsonString = emailUpdate.call();
            parserJson(jsonString);
            return null;
        }

        void parserJson(String message) {
            try {
                JSONObject object = new JSONObject(message);
                syscode = object.getString("syscode");
                sysmsg = object.getString("sysmsg");
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public void showDatePickerDialog() {
        // 設定初始日期
        int mYear, mMonth, mDay;

        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        // 跳出日期選擇器
        DatePickerDialog dpd = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        // 完成選擇，顯示日期
                        tvBirthday.setText(year + "/" + (monthOfYear + 1) + "/"
                                + dayOfMonth);

                    }
                }, mYear, mMonth, mDay);
        dpd.show();
    }


    @Override
    public void onAttach(Activity activity) {
        mActivity = activity;
        super.onAttach(activity);
    }
}
