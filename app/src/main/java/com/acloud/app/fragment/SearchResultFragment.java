package com.acloud.app.fragment;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.acloud.app.MainActivity;
import com.acloud.app.R;
import com.acloud.app.adapter.StoreListAdapter;
import com.acloud.app.dialog.CustomAdvPopupWindow;
import com.acloud.app.superclass.BaseFragment;
import com.acloud.app.task.SearchQueryTask;
import com.acloud.app.tool.Pub;
import com.acloud.app.tool.TLog;
import com.acloud.app.util.LocatUtil;
import com.acloud.app.util.StoreListManager;

import java.util.Timer;
import java.util.TimerTask;

/**
 * 搜尋結果頁
 */
public class SearchResultFragment extends BaseFragment implements View.OnClickListener{

    static LinearLayout
            linearlayout;

    static TextView
            tvNoData;

    private String
            sdid,
            smid,
            scid,
            scaid,
            type;

    LocatUtil
            locatUtil;

    Location
            lc;

    String
            QueryText;

    public static final int SUCCESS = 0;

    CustomAdvPopupWindow advfliterWindows;

    ListView listView;

    StoreListAdapter mStoreListAdapter ;

    Handler uiHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case SUCCESS:
                    TLog.i("Ernest" , "uiHandler");
                    mStoreListAdapter= new StoreListAdapter(getActivity(),StoreListManager.getResultArrayList());
                    listView.setAdapter(mStoreListAdapter);
                    break;
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_search_result, container, false);
        initUI(rootView);
        // Enable the option menu for the Fragment
        setHasOptionsMenu(true);
        locatUtil = ((MainActivity) getActivity()).getLocatUtil();
        lc = locatUtil.getUpdatedLocation();

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                InputMethodManager imm = (InputMethodManager) rootView.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
            }
        }, 100);
        Bundle bundle = this.getArguments();
        QueryText = bundle.getString(Pub.VALUE_QUERYTEXT);
        TLog.i("Ernest", "bundle : " + QueryText);
        return rootView;
    }



    private void initUI(View view) {

        tvNoData = (TextView) view.findViewById(R.id.tvNoData);

        listView = (ListView) view.findViewById(R.id.listView);
        mStoreListAdapter = new StoreListAdapter(getActivity(), StoreListManager.getResultArrayList());
        listView.setAdapter(mStoreListAdapter);
        linearlayout = (LinearLayout) view.findViewById(R.id.linearlayout);
        if(mStoreListAdapter != null && mStoreListAdapter.getCount() == 0){
            tvNoData.setVisibility(View.VISIBLE);
        }else{
            tvNoData.setVisibility(View.INVISIBLE);
        }
        TLog.i("TAG", "getHeight : " + listView.getHeight());
        TLog.i("TAG", "getHeight : " + listView.getMeasuredHeight());

    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onResume() {
        super.onResume();
        int height = (((MainActivity) getActivity()).getToolBarHeight() + ((MainActivity) getActivity()).getMStatusBarHeight());
//        int height = ((MainActivity) getActivity()).getMStatusBarHeight();
        advfliterWindows = new CustomAdvPopupWindow(getActivity(), height,
                new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        super.handleMessage(msg);
                        switch (msg.what) {
                            case Pub.BUTTON_OK:
                                Bundle bundle = msg.getData();
                                SearchQueryTask queryTask = new SearchQueryTask(getActivity(),
                                        QueryText,
                                        lc != null ? lc.getLatitude() + "" : "0",
                                        lc != null ? lc.getLongitude() + "" : "0",
                                        sdid,
                                        smid,
                                        scid,
                                        scaid,
                                        type,
                                        uiHandler,
                                        bundle.getString(Pub.VALUE_LOW_PRICE),
                                        bundle.getString(Pub.VALUE_HEIGHT_PRICE),
                                        false
                                );
                                type = msg.arg1 + "";
                                queryTask.execute();
                                advfliterWindows.closeAllPopupWindow();
                                TLog.i("Ernest", "0");
                                break;
                            case Pub.BUTTON_SORT:
                                TLog.i("Ernest", "1");
                                type = msg.arg1 + "";
                                break;
                            case Pub.SEND_CATEGORY:
                                TLog.i("Ernest", "2");
                                scid = msg.arg1 + "";
                                scaid = msg.arg2 + "";
                                break;
                            case Pub.SEND_DISTRICT:
                                sdid = msg.arg1 + "";
                                smid = msg.arg2 + "";
                                break;
                        }
                    }
                }, false);
        advfliterWindows.initFliterWindow();

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_searchresult, menu);
        ImageButton filter = (ImageButton) menu.findItem(R.id.action_filter).getActionView();
        filter.setBackground(null);
        filter.setImageResource(R.drawable.funnel);
        filter.setOnClickListener(this);
        TLog.e("StoreFragment  ", "onCreateOptionsMenu");

        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();

        searchView.setQueryHint(getString(R.string.txt_search_hint));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                TLog.i("TAG", "query " + query);
                QueryText = query;
                SearchQueryTask task = new SearchQueryTask(getActivity(), query,
                        lc != null ? lc.getLatitude() + "" : "0",
                        lc != null ? lc.getLongitude() + "" : "0",
                        false,
                        uiHandler);
                task.execute();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                TLog.i("TAG", "new Text : " + newText);
                return false;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.action_filter:

                advfliterWindows.showAdvancePopupwindow(linearlayout);
                break;

        }
    }
}
