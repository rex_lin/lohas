package com.acloud.app.fragment;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.acloud.app.MainActivity;
import com.acloud.app.R;
import com.acloud.app.dialog.CustomDialogPink;
import com.acloud.app.superclass.BaseFragment;
import com.acloud.app.task.MyCouponDetailTask;
import com.acloud.app.task.MyCouponExchangeTask;
import com.acloud.app.thread.GetStoreInfoThread;
import com.acloud.app.tool.Pub;
import com.acloud.app.tool.TLog;
import com.acloud.app.util.BitmapHandler;
import com.acloud.app.util.LocatUtil;
import com.acloud.app.util.MyCouponDetailManager;
import com.acloud.app.util.PrefConstant;
import com.acloud.app.util.UserProfileManager;
import com.acloud.app.util.Util;

import java.io.InputStream;
import java.net.URL;

/**
 * CouponDetail
 */
public class MyCouponDetailFragment extends BaseFragment implements View.OnClickListener {

    String TAG = "CouponDetailFragment";
    TextView tvStoreName, tvDistance, tvSubTitle, tvDate, tvContent;
    ImageView imgStore;
    Activity mActivity;
    LocatUtil locatUtil;
    Button btnExchange;
    public static final int isSuccess = 1;
    public static final int isError = 0;
    Handler uiHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case isError:
                    break;
                case isSuccess:
                    btnExchange.setText(getString(R.string.txt_used));
                    btnExchange.setTextColor(getResources().getColor(R.color.txt_color2));
                    btnExchange.setBackground(getResources().getDrawable(R.drawable.btn_selector_gray_soldout));
                    btnExchange.setEnabled(false);
                    break;
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_mycoupondetail, container, false);
        initUI(rootView);
        ((MainActivity) getActivity()).setTitle(getString(R.string.title_coupon));

        tvStoreName.setText(MyCouponDetailManager.getMyCouponDetailResult().getStore_name());
//        tvDistance.setText(Util.distanceMtoKM(MyCouponDetailManager.getMyCouponDetailResult().getDistance()));
        tvSubTitle.setText(String.format(getString(R.string.txt_coupon_subtitle), MyCouponDetailManager.getMyCouponDetailResult().getTitle()));

        tvDate.setText(String.format(getString(R.string.txt_activity_date), Util.formatDate(
                        MyCouponDetailManager.getMyCouponDetailResult().getUsed_start_datetime()) + " - " +
                        Util.formatDate(
                                MyCouponDetailManager.getMyCouponDetailResult().getUsed_end_datetime())
        ));
        TLog.i(TAG, "CouponDetail coupon id : " + MyCouponDetailManager.getMyCouponDetailResult().getStatus());
        tvContent.setText(String.format(getString(R.string.txt_coupon_content), MyCouponDetailManager.getMyCouponDetailResult().getContent()));

        final Handler updateIconHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (msg != null) {
                    imgStore.setImageBitmap((Bitmap) msg.obj);
                }
            }
        };

        //開始讀取圖片
        new Thread() {
            @Override
            public void run() {
                Message msg = new Message();

                try {
                    URL url = new URL(MyCouponDetailManager.getMyCouponDetailResult().getImage_url());
                    InputStream is = (InputStream) url.getContent();
                    if (is != null) {
                        System.gc();
                        //DownLoad Image    BitmapHandler class
                        is = new BitmapHandler.FlushedInputStream(is);
                        msg.obj = BitmapFactory.decodeStream(is);
                        is.close();
                        System.gc();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (msg.obj != null)
                    updateIconHandler.sendMessage(msg);

            }
        }.start();
        locatUtil = ((MainActivity)mActivity).getLocatUtil();

        //代表使用過
        if (MyCouponDetailManager.getMyCouponDetailResult().getStatus() !=null && MyCouponDetailManager.getMyCouponDetailResult().getStatus().equals("1")) {
            btnExchange.setText(getString(R.string.txt_used));
            btnExchange.setTextColor(getResources().getColor(R.color.txt_color2));
            btnExchange.setBackground(getResources().getDrawable(R.drawable.btn_selector_gray_soldout));
            btnExchange.setEnabled(false);
        }
        return rootView;
    }

    @Override
    public void onClick(View view) {
        final Location lc = locatUtil.getUpdatedLocation();
        switch (view.getId()) {
            case R.id.btnExchange:

                if (!UserProfileManager.loginStatus(getActivity())) {
                    Toast.makeText(getActivity(), getString(R.string.txt_please_login), Toast.LENGTH_SHORT).show();
                    break;
                }

                // 判斷跳出倒數2分鐘或是Show Qrcode
                if (MyCouponDetailManager.getMyCouponDetailResult().getUsed_type().equals("1")) {
                    final CustomDialogPink dialog = new CustomDialogPink(getActivity(),
                            mActivity.getResources().getString(R.string.txt_exchange_coupon_title),
                            String.format(mActivity.getResources().getString(R.string.txt_coupon_used),
                                    MyCouponDetailManager.getMyCouponDetailResult().getTitle()),
                            mActivity.getResources().getString(R.string.btn_cancel),
                            mActivity.getResources().getString(R.string.btn_ok));
                    dialog.initProgressDialog(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            MyCouponExchangeTask exchangeTask = new MyCouponExchangeTask(getActivity(),
                                    MyCouponDetailManager.getMyCouponDetailResult().getEx_coupon_id(),
                                    MyCouponDetailManager.getMyCouponDetailResult().getVerify_code(),
                                    lc != null ? lc.getLatitude() + "" : "0",
                                    lc != null ? lc.getLongitude() + "" : "0",
                                    dialog,
                                    uiHandler);

                            exchangeTask.execute();
                        }
                    });
                    dialog.show();
                } else if (MyCouponDetailManager.getMyCouponDetailResult().getUsed_type().equals("2")) {
                    ((MainActivity) getActivity()).changeBackFragment(new ShowQRcodeFragment());
                }
                break;
            case R.id.btnStore:
                new GetStoreInfoThread(
                        getActivity(),
                        PrefConstant.getString(getActivity(), Pub.VALUE_UID, ""),

                        PrefConstant.getString(getActivity(), Pub.VALUE_AUTHTOKEN, ""),
                        MyCouponDetailManager.getMyCouponDetailResult().getStore_id(),
                        lc != null ? lc.getLatitude() + "" : "0",
                        lc != null ? lc.getLongitude() + "" : "0",
                        new Handler() {
                            @Override
                            public void handleMessage(Message msg) {
                                switch (msg.what) {
                                    case 1:
                                        //正常
                                        Fragment nextFragment = new StoreDetailFragment();
                                        nextFragment.setArguments(msg.getData());
                                        ((MainActivity) getActivity()).changeBackFragment(nextFragment);
                                    case 0:
                                        //TODO 連線失敗提示錯誤訊息

                                        break;
                                }
                            }
                        }

                ).start();

                break;
        }
    }

    private void initUI(View view) {
        btnExchange = (Button) view.findViewById(R.id.btnExchange);
        Button btnStore = (Button) view.findViewById(R.id.btnStore);
        tvStoreName = (TextView) view.findViewById(R.id.tvStoreName);
//        tvDistance = (TextView) view.findViewById(R.id.tvDistance_white);
        tvSubTitle = (TextView) view.findViewById(R.id.tvSubtitle);
        tvDate = (TextView) view.findViewById(R.id.tvDate);
        tvContent = (TextView) view.findViewById(R.id.tvContent);
        imgStore = (ImageView) view.findViewById(R.id.img);
        LinearLayout contentLayout = (LinearLayout) view.findViewById(R.id.ContentLayout);
        contentLayout.setEnabled(false);

        btnExchange.setOnClickListener(this);
        btnStore.setOnClickListener(this);


    }

    @Override
    public void onResume() {
        super.onResume();
        TLog.i(TAG, "onResume");
        if (((MainActivity) getActivity()).nextFragment instanceof ShowQRcodeFragment) {
            MyCouponDetailTask detailTask = new MyCouponDetailTask(getActivity(),
                    PrefConstant.getString(getActivity(), Pub.VALUE_UID, ""),
                    PrefConstant.getString(getActivity(), Pub.VALUE_AUTHTOKEN, ""),
                    MyCouponDetailManager.getMyCouponDetailResult().getEx_coupon_id(),
                    "0",
                    "0"
            );
            detailTask.execute();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }
}
