package com.acloud.app.fragment;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.acloud.app.MainActivity;
import com.acloud.app.R;
import com.acloud.app.superclass.BaseFragment;
import com.acloud.app.tool.Pub;
import com.acloud.app.util.MyCouponDetailManager;
import com.acloud.app.util.PrefConstant;

/**
 * QrCode Fragment
 */
public class ShowQRcodeFragment extends BaseFragment {

    String TAG = "ShowQRcodeFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((MainActivity) getActivity()).setTitle(getString(R.string.txt_exchange_coupon_title));
        View rootView = inflater.inflate(R.layout.fragment_showqrcode, container, false);
        TextView tvStoreName = (TextView) rootView.findViewById(R.id.tvStoreName);
        TextView tvCouponName = (TextView) rootView.findViewById(R.id.tvSubtitle);
        ImageView img = (ImageView) rootView.findViewById(R.id.img);
        LinearLayout tvContent = (LinearLayout)rootView.findViewById(R.id.tvContent);
        tvContent.setEnabled(false);

        tvStoreName.setText(String.format(getString(R.string.txt_store_name), MyCouponDetailManager.getMyCouponDetailResult().getStore_name()));
        tvCouponName.setText(String.format(getString(R.string.txt_coupon_subtitle), MyCouponDetailManager.getMyCouponDetailResult().getTitle()));
        QRCodeWriter writer = new QRCodeWriter();
        try {
            BitMatrix bitMatrix = writer.encode(qrcodeStgring(
                    PrefConstant.getString(getActivity(), Pub.VALUE_UID, ""),
                    MyCouponDetailManager.getMyCouponDetailResult().getEx_coupon_id(),
                    MyCouponDetailManager.getMyCouponDetailResult().getVerify_code()
            ), BarcodeFormat.QR_CODE, 512, 512);

            int width = bitMatrix.getWidth();
            int height = bitMatrix.getHeight();
            Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    bmp.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE);
                }
            }
            img.setImageBitmap(bmp);

        } catch (WriterException e) {
            e.printStackTrace();
        }
        return rootView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    /**
     * qrcode string formate
     *
     * @param uid
     * @param excouponid
     * @param verify_code
     * @return
     */
    private String qrcodeStgring(String uid, String excouponid, String verify_code) {
        String code = "{\n" +
                "   \"uid\":\"" + uid + "\",\n" +
                "   \"ex_coupon_id\":\"" + excouponid + "\",\n" +
                "   \"verify_code\":\"" + verify_code + "\"\n" +
                "}\n";
        return code;
    }

}
